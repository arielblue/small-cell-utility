Small Cell Utility
========

Upload log data to the server. Server parses elogs automatically and 
generates diagrams online to help users understand the behavior of 
small-cells.

Features
--------

- Database Reset. Drop the whole database and rebuild one.
- Manully uploading logs. Automatically deleting previous data from 
database and parsing uploaded logs into database.
- Create 4 charts at most based on log data.

Installation
------------

1. Web Server Installation refers to SOP of Web Server Setup
2. Extract the package to the DocumentRoot of your Web Server
3. Environment setup refers to SOP of Web Server Setup
4. Start GUI refers to SOP of Web Server Setup

Environment Requirement
----------

- Apache HTTPd 2.22+
- PHP 5.3+ and PHP 5 only
- MySQL 5.5+

Log Requirement
----------

LogCategory/LogModule/LogLevel
03/01/01
06/09/03
08/00/03
08/02/02
08/03/02
08/05/02
08/06/02
08/07/02
08/09/02
08/10/02

Technologies
----------

- Core by CodeIgniter 2.2.1
- jQuery 1.11.2
- jQuery UI - v1.11.4 theme Smoothness
- Flot for jQuery 0.8.3.
- Axis Labels Plugin for flot
- jQuery Form Plugin version: 3.51.0
- jQuery Timepicker Addon - v1.5.0
- validate.js 1.4
- flot-tickrotor v. 2.0.
- jQuery UI MultiSelect Widget 1.13
- tostie.js Version version: 0.1
- jquery-scrollto.js v1.4.4
- jquery.table2excel.js
- tablesorter Version: 2.0.5