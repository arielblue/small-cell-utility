
function get_mainmenu()
{
	var path = getBaseURL()+getProjectName()+"/index.php/user/get_userRole";
	jQuery.ajax({
		url: path,
		dataType: "TEXT",
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(data) {
			mainmenu(data);
		}
	});
}

function mainmenu(role)
{
	var mainmenu1 =
	[
		{
			"name": "Preference",
			"submenu":
			[
				{
					"name": "KPI",
					"url": "javascript:get_kpiPref()"
				},
				{
					"name": "Counter",
					"url": "javascript:get_counterPref()"
				},
				{
					"name": "HNB",
					"url": "javascript:get_HNBPref()"
				}
			]
		},
		{
			"name": "KPI Target",
			"submenu":
			[
				{
					"name": "KPI Target Info",
					"url": "javascript:get_kpiTarget_info('show')"
				}
			]
		},
		{
			"name": "Servers",
			"submenu":
			[
				{
					"name": "HNB Gateway",
					"url": "javascript:get_info('show','hnb')"
				},
				{
					"name": "Backup Server",
					"url": "javascript:get_info('show','backup')"
				}
			]
		},
		{
			"name": "Users",
			"submenu":
			[
				{
					"name": "User Management",
					"url": "javascript:get_allUserInfo()"
				},
				{
					"name": "Manage User Roles",
					"url": "javascript:get_allRoles()"
				}
			]
		}
	];

	var mainmenu3 =
	[
		{
			"name": "Preference",
			"submenu":
			[
				{
					"name": "KPI",
					"url": "javascript:get_kpiPref()"
				},
				{
					"name": "Counter",
					"url": "javascript:get_counterPref()"
				},
				{
					"name": "HNB",
					"url": "javascript:get_HNBPref()"
				}
			]
		}
	];

	switch(role)
	{
		case "1":
			new AccordionMenu({menuArrs:mainmenu1});
			break;
		case "2":
			new AccordionMenu({menuArrs:mainmenu1});
			break;
		case "3":
			new AccordionMenu({menuArrs:mainmenu3});
			break;
	}
}

// ** menu effect **
function AccordionMenu(options) {
	this.config = {
		containerCls        : '.wrap-menu',	// outside container
		menuArrs            :  '',			// data that JSON pass in
		type                :  'click',	// default click, could be mouseover
		renderCallBack      :  null,	// callback after html structure rendering
		clickItemCallBack   :  null		// callback on clicking an item
	};
	this.cache = {

	};
	this.init(options);
}
AccordionMenu.prototype = {

	constructor: AccordionMenu,

	init: function(options){
		this.config = $.extend(this.config,options || {});
		var self = this,
			_config = self.config,
			_cache = self.cache;

		// render html structure
		$(_config.containerCls).each(function(index,item){

			self._renderHTML(item);

			// handle click event
			self._bindEnv(item);
		});
	},
	_renderHTML: function(container){
		var self = this,
			_config = self.config,
			_cache = self.cache;
		var ulhtml = $('<ul></ul>');
		$(_config.menuArrs).each(function(index,item){
			var lihtml = $('<li><h2>'+item.name+'</h2></li>');

			if(item.submenu && item.submenu.length > 0) {
				self._createSubMenu(item.submenu,lihtml);
			}

			$(ulhtml).append(lihtml);
		});
		$(container).append(ulhtml);

		_config.renderCallBack && $.isFunction(_config.renderCallBack) && _config.renderCallBack();

		// handling level indent
		self._levelIndent(ulhtml);
	},
	/**
	 * Create Submenu
	 * @param {array} submenu
	 * @param {lihtml} li item
	 */
	_createSubMenu: function(submenu,lihtml){
		var self = this,
			_config = self.config,
			_cache = self.cache;
		var subUl = $('<ul></ul>'),
			callee = arguments.callee,
			subLi;

		$(submenu).each(function(index,item){
			var url = item.url || 'javascript:void(0)';

			subLi = $('<li><a href="'+url+'">'+item.name+'</a></li>');
			if(item.submenu && item.submenu.length > 0) {

				$(subLi).children('a').prepend('<img src="'+getRootPath()+'assets/img/blank.gif" alt=""/>');
               callee(item.submenu, subLi);
			}
			$(subUl).append(subLi);
		});
		$(lihtml).append(subUl);
	},
	/**
	 * handling level indent
	 */
	_levelIndent: function(ulList){
		var self = this,
			_config = self.config,
			_cache = self.cache,
			callee = arguments.callee;

		var initTextIndent = 1,
			lev = 1,
			$oUl = $(ulList);

		while($oUl.find('ul').length > 0){
			initTextIndent = parseInt(initTextIndent,10) + 1 + 'em';
			$oUl.children().children('ul').addClass('lev-' + lev)
						.children('li').css('text-indent', initTextIndent);
			$oUl = $oUl.children().children('ul');
			lev++;
		}
		$(ulList).find('ul').hide();
	},
	/**
	 * Bind Event
	 */
	_bindEnv: function(container) {
		var self = this,
			_config = self.config;

		$('h2,a',container).unbind(_config.type);
		$('h2,a',container).bind(_config.type,function(e){
			if($(this).siblings('ul').length > 0) {
				$(this).siblings('ul').slideToggle('300').end().children('img').toggleClass('unfold');
			}

			$(this).parent('li').siblings().find('ul').hide()
				   .end().find('img.unfold').removeClass('unfold');
			_config.clickItemCallBack && $.isFunction(_config.clickItemCallBack) && _config.clickItemCallBack($(this));
			$('.wrap-menu a').click(function(){
				$(".wrap-menu").find('a').css("background-color","");
				$(this).css("background-color","#ccc");
			});
		});
	}
};

function get_info(act, server)
{
	resetDiv();

	var path = getRootPath()+'/get_info/'+server;

	jQuery.ajax({
		url: path,
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: get_info_action(act, server)
	});
}

var get_info_action = function (act, server) {
	return function (data) {
		switch(act)
		{
			case "show":
				show_server_info(data, server);
				break;

			case "add":
				add_server(data, server);
				break;
		}
	}
}

function define_serverTitle(server)
{
	if(server == "hnb")
		return "HNB Gateway";
	else if(server == "backup")
		return "Backup Server";
}

function show_server_info(all_data, server)
{
	console.log(all_data);
	all_data.sort(function(a, b){
		var ipA = a.ip, ipB = b.ip;
		if (ipA < ipB) //sort string ascending
			return -1
		if (ipA > ipB)
			return 1
		return 0 //default return value (no sorting)
	});
	console.log(all_data);
	var title = define_serverTitle(server);

	$("#upheader_content").html("<h1>"+title+" FTP Info</h1>");
	$("#item_panel").addClass('item_panel');
	$("#item_panel").html('<i class="fa fa-plus fa-1x" title="Add Server"></i>&nbsp;<i class="fa fa-times fa-1x" title="Delete Server"></i>');

	buf = "<table id='list_table'><tr><th><input type='checkbox' id='selectall'></th><th>Edit</th><th>IP</th><th>Port</th><th>Category</th><th>Login Username</th><th>FTP Login Type</th><th>Path</th><th>Update Time</th><th>Update User</th></tr>";
	for(i = 0; i < all_data.length-1; i++)
	{
		temp = all_data[i];
		buf += "<tr><td><input type='checkbox' class='myCheckbox' id='myCheckbox"+i+"'></td><td><span id='edit"+i+"' class='ui-icon ui-icon-pencil edit_icon'></span></td><td>"+temp['ip']+"</td><td>"+temp['port']+"</td><td>"+temp['configName']+"</td><td>"+temp['loginName']+"</td><td>"+temp['type']+"</td><td>"+temp['directoryPath']+"</td><td>"+temp['updateTime']+"</td><td>"+temp['username']+"</td></tr>";
	
	}
	buf += "</table>";
	$("#mid_content").html(buf);

	td_highlight();

	/** select all checkboxes **/
	$('#selectall').click( function (event) {
		if(this.checked) { // check select status
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = true;  //select all checkboxes with class "myCheckbox"
			});
		}else{
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = false; //deselect all checkboxes with class "myCheckbox"
			});
		}
	});

	/** edit a server info event **/
	$(".ui-icon-pencil").click(function(event){
		$("#item_panel").html("");
		var theInfo = {};
		theInfo['ip'] = all_data[parseInt(this.id.substr(4))]['ip'];
		theInfo['port'] = all_data[parseInt(this.id.substr(4))]['port'];
		theInfo['loginName'] = all_data[parseInt(this.id.substr(4))]['loginName'];
		theInfo['loginType'] = all_data[parseInt(this.id.substr(4))]['type'];
		theInfo['path'] = all_data[parseInt(this.id.substr(4))]['directoryPath'];
		theInfo['configName'] = all_data[parseInt(this.id.substr(4))]['configName'];

		edit_server(theInfo, server, all_data[all_data.length-1]);
	});

	$("#dialog_confirm p").html('<span id="ui_icon_alert" class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>The server will be permanently deleted and cannot be recovered. Are you sure?');

	/**  delete server button event **/
	$("#item_panel .fa-times").click( function (event) {
		$("#bottom_msg").html("");
		event.preventDefault();
		var checkboxes = $("#list_table").find(".myCheckbox:checked");
		var selectedIP = [];
		if(checkboxes.length > 0)
		{
			for(i = 0; i < checkboxes.length; i++)
			{
				j = parseInt(checkboxes[i].id.substr(10));
				selectedIP.push([all_data[j]['configName'],all_data[j]['ip']]);
			}

			dialog.dialog( "open" );
			dialog = $("#dialog_confirm").dialog({
				autoOpen: false,
				resizable: false,
				draggable: true,
				height: 165,
				modal: true,
				title: "Delete the server?",
				buttons: {
					"YES": function(event, ui) {
						$( this ).dialog( "close" );
						delete_server(selectedIP, server);
					},
					Cancel: function(event, ui) {
						$( this ).dialog( "close" );
					}
				}
			});
		}
		else
			$().tostie({type:"error", message:"Select something please."});
	});

	/**  add server button event **/
	$("#item_panel .fa-plus").click( function (event) {
		get_info('add', server);
	});
}

function edit_server(theInfo, server, ftp_logintype)
{
	$("#bottom_msg").html("");
	$("#mid_content").html("");
	console.log(theInfo);

	var selected = theInfo['loginType'];
	var title = define_serverTitle(server);

	$("#upheader_content").html("<h1>Edit "+title+" "+theInfo['ip']+" FTP Info</h1>");

	// FTP login type selection
	var selectOption = '<select id="ftp_logintype" name="ftp_logintype" form="editform">';
	for(var i = 0; i < ftp_logintype.length; i++)
		selectOption += '<option value="'+ftp_logintype[i]+'">'+ftp_logintype[i]+'</option>';
	selectOption += '</select>';

	var path = getBaseURL()+getProjectName()+'/index.php/configtool/update_server/';
	var buf = '<form id="editform" name="editform" class="editform" action="'+path+'" method="POST"><table id="list_table"><tr><th>IP</th><td>'+theInfo['ip']+'</td></tr><tr><th>Port</th><td><input type="text" id="port" name="port" value="'+theInfo['port']+'"></td></tr><tr><th>Category</th><td>'+theInfo['configName']+'</td></tr><tr><th>Login Username</th><td><input type="text" id="loginName" name="loginName" value="'+theInfo['loginName']+'"></td></tr><tr><th>Login Password</th><td><input type="password" id="password" name="password"></td></tr><tr><th>Password Confirm</th><td><input type="password" id="passconf" name="passconf"></td></tr><tr><th>FTP Login Type</th><td>'+selectOption+'</td></tr><tr><th>Path</th><td><input type="text" id="directoryPath" name="directoryPath" value="'+theInfo['path']+'" title="ex: / or /LOG ; leave it empty as /"></td></tr></table><input type="submit" class="juibtn" value="Update"></form>';
	$("#mid_content").html(buf);

	// show default selected option
	var opt = $("option[value="+selected+"]"), html = $("<div>").append(opt.clone()).html();
	html = html.replace(/\>/, ' selected="selected">');
	opt.replaceWith(html);

	$(".juibtn").button().click( function (event) {
		edit_server_rule(theInfo["ip"], "editform", server, theInfo['configName']);
	});
}

/** edit form validation **/
function edit_server_rule(ip, formname, server, configName)
{
	$("#bottom_msg").html("");
	var errorString = '';
	var validator = new FormValidator(formname,
		[
			{
				name: 'port',
				display: 'Port',
				rules: 'required|is_natural_no_zero|max_length[11]'
			},
			{
				name: 'loginName',
				display: 'Login Username',
				rules: 'required|min_length[1]|max_length[30]'
			},
			{
				name: 'password',
				display: 'Login Password',
				rules: 'min_length[1]'
			},
			{
				name: 'directoryPath',
				display: 'Path',
				rules: 'max_length[128]'
			}
		],
		function(errors,event)
		{
			errorString = '';
			if($("#password").val() != $("#passconf").val())
			{
				k = { message: "The Password Confirm field does not match the Login Password field." }
				errors.push(k);
			}
			var path = $("#directoryPath").val();
			if( path != "" && path.slice(0,1) != "/")
				errors.push({ message: "Path should start with '/'." });
			if (errors.length > 0)
			{
				for (var i = 0, errorLength = errors.length; i < errorLength; i++)
				{
					errorString += errors[i].message + '<br />';
				}

				$("#bottom_msg").html(errorString).css("color","red");
			}
		}
	);
	$('#'+formname).ajaxForm({
		data: { ip: ip, server: server, configName: configName },
		complete: function(xhr, XMLHttpRequest, textStatus) {
			if(xhr.responseText != "success")
				$("#bottom_msg").html(xhr.responseText).css("color","red");
			else
				get_info("show", server);
		},
		clearForm: false
	});
}

function delete_server(selectedIP, server)
{
	var path = getRootPath()+'/delete_server/'+server;
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { selectedIP: selectedIP },
		dataType: 'text',
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, ajaxOptions, thrownError){
			$("#bottom_msg").append(xhr).css("color","red");
			get_info("show", server);
		}
	});
}

function add_server(data, server)
{
	$("#bottom_msg").html("");
	$("#mid_content").html("");

	var title = define_serverTitle(server);

	$("#upheader_content").html("<h1>Add FTP server of "+title+"</h1>");

	var path = getBaseURL()+getProjectName()+'/index.php/configtool/add_server/';

	// FTP login type selection
	var selectOption = '<select id="ftp_logintype" name="ftp_logintype" form="addform">';
	for(var i = 0; i < data[data.length-1].length; i++)
		selectOption += '<option value="'+data[data.length-1][i]+'">'+data[data.length-1][i]+'</option>';
	selectOption += '</select>';

	var categoryOption = "<select id='configName' name='configName' forom='addform'>";
	switch(server)
	{
		case "hnb":
			categoryOption += "<option value='LOG'>LOG</option><option value='PM'>PM</option>";
			break;
		case "backup":
		categoryOption += "<option value='BACKUP'>BACKUP</option>";
			break;
	}
	categoryOption += "</select>";

	var buf = "<form id='addform' name='addform' class='editform' action='"+path+"' method='POST'><table id='list_table'><tr><th>IP</th><td><input type='text' name='addip' id='addip'></td></tr><tr><th>Port</th><td><input type='text' name='addport' id='addport'></td></tr><tr><th>Category</th><td>"+categoryOption+"</td></tr><tr><th>Login Username</th><td><input type='text' name='loginName'></td></tr><tr><th>Login Password</th><td><input type='password' name='password'></td></tr><tr><th>Password Confirm</th><td><input type='password' name='passconf'></td></tr><tr><th>FTP Login Type</th><td>"+selectOption+"</td></tr><tr><th>Path</th><td><input type='text' id='directoryPath' name='directoryPath' placeholder='ex: / or /LOG ; leave it empty as /' title='ex: / or /LOG ; leave it empty as /'></td></tr></table><input type='submit' class='juibtn' value='Add'></form><div id='bottom_msg' style='color:red;'></div>";
	$("#mid_content").html(buf);
	$(".juibtn").button().click( function (event) {
		$("#bottom_msg").html("");
		add_server_rule(data, "addform", server);
	});
}

/** add form validation **/
function add_server_rule(webinfo, formname, server)
{
	var errorString = '';
	var validator = new FormValidator(formname,
		[
			{
				name: 'addip',
				display: 'IP',
				rules: 'required|valid_ip'
			},
			{
				name: 'addport',
				display: 'Port',
				rules: 'required|is_natural_no_zero|max_length[11]'
			},
			{
				name: 'loginName',
				display: 'Login Username',
				rules: 'required|min_length[1]|max_length[30]'
			},
			{
				name: 'password',
				display: 'Login Password',
				rules: 'required|min_length[1]'
			},
			{
				name: 'passconf',
				display: 'Password Confirm',
				rules: 'required|matches[password]'
			},
			{
				name: 'directoryPath',
				display: 'Path',
				rules: 'max_length[128]'
			}
		],
		function(errors,event)
		{
			errorString = '';
			var path = $("#directoryPath").val();
			if( path != "" && path.slice(0,1) != "/")
				errors.push({ message: "Path should start with '/'." });
			if (errors.length > 0)
			{
				for (var i = 0, errorLength = errors.length; i < errorLength; i++)
				{
					errorString += errors[i].message + '<br />';
				}

				$("#bottom_msg").html(errorString).css( "color", "red" );
			}
		}
	);

	$("#"+formname).ajaxForm({
		data: { server: server },
		complete: function(xhr, XMLHttpRequest, textStatus) {
			$("#bottom_msg").html(xhr.responseText);
		}
	});
}

function get_housekeep_info(act)
{
	resetDiv();

	var path = getBaseURL()+getProjectName()+'/index.php/housekeeping/get_housekeep_info/';

	jQuery.ajax({
		url: path,
		dataType: 'json',
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: get_housekeepinfo_action(act)
	});
}

var get_housekeepinfo_action = function (act) {
	return function (data) {
		switch(act)
		{
			case "edit":
				update_housekeep_info(data);
				break;

			case "delete":
				housekeepnow(data);
				break;
		}
	}
}

function update_housekeep_info(data)
{
	$("#upheader_content").html("<h1>Edit Housekeeping Info</h1>");
	var path = getBaseURL()+getProjectName()+'/index.php/housekeeping/update_housekeep_info/';

	var buf = '<form id="housekeepform" name="housekeepform" class="housekeepform" action="'+path+'" method="POST"><table id="housekeep_table"><tr><th>Update User</th><td>'+data['username']+'</td></tr><tr><th>Update Time</th><td>'+data['updateTime']+'</td></tr><tr><th>Keep raw data for how many days</th><td><input type="text" name="keep_rawdata_days" id="keep_rawdata_days" value="'+data['keep_rawdata_days']+'"></td></tr><tr><th>Keep db data for how many days</th><td><input type="text" name="keep_dbdata_days" id="keep_dbdata_days" value="'+data['keep_dbdata_days']+'"></td></tr></table><input type="submit" class="juibtn" value="Update"></form>';
	$("#mid_content").html(buf);
	$(".juibtn").button().click( function (event) {
		$("#bottom_msg").html("");
		update_housekeepinfo_rule("housekeepform", data);
	});
}

function update_housekeepinfo_rule(formname, odata)
{
	var errorString = '';
	var validator = new FormValidator(formname, 
		[
			{
				name: 'keep_rawdata_days',
				display: 'Keep Rawdata Days',
				rules: 'required|is_natural|less_than[1000]'
			},
			{
				name: 'keep_dbdata_days',
				display: 'Keep Dbdata Days',
				rules: 'required|is_natural|less_than[1000]'
			}
		], 
		function(errors,event)
		{
			errorString = '';
			if (errors.length > 0)
			{
				for (var i = 0, errorLength = errors.length; i < errorLength; i++)
				{
					errorString += errors[i].message + '<br />';
				}
				$().tostie({type:"error", message:errorString});
			}
		}
	);
	$('#housekeepform').ajaxForm({
		beforeSubmit: function() { 
			if(odata['keep_rawdata_days'] == $("#keep_rawdata_days").val() && 
				odata['keep_dbdata_days'] == $("#keep_dbdata_days").val() )
			{
				$().tostie({type:"error", message:"Nothing changed."});
				return false; 
			}
		},
		complete: function(xhr, XMLHttpRequest, textStatus) {
			$().tostie({type:"notice", message:xhr.responseText});
		},
		clearForm: false
	});
}

function housekeepnow(data)
{
	$("#upheader_content").html("<h1>Delete Data Now</h1>");
	$("#mid_content").html("");
	$("#bottom_msg").html("");

	var buf = '<table id="housekeep_table"><tr><th>Delete raw data older than</th><td><input type="text" id="keep_rawdata_days" value="'+data['keep_rawdata_days']+'">days</td><td style="width: 70px;"><button type="button" id="btn_deleteraw" class="juibtn">Delete</button></td></tr>';
	buf += '<tr><th>Delete DB data older than</th><td><input type="text" id="keep_dbdata_days" value="'+data['keep_dbdata_days']+'">days</td><td style="width: 70px;"><button type="button" id="btn_deletedb" class="juibtn">Delete</button></td></tr></table>';
	$("#mid_content").html(buf);
	$("#btn_deleteraw").button().click( function (event) {
		delete_housekeepData($("#keep_rawdata_days").val(), "rawdata");
	});
	$("#btn_deletedb").button().click( function (event) {
		delete_housekeepData($("#keep_dbdata_days").val(), "dbdata");
	});
}

function delete_housekeepData(days, type)
{
	var path = getBaseURL()+getProjectName()+'/index.php/housekeeping/'+type+'/';
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { days: days },
		dataType: 'text',
		beforeSend: function (xhr, opts) {
			if(!isNumber(days) || days < 0 || days >1001)
			{
				$().tostie({type:"error", message:"Input a valid number between 0 and 1000 please."});
				return false;
			}
		},
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, ajaxOptions, thrownError){
			$("#bottom_msg").html("");
			$().tostie({type:"notice", message:xhr});
		}
	});
}

function get_kpiTarget_info(act)
{
	resetDiv();

	var path = getRootPath()+'/get_kpiTarget_info/'+act;
	jQuery.ajax({
		url: path,
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError){ console.log(xhr, ajaxOptions, thrownError); },
		success: get_kpiTarget_action(act)
	});
}

var get_kpiTarget_action = function (act) {
	return function (data) {
		switch(act)
		{
			case "show":
				show_kpiTarget(data);
				break;

			case "add":
				add_kpiTarget(data);
				break;
		}
	}
}

function show_kpiTarget(data)
{
	var title = "KPI Target Info";
	$("#upheader_content").html("<h1>"+title+"</h1>");
	$("#item_panel").addClass('item_panel');
	$("#item_panel").html('<i class="fa fa-plus fa-1x" title="Add KPI target"></i>&nbsp;<i class="fa fa-pencil-square-o fa-1x" title="Update KPI target"></i>&nbsp;<i class="fa fa-times fa-1x" title="Delete KPI target"></i>');

	buf = "<table id='list_table'><tr><th><input type='checkbox' id='selectall'></th><th>KPI Name</th><th>Target</th><th>Update Time</th><th>Update User</th></tr>";
	for(i = 0; i < data.length; i++)
	{
		temp = data[i];
		buf += "<tr><td><input type='checkbox' class='myCheckbox' id='myCheckbox"+i+"'></td><td>"+temp['KpiDisplayName']+"</td><td><input type='text' id='target"+i+"' value='"+temp['target']+"'></td><td>"+temp['updateTime']+"</td><td>"+temp['updateUser']+"</td></tr>";
	}
	buf += "</table>";
	$("#mid_content").html(buf);

	td_highlight();

	/** select all checkboxes **/
	$('#selectall').click( function (event) {
		if(this.checked) { // check select status
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = true;  //select all checkboxes with class "myCheckbox"
			});
		}else{
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = false; //deselect all checkboxes with class "myCheckbox"
			});
		}
	});

	/** update modified target value event **/
	var inputs = $('#list_table input:text').each(function() {
			$(this).data('original', this.value);
		});

	$("#item_panel .fa-pencil-square-o").click( function (event) {
		$("#bottom_msg").html("");
		var checkboxes = $("#list_table").find(".myCheckbox:checked");
		var newTarget = {};
		var errorTarget = false;
		if(checkboxes.length > 0)
		{
			inputs.each( function() {
				if($(this).data('original') !== this.value)
				{
					if(!isNumber(this.value) || this.value <= 0 || this.value >101)
						errorTarget = true;
					var index = this.id.substr(6);
					newTarget[data[index]['kpiName']] = this.value;
				}
			});

			if(Object.size(newTarget) > 0 && !errorTarget)
				update_kpiTarget(newTarget);
			else
			{
				if(errorTarget)
				{
					$().tostie({type:"error", message:"Input a valid number between 1 and 101 please."});
				}
				if(Object.size(newTarget) == 0)
				{
					$().tostie({type:"error", message:"Nothing changed."});
				}
			}
		}
		else
			$().tostie({type:"error", message:"Select something please."});
	});

	$("#dialog_confirm p").html('<span id="ui_icon_alert" class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure?');

	/**  delete kpi target button event **/
	$("#item_panel .fa-times").click( function (event) {
		$("#bottom_msg").html("");
		event.preventDefault();
		var checkboxes = $("#list_table").find(".myCheckbox:checked");
		var selectedKpi = [];
		if(checkboxes.length > 0)
		{
			for(i = 0; i < checkboxes.length; i++)
			{
				j = parseInt(checkboxes[i].id.substr(10));
				selectedKpi.push(data[j]['kpiName']);
			}

			dialog.dialog( "open" );
			dialog = $("#dialog_confirm").dialog({
				autoOpen: false,
				resizable: false,
				draggable: true,
				height: 143,
				modal: true,
				title: "Delete KPI targets?",
				buttons: {
					"YES": function(event, ui) {
						$( this ).dialog( "close" );
						delete_kpiTarget(selectedKpi);
					},
					Cancel: function(event, ui) {
						$( this ).dialog( "close" );
					}
				}
			});
		}
		else
			$().tostie({type:"error", message:"Select something please."});
	});

	/**  add KPI target event **/
	$("#item_panel .fa-plus").click( function (event) {
		get_kpiTarget_info('add');
	});
}

function update_kpiTarget(data)
{
	var path = getRootPath()+'/update_kpiTarget/';

	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { target: data },
		dataType: 'text',
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, ajaxOptions, thrownError){
			if(xhr == "success")
				get_kpiTarget_info('show');
			else
			{
				console.log(xhr);
				$().tostie({type:"error", message:xhr});
			}
		}
	});
}

function add_kpiTarget(data)
{
	// KPI selection
	var selectOption = '<select id="kpi_option" name="kpi_option">';// form="editform"

	for(var k in data["kpi"])
	{
		var key = data["kpi"][k];
		selectOption += '<option value="'+k+'">'+key+'</option>';
	}
	selectOption += '</select>';

	$("#upheader_content").html("<h1>Add KPI Target</h1>");
	var path = getBaseURL()+getProjectName()+'/index.php/configtool/add_kpiTarget/';

	var buf = "<form id='addform' name='addform' class='editform' action='"+path+"' method='POST'><table id='list_table'><tr><th>KPI Name</th><td>"+selectOption+"</td></tr><tr><th>Target</th><td><input type='text' name='addTarget' id='addTarget' placeholder='ex: 1 or 101' title='range:1~101'></td></tr></table><input type='submit' class='btn btn-default btn-sm juibtn' value='Add'></form>";

	$("#mid_content").html(buf);
	$(".juibtn").button().click( function (event) {
		$("#bottom_msg").html("");
		add_kpi_rule("addform", data);
	});
}

function add_kpi_rule(formname, data)
{
	var errorString = "";
	var validator = new FormValidator(formname,
		[
			{
				name: 'addTarget',
				display: 'Target',
				rules: 'required|is_natural_no_zero|less_than[102]'
			}
		],
		function(errors, event)
		{
			errorString = "";
			if(data["kpidata"].indexOf($('#kpi_option option:selected').val()) >= 0)
				errorString = "Duplicate KPI<br />";
			if (errors.length > 0)
			{
				for (var i = 0, errorLength = errors.length; i < errorLength; i++)
				{
					errorString += errors[i].message + '<br />';
				}

				$("#bottom_msg").html(errorString).css("color","red");
			}
		}
	);

	$("#"+formname).ajaxForm({
		beforeSubmit: function(formData, jqForm, options) {
			if(errorString)
			{
				$("#bottom_msg").html(errorString).css("color","red");
				return false;
			}
		},
		complete: function(xhr, XMLHttpRequest, textStatus) {
			$("#bottom_msg").html(xhr.responseText).css("color","red");
		},
		clearForm: true
	});
}

function delete_kpiTarget(selectedKpi)
{
	var path = getRootPath()+'/delete_kpiTarget/';
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { selectedKpi: selectedKpi },
		dataType: 'text',
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, ajaxOptions, thrownError){
			get_kpiTarget_info('show');
		}
	});
}

function get_kpiPref()
{
	resetDiv();

	var path = getRootPath()+'/get_kpiPref/';
	jQuery.ajax({
		url: path,
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError){ console.log(xhr, ajaxOptions, thrownError); },
		success: show_pref('kpi')
	});
}

var show_pref = function (act) {
	return function (data) {
		switch(act)
		{
			case "kpi":
				show_kpiPref(data);
				break;

			case "counter":
				show_counterPref(data);
				break;

			case "hnb":
				show_hnbPref(data);
				break;
		}
	}
}

function show_kpiPref(data)
{
	float_button();
	$("#upheader_content").html("<h1>"+data['user']+"'s KPI Preference</h1>");
	var kpiPref = data["kpi_pref"];
	delete kpiPref["Num_of_HNBs_PM_Reported"]; 
	var buf = "<table id='list_table'><colgroup><col span='1' style='width:10%'><col span='1' style='width:80%'><col span='1' style='width:10%'></colgroup><tr><th>No</th><th>KPI Name</th><th><input type='checkbox' id='selectall'></th></tr>";
	var keysOfkpiPref = Object.keys(kpiPref);

	/** remove Num_of_HNBs_PM_Reported (Num of HNBs (PM Reported)) **/
	var index = keysOfkpiPref.indexOf("Num_of_HNBs_PM_Reported");
	if (index > -1) { keysOfkpiPref.splice(index, 1); }

	keysOfkpiPref.sort();

	var i = 0;
	for(k in keysOfkpiPref)
	{
		buf += "<tr><td>"+(i+1)+"</td><td>"+kpiPref[keysOfkpiPref[k]]+"</td><td><input type='checkbox' class='myCheckbox' id='"+keysOfkpiPref[k]+"'></td></tr>";
		i++;
	}
	buf += "</table>";
	$("#mid_content").html(buf);

	td_highlight();

	if(data['userKpiPref'][0] != "")
	{
		for(k in data['userKpiPref'])
			$('#'+data['userKpiPref'][k]).prop('checked', true);
	}

	/** select all checkboxes **/
	$('#selectall').click( function (event) {
		if(this.checked) { // check select status
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = true;  //select all checkboxes with class "myCheckbox"
			});
		}else{
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = false; //deselect all checkboxes with class "myCheckbox"
			});
		}
	});

	/** update KPI Preference event **/
	$(".juibtn").button().click( function (event) {
		event.preventDefault();
		var checkboxes = $("#list_table").find(".myCheckbox:checked");
		var selectedKpi = [];
		if(checkboxes.length >= 0)
		{
			for(i = 0; i < checkboxes.length; i++)
				selectedKpi.push(checkboxes[i].id);
			add_kpiPref(selectedKpi, data['user']);
		}
	});
}

function add_kpiPref(data, user)
{
	var path = getRootPath()+'/add_kpiPref/';
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { kpiPref: data, user: user },
		dataType: 'text',
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, ajaxOptions, thrownError){
			$().tostie({type:"notice", message: xhr});
		}
	});
}

function get_counterPref()
{
	resetDiv();

	var path = getRootPath()+'/get_counterPref/';
	jQuery.ajax({
		url: path,
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError){ console.log(xhr, ajaxOptions, thrownError); },
		success: show_pref('counter')
	});
}

function show_counterPref(data)
{
	float_button();
	$("#upheader_content").html("<h1>"+data['user']+"'s Counter Preference</h1>");

	var counterPref = data["counter_pref"];
	var buf = "<table id='list_table'><colgroup><col span='1' style='width:10%'><col span='1' style='width:80%'><col span='1' style='width:10%'></colgroup><tr><th>No</th><th>Counter Name</th><th><input type='checkbox' id='selectall'></th></tr>";//<button class='juibtn' id='saveBtn2'>Save</button>

	// turn object data["counter_pref"] to array and sort it
	var counterPref = $.map(counterPref, function(value, index) {
		return [value];
	});
	counterPref.sort();

	var i = 0;
	for(k in counterPref)
	{
		buf += "<tr><td>"+(i+1)+"</td><td>"+counterPref[k]+"</td><td><input type='checkbox' class='myCheckbox' id='"+counterPref[k]+"'></td></tr>";
		i++;
	}
	buf += "</table>";
	$("#mid_content").html(buf);

	td_highlight();

	if(data['userCounterPref'][0] != "")
	{
		for(k in data['userCounterPref'])
			$('#'+data['userCounterPref'][k]).prop('checked', true);
	}

	/** select all checkboxes **/
	$('#selectall').click( function (event) {
		if(this.checked) { // check select status
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = true;  //select all checkboxes with class "myCheckbox"              
			});
		}else{
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = false; //deselect all checkboxes with class "myCheckbox"                      
			});
		}
	});

	/** update Counter Preference event **/
	$(".juibtn").button().click( function (event) {
		event.preventDefault();
		var checkboxes = $("#list_table").find(".myCheckbox:checked");
		var selectedCounter = [];
		if(checkboxes.length >= 0)
		{
			for(i = 0; i < checkboxes.length; i++)
				selectedCounter.push(checkboxes[i].id);
			add_counterPref(selectedCounter, data['user']);
		}
	});
}

function add_counterPref(data, user)
{
	var path = getRootPath()+'/add_counterPref/';
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { counterPref: data, user: user },
		dataType: 'text',
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, ajaxOptions, thrownError){
			$().tostie({type:"notice", message: xhr});
		}
	});
}

function get_HNBPref()
{
	resetDiv();

	var path = getRootPath()+'/get_HNBPref/';
	jQuery.ajax({
		url: path,
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError){ console.log(xhr, ajaxOptions, thrownError); },
		success: show_pref('hnb')
	});
}

function show_hnbPref(data)
{
	float_button();
	$("#upheader_content").html("<h1>"+data['user']+"'s HNB Preference</h1>");

	var hnblist = data["hnblist"];
	var buf = "<table id='list_table'><colgroup><col span='1' style='width:10%'><col span='1' style='width:80%'><col span='1' style='width:10%'></colgroup><tr><th>No</th><th>HNB</th><th><input type='checkbox' id='selectall'></th></tr>";//<button class='juibtn' id='saveBtn2'>Save</button>
	var i = 0;
	for(k in hnblist)
	{
		buf += "<tr><td>"+(i+1)+"</td><td>"+hnblist[k]+"</td><td><input type='checkbox' class='myCheckbox' id='"+hnblist[k]+"'></td></tr>";
		i++;
	}
	buf += "</table>";
	$("#mid_content").html(buf);

	td_highlight();

	if(data['userHnbPref'][0] != ""	)
	{
		for(k in data['userHnbPref'])
			$('#'+data['userHnbPref'][k]).prop('checked', true);
	}

	/** select all checkboxes **/
	$('#selectall').click( function (event) {
		if(this.checked) { // check select status
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = true;  //select all checkboxes with class "myCheckbox"
			});
		}else{
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = false; //deselect all checkboxes with class "myCheckbox"
			});
		}
	});

	/** update HNB Preference event **/
	$(".juibtn").button().click( function (event) {
		event.preventDefault();
		var checkboxes = $("#list_table").find(".myCheckbox:checked");
		var selectedHnb = [];
		if(checkboxes.length >= 0)
		{
			for(i = 0; i < checkboxes.length; i++)
				selectedHnb.push(checkboxes[i].id);
			add_hnbPref(selectedHnb, data['user']);
		}
	});
}

function add_hnbPref(data, user)
{
	var path = getRootPath()+'/add_hnbPref/';
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { hnbPref: data },
		dataType: 'text',
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, ajaxOptions, thrownError){
			$().tostie({type:"notice", message: xhr});
		}
	});
}

function get_allUserInfo()
{
	resetDiv();

	var path = getBaseURL()+getProjectName()+'/index.php/user/get_allUserInfo/';

	jQuery.ajax({
		url: path,
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(data){
			show_user(data);
		}
	});
}

var dialog;
function show_user(data)
{
	var role = data["role"];
	var user = data["user"];

	$("#upheader_content").html("<h1>User Management</h1>");
	$("#item_panel").addClass('item_panel');
	$("#item_panel").html('<i class="fa fa-plus fa-1x" title="Add User"></i>&nbsp;<i class="fa fa-times fa-1x" title="Delete User"></i>');

	buf = "<table id='list_table' class='tablesorter'><thead><tr><th><input type='checkbox' id='selectall'></th><th>Edit</th><th>No</th><th>Username</th><th>Email</th><th>Role</th></tr></thead><tbody>";
	for(i = 0; i < user.length; i++)
	{
		temp = user[i];
		buf += "<tr><td><input type='checkbox' class='myCheckbox' id='myCheckbox"+i+"'></td><td><span id='edit"+i+"' class='ui-icon ui-icon-pencil edit_icon'></span></td><td>"+(i+1)+"</td><td>"+temp['username']+"</td><td>"+temp['email']+"</td><td>"+temp['roleName']+"</td></tr>";
	}
	buf += "</tbody></table>";
	$("#mid_content").html(buf);

	td_highlight();
	$("#list_table").tablesorter({
		headers: {
			0: { sorter: false },
			1: { sorter: false },
			2: { sorter: false }
		}
	});

	/** select all checkboxes **/
	$('#selectall').click( function (event) {
		if(this.checked) { // check select status
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = true;  //select all checkboxes with class "myCheckbox"
			});
		}else{
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = false; //deselect all checkboxes with class "myCheckbox"
			});
		}
	});

	/** edit a server info event **/
	$(".ui-icon-pencil").click(function(event){
		$("#bottom_msg").html("");
		var no = parseInt(this.id.substr(4));
		edit_user(user[no], role);
	});

	$("#dialog_confirm p").html('<span id="ui_icon_alert" class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>The user will be permanently deleted and cannot be recovered. Are you sure?');

	/**  delete user button event **/
	$("#item_panel .fa-times").click( function (event) {
		$("#bottom_msg").html("");
		event.preventDefault();
		var checkboxes = $("#list_table").find(".myCheckbox:checked");
		var selectedUser = [], userRole = [];
		if(checkboxes.length > 0)
		{
			for(i = 0; i < checkboxes.length; i++)
			{
				j = parseInt(checkboxes[i].id.substr(10));
				selectedUser.push(user[j]['username']);
				userRole.push(user[j]['roleName']);
			}

			dialog.dialog( "open" );
			dialog = $("#dialog_confirm").dialog({
				autoOpen: false,
				resizable: false,
				draggable: true,
				height: 165,
				modal: true,
				title: "Delete the user?",
				buttons: {
					"YES": function(event, ui) {
						$( this ).dialog( "close" );
						delete_user(selectedUser, userRole);
					},
					Cancel: function(event, ui) {
						$( this ).dialog( "close" );
					}
				}
			});
		}
		else
			$().tostie({type:"error", message:"Select something please."});
	});

	/**  add user button event **/
	$("#item_panel .fa-plus").click( function (event) {
		add_user(role);
	});
}

function add_user(data)
{
	console.log(data);
	$("#bottom_msg").html("");
	$("#mid_content").html("");
	$("#item_panel").html("");

	$("#upheader_content").html("<h1>Add new user</h1>");

	var path = getBaseURL()+getProjectName()+'/index.php/user/add_user/';

	// User Role selection
	var roleOption = '<select id="role_option" name="role_option" form="addform">';
	for(var k in data)
	{
		if(data[k].roleName == "User")
			roleOption += '<option value="'+data[k].roleId+'" selected>'+data[k].roleName+'</option>';
		else
			roleOption += '<option value="'+data[k].roleId+'">'+data[k].roleName+'</option>';
	}
	roleOption += '</select>';

	var buf = "<form id='addform' name='addform' class='editform' action='"+path+"' method='POST'><table id='list_table'><tr><th>Username</th><td><input type='text' name='username' id='username'></td></tr><tr><th>Password</th><td><input type='password' name='password' id='password'></td></tr><tr><th>Password Confirm</th><td><input type='password' id='passconf' name='passconf'></td></tr><tr><th>Email</th><td><input type='email' id='email' name='email'></td></tr><tr><tr><th>Role</th><td>"+roleOption+"</td></tr></table><input type='submit' class='juibtn' value='Add'></form>";
	$("#mid_content").html(buf);
	$(".juibtn").button().click( function (event) {
		$("#bottom_msg").html("");
		add_user_rule("addform");
	});
}

function add_user_rule(formname)
{
	var errorString = '';
	var validator = new FormValidator(formname,
		[
			{
				name: 'username',
				display: 'Username',
				rules: 'required|min_length[3]|max_length[30]'
			},
			{
				name: 'password',
				display: 'Password',
				rules: 'required|min_length[1]'
			},
			{
				name: 'passconf',
				display: 'Password Confirm',
				rules: 'required|matches[password]',
			},
			{
				name: 'email',
				display: 'Email',
				rules: 'required|valid_email',
			}
		],
		function(errors,event)
		{
			errorString = '';
			if (errors.length > 0)
			{
				for (var i = 0, errorLength = errors.length; i < errorLength; i++)
				{
					errorString += errors[i].message + '<br />';
				}
				$("#bottom_msg").html(errorString).css("color","red");
				return false;
			}
		}
	);
	$("#"+formname).ajaxForm({
		error: function(xhr, ajaxOptions, thrownError) {
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, XMLHttpRequest, textStatus) {
			if(xhr == "success")
				$('#addform').resetForm();
			$("#bottom_msg").html(xhr).css("color","red");
		}
	});
}

function delete_user(selectedUser, rolename)
{
	var path = getBaseURL()+getProjectName()+'/index.php/user/delete_user/';
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { selectedUser: selectedUser, rolename: rolename },
		dataType: 'text',
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, ajaxOptions, thrownError){
			console.log(xhr);
			if(xhr != "success")
			{
				$().tostie({type:"error", message:xhr});
			}
			else
				get_allUserInfo();
		}
	});
}

function edit_user(user, role)
{
	console.log(user);

	$("#bottom_msg").html("");
	$("#mid_content").html("");
	$("#item_panel").html("");

	$("#upheader_content").html("<h1>Edit User '"+user['username']+"'</h1>");

	var path = getBaseURL()+getProjectName()+'/index.php/user/update_user/';
	var roleOption = '<select id="role_option" name="role_option" form="addform">';
	for(var k in role)
	{
		if(role[k].roleName == user['roleName'])
			roleOption += '<option value="'+role[k].roleId+'" selected>'+role[k].roleName+'</option>';
		else
			roleOption += '<option value="'+role[k].roleId+'">'+role[k].roleName+'</option>';
	}
	roleOption += '</select>';

	var buf = '<form id="editform" name="editform" class="editform" action="'+path+'" method="POST"><table id="list_table"><tr><th>Username</th><td>'+user['username']+'</td></tr><tr><th>Password</th><td><input type="password" name="password" id="password"></td></tr><tr><th>Password Confirm</th><td><input type="password" id="passconf" name="passconf"></td></tr><tr><th>Email</th><td><input type="email" id="email" name="email" value="'+user['email']+'"></td></tr><tr><th>Role</th><td>'+roleOption+'</td></tr></table><input type="submit" class="juibtn" value="Save"></form>';
	$("#mid_content").html(buf);

	$(".juibtn").button().click( function (event) {
		edit_user_rule("editform", user['username']);
	});
}

function edit_user_rule(formname, username)
{
	$("#bottom_msg").html("");
	var errorString = '';
	var validator = new FormValidator(formname,
		[
			{
				name: 'password',
				display: 'Login Password',
				rules: 'min_length[1]'
			},
			{
				name: 'email',
				display: 'Email',
				rules: 'required|valid_email',
			}
		],
		function(errors,event)
		{
			errorString = '';
			if($("#password").val() != $("#passconf").val())
			{
				k = { message: "The Password Confirm field does not match the Login Password field." };
				errors.push(k);
			}
			if (errors.length > 0)
			{
				for (var i = 0, errorLength = errors.length; i < errorLength; i++)
				{
					errorString += errors[i].message + '<br />';
				}
				$("#bottom_msg").html(errorString).css("color","red");

				return false;
			}
		}
	);
	$('#'+formname).ajaxForm({
		data: { username: username, roleId: $('#role_option option:selected').val() },
		error: function(xhr, ajaxOptions, thrownError) {
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, XMLHttpRequest, textStatus) {
			if(xhr != "success")
				$("#bottom_msg").html(xhr.responseText).css("color","red");
			else
				get_allUserInfo();
		}
	});
}

function get_allRoles()
{
	resetDiv();

	var path = getBaseURL()+getProjectName()+"/index.php/user/get_allRoleInfo";
	jQuery.ajax({
		url: path,
		dataType: "JSON",
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(data) {
			show_role(data)
		}
	});
}

function show_role(data)
{
	$("#bottom_msg").html("");
	$("#mid_content").html("");
	$("#item_panel").html("");

	$("#upheader_content").html("<h1>Manage User Roles</h1>");

	var buf = "<table id='list_table'><colgroup><col span='1'><col span='1'><col span='1' style='width:60%'></colgroup><thead><tr><th>Account Type</th><th># Users</th><th>Description</th></tr></thead><tbody>";
	for(i = 0; i < data.length; i++)
	{
		temp = data[i];
		buf += "<tr><td>"+temp['roleName']+"</td><td>"+temp['count']+"</td><td>"+temp['description']+"</td></tr>";
	}
	buf += "</tbody></table>";
	$("#mid_content").html(buf);

	td_highlight();

	/** select all checkboxes **/
	$('#selectall').click( function (event) {
		if(this.checked) { // check select status
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = true;  //select all checkboxes with class "myCheckbox"
			});
		}else{
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = false; //deselect all checkboxes with class "myCheckbox"
			});
		}
	});

}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function float_button()
{
	$("#mid_content").addClass( "mid_content_floatleft" );
	$("#sidebar").addClass( "sidebar" );
	$("#sidebar").html("<button class='juibtn' id='saveBtn'>Save</button>");

	fixDiv();
	sticky_header();
}

function fixDiv()
{
	var offset = $("#sidebar").offset();
	var topPadding = 265;

	$(".contentr").scroll(function() {
		if ($(".contentr").scrollTop() > 10)
		{
			$(".sidebar").stop().animate({ marginTop: $(".contentr").scrollTop() - offset.top + topPadding });
		}
		else
		{
			$(".sidebar").stop().animate({ marginTop: 0 });
		};
	});
}

function sticky_header()
{
	$(".contentr").scroll(function(){
		if( $(".contentr").scrollTop() > 10 )
		{
			$("#upheader_content").addClass( "sticky" );
		}
		else
		{
			$("#upheader_content").removeClass( "sticky" );
		}
	});
}
