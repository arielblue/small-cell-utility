function init()
{
	$('#submenu li').click( function() {
		$(this).addClass("subItemStyle");
		$(this).siblings("li").removeClass("subItemStyle");

		clean();
		switch($(this).index())
		{
			case 0: //HO
				console.log(0, "HO");
				getHnbList("ho");
				break;
			case 1: //Preemption
				console.log(1, "Preemption");
				getHnbList("preemption");
				break;
			case 2: //Reconfiguration
				console.log(2, "Reconfiguration");
				getHnbList("reconfiguration");
				break;
			case 3: //Cell Update
				console.log(3, "Cell Update");
				getHnbList("cellupdate");
				break;
			case 4: //Error
				console.log(4, "Error");
				getHnbList("error");
				break;
			case 5: //RTT
				console.log(5, "RTT");
				getHnbList("rtt");
				break;
		}
		$("#mid_content").ScrollTo();
	});
}

function clean()
{
	$("#mid_content").html("");
	$("#detail1").html("");
	$("#detail2").html("");
	$("#detail3").html("");
	$("#detail4").html("");
	$("#detail1_h2").html("");
	$("#detail2_h2").html("");
	$("#detail3_h2").html("");
	$("#detail4_h2").html("");
}

function cleanGraph()
{
	$("#detail1").html("");
	$("#detail2").html("");
	$("#detail3").html("");
	$("#detail4").html("");
	$("#detail1_h2").html("");
	$("#detail2_h2").html("");
	$("#detail3_h2").html("");
	$("#detail4_h2").html("");
}

function getHnbList(type)
{
	var path = getBaseURL()+getProjectName()+'/index.php/log/getHnbList';
	jQuery.ajax({
		url: path,
		type: 'POST',
		async: false,
		data: { type: type },
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError, error) {
			console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
		},
		success: function(xhr, XMLHttpRequest, textStatus) {
			switch(type)
			{
				case "rtt":
					rtt(xhr);
					break;
				case "error":
					error(xhr);
					break;
				case "cellupdate":
					cellupdate(xhr);
					break;
				case "reconfiguration":
					reconfiguration(xhr);
					break;
				case "preemption":
					preemption(xhr);
					break;
				case "ho":
					ho(xhr);
					break;
			}
		}
	});
}

function dateTimeBox()
{
	$('input[type!="button"][type!="submit"], select, textarea').val('').blur();

	/** restrict within 7 days **/
	var startDateTextBox = $("#datepickerStart");
	var endDateTextBox = $("#datepickerEnd");
	startDateTextBox.datetimepicker({
		showTimepicker: false,
		dateFormat : "yy-mm-dd",
		maxDate: '0',
		onClose: function(dateText, inst) {
			var today = $.datepicker.formatDate('yy-mm-dd', new Date() );
			var startDate = startDateTextBox.datetimepicker('getDate');
			var testEndDate = $.datepicker.formatDate('yy-mm-dd', dateAdd(startDate, 'day', 6) );

			if(testEndDate > today) testEndDate = today;
			endDateTextBox.val( testEndDate );

			if(startDate == null) $.datepicker._clearDate('#datepickerEnd');
		}
	});
	endDateTextBox.datetimepicker({
		showTimepicker: false,
		dateFormat : "yy-mm-dd",
		maxDate: '0',
		onClose: function(dateText, inst) {
			var startDate = $("#datepickerStart").datetimepicker('getDate');
			var endDate = endDateTextBox.datetimepicker('getDate');
			var testStartDate = $.datepicker.formatDate('yy-mm-dd', dateAdd(endDate, 'day', -6) );

			if($.datepicker.formatDate('yy-mm-dd', startDate ) < testStartDate)
				startDateTextBox.val( testStartDate );
			if($.datepicker.formatDate('yy-mm-dd', endDate ) < $.datepicker.formatDate('yy-mm-dd', startDate) )
				startDateTextBox.val( testStartDate );

			if(endDate == null) $.datepicker._clearDate('#datepickerStart');
		}
	});
}

function isValidDate(s) { // Expect input as y/m/d
	var bits = s.split('-');
	var d = new Date(parseInt(bits[0]), parseInt(bits[1]) - 1, parseInt(bits[2]));
	return d && (d.getMonth() + 1) == parseInt(bits[1]) && d.getDate() == Number(parseInt(bits[2]));
}

function rtt(data)
{
	var temp = "";
	var str = "<table class='list_table' id='list_table'><tr><td colspan='2'>HNB</td><td colspan='1'><select id='hnbSelect'><option value=''></option>";
	for(var i = 0; i < data.length; i++)
	{
		temp += "<option value='"+data[i]+"'>"+data[i]+"</option>";
	}
	str = str + temp + "</select></td></tr><tr><td colspan='2'>Start Date</td><td colspan='1'><input type='text' id='datepickerStart'></td></tr><tr><td colspan='2'>End Date</td><td colspan='1'><input type='text' id='datepickerEnd'></td></tr><tr><td>RRC</td><td>RRC CON REQ → RRC CON COMPLETE</td><td><input type='text' id='rrc' title='OR condition'></td></tr><tr><td></td><td>RRC Failed</td><td><input type='checkbox' id='rrcfail' title='OR condition'></td></tr><tr><td>SMS</td><td>(MO) cp data → (CN) cp ack</td><td><input type='text' id='smsmo' title='OR condition'></tr><tr><td></td><td>(CN) cp ack → (CN)cp-data-ack</td><td><input type='text' id='smscn1' title='OR condition'></td></tr><tr><td></td><td>(CN) cp-data-ack → (MO) cp-ack</td><td><input type='text' id='smscn2' title='OR condition'></td></tr><tr><td></td><td>(CN) cp-data → (MT) cp-ack</td><td><input type='text' id='smscn3' title='OR condition'></td></tr><tr><td></td><td>(MT) cp-ack → (MT) cp-data-ack</td><td><input type='text' id='smsmt1' title='OR condition'></td></tr><tr><td></td><td>(MT) cp-data-ack → (CN) cp-ack</td><td><input type='text' id='smsmt2' title='OR condition'></td></tr><tr><td></td><td>SMS Failed</td><td><input type='checkbox' id='smsfail' title='OR condition'></td></tr><tr><td>CS</td><td>Cm service request → Radio bearer setup complete</td><td><input type='text' id='cscm' title='OR condition'></td></tr><tr><td></td><td>Radio Bearer setup complete → Alerting</td><td><input type='text' id='csradio' title='OR condition'></td></tr><tr><td></td><td>Alerting → Connect Ack</td><td><input type='text' id='csalert' title='OR condition'></td></tr><tr><td></td><td>CS Failed</td><td><input type='checkbox' id='csfail' title='OR condition'></td></tr><tr><td>PS</td><td>Service request → Radio bearer setup complete</td><td><input type='text' id='psser' title='OR condition'></td></tr><tr><td></td><td>PS Failed</td><td><input type='checkbox' id='psfail' title='OR condition'></td></tr></table><table style='width: 50%;margin: 0px auto;'><tr><td><button type='button' style='float:right' class='juibtn' id='SubmitBtn'>Submit</button></td></tr></table>";
	$("#mid_content").html(str);
	$("#SubmitBtn").button();
	dateTimeBox();
	td_highlight();
	RTTsubmitAction();
}

function error(data)
{
	var temp = "";
	var str = "<table class='list_table' id='list_table'><tr><td>HNB</td><td><select id='hnbSelect'><option value=''></option>";
	for(var i = 0; i < data.length; i++)
	{
		temp += "<option value='"+data[i]+"'>"+data[i]+"</option>";
	}
	str = str + temp + "</select></td></tr><tr><td>Start Date</td><td><input type='text' id='datepickerStart'></td></tr><tr><td>End Date</td><td><input type='text' id='datepickerEnd'></td></tr><tr><td>Radio link failure Indication</td><td><input type='checkbox' id='radio' title='OR condition'></td></tr><tr><td>Iu release request</td><td><input type='checkbox' id='iurelease' title='OR condition'></td></tr><tr><td>Existed Process</td><td><input type='checkbox' id='existed' title='OR condition'></td></tr><tr><td>Channelization code error</td><td><input type='checkbox' id='channel' title='OR condition'></td></tr><tr><td>DIRAM UL error</td><td><input type='checkbox' id='ulerror' title='OR condition'></td></tr><tr><td>DIRAM DL error</td><td><input type='checkbox' id='dlerror' title='OR condition'></td></tr><tr><td>Demulator error</td><td><input type='checkbox' id='demulator' title='OR condition'></td></tr><tr><td>RL setup request failed</td><td><input type='checkbox' id='rlsetup' title='OR condition'></td></tr><tr><td>RL activation Failed</td><td><input type='checkbox' id='rlact' title='OR condition'></td></tr><tr><td>RL reconfig prepare request failed</td><td><input type='checkbox' id='rlrepre' title='OR condition'></td></tr><tr><td>RL reconfig commit failed</td><td><input type='checkbox' id='rlrecom' title='OR condition'></td></tr><tr><td>RL delete failed</td><td><input type='checkbox' id='rldelete' title='OR condition'></td></tr><tr><td>Compress mode failed</td><td><input type='checkbox' id='compress' title='OR condition'></td></tr><tr><td>Reconfig cancel request failed</td><td><input type='checkbox' id='reconfig' title='OR condition'></td></tr></table><table style='width: 50%;margin: 0px auto;'><tr><td><button type='button' style='float:right' class='juibtn' id='SubmitBtn' title='OR condition'>Submit</button></td></tr></table>";
	$("#mid_content").html(str);
	$("#SubmitBtn").button();
	dateTimeBox();
	td_highlight();
	ErrorSubmitAction();
}

function cellupdate(data)
{
	var temp = "";
	var str = "<table class='list_table' id='list_table'><tr><td>HNB</td><td><select id='hnbSelect'><option value=''></option>";
	for(var i = 0; i < data.length; i++)
	{
		temp += "<option value='"+data[i]+"'>"+data[i]+"</option>";
	}
	str = str + temp + "</select></td></tr><tr><td>Start Date</td><td><input type='text' id='datepickerStart'></td></tr><tr><td>End Date</td><td><input type='text' id='datepickerEnd'></td></tr><tr><td>RLC unrecoverable error</td><td><input type='checkbox' id='rlcerror' title='OR condition'></td></tr><tr><td>Radio link failure</td><td><input type='checkbox' id='radio' title='OR condition'></td></tr><tr><td>RLC unrecoverable error detected by FAP</td><td><input type='checkbox' id='rlcbyfap' title='OR condition'></td></tr></table><table style='width: 50%;margin: 0px auto;'><tr><td><button type='button' style='float:right' class='juibtn' id='SubmitBtn'>Submit</button></td></tr></table>";
	$("#mid_content").html(str);
	$("#SubmitBtn").button();
	dateTimeBox();
	td_highlight();
	CellupdateSubmitAction();
}

function reconfiguration(data)
{
	var temp = "";
	var path = getBaseURL()+getProjectName()+'/index.php/log/getReconfigurationData/';
	var str = "<table class='list_table' id='list_table'><tr><td>HNB</td><td><select id='hnbSelect' name='hnbSelect'><option value=''></option>";
	for(var i = 0; i < data.length; i++)
	{
		temp += "<option value='"+data[i]+"'>"+data[i]+"</option>";
	}
	str = str + temp + "</select></td></tr><tr><td>Start Date</td><td><input type='text' id='datepickerStart' name='datepickerStart'></td></tr><tr><td>End Date</td><td><input type='text' id='datepickerEnd' name='datepickerEnd'></td></tr><tr><td>Reconfiguration of DCH</td><td><input type='text' id='recondch' name='recondch' placeholder='integer equal or greater than 0' title='greater than & AND condition'></td></tr><tr><td>Reconfiguration of FACH</td><td><input type='text' id='reconfach' name='reconfach' placeholder='integer equal or greater than 0' title='greater than & AND condition'></td></tr><tr><td>Reconfiguration of PCH</td><td><input type='text' id='reconpch' name='reconpch' placeholder='integer equal or greater than 0' title='greater than & AND condition'></td></tr><tr><td>Rate Adaption</td><td><input type='text' id='rate' name='rate' placeholder='integer equal or greater than 0' title='greater than & AND condition'></td></tr></table><table style='width: 50%;margin: 0px auto;'><tr><td><button type='button' style='float:right' class='juibtn' id='SubmitBtn'>Submit</button></td></tr></table>"; //<input type='submit' style='float:right' id='SubmitBtn' class='juibtn' value='Submit'>

	$("#mid_content").html(str);
	$("#SubmitBtn").button();
	dateTimeBox();
	td_highlight();
	ReconSubmitAction();
}

function ho(data)
{
	hoevents = data["hoevents"];
	data = data["data"];

	var temp = "";
	var path = getBaseURL()+getProjectName()+'/index.php/log/getReconfigurationData/';

	var str = "<table class='list_table' id='list_table'><tr><td>HNB</td><td><select id='hnbSelect' name='hnbSelect'><option value=''></option>";
	for(var i = 0; i < data.length; i++)
	{
		temp += "<option value='"+data[i]+"'>"+data[i]+"</option>";
	}
	str = str + temp + "</select></td></tr><tr><td>Start Date</td><td><input type='text' id='datepickerStart' name='datepickerStart'></td></tr><tr><td>End Date</td><td><input type='text' id='datepickerEnd' name='datepickerEnd'></td></tr><tr><td>HO Events</td><td><select id='hoevents' multiple='multiple'>";
	temp = "";
	for(var k in hoevents)
	{
		temp += "<option value='"+k+"'>"+hoevents[k]+"</option>";
	}
	str = str + temp + "</select></td></tr><tr><td>HO Domain</td><td><select id='hodomain'  multiple='multiple'><option value='CS'>CS</option><option value='PS'>PS</option></select></td></tr><tr><td>HO Type</td><td><select id='hotype' multiple='multiple'><option value='3G'>3G</option><option value='2G'>2G</option><option value='interHNB'>interHNB</option></select></td></tr></table><table style='width: 50%;margin: 0px auto;'><tr><td><button type='button' style='float:right' class='juibtn' id='SubmitBtn'>Submit</button></td></tr></table>";
	$("#mid_content").html(str);
	$("#SubmitBtn").button();

	dateTimeBox();
	td_highlight();
	HoSubmitAction();
}

function validatePositiveInteger(num, display)
{
	var result;
	if(num != "") result = isPositiveInteger(num);
	if(result == false)	return "Value of "+display+" is invalid.<br>";
	else return "";
}

function preemption(data)
{
	var temp = "";
	var str = "<table class='list_table' id='list_table'><tr><td>HNB</td><td><select id='hnbSelect'><option value=''></option>";
	for(var i = 0; i < data.length; i++)
	{
		temp += "<option value='"+data[i]+"'>"+data[i]+"</option>";
	}
	str = str + temp + "</select></td></tr><tr><td>Start Date</td><td><input type='text' id='datepickerStart'></td></tr><tr><td>End Date</td><td><input type='text' id='datepickerEnd'></td></tr><tr><td>preempt Back</td><td><input type='checkbox' id='back' title='OR condition'></td></tr><tr><td>preempter</td><td><input type='checkbox' id='preempter' title='OR condition'></tr><tr><td>preempt to IDLE</td><td><input type='checkbox' id='toidle' title='OR condition'></td></tr></table><table style='width: 50%;margin: 0px auto;'><tr><td><button type='button' style='float:right' class='juibtn' id='SubmitBtn'>Submit</button></td></tr></table>";
	$("#mid_content").html(str);
	$("#SubmitBtn").button();
	dateTimeBox();
	td_highlight();
	PreemptionSubmitAction();
}

function RTTsubmitAction()
{
	$("#SubmitBtn").click( function(){
		var hnbid = $("#hnbSelect").val();
		var startDate = $("#datepickerStart").val();
		var endDate = $("#datepickerEnd").val();

		if(startDate !== "" && endDate !== "" && isValidDate(startDate) && isValidDate(endDate))
		{
			var rrc = $("#rrc").val();
			var rrcfail = $("#rrcfail").is(':checked');
			var smsmo = $("#smsmo").val();
			var smscn1 = $("#smscn1").val();
			var smscn2 = $("#smscn2").val();
			var smscn3 = $("#smscn3").val();
			var smsmt1 = $("#smsmt1").val();
			var smsmt2 = $("#smsmt2").val();
			var smsfail = $("#smsfail").is(':checked');
			var cscm = $("#cscm").val();
			var csradio = $("#csradio").val();
			var csalert = $("#csalert").val();
			var csfail = $("#csfail").is(':checked');
			var psser = $("#psser").val();
			var psfail = $("#psfail").is(':checked');
			var path = getBaseURL()+getProjectName()+'/index.php/log/getRTTdata';

			$("#SubmitBtn").attr('disabled','disabled');
			jQuery.ajax({
				url: path,
				type: 'POST',
				data: { hnbid: hnbid, startDate: startDate, endDate: endDate, rrc: rrc, rrcfail: rrcfail, smsmo: smsmo, smscn1: smscn1, smscn2: smscn2, smscn3: smscn3, smsmt1: smsmt1, smsmt2: smsmt2, smsfail: smsfail, cscm: cscm, csradio: csradio, csalert: csalert, csfail: csfail, psser: psser, psfail: psfail },
				async: false,
				dataType: 'JSON',
				beforeSend: function (xhr, settings) {
					cleanGraph();
					if(!rrc&!smsmo&!smscn1&!smscn2&!smscn3&!smsmt1&!smsmt2&!cscm&!csradio&!csalert&!psser&!rrcfail&!smsfail&!csfail&!psfail)
					{
						$().tostie({type:"error", message: "Use one condition at least please.", toastDuration:6000,});
						$("#SubmitBtn").removeAttr('disabled');
						return false;
					}
					else
						$("#detail1").html("wait please...");
				},
				error: function(xhr, ajaxOptions, thrownError, error) {
					$("#SubmitBtn").removeAttr('disabled');
					console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
				},
				success: function(xhr, XMLHttpRequest, textStatus) {
					$("#SubmitBtn").removeAttr('disabled');
					console.log(xhr);
					if(Object.size(xhr["data"]) > 0)
						showRttDetail1(xhr, startDate, endDate);
					else
						$("#detail1").html("NO DATA");
				}
			});
		}
		else
		{
			$().tostie({type:"error", message: "Invalid Start Date or End Date.", toastDuration:5000,});
		}
	});
}

function ErrorSubmitAction()
{
	$("#SubmitBtn").click( function(){
		cleanGraph();
		var hnbid = $("#hnbSelect").val();
		var startDate = $("#datepickerStart").val();
		var endDate = $("#datepickerEnd").val();

		if(startDate !== "" && endDate !== "" && isValidDate(startDate) && isValidDate(endDate))
		{
			var radio = $("#radio").is(':checked');
			var iurelease = $("#iurelease").is(':checked');
			var existed = $("#existed").is(':checked');
			var channel = $("#channel").is(':checked');
			var ulerror = $("#ulerror").is(':checked');
			var dlerror = $("#dlerror").is(':checked');
			var demulator = $("#demulator").is(':checked');
			var rlsetup = $("#rlsetup").is(':checked');
			var rlact = $("#rlact").is(':checked');
			var rlrepre = $("#rlrepre").is(':checked');
			var rlrecom = $("#rlrecom").is(':checked');
			var rldelete = $("#rldelete").is(':checked');
			var compress = $("#compress").is(':checked');
			var reconfig = $("#reconfig").is(':checked');

			if(radio || iurelease || existed || channel || ulerror || dlerror || demulator || rlsetup || rlact || rlrepre || rlrecom || rldelete || compress || reconfig)
			{
				cleanGraph();
				var path = getBaseURL()+getProjectName()+'/index.php/log/getERRORdata';
				$("#detail1").html("wait please...");
				$("#SubmitBtn").attr('disabled','disabled');
				jQuery.ajax({
					url: path,
					type: 'POST',
					data: { hnbid: hnbid, startDate: startDate, endDate: endDate, radio: radio, iurelease: iurelease, existed: existed, channel: channel, ulerror: ulerror, dlerror: dlerror, demulator: demulator, rlsetup: rlsetup, rlact: rlact, rlrepre: rlrepre, rlrecom: rlrecom, rldelete: rldelete, compress: compress, reconfig: reconfig },
					async: false,
					dataType: 'JSON',
					error: function(xhr, ajaxOptions, thrownError, error) {
						$("#SubmitBtn").removeAttr('disabled');
						console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
					},
					success: function(xhr, XMLHttpRequest, textStatus) {
						$("#SubmitBtn").removeAttr('disabled');
						if(Object.size(xhr["data"]) > 0)
							showERRORdetail1(xhr, startDate, endDate, "error");
						else
							$("#detail1").html("NO DATA");
					}
				});
			}
			else
				$().tostie({type:"error", message: "Select one condition at least please.", toastDuration:5000,});
		}
		else
			$().tostie({type:"error", message: "Invalid Start Date or End Date.", toastDuration:5000,});
	});
}

function CellupdateSubmitAction()
{
	$("#SubmitBtn").click( function(){
		var hnbid = $("#hnbSelect").val();
		var startDate = $("#datepickerStart").val();
		var endDate = $("#datepickerEnd").val();

		if(startDate !== "" && endDate !== "" && isValidDate(startDate) && isValidDate(endDate))
		{
			var rlcerror = $("#rlcerror").is(':checked');
			var radio = $("#radio").is(':checked');
			var rlcbyfap = $("#rlcbyfap").is(':checked');

			$("#detail1").html("wait please...");
			var path = getBaseURL()+getProjectName()+'/index.php/log/getCellUpdateData';

			$("#SubmitBtn").attr('disabled','disabled');
			jQuery.ajax({
				url: path,
				type: 'POST',
				data: { hnbid: hnbid, startDate: startDate, endDate: endDate, rlcerror: rlcerror, radio: radio, rlcbyfap: rlcbyfap },
				async: false,
				dataType: 'JSON',
				beforeSend: function (xhr, settings) {
					cleanGraph();
					if(!rlcerror & !radio & !rlcbyfap)
					{
						$().tostie({type:"error", message: "Select one condition at least please.", toastDuration:6000,});
						$("#SubmitBtn").removeAttr('disabled');
						return false;
					}
					else
						$("#detail1").html("wait please...");
				},
				error: function(xhr, ajaxOptions, thrownError, error) {
					$("#SubmitBtn").removeAttr('disabled');
					console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
				},
				success: function(xhr, XMLHttpRequest, textStatus) {
					$("#SubmitBtn").removeAttr('disabled');
					if(Object.size(xhr["data"]) > 0)
						showERRORdetail1(xhr, startDate, endDate, "cellupdate");
					else
						$("#detail1").html("NO DATA");
				}
			});
		}
		else
		{
			$().tostie({type:"error", message: "Invalid Start Date or End Date.", toastDuration:5000,});
		}
	});
}

function ReconSubmitAction()
{
	$("#SubmitBtn").click( function(){
		var hnbid = $("#hnbSelect").val();
		var startDate = $("#datepickerStart").val();
		var endDate = $("#datepickerEnd").val();

		if(startDate !== "" && endDate !== "" && isValidDate(startDate) && isValidDate(endDate))
		{
			var msg = "";
			var recondch = $("#recondch").val();
			var reconfach = $("#reconfach").val();
			var reconpch = $("#reconpch").val();
			var rate = $("#rate").val();
			msg += validatePositiveInteger(recondch, "DCH");
			msg += validatePositiveInteger(reconfach, "FACH");
			msg += validatePositiveInteger(reconpch, "PCH");
			msg += validatePositiveInteger(rate, "Rate Adaption");
	
			var path = getBaseURL()+getProjectName()+'/index.php/log/getReconfigurationData';
			$("#SubmitBtn").attr('disabled','disabled');
			jQuery.ajax({
				url: path,
				type: 'POST',
				data: { hnbid: hnbid, startDate: startDate, endDate: endDate, recondch: recondch, reconfach: reconfach, reconpch: reconpch, rate: rate },
				async: false,
				dataType: 'JSON',
				beforeSend: function (xhr, settings) {
					cleanGraph();
					if(msg != "")
					{
						$().tostie({type:"error", message: msg, toastDuration:6000,});
						$("#SubmitBtn").removeAttr('disabled');
						return false;
					}
					else
						$("#detail1").html("wait please...");
				},
				error: function(xhr, ajaxOptions, thrownError, error) {
					$("#SubmitBtn").removeAttr('disabled');
					console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
				},
				success: function(xhr, XMLHttpRequest, textStatus) {
					$("#SubmitBtn").removeAttr('disabled');

					if(Object.size(xhr["data"]) > 0)
						showReconfigurationDetail1(xhr);
					else
						$("#detail1").html("NO DATA");
				}
			});
		}
		else
		{
			$().tostie({type:"error", message: "Invalid Start Date or End Date.", toastDuration:5000,});
		}
	});
}

function PreemptionSubmitAction()
{
	$("#SubmitBtn").click( function(){
		var hnbid = $("#hnbSelect").val();
		var startDate = $("#datepickerStart").val();
		var endDate = $("#datepickerEnd").val();

		if(startDate !== "" && endDate !== "" && isValidDate(startDate) && isValidDate(endDate))
		{
			var back = $("#back").is(':checked');
			var preempter = $("#preempter").is(':checked');
			var toidle = $("#toidle").is(':checked');

			$("#detail1").html("wait please...");
			var path = getBaseURL()+getProjectName()+'/index.php/log/getPreemptionData';

			$("#SubmitBtn").attr('disabled','disabled');
			jQuery.ajax({
				url: path,
				type: 'POST',
				data: { hnbid: hnbid, startDate: startDate, endDate: endDate, back: back, preempter: preempter, toidle: toidle },
				async: false,
				dataType: 'JSON',
				beforeSend: function (xhr, settings) {
					cleanGraph();
					if(!back & !preempter & !toidle)
					{
						$().tostie({type:"error", message: "Select one condition at least please.", toastDuration:6000,});
						$("#SubmitBtn").removeAttr('disabled');
						return false;
					}
					else
						$("#detail1").html("wait please...");
				},
				error: function(xhr, ajaxOptions, thrownError, error) {
					$("#SubmitBtn").removeAttr('disabled');
					console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
				},
				success: function(xhr, XMLHttpRequest, textStatus) {
					$("#SubmitBtn").removeAttr('disabled');
					if(Object.size(xhr["data"]) > 0)
						showERRORdetail1(xhr, startDate, endDate, "preempt");
					else
						$("#detail1").html("NO DATA");
				}
			});
		}
		else
		{
			$().tostie({type:"error", message: "Invalid Start Date or End Date.", toastDuration:5000,});
		}
	});
}

function HoSubmitAction()
{
	$("#SubmitBtn").click( function(){
		var hnbid = $("#hnbSelect").val();
		var startDate = $("#datepickerStart").val();
		var endDate = $("#datepickerEnd").val();

		if(startDate !== "" && endDate !== "" && isValidDate(startDate) && isValidDate(endDate))
		{
			var hoevents = $("#hoevents option:selected").map(function(){ return this.value }).get();
			var hodomain = $("#hodomain option:selected").map(function(){ return this.value }).get();
			var hotype = $("#hotype option:selected").map(function(){ return this.value }).get();

			var path = getBaseURL()+getProjectName()+'/index.php/log/getHoData';

			$("#SubmitBtn").attr('disabled','disabled');
			jQuery.ajax({
				url: path,
				type: 'POST',
				data: { hnbid: hnbid, startDate: startDate, endDate: endDate, hoevents: hoevents, hodomain: hodomain, hotype: hotype },
				async: false,
				dataType: 'JSON',
				beforeSend: function (xhr, settings) {
					cleanGraph();
					if(hoevents.length+hodomain.length+hotype.length == 0)
					{
						$().tostie({type:"error", message: "Select one condition at least please.", toastDuration:6000,});
						$("#SubmitBtn").removeAttr('disabled');
						return false;
					}
					else
						$("#detail1").html("wait please...");
				},
				error: function(xhr, ajaxOptions, thrownError, error) {
					$("#SubmitBtn").removeAttr('disabled');
					console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
				},
				success: function(xhr, XMLHttpRequest, textStatus) {
					$("#SubmitBtn").removeAttr('disabled');
					if(Object.size(xhr["data"]) > 0)
						showHoDetail1(xhr, startDate, endDate);
					else
						$("#detail1").html("NO DATA");
				}
			});
		}
		else
		{
			$().tostie({type:"error", message: "Invalid Start Date or End Date.", toastDuration:5000,});
		}
	});
}

function errorLabelFormatter (label, series) {
		return "<div style='font-size:10pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + series.data[0][1] + " (" + Math.round(series.percent) + "%)</div>";
}

function setPieOption()
{
	var pieoption = {
		series: {
			pie: { 
				show: true,
				radius: 1,
				label: {
					show: true,
					radius: 3/4,
					formatter: errorLabelFormatter,
					background: { opacity: 0.5, color: '#000' }
				}
			}
		},
		legend: { show: false },
		grid: { hoverable: true, clickable: true }
	};
	return pieoption;
}

function showRttDetail1(result, startDate, endDate)
{
	var hnb = result["hnb"];
	var data = result["data"];
	var limit = result["limit"];
	var error = result["error"];

	var hnbstr = "";
	if(hnb != "") hnbstr = "HNB "+hnb+" <br>";
	$("#detail1_h2").html(hnbstr+"RTT Call Summary during "+startDate+" ~ "+endDate);

	var placeholder = $("#detail1");
	placeholder.addClass("placeholder");

	var pieoption = setPieOption();
	var piedata = [];
	var i = 0;
	for(var k in data)
	{
		var v = data[k];
		piedata[i] = { label: k, data: v["num"] }
		i++;
	}

	$.plot(placeholder, piedata, pieoption);
	placeholder.ScrollTo();

	placeholder.unbind( "plotclick" );
	placeholder.bind("plotclick", function (event, pos, item) {
		$("#detail2").html("wait please...");
		$("#detail2_h2").html("");
		$("#detail3").html("");
		showRttDetail2(data, hnb, startDate, endDate, item['series']['label'], limit, error);
	});
}

function showRttDetail2(data, hnb, startDate, endDate, callid, limit, error)
{
	var hnbstr = "";
	if(hnb != "") hnbstr = "HNB "+hnb+" <br>";
	$("#detail2_h2").html(hnbstr+callid+" RTT Call Summary during "+startDate+" ~ "+endDate);

	var placeholder = $("#detail2");
	placeholder.addClass("placeholder");

	var pieoption = setPieOption();

	var temp = data[callid];
	delete temp["num"];
	var piedata = [];
	var i = 0;
	for(var k in temp)
	{
		piedata[i] = { label: k, data: temp[k] }
		i++;
	}

	$.plot(placeholder, piedata, pieoption);
	placeholder.ScrollTo();

	placeholder.unbind( "plotclick" );
	placeholder.bind("plotclick", function (event, pos, item) {
		$("#detail3").html("wait please...");
		getRttDetail3(callid, hnb, startDate, endDate, item['series']['label'], limit, error);
	});
}

function getRttDetail3(callid, hnb, startDate, endDate, type, limit, error)
{
	var path = getBaseURL()+getProjectName()+'/index.php/log/getRttDetail3';
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { hnbid: hnb, startDate: startDate, endDate: endDate, callid: callid, limit: limit, error: error, type, type },
		async: false,
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError, error) {
			console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
		},
		success: function(xhr, XMLHttpRequest, textStatus) {
			console.log(Object.size(xhr));
			if(Object.size(xhr) > 0)
				showRttDetail3(xhr);
			else
				$("#detail3").html("NO DATA");
		}
	});
}

function showRttDetail3(data)
{
	var str = "<table class='list_table'><tr><th>Log Time</th><th>Call ID</th></tr>";
	for(k in data)
	{
		str += "<tr><td>"+data[k]["logtime"]+"</td><td>"+data[k]["callid"]+"</td></tr>";
	}
	str += "</table>";
	$("#detail3").html(str);
	$("#detail3").ScrollTo();
}

function showERRORdetail1(result, startDate, endDate, type)
{
	var hnb = result["hnb"];
	var data = result["data"];

	var hnbstr = "";
	if(hnb != "") hnbstr = "HNB "+hnb+" <br>";

	var graphType = "";
	switch(type)
	{
		case "error":
			graphType = "Error";
			break;
		case "cellupdate":
			graphType = "Cell Update";
			break;
		case "preempt":
			graphType = "Preemption";
			break;
	}
	$("#detail1_h2").html(hnbstr+graphType+" Call Summary during "+startDate+" ~ "+endDate);

	var placeholder = $("#detail1");
	placeholder.addClass("placeholder");

	var pieoption = setPieOption();
	var piedata = [];
	var i = 0;
	for(var k in data)
	{
		var v = data[k];
		piedata[i] = { label: k, data: v["num"] }
		i++;
	}

	$.plot(placeholder, piedata, pieoption);
	placeholder.ScrollTo();

	placeholder.unbind( "plotclick" );
	placeholder.bind("plotclick", function (event, pos, item) {
		$("#detail2").html("wait please...");
		$("#detail2_h2").html("");
		$("#detail3").html("");
		showERRORdetail2(data, hnb, startDate, endDate, item['series']['label'], type);
	});
}

function showReconfigurationDetail1(result)
{
	var hnb = result["hnb"];
	var data = result["data"];
	var startDate = result["startDate"];
	var endDate = result["endDate"];

	var hnbstr = "";
	if(hnb != "") hnbstr = "HNB "+hnb+" <br>";
	$("#detail1_h2").html(hnbstr+"Reconfiguration Call Summary during "+startDate+" ~ "+endDate);

	var placeholder = $("#detail1");
	placeholder.addClass("placeholder");

	var pieoption = setPieOption();

	var piedata = [];
	var i = 0;
	for(var k in data)
	{
		var v = data[k];
		piedata[i] = { label: k, data: v["num"] }
		i++;
	}

	$.plot(placeholder, piedata, pieoption);
	placeholder.ScrollTo();

	placeholder.unbind( "plotclick" );
	placeholder.bind("plotclick", function (event, pos, item) {
		$("#detail2").html("wait please...");
		$("#detail2_h2").html("");
		$("#detail3").html("");
		getReconfigurationDetail2(hnb, startDate, endDate, result["limit"], item['series']['label']);
	});
}

function showHoDetail1(result, startDate, endDate)
{
	var hnb = result["hnb"];
	var data = result["data"];
	var events = result["hoevents"];

	var hnbstr = "";
	if(hnb != "") hnbstr = "HNB "+hnb+" <br>";
	$("#detail1_h2").html(hnbstr+"HO Call Summary during "+startDate+" ~ "+endDate);

	var placeholder = $("#detail1");
	placeholder.addClass("placeholder");

	var pieoption = setPieOption();
	var piedata = [];
	var i = 0;
	for(var k in data)
	{
		var v = data[k];
		piedata[i] = { label: k, data: v["num"] }
		i++;
	}

	$.plot(placeholder, piedata, pieoption);
	placeholder.ScrollTo();

	placeholder.unbind( "plotclick" );
	placeholder.bind("plotclick", function (event, pos, item) {
		$("#detail2").html("wait please...");
		$("#detail2_h2").html("");
		$("#detail3").html("");
		showHoDetail2(data, hnb, startDate, endDate, item['series']['label'], events);
	});
}

function showERRORdetail2(data, hnb, startDate, endDate, callid, type)
{
	var hnbstr = "";
	if(hnb != "") hnbstr = "HNB "+hnb+" <br>";
	var graphType = "";
	switch(type)
	{
		case "error":
			graphType = "Error";
			break;
		case "cellupdate":
			graphType = "Cell Update";
			break;
		case "preempt":
			graphType = "Preemption";
			break;
	}
	$("#detail2_h2").html(hnbstr+callid+" "+graphType+" Call Summary during "+startDate+" ~ "+endDate);

	var placeholder = $("#detail2");
	placeholder.addClass("placeholder");

	var pieoption = setPieOption();

	var temp = data[callid];
	delete temp["num"];
	var piedata = [];
	var i = 0;
	for(var k in temp)
	{
		piedata[i] = { label: k, data: temp[k] }
		i++;
	}

	$.plot(placeholder, piedata, pieoption);
	placeholder.ScrollTo();

	placeholder.unbind( "plotclick" );
	placeholder.bind("plotclick", function (event, pos, item) {
		$("#detail3").html("wait please...");
		getERRORdetail3(callid, hnb, startDate, endDate, item['series']['label'], type);
	});
}

function getERRORdetail3(callid, hnb, startDate, endDate, limit, type)
{
	switch(type)
	{
		case "error":
			func = "getERRORdetail3";
			break;
		case "cellupdate":
			func = "getCellUpdatedetail3";
			break;
		case "preempt":
			func = "getPremptionDetail3";
			break;
	}
	var path = getBaseURL()+getProjectName()+'/index.php/log/'+func;
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { hnbid: hnb, startDate: startDate, endDate: endDate, callid: callid, limit: limit },
		async: false,
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError, error) {
			console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
		},
		success: function(xhr, XMLHttpRequest, textStatus) {
			showERRORdetail3(xhr);
		}
	});
}

function showERRORdetail3(data)
{
	var str = "<table class='list_table'><tr><th>Log Time</th><th>Call ID</th></tr>";
	for(k in data)
	{
		str += "<tr><td>"+data[k]["logtime"]+"</td><td>"+data[k]["callid"]+"</td></tr>";
	}
	str += "</table>";
	$("#detail3").html(str);
	$("#detail3").ScrollTo();
}

function getReconfigurationDetail2(hnb, startDate, endDate, limit, callid)
{
	var path = getBaseURL()+getProjectName()+'/index.php/log/getReconfigurationDetail2';
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { hnb: hnb, startDate: startDate, endDate: endDate, limit: limit, callid: callid },
		async: false,
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError, error) {
			console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
		},
		success: function(xhr, XMLHttpRequest, textStatus) {
			showReconfigurationdetail2(xhr, hnb, startDate, endDate, callid);
		}
	});
}

function showReconfigurationdetail2(data, hnb, startDate, endDate, callid)
{
	var hnbstr = "";
	if(hnb != "") hnbstr = "HNB "+hnb+" <br>";
	$("#detail2_h2").html(hnbstr+callid+" Reconfiguration Call Summary during "+startDate+" ~ "+endDate);

	var placeholder = $("#detail2");
	placeholder.addClass("placeholder");

	var pieoption = {
		series: {
			pie: { 
				show: true,
				radius: 1,
				label: {
					show: true,
					radius: 3/4,
					formatter: errorLabelFormatter,
					background: { opacity: 0.5, color: '#000' }
				}
			}
		},
		legend: { show: false },
		grid: { hoverable: true, clickable: false }
	};

	// attribute distribution data
	var temp = data[callid];
	var piedata = [];
	var i = 0;
	for(var k in temp)
	{
		piedata[i] = { label: k, data: temp[k] }
		i++;
	}

	placeholder.unbind( "plotclick" );
	$.plot(placeholder, piedata, pieoption);
	placeholder.ScrollTo();

	// callid logtime data
	var str = "<table class='list_table'><tr><th>Log Time</th><th>Call ID</th></tr><tr><td>"+data["logtime"]+"</td><td>"+callid+"</td></tr></table>";
	$("#detail3").html(str);
}

function showHoDetail2(data, hnb, startDate, endDate, callid, events)
{
	var hnbstr = "";
	if(hnb != "") hnbstr = "HNB "+hnb+" <br>";
	$("#detail2_h2").html(hnbstr+callid+" HO Call Summary during "+startDate+" ~ "+endDate);

	var placeholder = $("#detail2");
	placeholder.addClass("placeholder");

	var pieoption = setPieOption();

	var temp = data[callid];
	console.log(temp);
	delete temp["num"];
	var piedata = [];
	var i = 0;
	for(var k in temp)
	{
		piedata[i] = { label: k, data: temp[k]["num"] }
		i++;
	}

	$.plot(placeholder, piedata, pieoption);
	placeholder.ScrollTo();

	placeholder.unbind( "plotclick" );
	placeholder.bind("plotclick", function (event, pos, item) {
		$("#detail3").html("wait please...");
		showHoDetail3(data, hnb, startDate, endDate, callid, item['series']['label'], events);
	});
}

function showHoDetail3(data, hnb, startDate, endDate, callid, domain, events)
{
	var hnbstr = "";
	if(hnb != "") hnbstr = "HNB "+hnb+" <br>";
	$("#detail3_h2").html(hnbstr+callid+" "+domain+" HO Call Summary during "+startDate+" ~ "+endDate);

	var placeholder = $("#detail3");
	placeholder.addClass("placeholder");

	var pieoption = setPieOption();

	var temp = data[callid][domain];
	delete temp["num"];
	var piedata = [];
	var i = 0;
	for(var k in temp)
	{
		piedata[i] = { label: k, data: temp[k] }
		i++;
	}

	$.plot(placeholder, piedata, pieoption);
	placeholder.ScrollTo();

	placeholder.unbind( "plotclick" );
	placeholder.bind("plotclick", function (event, pos, item) {
		$("#detail4").html("wait please...");
		getHoDetail4(hnb, callid, startDate, endDate, events, domain, item['series']['label']);
	});
}

function getHoDetail4(hnb, callid, startDate, endDate, events, domain, type)
{
	var path = getBaseURL()+getProjectName()+'/index.php/log/getHoDetail4';
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { hnb: hnb, startDate: startDate, endDate: endDate, callid: callid, events: events, domain: domain, type: type },
		async: false,
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError, error) {
			console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
		},
		success: function(xhr, XMLHttpRequest, textStatus) {
			showHoDetail4(xhr, hnb, startDate, endDate, callid);
		}
	});
}

function showHoDetail4(data)
{
	var str = "<table class='list_table'><tr><th>Log Time</th><th>HNB</th><th>Call ID</th><th>HO Events</th><th>HO Domain</th><th>HO Type</th></tr>";
	for(k in data)
	{
		str += "<tr><td>"+data[k]["logtime"]+"</td><td>"+data[k]["hnb"]+"</td><td>"+data[k]["callid"]+"</td><td>"+data[k]["hoevents"]+"</td><td>"+data[k]["hodomain"]+"</td><td>"+data[k]["hotype"]+"</td></tr>";
	}
	str += "</table>";
	$("#detail4").html(str);
	$("#detail4").ScrollTo();
}

function isPositiveInteger(s)
{
    return !!s.match(/^[0-9]+$/);
    // or Rob W suggests
    return /^\d+$/.test(s);
}

