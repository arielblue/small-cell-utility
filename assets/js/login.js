
function initial()
{
	// detect if cookie is enabled or not
	if (!navigator.cookieEnabled)
	{
		$(".juibtn").button().click( function (event) {
			return false;
		});
		$("#bottom_msg").html("Enable cookies please or you can't login");
	}
	else
	{
		$(".juibtn").button().click( function (event) {
			$("#bottom_msg").html("");
			login_rule("loginform");
		});

		$("#register").click( function (event) {
			document.title = "Register Page";
			$("#bottom_msg").html("");
			var regpath = getBaseURL()+getProjectName()+'/index.php/login/register';
			var loginpath = getBaseURL()+getProjectName();
			var buf = '<form id="registerform" name="registerform" action="'+regpath+'" method="POST"><table id="list_table"><tr><th>Register Form</th></tr><tr><td><input type="text" id="username" name="username" placeholder="Your Username"></td></tr><tr><td><input type="password" id="password" name="password" placeholder="Your Password"></td></tr><tr><td><input type="password" id="passconf" name="passconf" placeholder="Confirm Password"></td></tr><tr><td><input type="email" id="email" name="email" placeholder="Your Email"></td></tr><tr><td><input type="submit" class="juibtn" value="Sign Up"></td></tr><tr><td></td></tr><tr><td style="text-align: right;"><a href="'+loginpath+'">Log in</a></td></tr></table></form>';
			$("#form").html(buf);

			$(".juibtn").button().click( function (event) {
				register_rule("registerform",$("#username").val(),$("#password").val(),$("#email").val());
			});
		});
	}
}

function login_rule(formname)
{
	var errorString = '';
	var validator = new FormValidator(formname, 
		[
			{
				name: 'username',
				display: 'Username',
				rules: 'required|min_length[3]|max_length[30]'
			},
			{
				name: 'password',
				display: 'Password',
				rules: 'required|min_length[3]'
			}
		], 
		function(errors,event)
		{
			errorString = '';
			if (errors.length > 0)
			{
				for (var i = 0, errorLength = errors.length; i < errorLength; i++)
				{
					errorString += errors[i].message + '<br />';
				}
				$("#bottom_msg").html(errorString);
				return false;
			}
		}
	);
	$("#"+formname).ajaxForm({
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		complete: function(xhr, XMLHttpRequest, textStatus) {
			if(xhr.responseText == true)
				window.location = getBaseURL()+getProjectName()+"/index.php/kpi";
			else
				$("#bottom_msg").html(xhr.responseText);
		},
		clearForm: false
	});
}

function register_rule(formname,username,password,email)
{
	var errorString = '';
	var validator = new FormValidator(formname,
		[
			{
				name: 'username',
				display: 'Username',
				rules: 'required|min_length[3]|max_length[30]'
			},
			{
				name: 'password',
				display: 'Password',
				rules: 'required|min_length[3]'
			},
			{
				name: 'passconf',
				display: 'Password Confirm',
				rules: 'required|matches[password]',
			},
			{
				name: 'email',
				display: 'Email',
				rules: 'required|valid_email',
			}
		],
		function(errors,event)
		{
			errorString = '';
			if (errors.length > 0)
			{
				for (var i = 0, errorLength = errors.length; i < errorLength; i++)
				{
					errorString += errors[i].message + '<br />';
				}

				$("#bottom_msg").html(errorString).css("color","red");
				return false;
			}
		}
	);
	$("#"+formname).ajaxForm({

		error: function(xhr, ajaxOptions, thrownError) {
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, XMLHttpRequest, textStatus) {
			if(xhr != "success")
				$("#bottom_msg").html(xhr).css("color","red");
			else
			{
				$("#bottom_msg").html("Success<br>Login to main page...");
				signin(username, password);
			}
		},
		clearForm: false
	});
}

function signin(username, password)
{
	var path = getBaseURL()+getProjectName()+'/index.php/login/signin';
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { username: username, password: password },
		dataType: 'text',
		error: function(xhr, ajaxOptions, thrownError) {
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, XMLHttpRequest, textStatus) {
			console.log(xhr, XMLHttpRequest, textStatus);
			if(xhr == "1")	// TRUE
				window.location = getBaseURL()+getProjectName()+"/index.php/kpi";
			else
			{
				$("#bottom_msg").html(xhr);
				console.log(xhr, XMLHttpRequest, textStatus);
			}
		}
	});
}

function getProjectName()
{
	var strFullPath=window.document.location.href;
	var strPath=window.document.location.pathname;
	var pos=strFullPath.indexOf(strPath);
	var prePath=strFullPath.substring(0,pos);
	var postPath=strPath.substring(0,strPath.substr(1).indexOf('/')+1);
	return(postPath.substring(1));
}

function getRootPath()
{
	var strFullPath=window.document.location.href;

	return(strFullPath);
}

function getBaseURL() {
   return location.protocol + "//" + location.hostname + (location.port && ":" + location.port) + "/";
}