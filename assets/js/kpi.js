function init()
{
	setDateBox();
	$("#SubmitBtn").button();
	kpiSelect();
}

function getHnbMAClist()
{
	var path = getBaseURL()+getProjectName()+"/index.php/report/get_hnbMAC_list";

	jQuery.ajax({
		url: path,
		dataType: 'json',
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError); 
		},
		success: function(data){
			show_hnbMac_list(data);
		}
	});
}

function show_hnbMac_list(data)
{
	var buf = "<option value=''></option>";
	for(index in data)
	{
		buf += "<option value='"+data[index]+"'>"+data[index]+"</option>";
	}
	$("#hnbMAC").append(buf);
}

function getGraph4Kpi()
{
	var graph4Kpi = "";
	switch($('#kpiSelect option:selected').val())
	{
		case "CS_RAB_SR_Distribution":
			graph4Kpi = "CS_RAB_SR_DayHourly";
			break;
		case "PS_RAB_SR_Distribution":
			graph4Kpi = "PS_RAB_SR_DayHourly";
			break;
		case "RRC_Connection_SR_Distribution":
			graph4Kpi = "RRC_Connection_SR_DayHourly";
			break;
		case "Paging_SR_Distribution":
			graph4Kpi = "Paging_SR_DayHourly";
			break;
		case "CS_Call_Drop_Rate_Distribution":
			graph4Kpi = "CS_Call_Drop_Rate_DayHourly";
			break;
		case "PS_Call_Drop_Rate_Distribution":
			graph4Kpi = "PS_Call_Drop_Rate_DayHourly";
			break;
		case "Overall_Call_Drop_Rate_Distribution":
			graph4Kpi = "Overall_Call_Drop_Rate_DayHourly";
			break;
		case "IPSec_Tunnel_Est_Distribution":
			graph4Kpi = "IPSec_Tunnel_Est_DayHourly";
			break;
		case "HNB_Registration_Attempt_Distribution":
			graph4Kpi = "HNB_Registration_Attempt_DayHourly";
			break;
		case "CS_Attach_Success_Rate_Distribution":
			graph4Kpi = "CS_Attach_Success_Rate_DayHourly";
			break;
		case "PS_Attach_Success_Rate_Distribution":
			graph4Kpi = "PS_Attach_Success_Rate_DayHourly";
			break;
		case "SCTP_Abort_Distribution":
			graph4Kpi = "SCTP_Abort_DayHourly";
			break;
		case "Cell_Availability_Distribution":
			graph4Kpi = "Cell_Availability_DayHourly";
			break;
	}

	return graph4Kpi;
}

function kpiSelect()
{
	$("#SubmitBtn").click( function(){

		$("#midMsg").html("");
		$("#graph1").html("");
		$("#summaryTargetBar").html("");
		$("#targetErrMsg").html("");
		$("#graph2").html("");
		$("#graph3").html("");
		$("#graph4").html("");
		globalHnb = "";
		globalDate = "";

		var kpiName = $('#kpiSelect option:selected').val();
		var startTime = $("#datepicker_start").val();
		var endTime = $("#datepicker_end").val();
		var version = msieversion();
		var path = getBaseURL()+getProjectName()+'/index.php/kpi/get_kpi_data';
		var url = "http://"+getHost()+":8080/KPIcalculator/calculatorAction";
		console.log(kpiName, startTime, endTime, "browser V:",version, path, url);

		if(startTime != "" && endTime != "" && kpiName != "")
		{
			$("#midMsg").html("<font color='black'>wait please...</font>");
			$("#SubmitBtn").attr('disabled','disabled');
			jQuery.ajax({
				url: path,
				type: 'POST',
				data: { kpi: kpiName, startTime: startTime+"_00:00:00", endTime: endTime+"_23:59:59", version: version, url: url },
				dataType: 'JSON',
				error: function(xhr, ajaxOptions, thrownError) {
					$("#SubmitBtn").removeAttr('disabled');
					console.log("ERROR!", xhr, ajaxOptions, thrownError, xhr.status);
				},
				success: function(xhr, XMLHttpRequest, jsonStatus) {
					$("#SubmitBtn").removeAttr('disabled');
					$("#midMsg").html("");
					prepare_graph1_placeholder(xhr["Target"]);
					drawGraph1(xhr);
					calculateGraph2(xhr);
				}
			});
		}
		else
		{
			if(kpiName == "" || kpiName == null)
				$("#midMsg").append("<font color='red'>Select a Graph.</font><br>");

			if(startTime == "" || endTime == "")
				$("#midMsg").append("<font color='red'>Start time or End time should not be empty.</font><br>");
		}
	});
}

function msieversion()
{
	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");

	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))
	{
		// If it's Internet Explorer, return version number
		var version = parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
		return version;
	}
	else
	{
		// If it's another browser, return -100
		return -100;
	}
}

function defineLabel()
{
	var label = "";
	var kpi = $('#kpiSelect option:selected').val();
	switch(kpi)
	{
		case "CS_RAB_SR_Distribution":
		case "CS_Call_Drop_Rate_Distribution":
		case "CS_Attach_Success_Rate_Distribution":
			label = "CS";
			break;

		case "PS_RAB_SR_Distribution":
		case "PS_Call_Drop_Rate_Distribution":
		case "PS_Attach_Success_Rate_Distribution":
			label = "PS";
			break;

		case "Overall_Call_Drop_Rate_Distribution":
			label = "Overall";
			break;

		case "IPSec_Tunnel_Est_Distribution":
			label = "IPSec Tunnel Est";
			break;

		case "HNB_Registration_Attempt_Distribution":
			label = "HNB Registration";
			break;

		case "SCTP_Abort_Distribution":
			label = "SCTP Abort";
			break;
	}
	return label;
}

//// Plot Graph ///////////////////////////////////////////////////////

function prepare_graph1_placeholder(target)
{
	var buf = "<div class='gap'></div><div class='_title' id='distribution_title'></div><div class='_placeholder' id='distribution_placeholder'></div><div class='gap'></div>";
	$("#graph1").html(buf);

	var buf = "Target: <input type='text' id='targetInput' style='width:30px;' title='range:0~101' value='"+target+"'>  <button type='button' class='juibtn' id='GetSummary'>Go</button>";
	$("#summaryTargetBar").html(buf);
}

function percent(v, axis) {
	return v.toFixed(axis.tickDecimals) + "%";
}

function drawGraph1(data)
{
	console.log("data:",data);
	$("#GetSummary").button().click( function(){
		calculateGraph2(data);	// refresh graph2 when click 'go'
	});

	switch($('#kpiSelect option:selected').val())
	{
		case "CS_RAB_SR_Distribution":
		case "PS_RAB_SR_Distribution":
		case "RRC_Connection_SR_Distribution":
		case "Paging_SR_Distribution":
		case "CS_Call_Drop_Rate_Distribution":
		case "PS_Call_Drop_Rate_Distribution":
		case "Overall_Call_Drop_Rate_Distribution":
		case "IPSec_Tunnel_Est_Distribution":
		case "HNB_Registration_Attempt_Distribution":
		case "CS_Attach_Success_Rate_Distribution":
		case "PS_Attach_Success_Rate_Distribution":
		case "SCTP_Abort_Distribution":
		case "Cell_Availability_Distribution":
			CS_RAB_SR_Distribution(data);
			break;
	}
}

function CS_RAB_SR_Distribution(data)
{
	var label = defineLabel();
	var xtick = [], pdfAttr = [], xlabel = "", disTitle = "";
	var kpi = $('#kpiSelect option:selected').val();
	switch(kpi)
	{
		case "CS_RAB_SR_Distribution":
		case "PS_RAB_SR_Distribution":
			xtick = [[0,'0-10%'], [1,'10-20%'], [2,'20-30%'], [3,'30-40%'], [4,'40-50%'], [5,'50-60%'], [6,'60-70%'], [7,'70-80%'], [8,'80-90%'], [9,'90-95%'], [10,'95-96%'], [11,'96-97%'], [12,'97-98%'], [13,'98-99%'], [14,'99-100%']];
			pdfAttr = ['0-10%', '10-20%', '20-30%', '30-40%', '40-50%', '50-60%', '60-70%', '70-80%', '80-90%', '90-95%', '95-96%', '96-97%', '97-98%', '98-99%', '99-100%'];
			disTitle = label+" RAB Successful Rate Distribution";
			xlabel = label+" RAB SR Range";
			break;

		case "RRC_Connection_SR_Distribution":
			xtick = [[0,'0-60%'], [1,'60-70%'], [2,'70-80%'], [3,'80-85%'], [4,'85-90%'], [5,'90-95%'], [6,'95-96%'], [7,'96-97%'], [8,'97-98%'], [9,'98-99%'], [10,'99-100%']];
			pdfAttr = ['0-60%', '60-70%', '70-80%', '80-85%', '85-90%', '90-95%', '95-96%', '96-97%', '97-98%', '98-99%', '99-100%'];
			disTitle = "RRC Connection Successful Rate Distribution";
			xlabel = "RRC Connection SR Range";
			break;

		case "Paging_SR_Distribution":
			xtick = [[0,'0-10%'], [1,'10-20%'], [2,'20-30%'], [3,'30-40%'], [4,'40-50%'], [5,'50-60%'], [6,'60-70%'], [7,'70-80%'], [8,'80-90%'], [9,'90-100%']];
			pdfAttr = ['0-10%', '10-20%', '20-30%', '30-40%', '40-50%', '50-60%', '60-70%', '70-80%', '80-90%', '90-100%'];
			disTitle = "Paging Successful Rate Distribution";
			xlabel = "Paging SR Range";
			break;

		case "CS_Call_Drop_Rate_Distribution":
		case "PS_Call_Drop_Rate_Distribution":
		case "Overall_Call_Drop_Rate_Distribution":
			xtick = [[0,"0%"],[1,"0-1%"],[2,"1-2%"],[3,"2-3%"],[4,"3-4%"],[5,"4-5%"],[6,"5-10%"],[7,"10-20%"],[8,"20-30%"],[9,"30-40%"],[10,"40-50%"],[11,"50-60%"],[12,"60-70%"],[13,"70-80%"],[14,"80-90%"],[15,"90-100%"]];
			pdfAttr = ["0%","0-1%","1-2%","2-3%","3-4%","4-5%","5-10%","10-20%","20-30%","30-40%","40-50%","50-60%","60-70%","70-80%","80-90%","90-100%"];
			disTitle = label+" Call Drop Successful Rate Distribution";
			xlabel = label+" Call Drop SR Range";
			break;

		case "IPSec_Tunnel_Est_Distribution":
			xtick = [[0,'0'], [1,'1-3'], [2,'4-5'], [3,'6-10'], [4,'11-20'], [5,'21-30'], [6,'31-40'], [7,'>41']];
			pdfAttr = ['0', '1-3', '4-5', '6-10', '11-20', '21-30', '31-40', '>41'];
			disTitle = label+" Distribution";
			xlabel = label+" SR Range";
			break;

		case "HNB_Registration_Attempt_Distribution":
			xtick = [[0,'0'], [1,'1-3'], [2,'4-5'], [3,'6-10'], [4,'11-20'], [5,'21-30'], [6,'31-40'], [7,'41-50'], [8,'>50']];
			pdfAttr = ['0', '1-3', '4-5', '6-10', '11-20', '21-30', '31-40', '41-50', '>50'];
			disTitle = label+" Distribution";
			xlabel = label+" SR Range";
			break;

		case "CS_Attach_Success_Rate_Distribution":
		case "PS_Attach_Success_Rate_Distribution":
			xtick = [[0,'0%'], [1,'0-10%'], [2,'10-20%'], [3,'20-30%'], [4,'30-40%'], [5,'40-50%'], [6,'50-60%'], [7,'60-70%'], [8,'70-80%'], [9,'80-85%'], [10,'85-90%'], [11,'90-95%'], [12,'95-100%']];
			pdfAttr = ['0%', '0-10%', '10-20%', '20-30%', '30-40%', '40-50%', '50-60%', '60-70%', '70-80%', '80-85%', '85-90%', '90-95%', '90-100%'];
			disTitle = label+" Attach Successful Rate Distribution";
			xlabel = label+" Attach SR Range";
			break;

		case "SCTP_Abort_Distribution":
			xtick = [[0,'0-3'], [1,'4-6'], [2,'7-10'], [3,'11-15'], [4,'16-20'], [5,'21-30'], [6,'>30']];
			pdfAttr = ['0-3', '4-6', '7-10', '11-15', '16-20', '21-30', '>30'];
			disTitle = label+" Distribution";
			xlabel = label+" SR Range";
			break;

		case "Cell_Availability_Distribution":
			xtick = [[0,'0%'], [1,'0-10%'], [2,'10-20%'], [3,'20-30%'], [4,'30-40%'], [5,'40-50%'], [6,'50-60%'], [7,'60-70%'], [8,'70-80%'], [9,'80-90%'], [10,'90-95%'], [11,'95-96%'], [12,'96-97%'], [13,'97-98%'], [14,'98-99%'], [15,'99-100%']];
			pdfAttr = ['0%', '0-10%', '10-20%', '20-30%', '30-40%', '40-50%', '50-60%', '60-70%', '70-80%', '80-90%', '90-95%', '95-96%', '96-97%', '97-98%', '98-99%', '99-100%'];
			disTitle = "Cell Availability Successful Rate Distribution";
			xlabel = "Cell Availability SR Range";
			break;
	}
	$("#distribution_title").text(disTitle);

	var set1={}, set2={};
	set1['label']	= 'PDF';
	set1['data']	= data['pdf'];
	set1['bars'] 	= { show: true, align: "center", barWidth: 0.5, }
	set1['yaxis']	= 2;
	set1['color']	= 1;

	set2['label']	= 'CDF';
	set2['data']	= data['cdf'];
	set2['lines']	= { show: true, fill: false };
	set2['points']	= { show: true, radius: 3 };
	set2['yaxis']	= 1;
	set2['color']	= 0;

	var options = {

		grid:	{ hoverable: true, clickable: false },
		xaxis: { mode: "categories", ticks: xtick, axisLabel: xlabel },
		yaxes: [
					{ position: "left", axisLabelPadding: 1, max: 100, min: 0, tickSize: 10, tickFormatter: percent, axisLabel: "Distribution"  },
					{ position: "right", axisLabelPadding: 1, tickDecimals: 0, axisLabel: "Count" },

				],
		legend:	{ backgroundOpacity: 0.4, position: 'ne' }
	};

	$.plot("#distribution_placeholder", [set1,set2], options);
	$(window).resize(function() {$.plot("#distribution_placeholder", [set1,set2], options);});

	// Tooltip
	$("#distribution_placeholder").bind("plothover", function (event, pos, item) {
		if(item)
		{
			$("#tooltip").remove();
			var x = item.datapoint[0].toFixed(0);

			switch(item.series.label)
			{
				case "PDF":
					y = item.datapoint[1].toFixed(0) + " (PDF)";
					break;

				case "CDF":
					y = item.datapoint[1].toFixed(1) + "% (CDF)";
					break;
			}
			showTooltip(item.pageX, item.pageY, pdfAttr[parseInt(x)]+': '+y);
		}
		else
		{
			$("#tooltip").remove();
		}
	});
}

function calculateGraph2(data)
{
	var kpi = $('#kpiSelect option:selected').val();
	var targetInput = $("#targetInput").val();
	var url = getBaseURL()+getProjectName()+'/index.php/kpi/calculateGraph2';
	if(isNumber(targetInput) && !isNaN(targetInput))
	{
		if(!(targetInput < 0) && !(targetInput > 101) )
		{
			$("#targetErrMsg").html("");
			$("#GetSummary").attr('disabled','disabled');
			jQuery.ajax({
				url: url,
				type: 'POST',
				data: { kpi: kpi, target: targetInput, summary: data['Summary']},
				dataType: 'JSON',
				error: function(xhr, ajaxOptions, thrownError) {
					$("#GetSummary").removeAttr('disabled');
					console.log("ERROR!", xhr, ajaxOptions, thrownError, xhr.status);
				},
				success: function(xhr, XMLHttpRequest, jsonStatus) {
					$("#GetSummary").removeAttr('disabled');
					prepare_graph2_placeholder();
					drawGraph2(xhr, data);

					switch($('#kpiSelect option:selected').val())
					{
						case "CS_RAB_SR_Distribution":
						case "PS_RAB_SR_Distribution":
						case "RRC_Connection_SR_Distribution":
						case "Paging_SR_Distribution":
						case "CS_Call_Drop_Rate_Distribution":
						case "PS_Call_Drop_Rate_Distribution":
						case "Overall_Call_Drop_Rate_Distribution":
						case "CS_Attach_Success_Rate_Distribution":
						case "PS_Attach_Success_Rate_Distribution":
						case "Cell_Availability_Distribution":
							// draw graph3 or not
							if(globalHnb != null && globalHnb != "")
							{
								var hnblist = xhr['hnb'];
								console.log("already selected a hnb", xhr, hnblist.indexOf(globalHnb));
								if(hnblist.indexOf(globalHnb) > 0)
								{
									calculateGraph3(data, hnblist, globalHnb);

									// draw graph4 or not
									var date_in_hnb = get_graph3Date_in_hnb(data["Summary"][globalHnb]);
									console.log(date_in_hnb, date_in_hnb.indexOf(globalDate));
									if(globalDate != null && globalDate != "")
									{
										if(date_in_hnb.indexOf(globalDate) >= 0)
											getGraph4Data(globalHnb, globalDate);
										else
											$("#graph4").html("");
									}
								}
								else
								{
									$("#graph3").html("");
									$("#graph4").html("");
								}
							}
							break;
					}
				}
			});
		}
		else
		{
			$("#targetErrMsg").html("<font color='red'>Input valid value between 0~101 please</font>");
		}
	}
	else
	{
		$("#targetErrMsg").html("<font color='red'>Invalid</font>");
	}
}

function prepare_graph2_placeholder()
{
	$("#graph2").html("<div class='gap'></div><div class='_title' id='Summary_title'></div><div class='_placeholder' id='summary_placeholder'></div>");
}

function drawGraph2(data, origin)
{
	switch($('#kpiSelect option:selected').val())
	{
		case "CS_RAB_SR_Distribution":
		case "PS_RAB_SR_Distribution":
		case "RRC_Connection_SR_Distribution":
		case "Paging_SR_Distribution":
		case "CS_Call_Drop_Rate_Distribution":
		case "PS_Call_Drop_Rate_Distribution":
		case "Overall_Call_Drop_Rate_Distribution":
		case "CS_Attach_Success_Rate_Distribution":
		case "PS_Attach_Success_Rate_Distribution":
		case "Cell_Availability_Distribution":
			CS_RAB_SR_Summary(data, origin);
			break;

		case "IPSec_Tunnel_Est_Distribution":
		case "HNB_Registration_Attempt_Distribution":
		case "SCTP_Abort_Distribution":
			IPSec_Tunnel_Est_Summary(data, origin);
			break;
	}
}

var globalHnb = "";

function get_graph3Date_in_hnb(summary)
{
	var date_in_hnb = [];
	for(var i = 0; i < summary.length; i++)
		date_in_hnb.push(summary[i]["Date"]);
	return date_in_hnb;
}

var rotateAngle = 135;
function CS_RAB_SR_Summary(data, origin)
{
	console.log('data:',data, 'origin:',origin);
	var target = $("#targetInput").val();
	var lower = "";
	switch($('#kpiSelect option:selected').val())
	{
		case "CS_RAB_SR_Distribution":
		case "PS_RAB_SR_Distribution":
		case "RRC_Connection_SR_Distribution":
		case "Paging_SR_Distribution":
		case "CS_Attach_Success_Rate_Distribution":
		case "PS_Attach_Success_Rate_Distribution":
		case "Cell_Availability_Distribution":
			lower = "<";
			break;
		case "CS_Call_Drop_Rate_Distribution":
		case "PS_Call_Drop_Rate_Distribution":
		case "Overall_Call_Drop_Rate_Distribution":
			lower = ">";
			break;
	}
	$("#Summary_title").text("Average and Standard Deviation for HNBs (SR"+lower+target+"%)");

	var hnb = [];
	var data1 = data['average_SR'];
	var data2 = data['count'];
	var data3 = data['average_SR_Std'];
	for(var i = 0; i < data1.length; i++)
	{
		data1[i][0] = i;
		data2[i][0] = i;
		data3[i][0] = i;
		hnb.push([i, data['hnb'][i]]);
	}

	var set1={}, set2={}, set3={};
	set1['label']	= 'Average SR';
	set1['data']	= data1;
	set1['bars'] 	= { show: true, align: "center", barWidth: 0.4, }
	set1['yaxis']	= 1;
	set1['type']	= 'average_SR';
	set1['color']	= 1;

	set2['label']	= 'Count';
	set2['data']	= data2;
	set2['lines']	= { show: false, fill: false };
	set2['points']	= { show: true, radius: 3 };
	set2['yaxis']	= 2;
	set2['type']	= 'count';
	set2['color']	= 0;

	set3['label']	= 'SR Standard Deviation';
	set3['data']	= data3;
	set3['lines']	= { show: false };
	set3['points']	= { show: true, radius: 2, errorbars: "y",  yerr: { show: true, upperCap: "-", lowerCap: "-", radius: 5 } };
	set3['yaxis']	= 1;
	set3['type']	= 'average_SR_Std';
	set3['color']	= 2;

	var ylabel = "";
	switch($('#kpiSelect option:selected').val())
	{
		case "CS_RAB_SR_Distribution":
		case "PS_RAB_SR_Distribution":
			ylabel = defineLabel()+" RAB Successful Rate";
			break;

		case "RRC_Connection_SR_Distribution":
			ylabel = "RRC Connection Successful Rate";
			break;

		case "Paging_SR_Distribution":
			ylabel = "Paging Successful Rate";
			break;

		case "CS_Call_Drop_Rate_Distribution":
		case "PS_Call_Drop_Rate_Distribution":
		case "Overall_Call_Drop_Rate_Distribution":
			ylabel = defineLabel()+" Call Drop Successful Rate";
			break;

		case "CS_Attach_Success_Rate_Distribution":
		case "PS_Attach_Success_Rate_Distribution":
			ylabel = defineLabel()+" Attach Successful Rate";
			break;

		case "Cell_Availability_Distribution":
			ylabel = "Average Availability";
			set1['label'] = "Average Availability";
			set3['label'] = "Availability Standard Deviation";
			set3['type'] = "avg_availability_Std";
		 	break;
	}

	var options = {
		grid:	{ hoverable: true, clickable: true },
		xaxis: { mode: "categories", ticks: hnb, axisLabel: "HNB", rotateTicks: rotateAngle },
		yaxes: [
					{ position: "left", axisLabelPadding: 2, max: 101, min: 0, tickFormatter: percent, axisLabel: ylabel },
					{ position: "right", tickDecimals: 0, axisLabel: "Count" },
				],
		legend:	{ backgroundOpacity: 0.4, position: 'ne' }
	};
	$.plot("#summary_placeholder", [set1, set2, set3], options);
	$(window).resize(function() {$.plot("#summary_placeholder", [set1, set2, set3], options);});

	//Tooltip
	$("#summary_placeholder").bind("plothover", function (event, pos, item) {
		if(item)
		{
			$("#tooltip").remove();
			var x = item.datapoint[0].toFixed(0),
				y = item.datapoint[1].toFixed(0);
			switch(item.series.type)
			{
				case "average_SR":
					y = item.datapoint[1].toFixed(1) + "%";
					break;

				case "average_SR_Std":
					y = "SR Standard Deviation "+item.datapoint[2].toFixed(2);
					break;

				case "avg_availability_Std":
					y = "Availability Standard Deviation "+item.datapoint[2].toFixed(2);
					break;
			}
			showTooltip(item.pageX, item.pageY, hnb[parseInt(x)][1]+': '+y);
		}
		else
		{
			$("#tooltip").remove();
		}
	});

	$("#summary_placeholder").bind("plotclick", function (event, pos, item) {
		if (item.series.type == "average_SR") {
			// draw graph3
			globalHnb = hnb[parseInt(item.dataIndex)][1];
			calculateGraph3(origin, data['hnb'], globalHnb);

			// draw graph4 or not
			var date_in_hnb = get_graph3Date_in_hnb(origin["Summary"][globalHnb]);
			console.log(date_in_hnb, date_in_hnb.indexOf(globalDate));
			if(globalDate != null && globalDate != "")
			{
				if(date_in_hnb.indexOf(globalDate) >= 0)
					getGraph4Data(globalHnb, globalDate);
				else
					$("#graph4").html("");
			}
		}
	});
}

function IPSec_Tunnel_Est_Summary(data, origin)
{
	console.log('data:',data, 'origin:',origin);
	var target = $("#targetInput").val();
	var lower = ">";
	var stitle = "";

	switch($('#kpiSelect option:selected').val())
	{
		case "IPSec_Tunnel_Est_Distribution":
			stitle = defineLabel();
			break;

		case "HNB_Registration_Attempt_Distribution":
			stitle = "HNB Reg.";
			break;

		case "SCTP_Abort_Distribution":
			stitle = defineLabel();
			break;
	}
	$("#Summary_title").text(stitle+" Avg. and SD for HNBs (Attmpt"+lower+target+")");

	var hnb = [];
	var data1 = data['avg_Att'];
	var data2 = data['count'];
	var data3 = data['avg_Att_Std'];
	for(var i = 0; i < data1.length; i++)
	{
		data1[i][0] = i;
		data2[i][0] = i;
		data3[i][0] = i;
		hnb.push([i, data['hnb'][i]]);
	}

	var set1={}, set2={}, set3={};
	set1['label']	= 'Average Count';
	set1['data']	= data1;
	set1['bars'] 	= { show: true, align: "center", barWidth: 0.4, }
	set1['yaxis']	= 1;
	set1['type']	= 'avg_Att';
	set1['color']	= 1;

	set2['label']	= 'Count > '+target;
	set2['data']	= data2;
	set2['lines']	= { show: false, fill: false };
	set2['points']	= { show: true, radius: 3 };
	set2['yaxis']	= 2;
	set2['type']	= 'count';
	set2['color']	= 0;

	set3['label']	= 'Count SD';
	set3['data']	= data3;
	set3['lines']	= { show: false };
	set3['points']	= { show: true, radius: 2, errorbars: "y",  yerr: { show: true, upperCap: "-", lowerCap: "-", radius: 5 } };
	set3['yaxis']	= 1;
	set3['type']	= 'avg_Att_Std';
	set3['color']	= 2;

	var options = {
		grid:	{ hoverable: true, clickable: true },
		xaxis: { mode: "categories", ticks: hnb, axisLabel: "HNB", rotateTicks: rotateAngle },
		yaxes: [
					{ position: "left", axisLabelPadding: 2, min: 0, axisLabel: set1['label'] },
					{ position: "right", tickDecimals: 0, axisLabel: set2['label'] },
				],
		legend:	{ backgroundOpacity: 0.4, position: 'ne' }
	};
	$.plot("#summary_placeholder", [set1, set2, set3], options);
	$(window).resize(function() {$.plot("#summary_placeholder", [set1, set2, set3], options);});

	//Tooltip
	$("#summary_placeholder").bind("plothover", function (event, pos, item) {
		if(item)
		{
			$("#tooltip").remove();
			var x = item.datapoint[0].toFixed(0),
				y = item.datapoint[1].toFixed(2);
			switch(item.series.type)
			{
				case "avg_Att":
					y = "Avg Count "+y;
					break;
				case "count":
					y = "'Count > "+target+"' -> "+item.datapoint[1].toFixed(0);
					break;
				case "avg_Att_Std":
					y = "Count SD "+y;
					break;
			}
			showTooltip(item.pageX, item.pageY, hnb[parseInt(x)][1]+': '+y);
		}
		else
		{
			$("#tooltip").remove();
		}
	});

	$("#summary_placeholder").bind("plotclick", function (event, pos, item) {
		if (item.series.type == "avg_Att") {
			// draw graph3
			globalHnb = hnb[parseInt(item.dataIndex)][1];
			calculateGraph3(origin, data['hnb'], globalHnb);

			// draw graph4 or not
			var date_in_hnb = get_graph3Date_in_hnb(origin["Summary"][globalHnb]);
			console.log(date_in_hnb, date_in_hnb.indexOf(globalDate));
			if(globalDate != null && globalDate != "")
			{
				if(date_in_hnb.indexOf(globalDate) >= 0)
					getGraph4Data(globalHnb, globalDate);
				else
					$("#graph4").html("");
			}
		}
	});
}

function calculateGraph3(data, hnblist, selectHnb)
{
	console.log("data:",data,"hnblist:",hnblist, selectHnb);
	var kpi = $('#kpiSelect option:selected').val();
	var target = $("#targetInput").val();
	var summary = data['Summary'];
	var url = getBaseURL()+getProjectName()+'/index.php/kpi/calculateGraph3';
	jQuery.ajax({
		url: url,
		type: 'POST',
		data: { hnb: selectHnb, target: target, avg_SR: data["Average_SR"], summary: summary[selectHnb], kpi: kpi },
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError) {
			console.log("ERROR!", xhr, ajaxOptions, thrownError, xhr.status);
		},
		success: function(xhr, XMLHttpRequest, jsonStatus) {
			prepare_graph3_placeholder();
			drawGraph3(xhr, selectHnb, kpi);
		}
	});
}

var popupWin = {
	popup: null,
	newPopup: function(url, wname, w, h, data, hnb, titleval, divt, divph)
	{
		var js = ["jquery-1.11.1.min.js", "jquery-ui.min.js", "jquery.flot.min.js", "jquery.flot.categories.min.js", "jquery.flot.axislabels.js", "jquery.flot.dashes.js", "jquery.flot.stack.min.js"];
		var baseurl = getBaseURL()+getProjectName()+"/assets/js/";
		var html = "";
		for(var i = 0; i < js.length; i++)
			html += '<script type="text/javascript" src="'+baseurl+js[i]+'"></script>';
		html += '<link rel="stylesheet" href="'+getBaseURL()+getProjectName()+'/assets/css/jquery-ui.min.css" type="text/css" media="screen"/><link rel="stylesheet" href="'+getBaseURL()+getProjectName()+'/assets/css/kpi.css" type="text/css" media="screen"/>';
		this.popup = window.open(url, wname, "width=" + w + ",height=" + h);
		this.popup.document.write("<html><head><title></title>"+html+"</head><body><div class='gap' style='width:"+w+"px;height:10px;text-align:center;'></div><div id='"+divt+"' style='width:"+w+"px;height:20px;line-height:20px;text-align:center;background-color:#D6D6C2;'>"+titleval+"</div><div id='"+divph+"' style='width:"+w+"px;height:"+h+"px;background-color:white;'></div><button type='button' id='btn'>click</button></body><script type='text/javascript'>$(function() { $('#btn').button().click(function(){alert('test');});});</script></html>");
		this.popup.document.close();
		var self = this;
	},

	daily: function(data, hnb, titleval, divt, divph)
	{
		this.newPopup( "", "", 550, 370, data, hnb, titleval, "daily_title", "daily_placeholder");
	}
}

function prepare_graph3_placeholder()
{
	$("#graph3").html("<div class='gap'></div><div class='_title' id='daily_title'></div><div class='_placeholder' id='daily_placeholder'></div>");
}

function drawGraph3(data, hnb, kpi)
{
	switch($('#kpiSelect option:selected').val())
	{
		case "CS_RAB_SR_Distribution":
		case "PS_RAB_SR_Distribution":
			popout_hnb_CS_RAB_daily(data, hnb);
			break;

		case "RRC_Connection_SR_Distribution":
		case "CS_Call_Drop_Rate_Distribution":
		case "PS_Call_Drop_Rate_Distribution":
		case "Overall_Call_Drop_Rate_Distribution":
		case "CS_Attach_Success_Rate_Distribution":
		case "PS_Attach_Success_Rate_Distribution":
			popout_hnb_RRC_Connection_daily(data, hnb, kpi);
			break;

		case "Paging_SR_Distribution":
			popout_hnb_Paging_daily(data, hnb);
			break;

		case "IPSec_Tunnel_Est_Distribution":
		case "HNB_Registration_Attempt_Distribution":
		case "SCTP_Abort_Distribution":
			popout_hnb_IPSec_Tunnel_Est_daily(data, hnb);
			break;

		case "Cell_Availability_Distribution":
			popout_hnb_Cell_Availability_daily(data, hnb);
			break;
	}
}

var globalDate = "";

function popout_hnb_CS_RAB_daily(data, hnb)
{
	console.log(data, hnb);
	var label = "";
	var kpi = $('#kpiSelect option:selected').val();
	switch(kpi)
	{
		case "CS_RAB_SR_Distribution":
			label = "CS";
			break;

		case "PS_RAB_SR_Distribution":
			label = "PS";
			break;
	}

	$("#daily_title").text(hnb+" "+label+" RAB Summary").css({"font-size":"16pt", "font-weight":"bold"});

	var set1={}, set2={}, set3={}, set4={}, set5={};
	set1['label']	= 'RAB attempt';
	set1['data']	= data["RAB_attempt"];
	set1['bars'] 	= { show: true, align: "center", barWidth: 0.4,  }
	set1['yaxis']	= 2;
	set1['type']	= 'RAB_attempt';
	set1['color']	= 1;

	set2['label']	= label+' SR';
	set2['data']	= data[label+"_SR"];
	set2['lines']	= { show: true, fill: false };
	set2['points']	= { show: true, radius: 3 };
	set2['yaxis']	= 1;
	set2['type']	= label+'_SR';
	set2['color']	= 0;

	// set3['label']	= 'Average SR';
	// set3['data']	= data["Average_SR"];
	// set3['lines']	= { show: true, fill: false };
	// set3['points']	= { show: true, radius: 3 };
	// set3['yaxis']	= 1;
	// set3['type']	= 'Average_SR';
	// set3['color']	= 4;

	set4['label']	= 'SR target';
	set4['data']	= data["SR_target"];
	set4['dashes']	= { show: true };
	set4['lines']	= { show: false, fill: false };
	set4['points']	= { show: true, radius: 3 };
	set4['yaxis']	= 1;
	set4['type']	= 'SR_target';
	set4['color']	= 2;

	set5['label']	= 'CDR';
	set5['data']	= data["CDR"];
	set5['dashes']	= { show: true };
	set5['lines']	= { show: false, fill: false };
	set5['points']	= { show: true, radius: 3 };
	set5['yaxis']	= 1;
	set5['type']	= 'CDR';
	set5['color']	= 3;

	var options = {
		grid:	{ hoverable: true, clickable: true },
		xaxis: { mode: "categories", tickLength: 0, axisLabel: "Time (Day)", rotateTicks: rotateAngle },
		yaxes: [
					{ position: "left", axisLabelPadding: 1, max: 101, min: 0, tickSize: 10, tickFormatter: percent, axisLabel: label+" RAB Successful Rate" },
					{ position: "right", tickDecimals: 0, axisLabel: "Count" },
				],
		legend:	{ backgroundOpacity: 0.4, position: 'ne' }
	};

	$.plot("#daily_placeholder", [set1, set2, set4, set5], options);
	$(window).resize(function() {$.plot("#daily_placeholder", [set1, set2, set4, set5], options);});

	//Tooltip
	$("#daily_placeholder").bind("plothover", function (event, pos, item) {
		if(item)
		{
			$("#tooltip").remove();
			var x = data["date"][item.datapoint[0].toFixed(0)],
				y = item.datapoint[1].toFixed(0);
			switch(item.series.type)
			{
				case "RAB_attempt":
					y = "RAB attempt " + y;
					break;

				case label+"_SR":
					y = item.datapoint[1].toFixed(1);
					y = label+" SR " + y + "%";
					break;

				case "SR_target":
					y = "SR target " + y + "%";
					break;

				case "CDR":
					y = item.datapoint[1].toFixed(1);
					y = "CDR " + y + "%";
					break;
			}
			showTooltip(item.pageX, item.pageY, x+': '+y);
		}
		else
		{
			$("#tooltip").remove();
		}
	});

	$("#daily_placeholder").bind("plotclick", function (event, pos, item) {
		if (item.series.type == "RAB_attempt") {
			globalDate = data["date"][item.dataIndex];
			getGraph4Data(hnb, globalDate);
		}
	});
}

function popout_hnb_RRC_Connection_daily(data, hnb, kpi)
{
	console.log(data, hnb, kpi);
	var ylabel = "", dailyTitle = "";
	var label = defineLabel();
	switch(kpi)
	{
		case "RRC_Connection_SR_Distribution":
			dailyTitle = " RRC Connection ";
			ylabel = "RRC Connection Successful Rate";
			break;

		case "CS_Call_Drop_Rate_Distribution":
		case "PS_Call_Drop_Rate_Distribution":
		case "Overall_Call_Drop_Rate_Distribution":
			dailyTitle = " "+label+" Call Drop Rate ";
			ylabel = label+" Call Drop Successful Rate";
			break;

		case "CS_Attach_Success_Rate_Distribution":
		case "PS_Attach_Success_Rate_Distribution":
			dailyTitle = " "+label+" Attach ";
			ylabel = label+" Attach Successful Rate";
			break;
	}
	$("#daily_title").text(hnb+dailyTitle+"Summary").css({"font-size":"16pt", "font-weight":"bold"});

	var SR = "";
	var set1={}, set2={}, set3={}, set4={};
	switch(kpi)
	{
		case "RRC_Connection_SR_Distribution":
			set1['label']	= "Fail Count";
			set1['data']	= data["failCount"];
			set1['type']	= "failCount";
			set2['label']	= "Successful Count";
			set2['data']	= data["succCount"];
			set2['type']	= "succCount";
			set3['label']	= "RRC SR";
			SR = "RRC_SR";
			break;

		case "CS_Call_Drop_Rate_Distribution":
		case "PS_Call_Drop_Rate_Distribution":
		case "Overall_Call_Drop_Rate_Distribution":
			set1['label']	= "luRelease Command";
			set1['data']	= data["luReleaseCommand"];
			set1['type']	= "luReleaseCommand";
			set2['label']	= "luRelease Request";
			set2['data']	= data["luReleaseRequest"];
			set2['type']	= "luReleaseRequest";
			set3['label']	= label+" CDR SR";
			SR = label+"_CDR_SR";
			break;

		case "CS_Attach_Success_Rate_Distribution":
		case "PS_Attach_Success_Rate_Distribution":
			set1['label']	= "Attach Reject Count";
			set1['data']	= data["failCount"];
			set1['type']	= "AttachReject";
			set2['label']	= "Attach success Count";
			set2['data']	= data["succCount"];
			set2['type']	= "AttachSucc";
			set3['label']	= label+" Attach SR";
			SR = label+"AttachSR";
			break;
	}

	// set1['label']	= "Fail Count";
	// set1['data']	= data["failCount"];
	set1['stack']	= true;
	set1['lines']	= { show: false, fill: true, steps: false };
	set1['bars'] 	= { show: true, align: "center", barWidth: 0.4, };
	set1['yaxis']	= 2;
	// set1['type']	= "failCount";

	// set2['label']	= "Successful Count";
	// set2['data']	= data["succCount"];
	set2['stack']	= true;
	set2['lines']	= { show: false, fill: true, steps: false };
	set2['bars']	= { show: true, align: "center", barWidth: 0.4, };
	set2['yaxis']	= 2;
	// set2['type']	= "succCount";

	// set3['label']	= "RRC SR";
	set3['data']	= data[SR];
	set3['lines']	= { show: true, fill: false };
	set3['points']	= { show: true, radius: 3 };
	set3['yaxis']	= 1;
	set3['type']	= SR;

	set4['label']	= 'SR target';
	set4['data']	= data["SR_target"];
	set4['dashes']	= { show: true };
	set4['lines']	= { show: false, fill: false };
	set4['points']	= { show: true, radius: 3 };
	set4['yaxis']	= 1;
	set4['type']	= 'SR_target';

	var options = {
		grid:	{ hoverable: true, clickable: true },
		xaxis: { mode: "categories", tickLength: 0, axisLabel: "Time (Day)", rotateTicks: rotateAngle },
		yaxes: [
					{ position: "left", axisLabelPadding: 1, max: 101, min: 0, tickFormatter: percent, axisLabel: ylabel },
					{ position: "right", tickDecimals: 0, axisLabel: "Count" },
				],
		legend:	{ backgroundOpacity: 0.4, position: 'ne' }
	};

	$.plot("#daily_placeholder", [set1, set2, set3, set4], options);
	$(window).resize(function() {$.plot("#daily_placeholder", [set1, set2, set3, set4], options);});

	//Tooltip
	$("#daily_placeholder").bind("plothover", function (event, pos, item) {
		if(item)
		{
			$("#tooltip").remove();
			var x = data["date"][item.datapoint[0].toFixed(0)],
				y = item.datapoint[1].toFixed(0);
			switch(item.series.type)
			{
				// RRC_Connection_SR_Distribution
				case "RRC_SR":
					y = "RRC SR " + item.datapoint[1].toFixed(1) + "%";
					break;
				case "SR_target":
					y = "SR target " + y + "%";
					break;
				case "failCount":
					y = "Fail Count " +data["failCount"][item.dataIndex][1];
					break;
				case "succCount":
					y = "Success Count " +data["succCount"][item.dataIndex][1];
					break;

				// CS_Call_Drop_Rate_Distribution,PS_Call_Drop_Rate_Distribution,Overall_Call_Drop_Rate_Distribution
				case label+"_CDR_SR":
					y = label+" CDR SR " + item.datapoint[1].toFixed(1) + "%";
					break;
				case "luReleaseCommand":
					y = "luRelease Command " +data["luReleaseCommand"][item.dataIndex][1];
					break;
				case "luReleaseRequest":
					y = "luRelease Request " +data["luReleaseRequest"][item.dataIndex][1];
					break;

				// CS_Attach_Success_Rate_Distribution, PS_Attach_Success_Rate_Distribution
				case label+"AttachSR":
					y = label+" Attach SR " + item.datapoint[1].toFixed(1) + "%";
					break;
				case "AttachReject":
					y = "Attach Reject " +data["failCount"][item.dataIndex][1];
					break;
				case "AttachSucc":
					y = "Attach Success " +data["succCount"][item.dataIndex][1];
					break;
			}
			showTooltip(item.pageX, item.pageY, x+': '+y);
		}
		else
		{
			$("#tooltip").remove();
		}
	});

	$("#daily_placeholder").bind("plotclick", function (event, pos, item) {
		globalDate = data["date"][item.dataIndex];
		switch(item.series.type)
		{
			// RRC Connection
			case "succCount":
			case "failCount":

			// CS CDR, PS CDR, Overall CDR
			case "luReleaseCommand":
			case "luReleaseRequest":

			// CS Attach, PS Attach
			case "AttachReject":
			case "AttachSucc":
				getGraph4Data(hnb, globalDate);
				break;
		}
	});
}

function popout_hnb_Paging_daily(data, hnb)
{
	console.log(data, hnb);
	$("#daily_title").text(hnb+" Paging Summary").css({"font-size":"16pt", "font-weight":"bold"});

	var set1={}, set2={}, set3={}, set4={}, set5={};
	set1['label']	= "Type1";
	set1['data']	= data["type1"];
	set1['stack']	= true;
	set1['lines']	= { show: false, fill: true, steps: false };
	set1['bars']	= { show: true, align: "center", barWidth: 0.4, };
	set1['yaxis']	= 2;
	set1['type']	= "type1";
	set1['color']	= 1;

	set2['label']	= "Type2";
	set2['data']	= data["type2"];
	set2['stack']	= true;
	set2['lines']	= { show: false, fill: true, steps: false };
	set2['bars']	= { show: true, align: "center", barWidth: 0.4, };
	set2['yaxis']	= 2;
	set2['type']	= "type2";
	set2['color']	= 2;

	set3['label']	= "Fail Count";
	set3['data']	= data["failCount"];
	set3['stack']	= true;
	set3['lines']	= { show: false, fill: true, steps: false };
	set3['bars'] 	= { show: true, align: "center", barWidth: 0.4, numbers: {show: true, xOffset: -30},  };
	set3['yaxis']	= 2;
	set3['type']	= "failCount";
	set3['color']	= 0;

	set4['label']	= "Paging SR";
	set4['data']	= data["Paging_SR"];
	set4['lines']	= { show: true, fill: false };
	set4['points']	= { show: true, radius: 3 };
	set4['yaxis']	= 1;
	set4['type']	= "Paging_SR";
	set4['color']	= 3;

	set5['label']	= 'SR target';
	set5['data']	= data["SR_target"];
	set5['dashes']	= { show: true };
	set5['lines']	= { show: false, fill: false };
	set5['points']	= { show: true, radius: 3 };
	set5['yaxis']	= 1;
	set5['type']	= 'SR_target';
	set5['color']	= 4;

	var options = {
		grid:	{ hoverable: true, clickable: true },
		xaxis: { mode: "categories", tickLength: 0, axisLabel: "Time (Day)", rotateTicks: rotateAngle },
		yaxes: [
					{ position: "left", axisLabelPadding: 1, max: 101, min: 0, tickSize: 10, tickFormatter: percent, axisLabel: "Paging Successful Rate" },//, tickSize: 5
					{ position: "right", tickDecimals: 0, axisLabel: "Count" },
				],
		legend:	{ backgroundOpacity: 0.4, position: 'ne' }
	};

	$.plot("#daily_placeholder", [set1, set2, set3, set4, set5], options);
	$(window).resize(function() {$.plot("#daily_placeholder", [set1, set2, set3, set4, set5], options);});

	//Tooltip
	$("#daily_placeholder").bind("plothover", function (event, pos, item) {
		if(item)
		{
			$("#tooltip").remove();
			var x = data["date"][item.datapoint[0].toFixed(0)],
				y = item.datapoint[1].toFixed(0);
			switch(item.series.type)
			{
				case "Paging_SR":
					y = "Paging SR " + item.datapoint[1].toFixed(1) + "%";
					break;
				case "SR_target":
					y = "SR target " + y + "%";
					break;
				case "failCount":
					y = "failCount " +data["failCount"][item.dataIndex][1];
					break;
				case "type1":
					y = "type1 -> " +data["type1"][item.dataIndex][1];
					break;
				case "type2":
					y = "type2 -> " +data["type2"][item.dataIndex][1];
					break;

			}
			showTooltip(item.pageX, item.pageY, x+': '+y);
		}
		else
		{
			$("#tooltip").remove();
		}
	});

	$("#daily_placeholder").bind("plotclick", function (event, pos, item) {
		switch(item.series.type)
		{
			case "type1":
			case "type2":
			case "failCount":
				globalDate = data["date"][item.dataIndex];
				getGraph4Data(hnb, globalDate);
				break;
		}
	});
}

function popout_hnb_IPSec_Tunnel_Est_daily(data, hnb)
{
	console.log(data, hnb);
	var dtitle = "";
	switch($('#kpiSelect option:selected').val())
	{
		case "IPSec_Tunnel_Est_Distribution":
			dtitle = "IPSec Establishment";
			break;

		case "HNB_Registration_Attempt_Distribution":
			dtitle = "Registration";
			break;

		case "SCTP_Abort_Distribution":
			dtitle = defineLabel();
			break;
	}
	$("#daily_title").text(hnb+" "+dtitle+" Summary").css({"font-size":"16pt", "font-weight":"bold"});

	var set1={}, set2={};
	switch($('#kpiSelect option:selected').val())
	{
		case "IPSec_Tunnel_Est_Distribution":
		case "HNB_Registration_Attempt_Distribution":
			set1['label']	= "Success Count";
			set1['data']	= data["succCount"];
			set1['type']	= "succCount";
			set2['label']	= "Fail Count";
			set2['data']	= data["failCount"];
			set2['type']	= "failCount";
			break;

		case "SCTP_Abort_Distribution":
			set1['label']	= "SCTP Abort Rcvd";
			set1['data']	= data["SCTPAbortRcvd"];
			set1['type']	= "SCTPAbortRcvd";
			set2['label']	= "SCTP Abort Sent";
			set2['data']	= data["SCTPAbortSent"];
			set2['type']	= "SCTPAbortSent";
			break;
	}

	// set1['label']	= "Success Count";
	// set1['data']	= data["succCount"];
	set1['stack']	= true;
	set1['lines']	= { show: false, fill: true, steps: false };
	set1['bars']	= { show: true, align: "center", barWidth: 0.4, };
	// set1['yaxis']	= 1;
	// set1['type']	= "succCount";
	set1['color']	= 1;

	// set2['label']	= "Fail Count";
	// set2['data']	= data["failCount"];
	set2['stack']	= true;
	set2['lines']	= { show: false, fill: true, steps: false };
	set2['bars']	= { show: true, align: "center", barWidth: 0.4, };
	// set2['yaxis']	= 1;
	// set2['type']	= "failCount";
	set2['color']	= 0;

	var options = {
		grid:	{ hoverable: true, clickable: true },
		xaxis: { mode: "categories", tickLength: 0, axisLabel: "Time (Day)", rotateTicks: rotateAngle },
		yaxis: { axisLabelPadding: 1, min: 0, tickDecimals: 0 },
		legend:	{ backgroundOpacity: 0.4, position: 'ne' }
	};
	$.plot("#daily_placeholder", [set1, set2], options);
	$(window).resize(function() {$.plot("#daily_placeholder", [set1, set2], options);});

	//Tooltip
	$("#daily_placeholder").bind("plothover", function (event, pos, item) {
		if(item)
		{
			$("#tooltip").remove();
			var x = data["date"][item.datapoint[0].toFixed(0)];
			switch(item.series.type)
			{
				case "succCount":
					y = "Success " +data["succCount"][item.dataIndex][1];
					break;

				case "failCount":
					y = "Fail " +data["failCount"][item.dataIndex][1];
					break;

				case "SCTPAbortRcvd":
					y = "SCTP Abort Rcvd " +data["SCTPAbortRcvd"][item.dataIndex][1];
					break;

				case "SCTPAbortSent":
					y = "SCTP Abort Sent " +data["SCTPAbortSent"][item.dataIndex][1];
					break;
			}
			showTooltip(item.pageX, item.pageY, x+': '+y);
		}
		else
		{
			$("#tooltip").remove();
		}
	});

	$("#daily_placeholder").bind("plotclick", function (event, pos, item) {
		switch(item.series.type)
		{
			// IP Sec, HNB Reg.
			case "succCount":
			case "failCount":

			// SCTP Abort
			case "SCTPAbortRcvd":
			case "SCTPAbortSent":
				globalDate = data["date"][item.dataIndex];
				getGraph4Data(hnb, globalDate);
				break;
		}
	});
}

function popout_hnb_Cell_Availability_daily(data, hnb)
{
	console.log(data, hnb);
	var label = "";
	var kpi = $('#kpiSelect option:selected').val();
	$("#daily_title").text(hnb+" Cell Availability Summary");

	var set1={}, set2={}, set3={};
	set1['label']	= 'Pwr Off Time';
	set1['data']	= data["PwrOffTime"];
	set1['bars'] 	= { show: true, align: "center", barWidth: 0.4,  }
	set1['yaxis']	= 2;
	set1['type']	= 'PwrOffTime';
	set1['color']	= 1;

	set2['label']	= "Availability";
	set2['data']	= data["availability"];
	set2['lines']	= { show: true, fill: false };
	set2['points']	= { show: true, radius: 3 };
	set2['yaxis']	= 1;
	set2['type']	= "availability";
	set2['color']	= 0;

	set3['label']	= 'Target';
	set3['data']	= data["SR_target"];
	set3['dashes']	= { show: true };
	set3['lines']	= { show: false, fill: false };
	set3['points']	= { show: true, radius: 3 };
	set3['yaxis']	= 1;
	set3['type']	= 'target';
	set3['color']	= 2;


	var options = {
		grid:	{ hoverable: true, clickable: true },
		xaxis: { mode: "categories", tickLength: 0, axisLabel: "Time (Day)", rotateTicks: rotateAngle },
		yaxes: [
					{ position: "left", axisLabelPadding: 1, min: 0, tickFormatter: percent, axisLabel: "Cell Availability Rate" },
					{ position: "right", tickDecimals: 0, axisLabel: "Sec" },
				],
		legend:	{ backgroundOpacity: 0.4, position: 'ne' }
	};

	$.plot("#daily_placeholder", [set3, set1, set2], options);
	$(window).resize(function() {$.plot("#daily_placeholder", [set3, set1, set2], options);});

	//Tooltip
	$("#daily_placeholder").bind("plothover", function (event, pos, item) {
		if(item)
		{
			$("#tooltip").remove();
			var x = data["date"][item.datapoint[0].toFixed(0)],
				y = item.datapoint[1].toFixed(0);

			switch(item.series.type)
			{
				case "PwrOffTime":
					y = "Pwr Off Time " + y + " sec";
					break;

				case "availability":
					y = item.datapoint[1].toFixed(1);
					y = y + "%";
					break;

				case "target":
					y = "Target " + y + "%";
					break;
			}
			showTooltip(item.pageX, item.pageY, x+': '+y);
		}
		else
		{
			$("#tooltip").remove();
		}
	});

	$("#daily_placeholder").bind("plotclick", function (event, pos, item) {
		if (item.series.type == "PwrOffTime") {
			globalDate = data["date"][item.dataIndex];
			getGraph4Data(hnb, globalDate);
		}
	});
}

function getGraph4Data(hnb, date)
{
	var version = msieversion();
	var kpi = getGraph4Kpi();

	var tempdate = date.split("/");
	var year = $("#datepicker_start").val().substr(0,4);
	var startTime = year+"-"+tempdate[0]+"-"+tempdate[1]+"_00:00:00";
	var endTime = year+"-"+tempdate[0]+"-"+tempdate[1]+"_23:59:59";

	var path = getBaseURL()+getProjectName()+"/index.php/kpi/get_kpi_data";
	var url = "http://"+getHost()+":8080/KPIcalculator/calculatorAction";
	console.log(hnb, date, kpi, startTime, endTime)
	jQuery.ajax({
		url: path,
		type: "POST",
		data: { kpi: kpi, hnbMAC: hnb, startTime: startTime, endTime: endTime, url: url, version: version },
		dataType: "JSON",
		error: function(xhr, ajaxOptions, thrownError) {
			console.log("ERROR!", xhr, ajaxOptions, thrownError, xhr.status);
		},
		success: function(xhr, XMLHttpRequest, jsonStatus) {
			console.log(xhr);
			calculateGraph4(xhr, hnb, date, kpi);
		}
	});
}

function calculateGraph4(data, hnb, date, kpi)
{
	console.log(data, hnb, date, kpi);
	var targetInput = $("#targetInput").val();
	var url = getBaseURL()+getProjectName()+'/index.php/kpi/calculateGraph4';
	if(targetInput != "" && targetInput != null)
	{
		if(isNumber(targetInput) && !(targetInput < 0) && !(targetInput > 101) )
		{
			$("#midMsg").html("");
			jQuery.ajax({
				url: url,
				type: 'POST',
				data: { kpi: kpi, target: targetInput, data: data },
				dataType: 'JSON',
				error: function(xhr, ajaxOptions, thrownError) {
					console.log("ERROR!", xhr, ajaxOptions, thrownError, xhr.status);
				},
				success: function(xhr, XMLHttpRequest, jsonStatus) {
					prepare_graph4_placeholder();
					drawGraph4(xhr, hnb, date, kpi);
				}
			});
		}
		else
		{
			$("#midMsg").html("<font color='red'>Input valid value between 0~101 please</font>");
		}
	}
	else
	{
		$("#midMsg").html("<font color='red'>Target should not be empty</font>");
	}
}

function prepare_graph4_placeholder()
{
	$("#graph4").html("<div class='gap'></div><div class='_title' id='hourly_title'></div><div class='_placeholder' id='hourly_placeholder'></div>");
}

function drawGraph4(data, hnb, date, kpi)
{
	switch(kpi)
	{
		case "PS_RAB_SR_DayHourly":
		case "CS_RAB_SR_DayHourly":
		case "RRC_Connection_SR_DayHourly":
		case "CS_Call_Drop_Rate_DayHourly":
		case "PS_Call_Drop_Rate_DayHourly":
		case "Overall_Call_Drop_Rate_DayHourly":
		case "CS_Attach_Success_Rate_DayHourly":
		case "PS_Attach_Success_Rate_DayHourly":
			popout_hnb_CS_RAB_hourly(data, hnb, date, kpi);
			break;

		case "Paging_SR_DayHourly":
			popout_hnb_Paging_hourly(data, hnb, date, kpi);
			break;

		case "IPSec_Tunnel_Est_DayHourly":
		case "HNB_Registration_Attempt_DayHourly":
		case "SCTP_Abort_DayHourly":
			popout_hnb_IPSec_Tunnel_Est_hourly(data, hnb, date, kpi);
			break;

		case "Cell_Availability_DayHourly":
			popout_hnb_Cell_Availability_hourly(data, hnb, date, kpi);
			break;
	}
}

function popout_hnb_Cell_Availability_hourly(data, hnb, date, kpi)
{
	console.log(data, hnb, date, kpi);
	$("#hourly_title").text(hnb+" Cell Availability Summary on "+date);

	if(data["errorBoot"] !=  null && data["errorBoot"] != "")
		alert("Power Off Time Error:\n"+JSON.stringify(data["errorBoot"]));

	var set1={}, set2={}, set3={}, set4={}, set5={};
	set1['label']	= 'Pwr Off Time';
	set1['data']	= data["PwrOffTime"];
	set1['bars'] 	= { show: true, align: "left", barWidth: 0.4, order: 1 };//0.4
	set1['yaxis']	= 2;
	set1['type']	= 'PwrOffTime';
	set1['color']	= 1;

	set2['label']	= "Availability";
	set2['data']	= data["availability"];
	set2['lines']	= { show: true, fill: false };
	set2['points']	= { show: true, radius: 3 };
	set2['yaxis']	= 1;
	set2['type']	= "availability";
	set2['color']	= 0;

	set3['label']	= "Target";
	set3['data']	= data["SR_target"];
	set3['dashes']	= { show: true };
	set3['lines']	= { show: false, fill: false };
	set3['points']	= { show: true, radius: 3 };
	set3['yaxis']	= 1;
	set3['type']	= "SR_target";
	set3['color']	= 2;

	// set4['label']	= 'IPSec Tunnel Success';
	// set4['data']	= data["IPSecTunnSucc"];
	// set4['stack']	= true;
	// set4['lines']	= { show: false, fill: true, steps: false };
	// set4['bars'] 	= { show: true, align: "left", barWidth: 0.13, order: 2 };
	// set4['yaxis']	= 3;
	// set4['type']	= 'IPSecTunnSucc';
	// set4['color']	= 3;

	// set5['label']	= "IPSec Tunnel Fail";
	// set5['data']	= data["IPSecTunnFail"];
	// set5['stack']	= true;
	// set5['lines']	= { show: false, fill: true, steps: false };
	// set5['bars']	= { show: true, align: "left", barWidth: 0.13, order: 2 };
	// set5['yaxis']	= 3;
	// set5['type']	= "IPSecTunnFail";
	// set5['color']	= 4;

	var options = {
		grid:	{ hoverable: true, clickable: true },
		xaxis: { mode: "categories", tickLength: 0, axisLabel: "Time (Hour)" },
		yaxes: [
					{ position: "left", axisLabelPadding: 1, min: 0, tickFormatter: percent, axisLabel: "Cell Availability Rate" },
					{ position: "right", tickDecimals: 0, axisLabel: "Sec" },
				],
		legend:	{ backgroundOpacity: 0.4, position: 'ne' }
	};

	$.plot("#hourly_placeholder", [set3, set1, set2], options);
	$(window).resize(function() {$.plot("#hourly_placeholder", [set3, set1, set2], options);});

	//Tooltip
	$("#hourly_placeholder").bind("plothover", function (event, pos, item) {
		if(item)
		{
			$("#tooltip").remove();
			var x = "'"+item.dataIndex+":00' ",
				y = item.datapoint[1].toFixed(0);
			switch(item.series.type)
			{
				case "availability":
					y = "Availability " + item.datapoint[1].toFixed(1) + "%";
					break;
				case "SR_target":
					y = "SR target " + y + "%";
					break;
				case "PwrOffTime":
					y = "Pwr Off Time " + y + " Sec";
					break;
			}
			showTooltip(item.pageX, item.pageY, x+': '+y);
		}
		else
		{
			$("#tooltip").remove();
		}
	});
}

function popout_hnb_CS_RAB_hourly(data, hnb, date, kpi)
{
	console.log(data, hnb, date, kpi);
	var label = "", hourTitle = "";
	switch(kpi)
	{
		case "PS_RAB_SR_DayHourly":
		case "CS_RAB_SR_DayHourly":
			hourTitle = hnb+" "+defineLabel()+" RAB Summary on "+date;
			label = defineLabel()+" RAB ";
			break;
		case "RRC_Connection_SR_DayHourly":
			hourTitle = hnb+" RRC Connection Summary on "+date;
			label = "RRC Connection ";
			break;
		case "CS_Call_Drop_Rate_DayHourly":
		case "PS_Call_Drop_Rate_DayHourly":
		case "Overall_Call_Drop_Rate_DayHourly":
			hourTitle = hnb+" "+defineLabel()+" CDR Summary on "+date;
			label = defineLabel()+" CDR ";
			break;
		case "CS_Attach_Success_Rate_DayHourly":
		case "PS_Attach_Success_Rate_DayHourly":
			hourTitle = hnb+" "+defineLabel()+" Attach Summary on "+date;
			label = defineLabel()+" Attach ";
			break;
	}
	$("#hourly_title").text(hourTitle).css({"font-size":"16pt", "font-weight":"bold"});

	var set1={}, set2={}, set3={}, set4={};
	switch(kpi)
	{
		case "PS_RAB_SR_DayHourly":
		case "CS_RAB_SR_DayHourly":
		case "RRC_Connection_SR_DayHourly":
			set1['label']	= 'Attempt Fail Count';
			set1['data']	= data["attempt_fail_count"];
			set1['type']	= 'attempt_fail_count';
			set2['label']	= 'Attempt Success Count';
			set2['data']	= data["attempt_success_count"];
			set2['type']	= 'attempt_success_count';
			break;
		case "CS_Call_Drop_Rate_DayHourly":
		case "PS_Call_Drop_Rate_DayHourly":
		case "Overall_Call_Drop_Rate_DayHourly":
			set1['label']	= 'luRelease Command';
			set1['data']	= data["luReleaseCommand"];
			set1['type']	= 'luReleaseCommand';
			set2['label']	= 'luRelease Request';
			set2['data']	= data["luReleaseRequest"];
			set2['type']	= 'luReleaseRequest';
			break;
		case "CS_Attach_Success_Rate_DayHourly":
		case "PS_Attach_Success_Rate_DayHourly":
			set1['label']	= 'Attach Reject Count';
			set1['data']	= data["attempt_fail_count"];
			set1['type']	= 'AttachReject';
			set2['label']	= 'Attach Success Count';
			set2['data']	= data["attempt_success_count"];
			set2['type']	= 'AttachSucc';
			break;
	}
	// set1['label']	= 'Attempt Fail Count';
	// set1['data']	= data["attempt_fail_count"];
	set1['stack']	= true;
	set1['lines']	= { show: false, fill: true, steps: false };
	set1['bars'] 	= { show: true, align: "left", barWidth: 0.4 };
	set1['yaxis']	= 2;
	// set1['type']	= 'attempt_fail_count';

	// set2['label']	= 'Attempt Success Count';
	// set2['data']	= data["attempt_success_count"];
	set2['stack']	= true;
	set2['lines']	= { show: false, fill: true, steps: false };
	set2['bars'] 	= { show: true, align: "left", barWidth: 0.4 };
	set2['yaxis']	= 2;
	// set2['type']	= 'attempt_success_count';

	set3['label']	= label+"SR";
	set3['data']	= data["SR"];
	set3['lines']	= { show: true, fill: false };
	set3['points']	= { show: true, radius: 3 };
	set3['yaxis']	= 1;
	set3['type']	= "SR";

	set4['label']	= "SR target";
	set4['data']	= data["SR_target"];
	set4['dashes']	= { show: true };
	set4['lines']	= { show: false, fill: false };
	set4['points']	= { show: true, radius: 3 };
	set4['yaxis']	= 1;
	set4['type']	= "SR_target";

	var options = {
		grid:	{ hoverable: true, clickable: true },
		xaxis: { mode: "categories", tickLength: 0, axisLabel: "Time (Hour)" },
		yaxes: [
					{ position: "left", axisLabelPadding: 1, max: 101, min: 0, tickFormatter: percent, axisLabel: label+"Successful Rate" },
					{ position: "right", tickDecimals: 0, axisLabel: "Count" },
				],
		legend:	{ backgroundOpacity: 0.4, position: 'ne' }
	};

	$.plot("#hourly_placeholder", [set1, set2, set3, set4], options);
	$(window).resize(function() {$.plot("#hourly_placeholder", [set1, set2, set3, set4], options);});

	//Tooltip
	$("#hourly_placeholder").bind("plothover", function (event, pos, item) {
		if(item)
		{
			$("#tooltip").remove();
			var x = "'"+item.datapoint[0].toFixed(0)+":00' ",
				y = item.datapoint[1].toFixed(0);
			switch(item.series.type)
			{
				case "SR":
					y = label+"SR " + item.datapoint[1].toFixed(1) + "%";
					break;
				case "SR_target":
					y = "SR target " + y + "%";
					break;

				// CS_RAB_SR_DayHourly, PS_RAB_SR_DayHourly, RRC_Connection_SR_DayHourly
				case "attempt_fail_count":
					y = "Fail " + data["attempt_fail_count"][item.dataIndex][1];
					break;
				case "attempt_success_count":
					y = "Success " + data["attempt_success_count"][item.dataIndex][1];
					break;

				// CS_Call_Drop_Rate_DayHourly, PS_Call_Drop_Rate_DayHourly, Overall_Call_Drop_Rate_DayHourly
				case "luReleaseCommand":
					y = "luRelease Command " + data["luReleaseCommand"][item.dataIndex][1];
					break;
				case "luReleaseRequest":
					y = "luRelease Request " + data["luReleaseRequest"][item.dataIndex][1];
					break;

				// CS_Attach_Success_Rate_DayHourly, PS_Attach_Success_Rate_DayHourly
				case "AttachReject":
					y = "Attach Reject " + data["attempt_fail_count"][item.dataIndex][1];
					break;
				case "AttachSucc":
					y = "Attach Success " + data["attempt_success_count"][item.dataIndex][1];
					break;
			}
			showTooltip(item.pageX, item.pageY, x+': '+y);
		}
		else
		{
			$("#tooltip").remove();
		}
	});
}

function popout_hnb_Paging_hourly(data, hnb, date, kpi)
{
	console.log(data, hnb, date, kpi);
	var label = "", hourTitle = "";
	switch(kpi)
	{
		case "Paging_SR_DayHourly":
			hourTitle = hnb+" Paging Summary on "+date;
			label = "Paging ";
			break;
	}
	$("#hourly_title").text(hourTitle).css({"font-size":"16pt", "font-weight":"bold"});

	var set1={}, set2={}, set3={}, set4={}, set5={};
	set1['label']	= "Type1";
	set1['data']	= data["type1"];
	set1['stack']	= true;
	set1['lines']	= { show: false, fill: true, steps: false };
	set1['bars'] 	= { show: true, align: "left", barWidth: 0.4 };
	set1['yaxis']	= 2;
	set1['type']	= 'type1';
	set1['color']	= 1;

	set2['label']	= "Type2";
	set2['data']	= data["type2"];
	set2['stack']	= true;
	set2['lines']	= { show: false, fill: true, steps: false };
	set2['bars']	= { show: true, align: "left", barWidth: 0.4 };
	set2['yaxis']	= 2;
	set2['type']	= "type2";
	set2['color']	= 2;

	set3['label']	= "Fail Count";
	set3['data']	= data["failCount"];
	set3['stack']	= true;
	set3['lines']	= { show: false, fill: true, steps: false };
	set3['bars'] 	= { show: true, align: "left", barWidth: 0.4 };
	set3['yaxis']	= 2;
	set3['type']	= 'failCount';
	set3['color']	= 0;

	set4['label']	= label+"SR";
	set4['data']	= data["SR"];
	set4['lines']	= { show: true, fill: false };
	set4['points']	= { show: true, radius: 3 };
	set4['yaxis']	= 1;
	set4['type']	= "SR";
	set4['color']	= 3;

	set5['label']	= "SR target";
	set5['data']	= data["SR_target"];
	set5['dashes']	= { show: true };
	set5['lines']	= { show: false, fill: false };
	set5['points']	= { show: true, radius: 3 };
	set5['yaxis']	= 1;
	set5['type']	= "SR_target";
	set5['color']	= 4;

	var options = {
		grid:	{ hoverable: true, clickable: true },
		xaxis: { mode: "categories", tickLength: 0, axisLabel: "Time (Hour)" },
		yaxes: [
					{ position: "left", axisLabelPadding: 1, max: 101, min: 0, tickFormatter: percent, axisLabel: label+"Successful Rate" },
					{ position: "right", tickDecimals: 0, axisLabel: "Count" },
				],
		legend:	{ backgroundOpacity: 0.4, position: 'ne' }
	};

	$.plot("#hourly_placeholder", [set1, set2, set3, set4, set5], options);
	$(window).resize(function() {$.plot("#hourly_placeholder", [set1, set2, set3, set4, set5], options);});

	//Tooltip
	$("#hourly_placeholder").bind("plothover", function (event, pos, item) {
		if(item)
		{
			$("#tooltip").remove();
			var x = "'"+item.datapoint[0].toFixed(0)+":00' ",
				y = item.datapoint[1].toFixed(0);
			switch(item.series.type)
			{
				case "failCount":
					y = "fail " +data["failCount"][item.dataIndex][1];
					break;
				case "type1":
					y = "type1 -> " +data["type1"][item.dataIndex][1];
					break;
				case "type2":
					y = "type2 -> " +data["type2"][item.dataIndex][1];
					break;
				case "SR":
					y = label+"SR " + item.datapoint[1].toFixed(1) + "%";
					break;
				case "SR_target":
					y = "SR target " + y + "%";
					break;
			}
			showTooltip(item.pageX, item.pageY, x+': '+y);
		}
		else
		{
			$("#tooltip").remove();
		}
	});
}

function popout_hnb_IPSec_Tunnel_Est_hourly(data, hnb, date, kpi)
{
	console.log(data, hnb, date, kpi);
	var label = "", hourTitle = "";
	switch(kpi)
	{
		case "IPSec_Tunnel_Est_DayHourly":
			hourTitle ="IPSec Establishment";
			break;
		case "HNB_Registration_Attempt_DayHourly":
			hourTitle = "Registration Attempt";
			break;
		case "SCTP_Abort_DayHourly":
			hourTitle = "SCTP Abort";
			break;
	}
	$("#hourly_title").text(hnb+" "+hourTitle+" Summary on "+date).css({"font-size":"16pt", "font-weight":"bold"});

	var set1={}, set2={};
	switch(kpi)
	{
		case "IPSec_Tunnel_Est_DayHourly":
		case "HNB_Registration_Attempt_DayHourly":
			set1['label']	= "Success Count";
			set1['data']	= data["succCount"];
			set1['type']	= "succCount";
			set2['label']	= "Fail Count";
			set2['data']	= data["failCount"];
			set2['type']	= "failCount";
			break;
		case "SCTP_Abort_DayHourly":
			set1['label']	= "SCTP Abort Rcvd";
			set1['data']	= data["SCTPAbortRcvd"];
			set1['type']	= "SCTPAbortRcvd";
			set2['label']	= "SCTP Abort Sent";
			set2['data']	= data["SCTPAbortSent"];
			set2['type']	= "SCTPAbortSent";
			break;
	}
	// set1['label']	= "Success Count";
	// set1['data']	= data["succCount"];
	set1['stack']	= true;
	set1['lines']	= { show: false, fill: true, steps: false };
	set1['bars']	= { show: true, align: "left", barWidth: 0.4, };
	// set1['yaxis']	= 1;
	// set1['type']	= "succCount";
	set1['color']	= 1;

	// set2['label']	= "Fail Count";
	// set2['data']	= data["failCount"];
	set2['stack']	= true;
	set2['lines']	= { show: false, fill: true, steps: false };
	set2['bars']	= { show: true, align: "left", barWidth: 0.4, };
	// set2['yaxis']	= 1;
	// set2['type']	= "failCount";
	set2['color']	= 0;

	var options = {
		grid:	{ hoverable: true, clickable: true },
		xaxis: { mode: "categories", tickLength: 0, axisLabel: "Time (Hour)" },
		yaxis: { axisLabelPadding: 1, min: 0, tickDecimals: 0 },
		legend:	{ backgroundOpacity: 0.4, position: 'ne' }
	};

	$.plot("#hourly_placeholder", [set1, set2], options);
	$(window).resize(function() {$.plot("#hourly_placeholder", [set1, set2], options);});

	//Tooltip
	$("#hourly_placeholder").bind("plothover", function (event, pos, item) {
		if(item)
		{
			$("#tooltip").remove();
			var x = "'"+item.datapoint[0].toFixed(0)+":00' ";

			switch(item.series.type)
			{
				case "failCount":
					y = "Fail " + data["failCount"][parseInt(item.dataIndex)][1];
					break;
				case "succCount":
					y = "Success " + data["succCount"][parseInt(item.dataIndex)][1];
					break;
				case "SCTPAbortRcvd":
					y = "SCTP Abort Rcvd " +data["SCTPAbortRcvd"][item.dataIndex][1];
					break;
				case "SCTPAbortSent":
					y = "SCTP Abort Sent " +data["SCTPAbortSent"][item.dataIndex][1];
					break;
			}
			showTooltip(item.pageX, item.pageY, x+': '+y);
		}
		else
		{
			$("#tooltip").remove();
		}
	});
}

function showTooltip(x, y, contents)
{
	$('<div id="tooltip">' + contents + '</div>').css( {
		position: 'absolute',
		display: 'none',
		top: y + 5,
		left: x + 5,
		border: '1px solid #fdd',
		padding: '2px',
		'background-color': '#fee',
		opacity: 1
	}).appendTo("body").fadeIn(200);
}

function logtime_transform(time)
{

	var temp_time;
	temp_time = time.split(" ");
	var date = temp_time[0].split('-');
	var timer = temp_time[1].split(':');
	time = Date.UTC(date[0],date[1]-1,date[2],timer[0],timer[1],timer[2]);

	return time;
}


Date.prototype.customFormat = function(formatString){
    var YYYY,YY,MMMM,MMM,MM,M,DDDD,DDD,DD,D,hhh,hh,h,mm,m,ss,s,ampm,AMPM,dMod,th;
    var dateObject = this;
    YY = ((YYYY=dateObject.getUTCFullYear())+"").slice(-2);
    MM = (M=dateObject.getUTCMonth()+1)<10?('0'+M):M;
    MMM = (MMMM=["January","February","March","April","May","June","July","August","September","October","November","December"][M-1]).substring(0,3);
    DD = (D=dateObject.getUTCDate())<10?('0'+D):D;
    DDD = (DDDD=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][dateObject.getUTCDay()]).substring(0,3);
    th=(D>=10&&D<=20)?'th':((dMod=D%10)==1)?'st':(dMod==2)?'nd':(dMod==3)?'rd':'th';
    formatString = formatString.replace("#YYYY#",YYYY).replace("#YY#",YY).replace("#MMMM#",MMMM).replace("#MMM#",MMM).replace("#MM#",MM).replace("#M#",M).replace("#DDDD#",DDDD).replace("#DDD#",DDD).replace("#DD#",DD).replace("#D#",D).replace("#th#",th);

    h=(hhh=dateObject.getUTCHours());
    if (h==0) h=24;
    if (h>12) h-=12;
    hh = h<10?('0'+h):h;
    AMPM=(ampm=hhh<12?'am':'pm').toUpperCase();
    mm=(m=dateObject.getUTCMinutes())<10?('0'+m):m;
    ss=(s=dateObject.getUTCSeconds())<10?('0'+s):s;
    return formatString.replace("#hhh#",hhh).replace("#hh#",hh).replace("#h#",h).replace("#mm#",mm).replace("#m#",m).replace("#ss#",ss).replace("#s#",s).replace("#ampm#",ampm).replace("#AMPM#",AMPM);
}

function dateAdd(date, interval, units) {
	var ret = new Date(date); //don't change original date
	switch(interval.toLowerCase()) {
		case 'year'   :  ret.setFullYear(ret.getFullYear() + units);  break;
		case 'quarter':  ret.setMonth(ret.getMonth() + 3*units);  break;
		case 'month'  :  ret.setMonth(ret.getMonth() + units);  break;
		case 'week'   :  ret.setDate(ret.getDate() + 7*units);  break;
		case 'day'    :  ret.setDate(ret.getDate() + units);  break;
		case 'hour'   :  ret.setHours(ret.getHours() + units);  break;
		case 'minute' :  ret.setMinutes(ret.getMinutes() + units);  break;
		case 'second' :  ret.setSeconds(ret.getSeconds() + units);  break;
		default       :  ret = undefined;  break;
	}
	return ret;
}