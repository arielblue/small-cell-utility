function init()
{
	$('input[type!="button"][type!="submit"], select, textarea').val('').blur();

	/** restrict within 7 days **/
	var startDateTextBox = $("#datepicker_start");
	var endDateTextBox = $("#datepicker_end");
	startDateTextBox.datetimepicker({
		showTimepicker: false,
		dateFormat : "yy-mm-dd",
		maxDate: '0',
		onClose: function(dateText, inst) {
			var today = $.datepicker.formatDate('yy-mm-dd', new Date() );
			var startDate = startDateTextBox.datetimepicker('getDate');
			var testEndDate = $.datepicker.formatDate('yy-mm-dd', dateAdd(startDate, 'day', 6) );

			if(testEndDate > today) testEndDate = today;
			endDateTextBox.val( testEndDate );

			if(startDate == null) $.datepicker._clearDate('#datepicker_end');
		}
	});
	endDateTextBox.datetimepicker({
		showTimepicker: false,
		dateFormat : "yy-mm-dd",
		maxDate: '0',
		onClose: function(dateText, inst) {
			var startDate = $("#datepicker_start").datetimepicker('getDate');
			var endDate = endDateTextBox.datetimepicker('getDate');
			var testStartDate = $.datepicker.formatDate('yy-mm-dd', dateAdd(endDate, 'day', -6) );

			if($.datepicker.formatDate('yy-mm-dd', startDate ) < testStartDate)
				startDateTextBox.val( testStartDate );
			if($.datepicker.formatDate('yy-mm-dd', endDate ) < $.datepicker.formatDate('yy-mm-dd', startDate) )
				startDateTextBox.val( testStartDate );

			if(endDate == null) $.datepicker._clearDate('#datepicker_start');
		}
	});

	$("#SubmitBtn").button();
	submitAction();
}

function submitAction()
{
	$("#SubmitBtn").click( function(){
		$("#mid_content").html("");
		$("#detail1").html("");
		$("#detail2").html("");
		$("#detail3").html("");
		$("#detail1_h2").html("");
		$("#detail2_h2").html("");
		$("#detail3_h2").html("");
		$("#graph").html("");

		var startDate = $("#datepicker_start").val();
		var endDate = $("#datepicker_end").val();
		var version = msieversion();
		var recalculate = $('#recalculate').is(':checked');

		if(startDate !== "" && endDate !== "")
		{
			$("#mid_content").html("wait please...");
			var path = getBaseURL()+getProjectName()+'/index.php/kpi_report/get_kpireport_rawdata';
			var url = "http://"+getHost()+":8080/KPIreportService/kpiReportAction";
			var timea = new Date;
			var timeb;
			console.log(timea.customFormat("#YYYY#-#MM#-#DD# #hh#:#mm#:#ss#"));

			var yesterday = dateAdd(timea, 'day', -1).customFormat("#YYYY#-#MM#-#DD#");

			if( Date.parse(endDate)>=Date.parse(yesterday) )
				$().tostie({type:"success", toastDuration: 13000, message: "It'll be slower to show the result since you pick today's or yesterday's data which need to be calculated now."});

			$("#SubmitBtn").attr('disabled','disabled');
			jQuery.ajax({
				url: path,
				type: 'POST',
				data: { startDate: startDate, endDate: endDate, version: version, url: url, recalculate: recalculate };
				dataType: 'JSON',
				error: function(xhr, ajaxOptions, thrownError, error) {
					$("#SubmitBtn").removeAttr('disabled');
					console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
				},
				success: function(xhr, XMLHttpRequest, textStatus) {
					$("#SubmitBtn").removeAttr('disabled');

					timeb = new Date;
					console.log(timeb.customFormat("#YYYY#-#MM#-#DD# #hh#:#mm#:#ss#"));
					console.log("Diff. Seconds : "+((timeb-timea)/1000).toString());

					show_rawdata(xhr, version);
				}
			});
		}
		else
		{
			$().tostie({type:"error", message: "Start time or End time should not be empty."});
		}
	});
}

function show_rawdata(data, version)
{
	//** for output excel **/
	appendRawData(data);

	$( "#DownloadKpiReportBtn" ).button().click(function() {

		var version = msieversion();
		if(version === -100)	// not IE
		{
			$("#reportRawData_tbl").table2excel({
					exclude: ".noExl",
					name: "Excel Document Name"
				});
		}
	});
}

function appendRawData(data)
{
	var size = Object.size(data['data']);

	if(size != 0)
	{
		var date = Object.keys(data["data"]);
		date.sort();

		if(date.length != 0)
		{
			var version = msieversion();
			var buf = "";
			if(version == -100) // NOT IE
			{
				buf = "<br><button type='button' class='juibtn' id='DownloadKpiReportBtn'>Download</button><br><div class='reportRawData_div'><table id='reportRawData_tbl'><colgroup><col span='1' style='width:265px;'></colgroup><tr><th>KPI Summary</th>";
			}
			else
			{
				buf = "<br><form action='kpi_report/getCSV' method='POST' style='display:inline;'><input type='hidden' name='csv_text' id='csv_text'><input type='submit' class='juibtn' id='clickExcelBtn' align='left' value='Download' onclick='getCSVData()'></form><br><div class='reportRawData_div'><table id='reportRawData_tbl'><colgroup><col span='1' style='width:265px;'></colgroup><tr><th>KPI Summary</th>";
			}

			var pref = Object.keys(data["data"][date[0]]);
			pref.sort();

			var kpiName = data["kpiName"];
			var data = data["data"];

			/** for the data without '%' **/
			var new_pref = ["Num_of_HNBs_PM_Reported"];	// new pref array after re-sort
			var no_percent = ["Num_of_HNBs_PM_Reported", "Conversion_Between_DCH_And_FACH","Conversion_Between_DCH_And_IDLE","Conversion_Between_FACH_And_PCH","Conversion_Between_FACH_And_IDLE","SCTP_Abort_Association"];
			var no_percent_but_needReport = ["Conversion_Between_DCH_And_FACH","Conversion_Between_DCH_And_IDLE","Conversion_Between_FACH_And_PCH","Conversion_Between_FACH_And_IDLE","SCTP_Abort_Association"];
			var counter = [];
			for(key in kpiName) // (1) store kpi first in new_pref
			{
				if(pref.indexOf(key) != -1 && key != 'Num_of_HNBs_PM_Reported')	// key in the preference and isn't Num_of_HNBs_PM_Reported
					new_pref.push(key);
			}
			for(key in pref) // (2) store counter in new_pref
			{
				if(new_pref.indexOf(pref[key]) == -1)
				{
					new_pref.push(pref[key]);
					counter.push(pref[key]);
				}
			}

			for(key in date)
				buf += "<th>"+date[key]+"</th>";
			buf += "</tr>";

			for(key in new_pref)
			{
				if(kpiName[new_pref[key]] != undefined)	//KPI
					buf += "<tr><td id='"+new_pref[key]+"'>"+kpiName[new_pref[key]]+"</td>";
				else //counter
					buf += "<tr><td id='"+new_pref[key]+"'>"+new_pref[key]+"</td>";

				for(k in date)
				{
					var percent = parseFloat(data[date[k]][new_pref[key]]);
					if(percent == -1) // if not returning a Float value
					{
						percent = "NULL";
						buf += "<td>"+percent+"</td>";
					}
					else if(no_percent.indexOf(new_pref[key]) == -1 && counter.indexOf(new_pref[key]) == -1) 
					{
						percent += "%";
						buf += "<td class='report'>"+percent+"</td>";
					}
					else if(no_percent_but_needReport.indexOf(new_pref[key]) > -1)
					{
						buf += "<td class='report'>"+percent+"</td>";
					}
					else
					{
						buf += "<td>"+percent+"</td>";
					}
				}
				buf +="</tr>";
			}
			buf += "</table></div>";

			$("#mid_content").html(buf);
			if(version != -100) $("#clickExcelBtn").button();
		}
		else
			$("#mid_content").html("<font color='red'>You have no preference of KPI & Counter.<br>Select KPI & Counter Preference in Personal Config first please.</font><br>");

		/** KPI detail report level 1 **/
		var last_selected1;	// remember last selected table(reportRawData_tbl) cell
		$('.report').click( function(){

			var verticSize = $('#reportRawData_tbl tr:first th').eq($(this).index()).text(),
				horizSize = $(this).prevAll('#reportRawData_tbl td:last').attr("id");

			/** highlight selected table cell **/
			if(last_selected1 != undefined) last_selected1.removeClass('selected1');
			last_selected1 = $(this);
			$(this).addClass('selected1');

			get_KPIDetail_report('report', horizSize, verticSize, kpiName[horizSize], no_percent_but_needReport);
		});
	}
	else
		$("#mid_content").html("NO DATA");
}

function getCSVData()
{
	var csv_value=$('.reportRawData_div table').table2CSV({delivery:'value'});
	$("#csv_text").val(csv_value);
}

function get_KPIDetail_report(queryType, kpiName, date, kpiDisplayName, no_percent_but_needReport)
{
	$("#detail1").html("wait please...");
	$("#detail2").html("");
	$("#detail2_h2").html("");
	$("#detail3").html("");
	$("#detail3_h2").html("");

	var path = getBaseURL()+getProjectName()+'/index.php/kpi_report/get_kpiDetail_report';
	var url = "http://"+getHost()+":8080/KPIdetailReportService/kpiDetailReportAction";

	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { queryType: queryType, kpiName: kpiName, date: date, url: url },
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError, error) {
			console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
		},
		success: function(xhr, XMLHttpRequest, textStatus) {
			show_KPIDetail_report(xhr, kpiName, date, kpiDisplayName, no_percent_but_needReport);
		}
	});
}

function show_KPIDetail_report(data, kpiName, date, kpiDisplayName, no_percent_but_needReport)
{
	var size = Object.size(data);
	var buf;
	if(size != 0)
	{
		var hnb = Object.keys(data);
		hnb.sort();

		$("#detail1_h2").text(kpiDisplayName+" on "+date);

		buf = "<table id='detail_report' class='reportRawData_div detail'><colgroup><col span='1' style='width:265px;'></colgroup><tr><th>HNB</th><th>";
		if(no_percent_but_needReport.indexOf(kpiName) == -1)
			buf += "Percentage</th></tr>";
		else
			buf += "Count</th></tr>";

		for(k in hnb)
		{
			if(no_percent_but_needReport.indexOf(kpiName) == -1)
				buf += "<tr class='hourly'><td>"+hnb[k]+"</td><td>"+data[hnb[k]]+"%</td></tr>";
			else
				buf += "<tr class='hourly'><td>"+hnb[k]+"</td><td>"+data[hnb[k]]+"</td></tr>";
		}
		buf += "</table>";

		$("#detail1").html(buf);
	}
	else
		$("#detail1").html("<font color='red'>Your HNB preference isn't related to the HNBs that PM reported<br>or<br>select HNB Preference first please<br> since you probably have no preference of HNB.</font><br>");

	$('#detail1').ScrollTo();

	$('.hourly').click( function(){

		var	horizSize = $(this).find("td").eq(0).text();

		/** highlight selected table cell **/
		$(this).addClass('selected2').siblings().removeClass("selected2");

		get_KPIDetail2_hourly("hourly", horizSize, kpiName, date, kpiDisplayName);
	});
}

function get_KPIDetail2_hourly(queryType, hnbMAC, kpiName, date, kpiDisplayName)
{
	$("#detail2").html("wait please...");
	$("#detail3").html("");
	$("#detail3_h2").html("");

	var path = getBaseURL()+getProjectName()+'/index.php/kpi_report/get_KPIDetail2_hourly';
	var url = "http://"+getHost()+":8080/KPIdetailReportService/kpiDetailReportAction";

	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { queryType: queryType, hnbMAC: hnbMAC, kpiName: kpiName, date: date, url: url },
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError, error) {
			console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
		},
		success: function(xhr, XMLHttpRequest, textStatus) {
			show_KPIDetail2_hourly(xhr, hnbMAC, kpiName, date, kpiDisplayName);
		}
	});
}

function show_KPIDetail2_hourly(data, hnbMAC, kpiName, date, kpiDisplayName)
{
	var size = Object.size(data);
	var buf;
	if(size != 0)
	{
		var date_hour = Object.keys(data);
		date_hour.sort();
		var counter = Object.keys(data[date_hour[0]]);

		$("#detail2_h2").text(kpiDisplayName+" of "+hnbMAC+" on "+date);

		buf = "<table id='detail_hourly' class='detail'><colgroup><col span='1' style='width:265px;'></colgroup><tr><th>Counter Hourly Report</th>";
		for(k in counter)
			buf += "<th><div>"+counter[k]+"</div></th>";
		buf += "</tr>";

		for(k in date_hour)
		{
			buf += "<tr class='raw'><td>"+date_hour[k]+"</td>";

			for(key in counter)
				buf += "<td>"+data[date_hour[k]][counter[key]]+"</td>";
			buf += "</tr>"
		}
		buf += "</table>";

		$("#detail2").html(buf);
	}
	else
		$("#detail2").html("<font color='red'>No Data</font><br>");

	$('#detail2').ScrollTo();

	$('.raw').click( function(){
		var	horizSize = $(this).find("td").eq(0).text();
		var hour = horizSize.substr(horizSize.length - 2);

		/** highlight selected table cell **/
		$(this).addClass('selected3').siblings().removeClass("selected3");
		get_KPIDetail3_raw("raw", hour, hnbMAC, kpiName, date, kpiDisplayName);
	});
}

function get_KPIDetail3_raw(queryType, hour, hnbMAC, kpiName, date, kpiDisplayName)
{
	$("#detail3").html("wait please...");
	var path = getBaseURL()+getProjectName()+'/index.php/kpi_report/get_KPIDetail3_raw';
	var url = "http://"+getHost()+":8080/KPIdetailReportService/kpiDetailReportAction";

	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { queryType: queryType, hnbMAC: hnbMAC, kpiName: kpiName, date: date, hour: hour, url: url },
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError, error) {
			console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
		},
		success: function(xhr, XMLHttpRequest, textStatus) {
			show_KPIDetai3_raw(xhr, hour, hnbMAC, kpiName, date, kpiDisplayName);
		}
	});
}

function show_KPIDetai3_raw(data, hour, hnbMAC, kpiName, date, kpiDisplayName)
{
	var size = Object.size(data);
	var buf;
	if(size != 0)
	{
		var date_min = Object.keys(data);
		date_min.sort();
		var counter = Object.keys(data[date_min[0]]);

		$("#detail3_h2").text(kpiDisplayName+" of "+hnbMAC+" at "+hour+":00 "+date);

		buf = "<table id='detail_raw' class='detail'><colgroup><col span='1' style='width:265px;'></colgroup><tr><th>Counter Raw data during "+hour+":00</th>";
		for(k in counter)
			buf += "<th>"+counter[k]+"</th>";
		buf += "</tr>";

		for(k in date_min)
		{
			buf += "<tr class='graph'><td>"+date_min[k]+"</td>";

			for(key in counter)
				buf += "<td>"+data[date_min[k]][counter[key]]+"</td>";
			buf += "</tr>"
		}
		buf += "</table>";

		$("#detail3").html(buf);
	}
	else
		$("#detail3").html("<font color='red'>No Data</font><br>");

	$('#detail3').ScrollTo();

	$('.graph').click( function(){
		var	datetime = $(this).find("td").eq(0).text();

		/** highlight selected table cell **/
		$(this).addClass('selected3').siblings().removeClass("selected3");

		get_graph_2hrData(hnbMAC, datetime);
	});
}

function get_graph_2hrData(hnb, datetime)
{
	var path = getBaseURL()+getProjectName()+'/index.php/rf/get_graph_2hrData/';
	console.log(path, hnb, datetime);

	jQuery.ajax({
		url: path,
		type: 'POST',
		cache: false,
		data: { hnb: hnb, datetime: datetime },
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, ajaxOptions, thrownError){
			console.log(xhr);
			plotting(xhr);

		}
	});
}

var currentSelected = ""; //record current checked checkbox
function plotting(data)
{
	var graph2_frame = '<div id="gap2" class="gap"></div><div id="float2_1" class="align_hor"><div id="placeholder2_title" class="_title OliveGreen">Tx Power Distribution</div><div id="placeholder2" class="_placeholder" style="height:420px;"></div></div><div id="float2_2" class="align_hor" style="height:445px;width:5px;"></div><div id="both_ue_distri_choices" class="align_hor"><div id="twpwr_distri_choices2" class="right_column" style="height:80px;overflow-y:auto"></div><div id="empty" class="right_column" style="height:5px;"></div><div id="reset2" class="right_column" style="height:25px;"></div><div id="empty" class="right_column" style="height:5px;"></div><div id="legend2" class="right_column" style="height:330px;overflow-y:auto"></div></div>';
	$(""+graph2_frame).appendTo("#graph");

	var graph3_frame = '<div id="gap3" class="gap"></div><div id="float3_1" class="align_hor"><div id="placeholder3_title" class="_title OliveGreen">UE Report</div><div id="placeholder3" class="_placeholder" style="height:500px;"></div></div><div id="float3_2" class="align_hor" style="height:525px;width:5px;"></div><div id="float3_3" class="align_hor"><div id="ue_choices3" class="right_column" style="height:130px;overflow-y:auto;"></div><div id="empty" class="right_column" style="height:5px;"></div><div id="mrm_choices3" class="right_column" style="height:180px;overflow-y:auto;"></div><div id="empty" class="right_column" style="height:5px;"></div><div id="reset3" class="right_column" style="height:25px;"></div><div id="legend3" class="right_column" style="height:180px;overflow-y:auto;"></div></div><div id="gap4" class="gap"></div>';
	$(""+graph3_frame).appendTo("#graph");
	$("#placeholder2").html("Calculating...").css('text-align','center');
	$("#placeholder3").html("Calculating...").css('text-align','center');
	$('#graph').ScrollTo();

	var gpc = get_psc_channel(data["FemtoTxPower"]);
	var uereport = ue_report(data, gpc.PSC, gpc.Channel);
	txpwr_distribution(data, uereport.uestate892, gpc.PSC, gpc.Channel, uereport.hnbstate);
}

function get_psc_channel(v)
{
	var fap_PSC = "", fap_Channel = "";

	if(v != '')
	{
		fap_PSC = v[v.length-1]['PSC'];
		fap_Channel = v[v.length-1]['Channel'];
	}
	else	console.log("Can't get PSC & Channel (832)\n");

	return {PSC: fap_PSC, Channel: fap_Channel};
}

//////////////////////// graph1 ///////////////////////////////

var datasets2_txpwr_all = {}; // for function txpwr_distribution only
function txpwr_distribution(data, ueState_in_892, fap_PSC, fap_Channel, hnbstate)
{
	datasets2_txpwr_all = {};

	var values = [[],[],[],[],[]];	// 0TxPower,1UL_RSSI_HNB,2NLPC_Power,3Mart_Power,4HUE_REG,5MUE_REG,6ROT,7LU_REQ,8RAU_REQ,9HNB_State
	var ueState_value = [], dif_ue = [];
	var i, j, k, l = 0, v, temp_time, temp_value;
	select_x_from2 	= 0.0, select_x_to2 = 0.0;
	currentSelected = "";
	var error_table = [];

	if(ueState_in_892 != "" || ueState_in_892.length != undefined)			// UE_State
	{
		i = 0;
		for(var key in ueState_in_892)
		{
			dif_ue.push(key);
			ueState_value[i] = ueState_in_892[key];
			i++;
		}
	}

	for(var key in data)
	{
		v = data[key];
		switch(key)
		{
			case "UL_RSSI_HNB":
				if(data[key] != '')
				{
					for(i = 0; i < v.length; i++)
					{
						v[i]["logtime"] = logtime_transform(v[i]["logtime"]);
						values[1].push([ v[i]["logtime"], v[i]["UL_RSSI_HNB"] ]); // ["logtime", "UL_RSSI_HNB"] 1
					}
				}
				else error_table.push(key);
				break;

			case "NLPC_Power":
				if(data[key] != '')
				{
					for(i = 0; i < v.length; i++)
					{
						v[i]["logtime"] = logtime_transform(v[i]["logtime"]);
						values[2].push([ v[i]["logtime"], v[i]["NLPC_Power"] ]); // ["logtime", "NLPC_Power"] 2
					}
				}
				else	error_table.push(key);
				break;

			case "Mart_Power":
				if(data[key] != '')
				{
					for(i = 0; i < v.length; i++)
					{
						v[i]["logtime"] = logtime_transform(v[i]["logtime"]);
						values[3].push([ v[i]["logtime"], v[i]["Mart_Power"] ] ); // ["logtime", "Mart_Power"] 3
					}
				}
				else error_table.push(key);
				break;
		}
	}


	/* generate graph2 dataset */;
	var set2={},set3={},set4={};
	// set1['label']	= "TxPower";
	// set1['data']	= values[0];
	// set1['points']	= { show: false, radius: 2, fill: false },
	// set1['yaxis']	= 1;
	// set1['type']	= 'others';
	// set1['color']	= 3;

	set2['label']	= "NLPC_Power";
	set2['data']	= values[2];
	set2['lines']	= { show: false, lineWidth: 5 };
	set2['points']	= { show: true, radius: 5, fill: true },
	set2['yaxis']	= 1;
	set2['type']	= 'others';
	set2['color']	= 2;

	set3['label']	= "UL_RSSI_HNB";
	set3['data']	= values[1];
	set3['lines']	= { show: false, lineWidth: 5 };
	set3['points']	= { show: true, radius: 2, fill: true },
	set3['yaxis']	= 2;
	set3['type']	= 'others';
	set3['color']	= 4;

	set4['label']	= "Mart_Power";
	set4['data']	= values[3];
	set4['lines']	= { show: false, lineWidth: 5 };
	set4['points']	= { show: true, radius: 5, fill: true },
	set4['yaxis']	= 1;
	set4['type']	= 'others';
	set4['color']	= 5;

	// this way makes the code more flexible
	var datasets2 = {};
	if(set3['data'] != "") datasets2[set3['label']] = set3;	// UL_RSSI_HNB
	if(set2['data'] != "") datasets2[set2['label']] = set2;	// NLPC_Power
	if(set4['data'] != "") datasets2[set4['label']] = set4;	// Mart_Power
	if(set3['data'] != "") datasets2_txpwr_all[set3['label']] = set3;	// UL_RSSI_HNB
	if(set2['data'] != "") datasets2_txpwr_all[set2['label']] = set2;	// NLPC_Power
	if(set4['data'] != "") datasets2_txpwr_all[set4['label']] = set4;	// Mart_Power

	var options2 = {
		lines:	{ show: true, lineWidth: 2 },
		grid:	{ hoverable: true, clickable: false },
		xaxes:	[ { mode: 'time', tickDecimals: 0, axisLabel: 'TIME', position: 'bottom' } ],
		yaxes:	[ { position: 'left', axisLabel : "Power" ,axisLabelPadding: 1 },
				{ position: 'right', axisLabel : "UL_RSSI" ,axisLabelPadding: 1 }, ],
		legend: { backgroundOpacity: 0.2, container: "#legend2" },
		selection: { mode: "x" }
	};


	// insert graph2 Txpwr Distribution checkboxes
	var TxpwrDistriChoice2Container = $("#twpwr_distri_choices2");
	$.each(datasets2, function(key, val) {
		TxpwrDistriChoice2Container.append('<input type="checkbox" name="'+key+'" checked="checked" id="id'+key+'" value="'+key+'"onclick="">'+'<label for="id'+key+'">'+ key + '</label><br>');
	});

	// Reset Button
	$("#reset2").append('<button type="button" id="resetbtn2" class="juibtn" name="Reset" value="Reset" onclick="CheckSelect2();">Reset</button>')
	$("#resetbtn2").button();

	// error report
	if(error_table != "" || ueState_value != "" || hnbstate == undefined)
	{
		var str = "Data isn't enough in Tx Power Distribution : <br>\n";
		if(data['UL_RSSI_HNB'] == "")	str += "No UL_RSSI_HNB (8 10 2)<br>\n";
		if(data['NLPC_Power'] == "")	str += "No NLPC_Power (803)<br>\n";
		if(data['Mart_Power'] == "")	str += "No Mart_Power (832)<br>\n";
		console.log(str);
	}
	if(data['UL_RSSI_HNB'] == "" && data['NLPC_Power'] == "" && data['Mart_Power'] == "")
	{
		$("#placeholder2").html(str);
		$("#ue_choices2_title").html("");
		$("#reset2").html("");
	}
	else $("#placeholder2").html("");


	// plotting
	var bothUeDistriChoices = $("#both_ue_distri_choices");
	bothUeDistriChoices.find("input").click(plotAccordingToChoices2);

	function plotAccordingToChoices2() {
		var data = [];
		bothUeDistriChoices.find("input:checked").each(function () {
			var key = $(this).attr("name");
			if(key && datasets2_txpwr_all[key])
				data.push(datasets2_txpwr_all[key]);
		});

		if(data.length > 0){
			if(select_x_from2 != 0.0 || select_x_to2 != 0.0)
			{
				$.plot($("#placeholder2"), data, $.extend(true, {}, options2, {xaxis: { min: select_x_from2, max: select_x_to2 }}));
				$(window).resize(function() { $.plot($("#placeholder2"), data, $.extend(true, {}, options2, {xaxis: { min: select_x_from2, max: select_x_to2 }})); });
			}
			else
			{
				$.plot($("#placeholder2"),data,options2);
				$(window).resize(function() {$.plot($("#placeholder2"),data,options2);} );
			}

			plotSelect2(data);
		}
	}
	plotAccordingToChoices2();

	// Tooltip
	var previousPoint = null;
	$("#placeholder2").bind("plothover", function (event, pos, item) {
		if(item)
		{
			if (previousPoint != item.dataIndex)
			{
				previousPoint = item.dataIndex;
				$("#tooltip").remove();
				var x = item.datapoint[0].toFixed(0),
					y = item.datapoint[1].toFixed(6);
				var date = new Date(parseInt(x));
					x = date.getUTCFullYear()+"/"+(date.getUTCMonth()+1)+"/"+date.getUTCDate()+" "+date.getUTCHours()+':'+date.getUTCMinutes()+':'+date.getUTCSeconds()+':'+date.getUTCMilliseconds(); //return global standard time
					showTooltip(item.pageX, item.pageY,'TIME: ' + x + ', ' + item.series.label + ': ' + y);
			}
		}
		else
		{
			$("#tooltip").remove();
			previousPoint = null;
		}
	});

	function plotSelect2(data)
	{
		$("#placeholder2").bind("plotselected", function (event, ranges) {
			// clamp the zooming to prevent eternal zoom
			if (ranges.xaxis.to - ranges.xaxis.from < 0.00001)
				ranges.xaxis.to = ranges.xaxis.from + 0.00001;
			// do the zooming
			plot = $.plot($("#placeholder2"),
						data, //[ v3, v4 ]
						$.extend(true, {}, options2, {
							xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to }
						}));
			select_x_from2 = ranges.xaxis.from;
			select_x_to2	= ranges.xaxis.to;
			$(window).resize(function() {$.plot($("#placeholder2"),
						data, //[ v3, v4 ]
						$.extend(true, {}, options2, {
							xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to }
						}));} );
		});
	}
}

function CheckSelect2()
{
	var options2 = {
		lines:	{ show: true, lineWidth: 2 },
		grid:	{ hoverable: true, clickable: false },
		xaxes:	[ { mode: 'time', tickDecimals: 0, axisLabel: 'TIME', position: 'bottom' } ],
		yaxes:	[ { position: 'left', axisLabel : "Power" ,axisLabelPadding: 1 },
				{ position: 'right', axisLabel : "UL_RSSI" ,axisLabelPadding: 1 }, ],
		legend: { backgroundOpacity: 0.2, container: "#legend2" },
		selection: { mode: "x" }
	};

	function plotAccordingToChoices2() {
		select_x_from2 	= 0.0, select_x_to2 = 0.0;
		var data = [];
		$("#both_ue_distri_choices").find("input:checked").each(function () {
			var key = $(this).attr("name");
			if(key && datasets2_txpwr_all[key])	data.push(datasets2_txpwr_all[key]);
		});
		if(data.length > 0)
		{
			$.plot($("#placeholder2"),data,options2);
			$(window).resize(function() {$.plot($("#placeholder2"),data,options2);} );
		}
	}
	plotAccordingToChoices2();
}

//////////////////////// graph2 ///////////////////////////////

var datasets3 = {}; // for function ue_report only
function ue_report(data, fap_PSC, fap_Channel)
{
	datasets3 = {};
	var tables = [], values = [];
	var dif_ue = [], dif_PSC = [];
	var true_hnb = [];
	var sets = 13;
	var i, j, k, v, temp_column, temp_time, temp_value;
	var error_table = [];
	var last_time = 0.0; 		// last point for HNB State
	var channel = [], MascUpdatedNCL_logtime = []; 	// for MascUpdatedNCL PSC
	var ue_in_872 = [];			// in 872 not in 892
	var ueState_in_892 = {};	// for function Txpwr_distribution
	var temp_hnbstate;
	var CellUpdateCause = {};	// for CellUpdate
	select_x_from3 	= 0.0, select_x_to3 = 0.0;
	currentSelected = "";

	tables.push('UeState');
	if(data['UeState'] != '')
	{	//["logtime", "IMSI", "UE_State"]
		temp_time = [], temp_column = [];
		v = data['UeState'];
		for(var key_col in v[0])	temp_column.push(key_col);// randomly pick an element and get its attributes

		j = 0;
		for(i = 0; i < v.length; i++)
		{
			v[i][temp_column[0]] = logtime_transform(v[i][temp_column[0]]);
			temp_time.push(v[i][temp_column[0]]);

			/* IMSI in UeState */
			/* IMSI is in 'dif_ue', then put it in 'dif_ue' & create its array */
			if( dif_ue.indexOf( v[i][temp_column[1]] ) < 0 )
			{
				dif_ue.push(v[i][temp_column[1]]);

				/* dynamically create 12 arrays according to the number of UE, I can use 'array object' but need to transfer into normal array again so... */
				values[j] = [];	// for UE x array+0 data structure : [logtime, UE_State]
				j++;
				values[j] = [];	// for UE x array+1 data structure : [logtime, Capped_TxPower] logtime is different from below
				j++;
				values[j] = [];	// for UE x array+2 data structure : [logtime, RSCP_FAP]
				j++;
				values[j] = [];	// for UE x array+3 data structure : [logtime, EcNo_FAP]
				j++;
				values[j] = [];	// for UE x array+4 data structure : [logtime, RSCP_Macro]
				j++;
				values[j] = [];	// for UE x array+5 data structure : [logtime, EcNo_Macro]
				j++;
				values[j] = [];	// for UE x array+6 data structure : [logtime, HNB_State]
				j++;
				values[j] = [];	// for UE x array+7 data structure : [logtime, PSC]
				j++;
				values[j] = [];	// for UE x array+8 data structure : [logtime] Trigger_HO
				j++;
				values[j] = [];	// for UE x array+9 data structure : [logtime, Inactive] for PowerOff_Duration
				j++;
				values[j] = [];	// for UE x array+10 data structure : [logtime] for Timer41_Expired
				j++;
				values[j] = [];	// for UE x array+11 data structure : [logtime] for CellUpdate
				j++;
				values[j] = [];	// for UE x array+12 data structure : [logtime] for Timer33_Expired
				j++;
			}
			if(v[i][temp_column[2]] == 4) v[i][temp_column[2]] = 0;
			else v[i][temp_column[2]] = 3 - v[i][temp_column[2]];
			values[dif_ue.indexOf( v[i][temp_column[1]] )*sets].push([ v[i][temp_column[0]], v[i][temp_column[2]] ]); // UE_State
		}

		temp_time.sort(function(a,b){return b-a});
		if(temp_time[0] > last_time) last_time = temp_time[0];

		/* add UE_state points to array(values[sets*j+0]) of UE_State table , not in database! */
		for(j = 0; j < dif_ue.length; j++)
		{
			temp_value = [];
			for(i = 0; i < values[sets*j].length - 1; i++)
			{
				temp_value.push(values[sets*j][i]);
				if(values[sets*j][i][1] != values[sets*j][i+1][1])
				{
					temp_value.push( [values[sets*j][i+1][0]-1, values[sets*j][i][1]] );
				}
			}
			if(temp_value.length > 0) temp_value.push(values[sets*j][values[sets*j].length-1]);
			else temp_value.push(values[sets*j][0]); // if there's only one log in that UE, then just put it in the array directly
			values[sets*j] = temp_value;

			ueState_in_892[dif_ue[j]] = temp_value;	// for function Txpwr_distribution
		}
	}
	else error_table.push('UeState');

	tables.push('UeIntraMRM');
	if(data['UeIntraMRM'] != '')
	{	// ["logtime","IMSI","RSCP","EcNo","PSC"]
		temp_time = [], temp_column = [];
		v = data['UeIntraMRM'];
		for(var key_col in v[0]) temp_column.push(key_col);// randomly pick an element and get its attributes

		for(i = 0; i < v.length; i++)
		{
			v[i][temp_column[0]] = logtime_transform(v[i][temp_column[0]]);
			temp_time.push(v[i][temp_column[0]]);

			/* IMSI in UeIntraMRM */
			if(dif_ue == "" || dif_ue.indexOf( v[i][temp_column[1]] ) < 0)
			{
				dif_ue.push(v[i][temp_column[1]]);
				if(values != '')
				{
					jj = values.length;
				}
				else
				{
					jj = 0;
				}
				values[jj] = [];	// for UE x array+0 data structure : [logtime, UE_State]
				jj++;
				values[jj] = [];	// for UE x array+1 data structure : [logtime, Capped_TxPower] logtime is different from below
				jj++;
				values[jj] = [];	// for UE x array+2 data structure : [logtime, RSCP_FAP]
				jj++;
				values[jj] = [];	// for UE x array+3 data structure : [logtime, EcNo_FAP]
				jj++;
				values[jj] = [];	// for UE x array+4 data structure : [logtime, RSCP_Macro]
				jj++;
				values[jj] = [];	// for UE x array+5 data structure : [logtime, EcNo_Macro]
				jj++;
				values[jj] = [];	// for UE x array+6 data structure : [logtime, HNB_State]
				jj++;
				values[jj] = [];	// for UE x array+7 data structure : [logtime, PSC]
				jj++;
				values[jj] = [];	// for UE x array+8 data structure : [logtime] Trigger_HO
				jj++;
				values[jj] = [];	// for UE x array+9 data structure : [logtime, Inactive] for PowerOff_Duration
				jj++;
				values[jj] = [];	// for UE x array+10 data structure : [logtime] for Timer41_Expired
				j++;
				values[jj] = [];	// for UE x array+11 data structure : [logtime] for CellUpdate
				j++;
				values[jj] = [];	// for UE x array+12 data structure : [logtime] for Timer33_Expired
				j++;
				if( ue_in_872.indexOf( v[i][temp_column[1]] ) < 0 ) ue_in_872.push(v[i][temp_column[1]]);	// in 892 not in 872
			}

			/* get different PSC, and put data in corresponding array */
			if( dif_PSC.indexOf( v[i][temp_column[4]] ) < 0 )	dif_PSC.push(v[i][temp_column[4]]);
			if(v[i][temp_column[4]] == fap_PSC)
			{	// FAP's RSCP & EcNo
				values[dif_ue.indexOf( v[i][temp_column[1]] )*sets + 2].push([ v[i][temp_column[0]], v[i][temp_column[2]] ]);
				values[dif_ue.indexOf( v[i][temp_column[1]] )*sets + 3].push([ v[i][temp_column[0]], v[i][temp_column[3]] ]);
			}
			else
			{
				if(v[i][temp_column[4]] == "101")
				{	// FAP's RSCP & EcNo
					values[dif_ue.indexOf( v[i][temp_column[1]] )*sets + 2].push([ v[i][temp_column[0]], v[i][temp_column[2]] ]);
					values[dif_ue.indexOf( v[i][temp_column[1]] )*sets + 3].push([ v[i][temp_column[0]], v[i][temp_column[3]] ]);
				}
				else
				{	// others RSCP & EcNo
					values[dif_ue.indexOf( v[i][temp_column[1]] )*sets + 4].push([ v[i][temp_column[0]], v[i][temp_column[2]] ]);
					values[dif_ue.indexOf( v[i][temp_column[1]] )*sets + 5].push([ v[i][temp_column[0]], v[i][temp_column[3]] ]);
				}
			}
		}
		//FAP_RSCP,FAP_EcNo,Macro_RSCP,Macro_EcNo

		temp_time.sort(function(a,b){return b-a});
		if(temp_time[0] > last_time)	last_time = temp_time[0];

		if(ue_in_872 != '')
		{
			for(i = 0; i < ue_in_872.length; i++)
			{
				values[dif_ue.indexOf( ue_in_872[i] )*sets].push([temp_time[temp_time.length-1],3]);
				values[dif_ue.indexOf( ue_in_872[i] )*sets].push([temp_time[0],3]);
			}
		}
	}
	else error_table.push('UeIntraMRM');

	for(var key in data)
	{
		/* get key name(table module number) and corresponding column name&values in object data */
		temp_time = [];
		tables.push(key);
		v = data[key]; // v is an array object

		/* get corresponding column in each table */
		temp_column = [];
		for(var key_col in v[0]) temp_column.push(key_col);	// randomly pick an element and get its attributes

		/* get data and do some process to draw graph. */
		switch(key)
		{
			case "ClipUeTxPower":
				if(data[key] != ''  && dif_ue != '')
				{	// ["logtime","IMSI","Capped_TxPower"]]
					for(i = 0; i < v.length; i++)
					{
						v[i][temp_column[0]] = logtime_transform(v[i][temp_column[0]]);
						temp_time.push(v[i][temp_column[0]]);
						if(dif_ue != "" && dif_ue.indexOf(v[i][temp_column[1]])>=0)
							values[dif_ue.indexOf( v[i][temp_column[1]] )*sets+1].push([ v[i][temp_column[0]], v[i][temp_column[2]] ]); // TxPower
					}

					temp_time.sort(function(a,b){return b-a});
					if(temp_time[0] > last_time) last_time = temp_time[0];
				}
				if(data[key] == '')	error_table.push(key);
				break;

			case "HNBState":
				if(data[key] != ''  && dif_ue != "")
				{	/* hnb state will be the same in each ue. */
					for(i = 0; i < v.length; i++)
					{
						v[i][temp_column[0]] = logtime_transform(v[i][temp_column[0]]);
						temp_time.push(v[i][temp_column[0]]);

						for(k = 0; k < dif_ue.length; k++)
						{
							// values[sets*k+6].push([ v[i][temp_column[0]], v[i][temp_column[1]] ]); //[logtime,HNB_number]
							switch(v[i][temp_column[1]])
							{
								case "3":
									values[sets*k+6].push([ v[i][temp_column[0]], 1 ]);
									true_hnb.push("Active");
									break;

								default:
									values[sets*k+6].push([ v[i][temp_column[0]], 0 ]);
									true_hnb.push("Inactive");
									break;
							}
						}
					}

					temp_time.sort(function(a,b){return b-a});
					if(temp_time[0] > last_time) last_time = temp_time[0];

					/* add HNB_State points to array(values[sets*j+6]), not in database! */
					j = 0;
					{
						temp_value = [], temp_true = [];
						for(i = 0; i < values[sets*j+6].length - 1; i++)
						{
							temp_value.push(values[sets*j+6][i]);
							if(j == 0)	temp_true.push(true_hnb[i]);
							if(values[sets*j+6][i][1] != values[sets*j+6][i+1][1])
							{
								temp_value.push( [values[sets*j+6][i+1][0]-1, values[sets*j+6][i][1]] );
								if(j == 0)	temp_true.push(true_hnb[i]);
							}
						}

						if(temp_value.length > 0)
						{
							temp_value.push(values[sets*j+6][values[sets*j+6].length-1]);
							if(j == 0)	temp_true.push(true_hnb[true_hnb.length-1]);
						}
						else
						{	// if there's only one log in HNB array, then just put it in the array directly
							temp_value.push(values[sets*j+6][0]);
							if(j == 0)	temp_true.push(true_hnb[0]);
						}
						values[sets*j+6] = temp_value;
						if(j == 0)	true_hnb = temp_true;

						// get the last point of all data, and use its logtime as the x value of the last point of HNB_State
						values[sets*j+6].push([ last_time, values[sets*j+6][values[sets*j+6].length-1][1] ]);
						if(j == 0)	true_hnb.push(true_hnb[true_hnb.length-1]);
					}
					// console.log(temp_value);
					for(j = 1; j < dif_ue.length; j++)
						values[sets*j+6] = temp_value;
					temp_hnbstate = temp_value;
				}

				if(data[key] == '')	error_table.push(key);
				break;

			case "MascUpdatedNCL": 	// Discovered_Cell
				if(data[key] != '')
				{	//["logtime","Channel","PSC","Status"]
					for(i = 0; i < v.length; i++)
					{
						if(v[i][temp_column[3]] == "2")
						{
							MascUpdatedNCL_logtime.push(v[i][temp_column[0]]);
							v[i][temp_column[0]] = logtime_transform(v[i][temp_column[0]]);
							values[7].push([ v[i][temp_column[0]], v[i][temp_column[2]] ]);	// logtime, PSC
							channel.push(v[i][temp_column[1]]);	// Channel
						}
					}

					if(values[7] != "")	// assign each ue the same HNB state
					{
						for(j = 1; j < dif_ue.length; j++)
							values[sets*j+7] = values[7];
					}
				}
				else	error_table.push(key);
				break;

			case "HO":
				if(data[key] != '')
				{
					for(i = 0; i < v.length; i++)
					{
						v[i][temp_column[0]] = logtime_transform(v[i][temp_column[0]]);
						if(dif_ue != "" && dif_ue.indexOf(v[i][temp_column[1]])>=0)
							values[dif_ue.indexOf( v[i][temp_column[1]] )*sets+8].push([ v[i][temp_column[0]], 3 ]);
					}
				}
				else	error_table.push(key);	// send error msg if not getting data from the table in db
				break;

			case "PowerOff_Duration":
				var add_pt;
				if(data[key] != '')
				{
					temp_value = [];
					for(i = 0; i < v.length; i++)
					{
						end = logtime_transform(v[i][temp_column[0]]); // end time
						if(v[i][temp_column[1]] !== "n/a")
						{
							start = logtime_transform(v[i][temp_column[1]]); // start time

							// add points here bewteen start time v[i][temp_column[1]] and end time v[i][temp_column[0]]
							temp_milli = start;
							while(temp_milli < end)
							{
								temp_value.push( [ temp_milli, 0 ] );

								add_pt = new Date(temp_milli);
								temp = dateAdd(add_pt, 'minute', 10);	// add 10 minutes
								temp_milli = temp.getTime();
							}
						}
						temp_value.push([ end, 0 ]);
					}

					for(j = 0; j < dif_ue.length; j++)
						values[sets*j+9] = temp_value;
				}
				else	error_table.push(key);	// send error msg if not getting data from the table in db
				break;

			case "Timer41_Expired":
				if(data[key] != '')
				{
					temp_value = [];
					for(i = 0; i < v.length; i++)
					{
						v[i][temp_column[0]] = logtime_transform(v[i][temp_column[0]]);
						temp_value.push([ v[i][temp_column[0]], 1 ]);
					}

					for(j = 0; j < dif_ue.length; j++)
						values[sets*j+10] = temp_value;
				}
				else	error_table.push(key);	// send error msg if not getting data from the table in db
				break;

			case "CellUpdate":
				if(data[key] != '')
				{
					temp_value = [];
					CellUpdateCause = [];
					for(i = 0; i < v.length; i++)
					{
						v[i][temp_column[0]] = logtime_transform(v[i][temp_column[0]]);
						temp_value.push([ v[i][temp_column[0]], 2 ]);
						CellUpdateCause.push( v[i][temp_column[1]] );
					}

					for(j = 0; j < dif_ue.length; j++)
						values[sets*j+11] = temp_value;
				}
				else	error_table.push(key);
				break;

			case "Timer33_Expired":
				if(data[key] != '')
				{
					temp_value = [];
					for(i = 0; i < v.length; i++)
					{
						v[i][temp_column[0]] = logtime_transform(v[i][temp_column[0]]);
						temp_value.push([ v[i][temp_column[0]], 0 ]);
					}

					for(j = 0; j < dif_ue.length; j++)
						values[sets*j+12] = temp_value;
				}
				else	error_table.push(key);	// send error msg if not getting data from the table in db
				break;
		}
	}

	/* insert data into dataset */
	// "RSCP_FAP","RSCP_Macro","EcNo_FAP","EcNo_Macro","Capped_TxPower","HNB_State","UE_State","PSC","Trigger_HO","PowerOff_Duration","Timer41_Expired","CellUpdate","Timer41_Expired"
	var set1,set2,set3,set4,set5,set6,set7,set8,set9,set10,set11,set12,set13;
	for(i = 0; i < dif_ue.length; i++)
	{
		set1={};set2={};set3={};set4={};set5={},set6={},set7={},set8={},set9={},set10={},set11={},set12={},set13={};

		set1['label']	= "FAP_RSCP";//columns[2];	// FAP_RSCP
		set1['data']	= values[i*sets + 2];
		set1['points']	= { show: true, radius: 2, symbol: symbols[4] };
		set1['yaxis']	= 1;
		set1['color']	= 2;	//red

		set2['label']	= "FAP_EcNo";//columns[3];	// FAP_EcNo
		set2['data']	= values[i*sets + 3];
		set2['points']	= { show: true, radius: 2, symbol: symbols[0] };
		set2['yaxis']	= 2;
		set2['color']	= 3;	//green

		set3['label']	= "Macro_RSCP";				// Macro_RSCP
		set3['points']	= { show: true, radius: 2, symbol: symbols[4] };
		set3['data']	= values[i*sets + 4];
		set3['yaxis']	= 1;
		set3['color']	= 4;	// purple

		set4['label']	= "Macro_EcNo";				// Macro_EcNo
		set4['data']	= values[i*sets + 5];
		set4['points']	= { show: true, radius: 2, symbol: symbols[0] };
		set4['yaxis']	= 2;
		set4['color']	= 6;	//blue

		set5['label']	= "Capped_TxPower";
		set5['data']	= values[i*sets + 1];		// Capped_TxPower
		set5['points']	= { show: true, radius: 4, symbol: symbols[3] };
		set5['yaxis']	= 2;
		set5['color']	= 5;

		set6['label']	= "HNB_State";				// HNB_State
		set6['data']	= values[i*sets + 6];
		set6['lines']	= { show: true, fill: false };
		set6['points']	= { show: false, radius: 2 };
		set6['yaxis']	= 4;
		set6['color']	= "#00A2E8";	// 1 light blue

		set7['label']	= "UE_State";				// UE_State
		set7['data']	= values[i*sets];
		set7['lines']	= { show: true, fill: false };
		set7['points']	= { show: false, radius: 2 };
		if(values[i*sets].length == 1)
			set7['points']	= { show: true, radius: 5, fill: true, symbol: symbols[3] };
		set7['yaxis']	= 3;
		set7['color']	= 0;	// yellow

		set8['label']	= "Discovered_Cell";		// PSC, Discovered_Cell
		set8['data']	= values[i*sets+7];
		set8['points']	= { show: true, fill: true, radius: 3, symbol: symbols[1] };// radius 5
		set8['yaxis']	= 5;
		set8['color']	= "#F50AC7";	// pink

		set9['label']	= "Trigger_HO";				// Trigger_HO
		set9['data']	= values[i*sets+8];
		set9['lines']	= { show: false, lineWidth: 5 };
		set9['points']	= { show: true, radius: 3, fill: true, symbol: symbols[2] },
		set9['yaxis']	= 3;
		set9['type']	= "Trigger_HO";
		set9['color']	= "#7F7F7F";	// gray

		set10['label']	= "PowerOff_Duration";		// PowerOff_Duration
		set10['data']	= values[i*sets+9];
		set10['lines']	= { show: false, fill: false };
		set10['points']	= { show: true, radius: 2, fill: true, symbol: symbols[0], fillColor: "#A23C3C" };
		set10['yaxis']	= 4;
		set10['type']	= "PowerOff_Duration";
		set10['color']	= 7;	//=#A23C3C

		set11['label']	= "Timer41_Expired";		// Timer41_Expired
		set11['data']	= values[i*sets+10];
		set11['lines']	= { show: false, lineWidth: 5 };
		set11['points']	= { show: true, radius: 3, fill: true, symbol: symbols[2] },
		set11['yaxis']	= 3;
		set11['type']	= "Timer41_Expired";
		set11['color']	= 8;

		set12['label']	= "CellUpdate";				// CellUpdate
		set12['data']	= values[i*sets+11];
		set12['lines']	= { show: false, lineWidth: 5 };
		set12['points']	= { show: true, radius: 3, fill: true, symbol: symbols[1] },
		set12['yaxis']	= 3;
		set12['type']	= "CellUpdate";
		set12['color']	= 10;

		set13['label']	= "Timer33_Expired";		// Timer33_Expired
		set13['data']	= values[i*sets+12];
		set13['lines']	= { show: false, lineWidth: 5 };
		set13['points']	= { show: true, radius: 3, fill: true, symbol: symbols[2] },
		set13['yaxis']	= 4;
		set13['type']	= "Timer33_Expired";
		set13['color']	= 12;
		//[Macro_RSCP,Macro_EcNo,FAP_RSCP,FAP_EcNo,Capped_TxPower,UE_State,HNB_State,Discovered_Cell,Trigger_HO,PowerOff_Duration,Timer41_Expired,CellUpdate,Timer33_Expired]
		datasets3[dif_ue[i]] = [set3,set4,set1,set2,set5,set6,set7,set8,set9,set10,set11,set12,set13];
	}

	var options3 = {
		points:	{ show: true, radius: 1 },
		grid:	{ hoverable: true, clickable: false },
		xaxes:	[ { mode: 'time', tickDecimals: 0, axisLabel: 'TIME', position: 'bottom' } ],
		yaxes: [{ position: 'left', axisLabel: 'RSCP', axisLabelPadding: 1 },
				{ position: 'right', axisLabel: 'EcNo', axisLabelPadding: 1 },
				{ ticks: uestate_array, position: 'right', axisLabelPadding: 1 },
				{ ticks: HNB_State_array, position: 'left', axisLabelPadding: 1 },
				{ position: 'right', axisLabelPadding: 1, min: 0, max: 511 }, ],
		legend: {backgroundOpacity: 0.4, container: "#legend3" },
		shadowSize: 0,
		selection: { mode: "x" }
	};

	// insert UE checkboxes
	// Make the state checkboxes has at least one checked : chooseOne(this)
	var ueChoice3Container = $("#ue_choices3");
	$.each(datasets3, function(key, val) {
		ueChoice3Container.append('<input type="checkbox" name="' + key + '" id="' + key + '" value="'+key+'" onclick="chooseOne(this);">' +	'<label for="id' + key + '">'+ key + '</label><br>');
	});
	ueChoice3Container.find("input").click(plotAccordingToChoices3);

	// Reset Button
	$("#reset3").append('<button type="button" id="resetbtn3" name="Reset" value="Reset" class="juibtn" onclick="CheckSelect3();">Reset</button>');
	$("#resetbtn3").button();


	// insert MRM data checkboxes
	if(dif_ue != "")
	{
		var mrmChoice3Container = $("#mrm_choices3");
		for(i = 0; i < sets; i++)
		{
			mrmChoice3Container.append('<input type="checkbox" id="'+i+'" name="'+datasets3[dif_ue[0]][i]['label']+'" value="'+datasets3[dif_ue[0]][i]['label']+'" checked="checked">'+datasets3[dif_ue[0]][i]['label']+'<br>');
		}
		mrmChoice3Container.find("input").click(plotAccordingToChoices3);
	}


	// error report
	if(dif_ue == "")
	{
		$("#placeholder3").html("No UE (892 or 872), No data").css('text-align','center');
		$("#reset3").html("");
	}
	else $("#placeholder3").html("");

	if(error_table != "")
	{
		var str = "Data isn't enough in UE Report : \n";
		if(data['UeState'] == "")		str += "No UE_State (892)\n";
		if(data['UeIntraMRM'] == "")	str += "No RSCP or EcNo (872)\n";
		if(data['ClipUeTxPower'] == "")	str += "No Capped_TxPower (862)\n";
		if(data['HNBState'] == "")	str += "No HNB_State (693)\n";
		if(data['MascUpdatedNCL'] == "")	str += "No Discovered_Cell (852)\n";
		if(data['HO'] == "")	str += "No Trigger HO (803)\n";
		if(data['PowerOff_Duration'] == "")	str += "No shut-down duration (600)\n";
		if(data['Timer41_Expired'] == "")	str += "No Timer41_Expired (313)\n";
		if(data['CellUpdate'] == "")	str += "No Cell Update (313)\n";
		if(data['Timer33_Expired'] == "")	str += "No Timer33_Expired (313)\n";
		console.log(str);
	}


	// plotting
	function plotAccordingToChoices3() {
		var data = [], MRMdata = [];

		ueChoice3Container.find("input:checked").each(function () {
			var key = $(this).attr("name");
			if (key && datasets3[key])	data.push(datasets3[key]);
			$("#placeholder3_title").html("UE Report - Intra MRM from UE '" + key + "'");
		});
		if (data.length > 0)
		{
			mrmChoice3Container.find("input:checked").each(function () {
				MRMdata.push(data[0][$(this).attr("id")]);
			});

			if(select_x_from3 != 0.0 || select_x_to3 != 0.0)
			{
				$.plot($("#placeholder3"), MRMdata, $.extend(true, {}, options3, {xaxis: { min: select_x_from3, max: select_x_to3 }}));
				$(window).resize(function() {$.plot($("#placeholder3"), MRMdata, $.extend(true, {}, options3, {xaxis: { min: select_x_from3, max: select_x_to3 }}));} );
			}
			else
			{
				$.plot($("#placeholder3"),MRMdata,options3);
				$(window).resize(function() {$.plot($("#placeholder3"),MRMdata,options3);} );
			}

			plotSelect3(MRMdata);
		}
	}
	plotAccordingToChoices3();

	// Tooltip
	var previousPoint3 = null;
	$("#placeholder3").bind("plothover", function (event, pos, item) {
		if(item)
		{
			if (previousPoint3 != item.dataIndex)
			{
				previousPoint3 = item.dataIndex;
				$("#tooltip").remove();
				var x = item.datapoint[0].toFixed(0),
					y = item.datapoint[1].toFixed(6);
				var date = new Date(parseInt(x));
				x = date.getUTCFullYear()+"/"+(date.getUTCMonth()+1)+"/"+date.getUTCDate()+" "+date.getUTCHours()+':'+date.getUTCMinutes()+':'+date.getUTCSeconds()+':'+date.getUTCMilliseconds();
				switch(item.series.label)
				{
					case "HNB_State":
						y = true_hnb[parseInt(item.dataIndex)];
						showTooltip(item.pageX, item.pageY, 'TIME: ' + x + ', ' + item.series.label + ': ' + y);
						break;

					case "UE_State":
						y = uestate_array[parseInt(y)][1];
						showTooltip(item.pageX, item.pageY, 'TIME: ' + x + ', ' + item.series.label + ': ' + y);
						break;

					case "Discovered_Cell":
						y = item.datapoint[1].toFixed(0);
						z = channel[item.dataIndex];
						showTooltip(item.pageX, item.pageY,'TIME: '+ x +',<br>PSC: '+ y +", CHANNEL: " + z);
						break;

					case "Trigger_HO":
					case "Timer41_Expired":
					case "Timer33_Expired":
						y = uestate_array[parseInt(y)][1];
						showTooltip(item.pageX, item.pageY,'TIME: '+ x + ', ' + item.series.label);
						break;

					case "PowerOff_Duration":
						y = "Inactive";
						showTooltip(item.pageX, item.pageY,'TIME: '+ x +', '+ item.series.label + ': ' + y);
						break;

					case "CellUpdate":
						showTooltip(item.pageX, item.pageY, 'TIME: ' + x + ', ' + 'Cell Update Cause: ' + CellUpdateCause[item.dataIndex]);
						break;

					default:
						showTooltip(item.pageX, item.pageY, 'TIME: ' + x + ', ' + item.series.label + ': ' + y);
				}
			}
		}
		else
		{
			$("#tooltip").remove();
			previousPoint3 = null;
		}
	});

	function plotSelect3(v)
	{
		$("#placeholder3").bind("plotselected", function (event, ranges) {
			// clamp the zooming to prevent eternal zoom
			if (ranges.xaxis.to - ranges.xaxis.from < 0.00001)
				ranges.xaxis.to = ranges.xaxis.from + 0.00001;

			// do the zooming
			plot = $.plot($("#placeholder3"),
						// [ v1, v2, v3, v4, v5, v6, v7 ],
						v,
						$.extend(true, {}, options3, {
							xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to }
						}));
			select_x_from3 = ranges.xaxis.from;
			select_x_to3	= ranges.xaxis.to;

			$(window).resize(function() {$.plot($("#placeholder3"),
						// [ v1, v2, v3, v4, v5, v6, v7 ],
						v,
						$.extend(true, {}, options3, {
							xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to }
						}));} );
		});
	}

	return {uestate892: ueState_in_892, hnbstate: temp_hnbstate};
}
function CheckSelect3()
{
	var options3 = {
		points:	{ show: true, radius: 1 },
		grid:	{ hoverable: true, clickable: false },
		xaxes:	[ { mode: 'time', tickDecimals: 0, axisLabel: 'TIME', position: 'bottom' } ],
		yaxes: [{ position: 'left', axisLabel: 'RSCP', axisLabelPadding: 1 },
				{ position: 'right', axisLabel: 'EcNo', axisLabelPadding: 1 },
				{ ticks: uestate_array, position: 'right', axisLabelPadding: 1 },
				{ ticks: HNB_State_array, position: 'left', axisLabelPadding: 1 },
				{ position: 'right', axisLabelPadding: 1, min: 0, max: 511 }, ],
		legend: {backgroundOpacity: 0.4, container: "#legend3" },
		shadowSize: 0,
		selection: { mode: "x" }
	};

	function plotAccordingToChoices3() {
		var data = [], MRMdata = [];

		select_x_from3 	= 0.0, select_x_to3 = 0.0;

		$("#ue_choices3").find("input:checked").each(function () {
			var key = $(this).attr("name");
			if (key && datasets3[key])
				data.push(datasets3[key]);
			$("#placeholder3_title").html("Intra MRM from UE '" + key + "'");
		});
		if (data.length > 0)
		{
			$("#mrm_choices3").find("input:checked").each(function () {
				MRMdata.push(data[0][$(this).attr("id")]);
			});
			$.plot($("#placeholder3"),MRMdata,options3);
			$(window).resize(function() { $.plot($("#placeholder3"),MRMdata,options3); } );
		}
	}
	plotAccordingToChoices3();
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
