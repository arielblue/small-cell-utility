function init()
{

	$('input[type!="button"][type!="submit"], select, textarea').val('').blur();

	/** restrict within 7 days **/
	var startDateTextBox = $("#datepicker_start");
	var endDateTextBox = $("#datepicker_end");
	startDateTextBox.datetimepicker({
		showTimepicker: false,
		dateFormat : "yy-mm-dd",
		maxDate: '0',
		onClose: function(dateText, inst) {
			var today = $.datepicker.formatDate('yy-mm-dd', new Date() );
			var startDate = startDateTextBox.datetimepicker('getDate');
			var testEndDate = $.datepicker.formatDate('yy-mm-dd', dateAdd(startDate, 'day', 6) );

			if(testEndDate > today) testEndDate = today;
			endDateTextBox.val( testEndDate );

			if(startDate == null) $.datepicker._clearDate('#datepicker_end');
		}
	});
	endDateTextBox.datetimepicker({
		showTimepicker: false,
		dateFormat : "yy-mm-dd",
		maxDate: '0',
		onClose: function(dateText, inst) {
			var startDate = $("#datepicker_start").datetimepicker('getDate');
			var endDate = endDateTextBox.datetimepicker('getDate');
			var testStartDate = $.datepicker.formatDate('yy-mm-dd', dateAdd(endDate, 'day', -6) );

			if($.datepicker.formatDate('yy-mm-dd', startDate ) < testStartDate)
				startDateTextBox.val( testStartDate );
			if($.datepicker.formatDate('yy-mm-dd', endDate ) < $.datepicker.formatDate('yy-mm-dd', startDate) )
				startDateTextBox.val( testStartDate );

			if(endDate == null) $.datepicker._clearDate('#datepicker_start');
		}
	});

	$("#SubmitBtn").button();
	submitAction();
}

function submitAction()
{
	$("#SubmitBtn").click( function(){

		$("#bottom_msg").html("");
		$("#mid_content").html("");
		$("#record").html("");
		$("#record_h2").html("");

		var startDate = $("#datepicker_start").val();
		var endDate = $("#datepicker_end").val();
		var version = msieversion();
		var clearedMode = $('#clearedMode').is(':checked');

		if(startDate !== "" && endDate !== "")
		{
			$("#mid_content").html("wait please...");
			var path = getBaseURL()+getProjectName()+'/index.php/alarm/get_alarm_report';
			var url = "http://"+getHost()+":8080/LogAlarmReportService/logAlarmReportAction";

			$("#SubmitBtn").attr('disabled','disabled');
			jQuery.ajax({
				url: path,
				type: 'POST',
				data: { startDate: startDate, endDate: endDate, version: version, url: url, clearedMode: clearedMode },//JSON.stringify({ index: index }),

				dataType: 'JSON',
				error: function(xhr, ajaxOptions, thrownError, error) {
					$("#SubmitBtn").removeAttr('disabled');
					console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
				},
				success: function(xhr, XMLHttpRequest, textStatus) {
					$("#SubmitBtn").removeAttr('disabled');

					show_alarmReport(xhr, version, clearedMode);
				}
			});
		}
		else
		{
			$().tostie({type:"error", message: "Start time or End time should not be empty."});
		}
	});
}

function show_alarmReport(data, version, clearedMode)
{
	var record = data['Record'];
	var report = data['Report'];
	var size = Object.size(report);
	console.log(data, size, "record",record, "report",report, version);

	if(size != 0)
	{
		var date = Object.keys(report);
		date.sort();
		var report_key = Object.keys(report[date[0]]);
		report_key.sort();

		var buf = "<br><div class='reportRawData_div'><table id='reportRawData_tbl'><tr><th>Type</th>";
		for(var i = 0; i < date.length; i++)
			buf += "<th>"+date[i]+"</th>";

		for(var i = 0; i < report_key.length; i++)
		{
			buf += "<tr><td>"+report_key[i]+"</td>";
			for(var j = 0; j < date.length; j++)
			{
				if(report[ date[j] ][ report_key[i] ] != 0)
					buf += "<td class='td_selected'>"+report[ date[j] ][ report_key[i] ]+"</td>";
				else
					buf += "<td>"+report[ date[j] ][ report_key[i] ]+"</td>";
			}
			buf += "</tr>";
		}

		buf += "</table></div>";
		$("#mid_content").html(buf);

		var last_selected1; // remember last selected cell
		$(".td_selected").click( function(){
			var type = $(this).siblings("td:first").text(),
				selected_date = $('.reportRawData_div tr:first th').eq($(this).index()).text();

			/** highlight selected table cell **/
			if(last_selected1 != undefined) last_selected1.removeClass('selected1');
			last_selected1 = $(this);
			$(this).addClass('selected1');

			if(clearedMode == true)
				show_alarmRecord(type, selected_date, record[selected_date][type], clearedMode);
			else
				get_alarmRecord(type, selected_date, clearedMode);
		});
	}
	else
	{
		$("#mid_content").html("NO DATA");
	}
}

function show_alarmRecord(type, date, record, clearedMode)
{
	console.log(type, date, record);
	$("#record_h2").html("Record of "+type+" on "+date);

	var buf = "<div class='reportRawData_div'><table><tr><th>HNB</th><th>Log Time</th><th>Event</th></tr>";
	if(clearedMode == true)
	{
		for(var i = 0; i < record.length; i++)
		{
			var temp = record[i];
			temp = temp.split("|");

			buf += "<tr>";

			for(var j = 0; j < temp.length; j++)
				buf += "<td>"+temp[j]+"</td>";
			buf += "</tr>";
		}
		buf += "</table></div>";
	}
	else
	{
		for(var i = 0; i < record.length; i++)
		{
			var temp = record[i];
			buf += "<tr><td>"+temp["hnbMAC"]+"</td><td>"+temp["logTime"]+"</td><td>"+temp["alarmEvents"]+"</td></tr>";
		}
		buf += "</table></div>";
	}
	$("#record").html(buf);
	$('#record').ScrollTo();
}

function get_alarmRecord(type, date, clearedMode)
{
	$("#bottom_msg").html("");
	$("#record").html("");
	$("#record_h2").html("");

	var version = msieversion();
	var path = getBaseURL()+getProjectName()+'/index.php/alarm/get_alarm_record';

	$("#SubmitBtn").attr('disabled','disabled');
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { date: date, type: type, version: version },
		dataType: 'JSON',
		error: function(xhr, ajaxOptions, thrownError, error) {
			$("#SubmitBtn").removeAttr('disabled');
			console.log("error!!!", xhr, ajaxOptions, thrownError, error, xhr.status);
		},
		success: function(xhr, XMLHttpRequest, textStatus) {
			$("#SubmitBtn").removeAttr('disabled');
			show_alarmRecord(type, date, xhr, clearedMode);
		}
	});
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};