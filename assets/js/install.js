
function setupdb()
{
	var path = getRootPath()+'/setup_database/';
	console.log("oops, test, just in case ruin everything.");
}

function setup_logdb()
{
	var path = getRootPath()+'/setup_logdb/';
	console.log(path);

	$("#tool_faplog").attr('disabled','disabled');
	jQuery.ajax({
		cache: false,
		assync: false,
		url: path,
		dataType: 'text',
		error: function (XMLHttpRequest) {
			$("#tool_faplog").removeAttr('disabled');
			$(".msg").html(XMLHttpRequest).fadeIn();
		},
		success: function (data) {
			$("#tool_faplog").removeAttr('disabled');
			$(".msg").html(data);
		}
	});
}
