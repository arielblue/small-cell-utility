
function kpiWarningInit()
{
	$('#allWarn').click( function (event) {
		var path = getRootPath().replace("kpi_warning","kpi_allwarning");
		window.location = path;
	});

	initiateDoneBtn();
	initiateSelectAllCheck();
}

function kpiAllWarningInit()
{
	$('#unproWarn').click( function (event) {
		var path = getBaseURL()+getProjectName()+"/index.php/kpi_warning";
		window.location = path;
	});
	$("#kpiSelect").multiselect();

	setDateBox();

	$("#SearchAllBtn").button().click( function (event) {
		var path = getBaseURL()+getProjectName()+"/index.php/kpi_allwarning";
		window.location = path;
	});

	searchAllWarningInit();
}

function alarmWarningInit()
{
	$('#alarmAllWarn').click( function (event) {
		var path = getRootPath().replace("alarm_warning","alarm_allwarning");
		window.location = path;
	});

	initiateSelectAllCheck();
	initiateAlarmUpdateBtn();
}

function alarmAllWarningInit()
{
	// isWarnProcessed();
	$('#unproAlarmWarn').click( function (event) {
		var path = getBaseURL()+getProjectName()+"/index.php/alarm_warning";
		window.location = path;
	});
}

// warning //////////////////////////////////////////////////////////////

// function selectWarning()
// {
// 	$("#kpiwarning").click( function (event) {
// 		console.log("kpiwarning");
// 	});

// 	$("#alarmwarning").click( function (event) {
// 		console.log("alarmwarning");
// 	});
// }

// unprocessed kpi warning //////////////////////////////////////////////

function updateWarning(data)
{
	console.log(data, typeof(data));
	var path = getBaseURL()+getProjectName()+"/index.php/kpi_warning/updateWarning";
	console.log(path);
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { data: JSON.stringify(data) },
		dataType: 'text',
		error: function(xhr, ajaxOptions, thrownError){
			console.log("error!", xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, ajaxOptions, thrownError){
			console.log(xhr);
			var path = getBaseURL()+getProjectName()+"/index.php/kpi_warning";
			window.location = path;
		}
	});
}

function initiateDoneBtn()
{
	/**  done button event **/
	$("#doneBtn").button().click( function (event) {
		event.preventDefault();
		var checkboxes = $("#list_table").find(".myCheckbox:checked");
		var updatedItem = [];
		var temp = [];
		if(checkboxes.length > 0)
		{
			for(i = 0; i < checkboxes.length; i++)
			{
				temp = {};
				j = parseInt(checkboxes[i].id.substr(10));
				temp["hnbMAC"] = $("#hnbMAC"+j).text();
				temp["endtime"] = $("#endtime"+j).text();
				temp["kpiName"] = $("#kpiName"+j).attr("value");
				temp["kpiTarget"] = $("#kpiTarget"+j).text();
				temp["note"] = $("#note"+j).val();
				updatedItem.push(temp);
			}
			updateWarning(updatedItem);
		}
		else
			$("#bottom_msg").html("<font color='red'>Select something please.</font>").fadeIn().delay(4000).fadeOut();
	});
}

function initiateSelectAllCheck()
{
	/** select all checkboxes **/
	$('#selectall').click( function (event) {
		if(this.checked) { // check select status
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = true;  //select all checkboxes with class "myCheckbox"              
			});
		}else{
			$('.myCheckbox').each(function() { //loop through each checkbox
				this.checked = false; //deselect all checkboxes with class "myCheckbox"                      
			});
		}
	});
}

// all kpi warning //////////////////////////////////////////////

function searchAllWarningInit()
{
	$("#SearchBtn").bind("click", function() {
		$("#errorMsg").text("");

		var kpiName = $("#kpiSelect option:selected").map(function(){ return this.value }).get();
		var startTime = $("#datepicker_start").val();
		var endTime = $("#datepicker_end").val();
		var timeConstraint = (startTime == "" || endTime == "");
		var kpiConstraint = (kpiName == "" || kpiName == null);

		if( timeConstraint && kpiConstraint )
		{
			$().tostie({type:"error", message:"Set constraints please."});
			if (timeConstraint)
			{
				$().tostie({type:"error", message:"Set both Start Time & End Time  if you need time constraint."});
			}

			return false;
		}
		else
		{
			if ((startTime == "") != (endTime == "")) // JS XOR
			{
				$().tostie({type:"error", message:"Set both Start Time & End Time  if you need time constraint."});
				return false;
			}
			else
			{
				if(!kpiConstraint)
				{
					$('#kpiInsert').attr('value', kpiName);
				}
				$('#searchForm').submit();
			}
		}
	});
}

function fakeLink()
{
	$('#searchForm').submit();
}

// unprocessed alarm warning //////////////////////////////////////////////

function initiateAlarmUpdateBtn()
{
	/**  done button event **/
	$("#AlarmUpdateBtn").button().click( function (event) {
		event.preventDefault();
		var checkboxes = $("#list_table").find(".myCheckbox:checked");
		var updatedItem = [];
		var temp = [];
		if(checkboxes.length > 0)
		{
			for(i = 0; i < checkboxes.length; i++)
			{
				temp = {};
				j = parseInt(checkboxes[i].id.substr(10));
				temp["hnbMAC"] = $("#hnbMAC"+j).text();
				temp["logTime"] = $("#logTime"+j).text();
				temp["note"] = $("#note"+j).val();
				updatedItem.push(temp);
			}
			console.log(updatedItem);
			updateAlarmWarning(updatedItem);
		}
		else
			$("#bottom_msg").html("<font color='red'>Select something please.</font>").fadeIn().delay(4000).fadeOut();
	});
}

function updateAlarmWarning(data)
{
	console.log(data, typeof(data));
	var path = getBaseURL()+getProjectName()+"/index.php/alarm_warning/updateAlarmWarning";
	console.log(path);
	jQuery.ajax({
		url: path,
		type: 'POST',
		data: { data: JSON.stringify(data) },
		dataType: 'text',
		error: function(xhr, ajaxOptions, thrownError){
			console.log("error!", xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, ajaxOptions, thrownError){
			console.log(xhr);
			var path = getBaseURL()+getProjectName()+"/index.php/alarm_warning";
			window.location = path;
		}
	});
}

// all alarm warning //////////////////////////////////////////////