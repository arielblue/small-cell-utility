function OpenNewTab(id)
{
	if(id == "")
	{	// for warning_view
		var path = getBaseURL()+getProjectName()+'/index.php/log';
		setTimeout(function(){ window.location.href = path; }, 0);
	}

	switch (id)
	{
		case 1:
			var path = getBaseURL()+getProjectName()+'/index.php/configtool';
			setTimeout(function(){ window.location.href = path; }, 0);
			break;

		case 2:
			var path = getBaseURL()+getProjectName()+'/index.php/counter_report';
			setTimeout(function(){ window.location.href = path; }, 0);
			break;

		case 3:
			var path = getBaseURL()+getProjectName()+'/index.php/kpi';
			setTimeout(function(){ window.location.href = path; }, 0);
			break;

		case 4:
			var path = getBaseURL()+getProjectName()+'/index.php/warning';
			setTimeout(function(){ window.location.href = path; }, 0);
			break;

		case 5:
			var path = getBaseURL()+getProjectName()+'/index.php/kpi_report';
			setTimeout(function(){ window.location.href = path; }, 0);
			break;

		case 6:
			var path = getBaseURL()+getProjectName()+'/index.php/alarm';
			setTimeout(function(){ window.location.href = path; }, 0);
			break;

		case 7:
			var path = getBaseURL()+getProjectName()+'/index.php/rf';
			setTimeout(function(){ window.location.href = path; }, 0);
			break;

		case 8:
			var path = getBaseURL()+getProjectName()+'/index.php/log';
			setTimeout(function(){ window.location.href = path; }, 0);
			break;
	}
}

function setDateBox()
{
	var startDateTextBox = $("#datepicker_start");
	var endDateTextBox = $("#datepicker_end");
	startDateTextBox.datetimepicker({
		showTimepicker: false,
		dateFormat : "yy-mm-dd",
		maxDate: '0',
		onClose: function(dateText, inst) {
			if (endDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					endDateTextBox.datetimepicker('setDate', testStartDate);
			}
			else {
				endDateTextBox.val( $.datepicker.formatDate('yy-mm-dd', new Date()) );
			}
		}
	});
	endDateTextBox.datetimepicker({
		showTimepicker: false,
		dateFormat : "yy-mm-dd",
		maxDate: '0',
		onClose: function(dateText, inst) {
			if (startDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					startDateTextBox.datetimepicker('setDate', testEndDate);
			}
			else {
				startDateTextBox.val(dateText);
			}
		}
	});
}

function get_data()
{
	update_user_rule("editform", this.document.getElementById("username").innerHTML, $("#list_table").find("td:last").attr("id"));
}

function update_user_rule(formname,username, roleId)
{
	var errorString = '';
	var validator = new FormValidator(formname,
		[
			{
				name: 'password',
				display: 'Password',
				rules: 'min_length[1]'
			},
			{
				name: 'email',
				display: 'Email',
				rules: 'valid_email',
			}
		],
		function(errors,event)
		{
			errorString = '';
			if($("#password").val() != $("#passconf").val())
			{
				k = { message: "The Password Confirm field does not match the Login Password field." }
				errors.push(k);
			}
			if (errors.length > 0)
			{
				for (var i = 0, errorLength = errors.length; i < errorLength; i++)
				{
					errorString += errors[i].message + '<br />';
				}

				$("#bottom_msg").html(errorString);
				return false;
			}
		}
	);
	$("#"+formname).ajaxForm({

		data: { username: username, roleId: roleId },
		error: function(xhr, ajaxOptions, thrownError) {
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(xhr, XMLHttpRequest, textStatus) {
			$("#bottom_msg").html(xhr);
		}
	});
}

function td_highlight()
{
	$("#list_table").delegate('td','mouseover mouseleave', function(e)
	{
		if (e.type == 'mouseover')
		{
			$(this).parent().addClass("tdhighlight");
		}
		else
		{
			$(this).parent().removeClass("tdhighlight");
		}
	});
}

function resetDiv()
{
	// recover from float_button()
	$("#bottom_msg").html("");
	$("#sidebar").css("marginTop","0");
	$("#item_panel").html("");
	$("#mid_content").removeClass( "mid_content_floatleft" );
	$("#sidebar").removeClass("sidebar");
	$("#sidebar").html("");

	$("#upheader_content").html("");
	$("#mid_content").html("");

	$("#upheader_content").removeClass( "sticky" );
}

function msieversion()
{
	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");

	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))
	{
		// If it's Internet Explorer, return version number
		var version = parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
		return version;
	}
	else
	{
		// If it's another browser, return -100
		return -100;
	}
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function getHost() {
	return(location.hostname);
}

function getProjectName()
{
	var strFullPath=window.document.location.href;
	var strPath=window.document.location.pathname;
	var pos=strFullPath.indexOf(strPath);
	var prePath=strFullPath.substring(0,pos);
	var postPath=strPath.substring(0,strPath.substr(1).indexOf('/')+1);
	return(postPath.substring(1));
}

function getRootPath()
{
	var strFullPath=window.document.location.href;

	return(strFullPath);
}

function getBaseURL() {
   return location.protocol + "//" + location.hostname + (location.port && ":" + location.port) + "/";
}

$.fn.menumaker = function(options)
{
	var cssmenu = $(this), settings = $.extend({
		title: "Menu",
		format: "dropdown",
		sticky: false
	}, options);

	return this.each(function() {
		cssmenu.prepend('<div id="menu-button">' + settings.title + '</div>');
		$(this).find("#menu-button").on('click', function() {
			$(this).toggleClass('menu-opened');
			var mainmenu = $(this).next('ul');
			if (mainmenu.hasClass('open')) {
				mainmenu.hide().removeClass('open');
			}
			else {
				mainmenu.show().addClass('open');
				if (settings.format === "dropdown") {
					mainmenu.find('ul').show();
				}
			}
		});

		cssmenu.find('li ul').parent().addClass('has-sub');

		multiTg = function() {
			cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
			cssmenu.find('.submenu-button').on('click', function() {
				$(this).toggleClass('submenu-opened');
				if ($(this).siblings('ul').hasClass('open')) {
					$(this).siblings('ul').removeClass('open').hide();
				}
				else {
					$(this).siblings('ul').addClass('open').show();
				}
			});
		};

		if (settings.format === 'multitoggle') multiTg();
		else cssmenu.addClass('dropdown');

		if (settings.sticky === true) cssmenu.css('position', 'fixed');

		resizeFix = function() {
			if ($( window ).width() > 768) {
				cssmenu.find('ul').show();
			}

			if ($(window).width() <= 768) {
				cssmenu.find('ul').hide().removeClass('open');
			}
		};

		resizeFix();
		return $(window).on('resize', resizeFix);
	});
};

$(document).ready(function(){
	$("#cssmenu").menumaker({
		title: "Menu",
		format: "multitoggle"
	});
});

openNew = function(verb, url, data, target) {
	var form = document.createElement("form");
	form.action = url;
	form.method = verb;
	form.target = target || "_self";
	if (data)
	{
		for (var key in data)
		{
			var input = document.createElement("textarea");
			input.name = key;
			input.value = typeof data[key] === "object" ? JSON.stringify(data[key]) : data[key];
			form.appendChild(input);
		}
	}
	form.style.display = 'none';
	document.body.appendChild(form);
	form.submit();
};

/** Date Calculation **/
Date.prototype.customFormat = function(formatString){
    var YYYY,YY,MMMM,MMM,MM,M,DDDD,DDD,DD,D,hhh,hh,h,mm,m,ss,s,ampm,AMPM,dMod,th;
    var dateObject = this;
    YY = ((YYYY=dateObject.getUTCFullYear())+"").slice(-2);
    MM = (M=dateObject.getUTCMonth()+1)<10?('0'+M):M;
    MMM = (MMMM=["January","February","March","April","May","June","July","August","September","October","November","December"][M-1]).substring(0,3);
    DD = (D=dateObject.getUTCDate())<10?('0'+D):D;
    DDD = (DDDD=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][dateObject.getUTCDay()]).substring(0,3);
    th=(D>=10&&D<=20)?'th':((dMod=D%10)==1)?'st':(dMod==2)?'nd':(dMod==3)?'rd':'th';
    formatString = formatString.replace("#YYYY#",YYYY).replace("#YY#",YY).replace("#MMMM#",MMMM).replace("#MMM#",MMM).replace("#MM#",MM).replace("#M#",M).replace("#DDDD#",DDDD).replace("#DDD#",DDD).replace("#DD#",DD).replace("#D#",D).replace("#th#",th);

    h=(hhh=dateObject.getUTCHours());
    if (h==0) h=24;
    if (h>12) h-=12;
    hh = h<10?('0'+h):h;
    AMPM=(ampm=hhh<12?'am':'pm').toUpperCase();
    mm=(m=dateObject.getUTCMinutes())<10?('0'+m):m;
    ss=(s=dateObject.getUTCSeconds())<10?('0'+s):s;
    return formatString.replace("#hhh#",hhh).replace("#hh#",hh).replace("#h#",h).replace("#mm#",mm).replace("#m#",m).replace("#ss#",ss).replace("#s#",s).replace("#ampm#",ampm).replace("#AMPM#",AMPM);
}
function dateAdd(date, interval, units) {
  var ret = new Date(date); //don't change original date
  switch(interval.toLowerCase()) {
    case 'year'   :  ret.setFullYear(ret.getFullYear() + units);  break;
    case 'quarter':  ret.setMonth(ret.getMonth() + 3*units);  break;
    case 'month'  :  ret.setMonth(ret.getMonth() + units);  break;
    case 'week'   :  ret.setDate(ret.getDate() + 7*units);  break;
    case 'day'    :  ret.setDate(ret.getDate() + units);  break;
    case 'hour'   :  ret.setHours(ret.getHours() + units);  break;
    case 'minute' :  ret.setMinutes(ret.getMinutes() + units);  break;
    case 'second' :  ret.setSeconds(ret.getSeconds() + units);  break;
    default       :  ret = undefined;  break;
  }
  return ret;
}


var EtherStat_array = [[0,'Link Down'], [1,''], [2,''], [3,'Link UP']];
var IPAddr_array	= [[0,'Disconnect'], [1,''], [2,'Connecting'], [3,'Connected']];
var NTP_array		= [[0,'Not Synced'], [1,''], [2,''], [3,'Synced']];
var TFCS_array		= [[0,'Not Synced'], [1,''], [2,'Syncing'], [3,'Synced']];
var IPSEC_array		= [[0,'Down'], [1,'Start'], [2,'Goes Up'], [3,'Up']];
var TR069_array		= [[0,'Down'], [1,'FW Upgrade'], [2,'Default Up'], [3,'Serving Up']];
var Snif_array		= [[0,'Down'], [1,'Eror<br>Error Timeout'], [2,'In Progress'], [3,'Success']];
var IM_array		= [[0,'Down'], [1,''], [2,'Starting/Re-Starting'], [3,'Up']];
var HNB_State		= ['Inactive','Active'];
var HNB_State_array = [ [0,HNB_State[0]], [1,HNB_State[1]] ];
var uestate_array	= [[0,'IDLE'], [1,'PCH'], [2,'FACH'], [3,'DCH']];
var symbols			= ['circle', 'diamond', 'square', 'cross', 'triangle'];
var select_x_from2 	= 0.0, select_x_to2 = 0.0, select_x_from3 	= 0.0, select_x_to3 = 0.0;

function chooseOne(cb)
{
	//determine if there's any checked checkbox, if there is make them unchecked
	if(currentSelected!="")	currentSelected.checked = false;
	if(cb.checked)
	{
		currentSelected=cb;	
	}
	else
		currentSelected="";
}

function showTooltip(x, y, contents)
{
	$('<div id="tooltip">' + contents + '</div>').css( {
		position: 'absolute',
		display: 'none',
		top: y + 5,
		left: x + 5,
		border: '1px solid #fdd',
		padding: '2px',
		'background-color': '#fee',
		opacity: 1//0.80
	}).appendTo("body").fadeIn(200);
}

function logtime_transform(time)
{
	var temp_time;
	temp_time = time.substr(0, 23).split(':');	// logtime
	time = Date.UTC(temp_time[0],temp_time[1]-1,temp_time[2],temp_time[3],temp_time[4],temp_time[5],temp_time[6]);

	return time;
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
