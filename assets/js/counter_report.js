function init()
{
	$('input[type!="button"][type!="submit"], select, textarea').val('').blur();
	setDateBox();
	getHnbMAClist();
	$("#SubmitBtn").button();
	submitAction();
}

function getHnbMAClist()
{
	var path = getBaseURL()+getProjectName()+"/index.php/counter_report/get_hnbMAC_list";

	jQuery.ajax({
		url: path,
		dataType: 'json',
		error: function(xhr, ajaxOptions, thrownError){
			console.log(xhr, ajaxOptions, thrownError);
		},
		success: function(data){
			show_hnbMac_list(data);
		}
	});
}

function show_hnbMac_list(data)
{
	var buf = "<option value=''></option>";
	for(index in data)
	{
		buf += "<option value='"+data[index]+"'>"+data[index]+"</option>";
	}
	$("#hnbMAC").append(buf);
}

//// 4 ///////////////////////////////////////////////////////

function submitAction()
{
	$("#SubmitBtn").click( function(){

		$("#bottom_msg").html("");
		$("#mid_content").html("");

		var hnb = $("#hnbMAC option:selected").text();
		var report_name = $('#reportSelect option:selected').val();
		var start_time = $("#datepicker_start").val();
		var end_time = $("#datepicker_end").val();
		var version = msieversion();

		if(start_time !== "" && end_time !== "")
		{
			if(report_name != "dailySum")
			{
				end_time = new Date(end_time);
				end_time = dateAdd(end_time, 'day', 1);
				end_time = end_time.customFormat("#YYYY#-#MM#-#DD#")+"_00:00:00";//not work in IE8
				start_time = start_time+"_00:00:01";
			}
			else
			{
				start_time = start_time+"_00:00:00";
				end_time = end_time+"_23:59:59";
			}
		}

		var path = getBaseURL()+getProjectName()+'/index.php/counter_report/get_report_rawdata';
		var url = "http://"+getHost()+":8080/RawData/queryRawDataAction";

		if(start_time != "" && end_time != "" && hnb != "" && report_name != "")
		{
			$("#mid_content").html("wait please...");
			jQuery.ajax({
				url: path,
				type: 'POST',
				data: { report_name: report_name, hnb: hnb, start_time: start_time, end_time: end_time, version: version, url: url },
				dataType: 'text',
				error: function(xhr, ajaxOptions, thrownError, error) {
					console.log("error!!!", xhr, ajaxOptions, thrownError, error);
				},
				success: function(xhr, XMLHttpRequest, textStatus) {
					show_rawdata(xhr);
				}
			});
		}
		else
		{
			if(hnb == "" || hnb == null)
				$("#bottom_msg").append("<font color='red'>Select a HNB.</font><br>");

			if(report_name == "" || report_name == null)
				$("#bottom_msg").append("<font color='red'>Select a Report.</font><br>");

			if(start_time == "" || end_time == "")
				$("#bottom_msg").append("<font color='red'>Start time or End time should not be empty.</font><br>");
		}
	});
}

function show_rawdata(data)
{
	//** for output excel **/
	$("#mid_content").html(data);
	$( "#clickExcelBtn" ).button().click(function() {
		var version = msieversion();

		if(version === -100)
		{
			window.open('data:application/vnd.ms-excel,' + $('#reportRawData_div').html());
		}
	});

}

function getCSVData()
{
	var csv_value=$('#reportRawData_tbl').table2CSV({delivery:'value'});
	$("#csv_text").val(csv_value);
}
