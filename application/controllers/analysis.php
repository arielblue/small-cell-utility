<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Analysis extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$this->load->model('analysis_model','ana');
		$this->load->helper(array('url','time_helper'));
	}

	public function index()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$postData = $this->input->post();
		$date = $this->get_date($postData);
		$data["startDate"] = $date["startDate"];
		$data["endDate"] = $date["endDate"];
		$data["hnb"] = $postData["hnb"];

		$data["pwr"] = $this->ana->get_NlpcMartPwr($data["hnb"], $data["startDate"], $data["endDate"]);
		$data["im_reg"] = $this->ana->get_MaxHueMue($data["hnb"], $data["startDate"], $data["endDate"]);
		$data["ueIntraMrm"] = $this->ana->get_ueintramrm($data["hnb"], $data["startDate"], $data["endDate"]);
		$data["ho"] = $this->ana->get_HObyIMSI($data["hnb"], $data["startDate"], $data["endDate"]);

		$ROTval	= $this->ana->get_ROTval($data["hnb"], $data["startDate"], $data["endDate"]);
		if($ROTval != NULL)	$data["ROTval"] = (float) $ROTval;
		else $data["ROTval"] = $ROTval;

		$data["criteria"] = $this->config->item("criteria");
		if($data["ROTval"] != NULL)
		{
			$data["ulrssi"] = $this->ana->get_ULRSSIHNB($data["ROTval"], $data["hnb"], $data["startDate"], $data["endDate"]);
			$data["ULAlarmThres"] = $this->config->item("ULAlarmThres")+$data["ROTval"];
		}

		// date to timestamp string
		$data["startDate"] = to_timestamp_string($date["startDate"]);
		$data["endDate"] = to_timestamp_string($date["endDate"]);

		$this->load->view("analysis_view", $data);
	}

	public function get_date($postData)
	{
		$data = array();
		$search  = array(" ","-");
		$replace = array(":",":");
		$data["startDate"] = str_replace($search, $replace, $postData["startDate"]).":00:00";
		$data["endDate"] = str_replace($search, $replace, $postData["endDate"]).":00:00";
		return $data;
	}
}

/* End of file analysis.php */
/* Location: ./app/controllers/analysis.php */