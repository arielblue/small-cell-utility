<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alarm extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));	# error_reporting(E_ALL & ~E_NOTICE);
		ini_set('display_errors', 1);
		set_time_limit(0);

		$this->load->helper(array('url'));

		$models = array(
			'alarm_model' => 'alarm',
			'common_model' => 'commondb'
		);

		foreach ($models as $file => $object_name)
		{
			$this->load->model($file, $object_name);
		}
	}

	public function index()
	{
		if(!$this->input->cookie('login_user'))
			redirect('login', 'refresh');
		else
			$data['user'] = ucfirst($this->input->cookie('login_user'));

		$data['kpiWarn'] = $this->isKpiWarnProcessed();
		$data['alarmWarn'] = $this->isAlarmWarnProcessed();

		$data['title'] = 'Alarm Page';
		$data['version'] = $this->config->item("version");

		$this->load->view('alarm_view', $data);
	}

	public function isKpiWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isKpiWarnProcessed();
	}

	public function isAlarmWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isAlarmWarnProcessed();
	}

	public function get_alarm_report()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$version = $_POST["version"];
		$url = $_POST["url"];

		$criteria["startDate"] = $_POST["startDate"];
		$criteria["endDate"] = $_POST["endDate"];
		$criteria["clearedMode"] = ($_POST["clearedMode"] == "true") ? ("true") : ("false");

		## for output excel
		## Curl Post
		$fields_string = "";
		//url-ify the data for the POST
		foreach($criteria as $key => $value) {
			$fields_string .= $key.'='.$value.'&';
		}
		$fields_string = rtrim($fields_string,'&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	// make result retrun string, not just boolean
		curl_setopt($ch, CURLOPT_POST, count($_POST));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

		//execute post
		$result = curl_exec($ch);
		// $err = curl_errno ( $ch );
		// $errmsg = curl_error ( $ch );
		// $header = curl_getinfo ( $ch );
		// $httpCode = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );

		//close connection
		curl_close($ch);
		// $result = json_decode($result);	## return data in json data array
		## Curl Post End

		if($criteria["clearedMode"] == "true")
			echo $result;
		else
		{
			echo $result;
		}
	}

	public function get_alarm_record()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$startDate	= $_POST["date"]." 00:00:00:000000";
		$endDate	= $_POST["date"]." 23:59:59:999999";
		$type		= $_POST["type"];

		$res = $this->alarm->get_alarm_record($startDate, $endDate, $type);
		echo json_encode($res);
	}

	public function message($to = 'World')
	{
		echo "Hello {$to}!".PHP_EOL;
	}
}

/* End of file kpi.php */
/* Location: ./application/controllers/kpi.php */