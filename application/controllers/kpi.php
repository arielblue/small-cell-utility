<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kpi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));	# error_reporting(E_ALL & ~E_NOTICE);
		ini_set('display_errors', 1);
		set_time_limit(0);

		$this->load->helper(array('url'));
		$this->load->library('array_filter_lib');

		$models = array(
			'report_model' => 'reportdb',
			'common_model' => 'commondb',
		);

		foreach ($models as $file => $object_name)
		{
			$this->load->model($file, $object_name);
		}
	}

	public function index()
	{
		if(!$this->input->cookie('login_user'))
			redirect('login', 'refresh');
		else
			$data['user'] = ucfirst($this->input->cookie('login_user'));

		$data['kpiWarn'] = $this->isKpiWarnProcessed();
		$data['alarmWarn'] = $this->isAlarmWarnProcessed();

		$data['title'] = 'KPI Page';
		$data['kpi'] = $this->config->item("kpi");
		$data['version'] = $this->config->item("version");

		$this->load->view('kpi_view', $data);
	}

	public function isKpiWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isKpiWarnProcessed();
	}

	public function isAlarmWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isAlarmWarnProcessed();
	}

	public function get_hnbMAC_list()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		echo $this->reportdb->get_hnbMAC_list();
	}

	public function get_kpi_data()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$version = $_POST["version"];
		$url = $_POST["url"];
		unset($_POST["url"]);
		unset($_POST["version"]);

		## Curl Post
		$fields_string = "";
		//url-ify the data for the POST
		foreach($_POST as $key => $value) {
			$fields_string .= $key.'='.$value.'&';
		}
		$fields_string = rtrim($fields_string,'&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	// make result retrun string, not just boolean
		curl_setopt($ch, CURLOPT_POST, count($_POST));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

		//execute post
		$result = curl_exec($ch);
		// $err = curl_errno ( $ch );
		// $errmsg = curl_error ( $ch );
		// $header = curl_getinfo ( $ch );
		// $httpCode = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );

		//close connection
		curl_close($ch);
		// $result = json_decode($result);
		// $kpi = $this->config->item("kpi");
		// $result->kpi = array_keys($kpi); // key($kpi)
		// $result = json_encode($result);
		echo $result;
		## Curl Post End;
	}

	public function calculateGraph2()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$target = (float) $_POST["target"];
		$kpi = $_POST["kpi"];
		$summary = $_POST["summary"];

		switch($kpi)
		{
			case "CS_RAB_SR_Distribution":
			case "PS_RAB_SR_Distribution":
			case "RRC_Connection_SR_Distribution":
			case "Paging_SR_Distribution":
			case "CS_Call_Drop_Rate_Distribution":
			case "PS_Call_Drop_Rate_Distribution":
			case "Overall_Call_Drop_Rate_Distribution":
			case "CS_Attach_Success_Rate_Distribution":
			case "PS_Attach_Success_Rate_Distribution":
			case "Cell_Availability_Distribution":
				$this->CS_RAB_SR_Summary($kpi, $target, $summary);
				break;

			case "IPSec_Tunnel_Est_Distribution":
			case "HNB_Registration_Attempt_Distribution":
			case "SCTP_Abort_Distribution":
				$this->IPSec_Tunnel_Est_Summary($kpi, $target, $summary);
				break;
		}
	}

	public function CS_RAB_SR_Summary($kpi, $target, $summary)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$hnb = array();
		$average_SR = array();
		$count = array();
		$avg_SR_Std = array();
		$SR = "";

		foreach($summary as $key => $value)
		{
			switch($kpi)
			{
				case "CS_RAB_SR_Distribution":
					$filter = "isLower";
					$SR = "CS_SR";
					break;

				case "PS_RAB_SR_Distribution":
					$filter = "isLower";
					$SR = "PS_SR";
					break;

				case "RRC_Connection_SR_Distribution":
					$filter = "isLower";
					$SR = "RRC_SR";
					break;

				case "Paging_SR_Distribution":
					$filter = "isLower";
					$SR = "Paging_SR";
					break;

				case "CS_Call_Drop_Rate_Distribution":
					$filter = "isHigher";
					$SR = "CS_CDR_SR";
					break;

				case "PS_Call_Drop_Rate_Distribution":
					$filter = "isHigher";
					$SR = "PS_CDR_SR";
					break;

				case "Overall_Call_Drop_Rate_Distribution":
					$filter = "isHigher";
					$SR = "Overall_CDR_SR";
					break;

				case "CS_Attach_Success_Rate_Distribution":
					$filter = "isLower";
					$SR = "CSAttachSR";
					break;

				case "PS_Attach_Success_Rate_Distribution":
					$filter = "isLower";
					$SR = "PSAttachSR";
					break;

				case "Cell_Availability_Distribution":
					$filter = "isLower";
					$SR = "CellAvailabilitySR";
					break;
			}

			$percent = array();
			for($i = 0; $i < count($value); $i++)
				array_push($percent, $value[$i][$SR]);

			$percent = array_filter($percent, array(new Array_filter_lib($target), $filter));

			if(count($percent) > 0)
			{
				array_push($hnb, $key);
				array_push($count, array($key, count($percent)));
				$avg = round(array_sum($percent)/count($percent), 1);
				$avgStd = round(stats_standard_deviation($percent), 2);
				array_push($average_SR, array($key, $avg));
				array_push($avg_SR_Std, array($key, $avg, $avgStd));
			}
		}

		$data = array("hnb"=> $hnb, "average_SR" => $average_SR, "average_SR_Std" => $avg_SR_Std, "count" => $count);
		$data = json_encode($data);
		echo $data;
	}

	public function IPSec_Tunnel_Est_Summary($kpi, $target, $summary)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$hnb = array();
		$count = array();
		$avg_Att = array();
		$avg_Att_Std = array();
		$filter = "isHigher";

		$att = "";
		switch($kpi)
		{
			case "IPSec_Tunnel_Est_Distribution":
				$att = "IPSecTunnAtt";
				break;

			case "HNB_Registration_Attempt_Distribution":
				$att = "AttHnbReg";
				break;

			case "SCTP_Abort_Distribution":
				$att = "SCTPAbort";
				break;
		}

		foreach($summary as $key => $value)
		{
			$attCount = array();
			for($i = 0; $i < count($value); $i++)
				array_push($attCount, $value[$i][$att]);

			$attCount = array_filter($attCount, array(new Array_filter_lib($target), $filter));

			if(count($attCount) > 0)
			{
				array_push($hnb, $key);
				array_push($count, array($key, count($attCount)));
				$avg = round(array_sum($attCount)/count($attCount),2);
				$avgStd = round(stats_standard_deviation($attCount), 2);
				array_push($avg_Att, array($key, $avg));
				array_push($avg_Att_Std, array($key, $avg, $avgStd));
			}
		}

		$data = array("hnb"=> $hnb, "avg_Att" => $avg_Att, "avg_Att_Std" => $avg_Att_Std, "count" => $count);
		$data = json_encode($data);
		echo $data;
	}

	public function calculateGraph3()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$kpi = $_POST["kpi"];
		$hnb = $_POST["hnb"];
		$target = (float) $_POST["target"];
		$avg_SR = $_POST["avg_SR"];
		$summary = $_POST["summary"];

		switch($kpi)
		{
			case "CS_RAB_SR_Distribution":
			case "PS_RAB_SR_Distribution":
				$this->hnb_CS_RAB_daily($kpi, $target, $summary, $avg_SR);
				break;

			case "RRC_Connection_SR_Distribution":
			case "CS_Attach_Success_Rate_Distribution":
			case "PS_Attach_Success_Rate_Distribution":
				$this->hnb_RRC_Connection_daily($kpi, $target, $summary);
				break;

			case "Paging_SR_Distribution":
				$this->hnb_Paging_daily($kpi, $target, $summary);
				break;

			case "CS_Call_Drop_Rate_Distribution":
			case "PS_Call_Drop_Rate_Distribution":
			case "Overall_Call_Drop_Rate_Distribution":
				$this->hnb_CS_Call_Drop_Rate_Daily($kpi, $target, $summary);
				break;

			case "IPSec_Tunnel_Est_Distribution":
			case "HNB_Registration_Attempt_Distribution":
				$this->IPSec_Tunnel_Est_Daily($kpi, $summary);
				break;

			case "SCTP_Abort_Distribution":
				$this->SCTP_Abort_Daily($kpi, $summary);
				break;

			case "Cell_Availability_Distribution":
				$this->Cell_Availability_Daily($kpi, $target, $summary);
				break;
		}
	}

	public function hnb_CS_RAB_daily($kpi, $target, $summary, $avg_SR)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$SR = "";
		switch($kpi)
		{
			case "CS_RAB_SR_Distribution":
				$SR = "CS_SR";
				break;

			case "PS_RAB_SR_Distribution":
				$SR = "PS_SR";
				break;
		}

		$date = array();
		$RAB_attempt = array();
		$SR_value = array();
		$SR_target = array();
		$CDR = array();
		$hnb_daily_avgSR = array();

		foreach($summary as $k => $v)
		{
			array_push($date, $v['Date']);
			array_push($RAB_attempt, array($v['Date'], $v['RAB_attempt']));
			array_push($SR_value, array($v['Date'], $v[$SR]));
			array_push($SR_target, array($v['Date'], $target));
			array_push($CDR, array($v['Date'], $v['CDR']));
		}

		$Average_SR = array();
		ksort($avg_SR);	# sort array by key
		foreach($avg_SR as $k => $v)
			if(!in_array($k, $date)) # if $avg_SR date key isn't in $date value
				unset($avg_SR[$k]);
			else
				array_push($Average_SR, array($k, $v));

		$data = array("date" => $date, "RAB_attempt" => $RAB_attempt, $SR => $SR_value, "Average_SR" => $Average_SR, "SR_target" => $SR_target, "CDR" => $CDR);
		$data = json_encode($data);

		echo $data;
	}

	public function hnb_RRC_Connection_daily($kpi, $target, $summary)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$date = array();
		$failCount = array();
		$succCount = array();
		$SR = array();
		$SR_target = array();

		$SRname = "";
		$succ = "";
		$fail = "";
		switch($kpi)
		{
			case "RRC_Connection_SR_Distribution":
				$SRname = "RRC_SR";
				$fail = "failCount";
				$succ = "succCount";
				break;

			case "CS_Attach_Success_Rate_Distribution":
				$SRname = "CSAttachSR";
				$fail = "CSAttachReject";
				$succ = "CSAttachSucc";
				break;
			case "PS_Attach_Success_Rate_Distribution":
				$SRname = "PSAttachSR";
				$fail = "PSAttachReject";
				$succ = "PSAttachSucc";
				break;
		}

		foreach($summary as $k => $v)
		{
			array_push($date, $v['Date']);
			array_push($failCount, array($v['Date'], $v[$fail]));
			array_push($succCount, array($v['Date'], $v[$succ]));
			array_push($SR, array($v['Date'], $v[$SRname]));
			array_push($SR_target, array($v['Date'], $target));
		}

		$data = array("date" => $date, "failCount" => $failCount, "succCount" => $succCount, $SRname => $SR, "SR_target" => $SR_target);
		$data = json_encode($data);

		echo $data;
	}

	public function hnb_CS_Call_Drop_Rate_Daily($kpi, $target, $summary)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$SR = "";
		switch($kpi)
		{
			case "CS_Call_Drop_Rate_Distribution":
				$SR = "CS_CDR_SR";
				break;

			case "PS_Call_Drop_Rate_Distribution":
				$SR = "PS_CDR_SR";
				break;

			case "Overall_Call_Drop_Rate_Distribution":
				$SR = "Overall_CDR_SR";
				break;
		}

		$date = array();
		$luReleaseCommand = array();
		$luReleaseRequest = array();
		$CDR = array();
		$SR_target = array();

		foreach($summary as $k => $v)
		{
			array_push($date, $v['Date']);
			array_push($luReleaseCommand, array($v['Date'], $v["luReleaseCommand"]));
			array_push($luReleaseRequest, array($v['Date'], $v["luReleaseRequest"]));
			array_push($CDR, array($v['Date'], $v[$SR]));
			array_push($SR_target, array($v['Date'], $target));
		}

		$data = array("date" => $date, "luReleaseCommand" => $luReleaseCommand, "luReleaseRequest" => $luReleaseRequest, $SR => $CDR, "SR_target" => $SR_target);
		$data = json_encode($data);

		echo $data;
	}

	public function hnb_Paging_daily($kpi, $target, $summary)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$date = array();
		$failCount = array();
		$type1 = array();
		$type2 = array();
		$Paging_SR = array();
		$SR_target = array();

		foreach($summary as $k => $v)
		{
			array_push($date, $v['Date']);
			array_push($failCount, array($v['Date'], $v["failCount"]));
			array_push($type1, array($v['Date'], $v["type1"]));
			array_push($type2, array($v['Date'], $v["type2"]));
			array_push($Paging_SR, array($v['Date'], $v["Paging_SR"]));
			array_push($SR_target, array($v['Date'], $target));
		}

		$data = array("date" => $date, "failCount" => $failCount, "type1" => $type1, "type2" => $type2, "Paging_SR" => $Paging_SR, "SR_target" => $SR_target);
		$data = json_encode($data);

		echo $data;
	}

	public function IPSec_Tunnel_Est_Daily($kpi, $summary)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$date = array();
		$failCount = array();
		$succCount = array();

		$fail = "";
		$succ = "";
		switch($kpi)
		{
			case "IPSec_Tunnel_Est_Distribution":
				$fail = "IPSecTunnelFail";
				$succ = "IPSecTunnSucc";
				break;

			case "HNB_Registration_Attempt_Distribution":
				$fail = "FailHnbReg";
				$succ = "SuccHnbReg";
				break;
		}

		foreach($summary as $k => $v)
		{
			array_push($date, $v['Date']);
			array_push($failCount, array($v['Date'], $v[$fail]));
			array_push($succCount, array($v['Date'], $v[$succ]));
		}

		$data = array("date" => $date, "failCount" => $failCount, "succCount" => $succCount);
		$data = json_encode($data);

		echo $data;
	}

	public function SCTP_Abort_Daily($kpi, $summary)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$date = array();
		$SCTPAbortSent = array();
		$SCTPAbortRcvd = array();

		foreach($summary as $k => $v)
		{
			array_push($date, $v['Date']);
			array_push($SCTPAbortSent, array($v['Date'], $v["SCTPAbortSent"]));
			array_push($SCTPAbortRcvd, array($v['Date'], $v["SCTPAbortRcvd"]));
		}

		$data = array("date" => $date, "SCTPAbortSent" => $SCTPAbortSent, "SCTPAbortRcvd" => $SCTPAbortRcvd);
		$data = json_encode($data);

		echo $data;
	}

	public function Cell_Availability_Daily($kpi, $target, $summary)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$date = array();
		$PwrOffTime = array();
		$SR_target = array();
		$availability = array();

		foreach($summary as $k => $v)
		{
			array_push($date, $v['Date']);
			array_push($PwrOffTime, array($v['Date'], 86400-$v["BootTime"]));
			array_push($availability, array($v['Date'], $v["CellAvailabilitySR"]));
			array_push($SR_target, array($v['Date'], $target));
		}

		$data = array("date" => $date, "PwrOffTime" => $PwrOffTime, "availability" => $availability, "SR_target" => $SR_target);
		$data = json_encode($data);

		echo $data;
	}

	public function calculateGraph4()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$target = (float) $_POST["target"];
		$kpi = $_POST["kpi"];
		$data = $_POST["data"];

		switch($kpi)
		{
			case "PS_RAB_SR_DayHourly":
			case "CS_RAB_SR_DayHourly":
			case "RRC_Connection_SR_DayHourly":
			case "CS_Attach_Success_Rate_DayHourly":
			case "PS_Attach_Success_Rate_DayHourly":
				$this->hnb_CS_RAB_hourly($kpi, $target, $data);
				break;

			case "Paging_SR_DayHourly":
				$this->hnb_Paging_hourly($target, $data);
				break;

			case "CS_Call_Drop_Rate_DayHourly":
			case "PS_Call_Drop_Rate_DayHourly":
			case "Overall_Call_Drop_Rate_DayHourly":
				$this->hnb_CS_CDR_hourly($target, $data);
				break;

			case "IPSec_Tunnel_Est_DayHourly":
			case "HNB_Registration_Attempt_DayHourly":
				$this->hnb_IPSec_Tunnel_Est_hourly($kpi, $target, $data);
				break;

			case "SCTP_Abort_DayHourly":
				$this->hnb_SCTP_Abort_hourly($target, $data);
				break;

			case "Cell_Availability_DayHourly":
				$this->hnb_Cell_Availability_hourly($kpi, $target, $data);
				break;
		}
	}

	public function hnb_Cell_Availability_hourly($kpi, $target, $data)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");

		$data = $data["DayHourly"];
		$PwrOffTime = array();
		$availability = array();
		$SR_target = array();
		$errorBoot = array();
		$IPSecTunnFail = array();
		$IPSecTunnSucc = array();

		foreach($data as $k => $v)
		{
			if(!empty($v["BootTime"]))
			{
				$temp_pwrOff = 3600-$v["BootTime"];
				array_push($PwrOffTime, array( $v["hour"], $temp_pwrOff ));
				if($temp_pwrOff < 0)
					$errorBoot[$v["hour"].":00"] = $temp_pwrOff;
			}
			else
				array_push($PwrOffTime, array( $v["hour"], $v["BootTime"] ));

			array_push($availability, array($v["hour"], $v["rate"]));

			if( !empty($v["BootTime"]) && !empty($v["rate"])  )
				array_push($SR_target, array($v["hour"], $target));

		}

		$data = array("PwrOffTime" => $PwrOffTime, "availability" => $availability, "SR_target" => $SR_target, "IPSecTunnSucc" => $IPSecTunnSucc, "IPSecTunnFail" => $IPSecTunnFail, "errorBoot" => $errorBoot);
		$data = json_encode($data);

		echo $data;
	}

	public function hnb_CS_RAB_hourly($kpi, $target, $data)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");

		$data = $data["DayHourly"];
		$attempt_fail_count = array();
		$attempt_success_count = array();
		$SR = array();
		$SR_target = array();

		$fail = "";
		$succ = "";
		switch($kpi)
		{
			case "PS_RAB_SR_DayHourly":
			case "CS_RAB_SR_DayHourly":
			case "RRC_Connection_SR_DayHourly":
				$fail = "failCount";
				$succ = "succCount";
				break;
			case "CS_Attach_Success_Rate_DayHourly":
				$fail = "CSAttachReject";
				$succ = "CSAttachSucc";
				break;
			case "PS_Attach_Success_Rate_DayHourly":
				$fail = "PSAttachReject";
				$succ = "PSAttachSucc";
				break;
		}

		foreach($data as $k => $v)
		{
			array_push($attempt_fail_count, array( $v["hour"], $v[$fail] ));
			array_push($attempt_success_count, array( $v["hour"], $v[$succ] ));
			array_push($SR, array($v["hour"], $v["rate"]));
			array_push($SR_target, array($v["hour"], $target));
		}

		$data = array("attempt_fail_count" => $attempt_fail_count, "attempt_success_count" => $attempt_success_count, "SR" => $SR, "SR_target" => $SR_target);
		$data = json_encode($data);

		echo $data;
	}

	public function hnb_Paging_hourly($target, $data)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");

		$data = $data["DayHourly"];
		$failCount = array();
		$type1 = array();
		$type2 = array();
		$SR = array();
		$SR_target = array();

		foreach($data as $k => $v)
		{
			array_push($failCount, array($v["hour"], $v["failCount"] ));
			array_push($type1, array($v["hour"], $v["type1"]));
			array_push($type2, array($v["hour"], $v["type2"]));
			array_push($SR, array($v["hour"], $v["rate"]));
			array_push($SR_target, array($v["hour"], $target));
		}

		$data = array("failCount" => $failCount, "type1" => $type1, "type2" => $type2, "SR" => $SR, "SR_target" => $SR_target);
		$data = json_encode($data);

		echo $data;
	}

	public function hnb_CS_CDR_hourly($target, $data)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");

		$data = $data["DayHourly"];
		$luReleaseCommand = array();
		$luReleaseRequest = array();
		$SR = array();
		$SR_target = array();

		foreach($data as $k => $v)
		{
			array_push($luReleaseCommand, array($v["hour"], $v["luReleaseCommand"] ));
			array_push($luReleaseRequest, array($v["hour"], $v["luReleaseRequest"]));
			array_push($SR, array($v["hour"], $v["rate"]));
			array_push($SR_target, array($v["hour"], $target));
		}

		$data = array("luReleaseCommand" => $luReleaseCommand, "luReleaseRequest" => $luReleaseRequest, "SR" => $SR, "SR_target" => $SR_target);
		$data = json_encode($data);

		echo $data;
	}

	public function hnb_IPSec_Tunnel_Est_hourly($kpi, $target, $data)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");

		$data = $data["DayHourly"];
		$failCount = array();
		$succCount = array();

		$fail = "";
		$succ = "";
		switch($kpi)
		{
			case "IPSec_Tunnel_Est_DayHourly":
				$fail = "IPSecTunnelFail";
				$succ = "IPSecTunnelSucc";
				break;

			case "HNB_Registration_Attempt_DayHourly":
				$fail = "FailHnbReg";
				$succ = "SuccHnbReg";
				break;
		}

		foreach($data as $k => $v)
		{
			array_push($failCount, array($v["hour"], $v[$fail]));
			array_push($succCount, array($v["hour"], $v[$succ]));
		}

		$data = array("failCount" => $failCount, "succCount" => $succCount);
		$data = json_encode($data);

		echo $data;
	}

	public function hnb_SCTP_Abort_hourly($target, $data)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");

		$data = $data["DayHourly"];
		$SCTPAbortSent = array();
		$SCTPAbortRcvd = array();

		foreach($data as $k => $v)
		{
			array_push($SCTPAbortSent, array($v["hour"], $v["SCTPAbortSent"]));
			array_push($SCTPAbortRcvd, array($v["hour"], $v["SCTPAbortRcvd"]));
		}

		$data = array("SCTPAbortSent" => $SCTPAbortSent, "SCTPAbortRcvd" => $SCTPAbortRcvd);
		$data = json_encode($data);

		echo $data;
	}
}

/* End of file kpi.php */
/* Location: ./application/controllers/kpi.php */