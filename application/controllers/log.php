<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Log extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));	# error_reporting(E_ALL & ~E_NOTICE);
		ini_set('display_errors', 1);
		set_time_limit(0);

		$this->load->helper(array('time_helper','file_helper','url'));

		$models = array(
			'log_model' => 'log',
			'rf_model' => 'rf',
			'common_model' => 'commondb',
		);

		foreach ($models as $file => $object_name)
		{
			$this->load->model($file, $object_name);
		}
	}

	public function index()
	{
		if(!$this->input->cookie('login_user'))
			redirect('login', 'refresh');
		else
			$data['user'] = ucfirst($this->input->cookie('login_user'));

		$data['kpiWarn'] = $this->isKpiWarnProcessed();
		$data['alarmWarn'] = $this->isAlarmWarnProcessed();

		$data['title'] = 'Log Page';
		$data['version'] = $this->config->item("version");


		$this->load->view('log_view', $data);
	}

	public function isKpiWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isKpiWarnProcessed();
	}

	public function isAlarmWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isAlarmWarnProcessed();
	}

	public function getHnbList()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$data = $this->rf->getHnbList();

		if($_POST["type"] == "ho")
		{
			$hoevents = $this->config->item("hoEvents");
			$result = array("data" => $data, "hoevents" => $hoevents);
			echo json_encode($result);
		}
		else echo json_encode($data);
	}

	public function getRTTdata()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$limit = array();
		$error = array();
		$hnbid = $_POST["hnbid"];
		$startDate = to_timestampe_string2($_POST["startDate"]);
		$endDate = to_timestampe_string2($_POST["endDate"]);

		if($_POST["rrcfail"] == "true") array_push($error, "RrcFailed");
		if($_POST["smsfail"] == "true") array_push($error, "SmsFailed");
		if($_POST["csfail"] == "true") array_push($error, "CsFailed");
		if($_POST["psfail"] == "true") array_push($error, "PsFailed");
	
		if($_POST["rrc"] !== "") $limit["RrcConReq"] = $_POST["rrc"];
		if($_POST["smsmo"] !== "") $limit["SmsMoCpData"] = $_POST["smsmo"];
		if($_POST["smscn1"] !== "") $limit["SmsCnCpAck"] = $_POST["smscn1"];
		if($_POST["smscn2"] !== "") $limit["SmsCnCpDataAck"] = $_POST["smscn2"];
		if($_POST["smscn3"] !== "") $limit["SmsCnCpData"] = $_POST["smscn3"];
		if($_POST["smsmt1"] !== "") $limit["SmsMtCpAck"] = $_POST["smsmt1"];
		if($_POST["smsmt2"] !== "") $limit["SmsMtCpDataAck"] = $_POST["smsmt2"];
		if($_POST["cscm"] !== "") $limit["CsCmServiceRequest"] = $_POST["cscm"];
		if($_POST["csradio"] !== "") $limit["CsRadioBearerSetupComplete"] = $_POST["csradio"];
		if($_POST["csalert"] !== "") $limit["CsAlert"] = $_POST["csalert"];
		if($_POST["psser"] !== "") $limit["PsServiceRequest"] = $_POST["psser"];

		$data = $this->log->getRTTdata($hnbid, $startDate, $endDate, $limit, $error);
		$result = array( "data" => $data, "hnb" => $hnbid, "limit" => $limit, "error" => $error );
		echo json_encode($result);
	}

	public function getERRORdata()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$limit = array();
		$hnbid = $_POST["hnbid"];
		$startDate = to_timestampe_string2($_POST["startDate"]);
		$endDate = to_timestampe_string2($_POST["endDate"]);
		if($_POST["radio"] == "true") array_push($limit, "radioLinkFailureIndication");
		if($_POST["iurelease"] == "true") array_push($limit, "iuReleaseRequest");
		if($_POST["existed"] == "true") array_push($limit, "existedProcess");
		if($_POST["channel"] == "true") array_push($limit, "channelizationCodeError");
		if($_POST["ulerror"] == "true") array_push($limit, "diramUlError");
		if($_POST["dlerror"] == "true") array_push($limit, "diramDlError");
		if($_POST["demulator"] == "true") array_push($limit, "demulatorError");
		if($_POST["rlsetup"] == "true") array_push($limit, "rlSetupRequestFailed");
		if($_POST["rlact"] == "true") array_push($limit, "rlActivationFailed");
		if($_POST["rlrepre"] == "true") array_push($limit, "rlReconfigPrepareRequestFailed");
		if($_POST["rlrecom"] == "true") array_push($limit, "rlReconfigCommitFailed");
		if($_POST["rldelete"] == "true") array_push($limit, "rlDeleteFailed");
		if($_POST["compress"] == "true") array_push($limit, "compressModeFailed");
		if($_POST["reconfig"] == "true") array_push($limit, "reconfigCancelRequestFailed");

		$data = $this->log->getERRORdata($hnbid, $startDate, $endDate, $limit);
		$result = array( "data" => $data, "hnb" => $hnbid );
		echo json_encode($result);
	}

	public function getCellUpdateData()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$limit = array();
		$hnbid = $_POST["hnbid"];
		$startDate = to_timestampe_string2($_POST["startDate"]);
		$endDate = to_timestampe_string2($_POST["endDate"]);
		if($_POST["rlcerror"] == "true") array_push($limit, "rlcUnrecoverableError");
		if($_POST["radio"] == "true") array_push($limit, "radioLinkFailure");
		if($_POST["rlcbyfap"] == "true") array_push($limit, "rlcUnrecoverableErrorDetectedByFap");
		$data = $this->log->getCellUpdateData($hnbid, $startDate, $endDate, $limit);
		$result = array( "data" => $data, "hnb" => $hnbid );
		echo json_encode($result);
	}

	public function getReconfigurationData()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$limit = array();
		$hnbid = $_POST["hnbid"];
		$startDate = to_timestampe_string2($_POST["startDate"]);
		$endDate = to_timestampe_string2($_POST["endDate"]);
		if($_POST["recondch"] !== "") $limit["reconfigurationOfDch"] = $_POST["recondch"];
		if($_POST["reconfach"] !== "") $limit["reconfigurationofFach"] = $_POST["reconfach"];
		if($_POST["reconpch"] !== "") $limit["reconfigurationOfPch"] = $_POST["reconpch"];
		if($_POST["rate"] !== "") $limit["rateAdaption"] = $_POST["rate"];
		$data = $this->log->getReconfigurationData($hnbid, $startDate, $endDate, $limit);
		$result = array( "data" => $data, "hnb" => $hnbid, "startDate" => $_POST["startDate"], "endDate" => $_POST["endDate"], "limit" => $limit );
		echo json_encode($result);
	}

	public function getPreemptionData()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$limit = array();
		$hnbid = $_POST["hnbid"];
		$startDate = to_timestampe_string2($_POST["startDate"]);
		$endDate = to_timestampe_string2($_POST["endDate"]);
		if($_POST["back"] == "true") array_push($limit, "preemptBack");
		if($_POST["preempter"] == "true") array_push($limit, "preempter");
		if($_POST["toidle"] == "true") array_push($limit, "preemptToIdle");
		$data = $this->log->getPreemptionData($hnbid, $startDate, $endDate, $limit);
		$result = array( "data" => $data, "hnb" => $hnbid );
		echo json_encode($result);
	}

	public function getHoData()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$limit = array();
		$hnbid = $_POST["hnbid"];
		$startDate = to_timestampe_string2($_POST["startDate"]);
		$endDate = to_timestampe_string2($_POST["endDate"]);
		if($_POST["hoevents"] !== "") $limit["hoEvents"] = $_POST["hoevents"];
		if($_POST["hodomain"] !== "") $limit["hoDomain"] = $_POST["hodomain"];
		if($_POST["hotype"] !== "") $limit["hoType"] = $_POST["hotype"];
		$data = $this->log->getHoData($hnbid, $startDate, $endDate, $limit);
		$result = array( "data" => $data, "hnb" => $hnbid, "hoevents" => $_POST["hoevents"] );
		echo json_encode($result);
	}

	public function getERRORdetail3()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$limit = $_POST["limit"];
		$hnbid = $_POST["hnbid"];
		$callid = $_POST["callid"];
		$startDate = to_timestampe_string2($_POST["startDate"]);
		$endDate = to_timestampe_string2($_POST["endDate"]);

		$data = $this->log->getERRORdetail3($hnbid, $callid, $startDate, $endDate, $limit);

		echo json_encode($data);
	}

	public function getReconfigurationDetail2()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$limit = $_POST["limit"];
		$hnbid = $_POST["hnb"];
		$callid = $_POST["callid"];
		$startDate = to_timestampe_string2($_POST["startDate"]);
		$endDate = to_timestampe_string2($_POST["endDate"]);

		$data = $this->log->getReconfigurationDetail2($hnbid, $callid, $startDate, $endDate, $limit);
		echo json_encode($data);
	}

	public function getCellUpdatedetail3()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$limit = $_POST["limit"];
		$hnbid = $_POST["hnbid"];
		$callid = $_POST["callid"];
		$startDate = to_timestampe_string2($_POST["startDate"]);
		$endDate = to_timestampe_string2($_POST["endDate"]);

		$data = $this->log->getCellUpdatedetail3($hnbid, $callid, $startDate, $endDate, $limit);

		echo json_encode($data);
	}

	public function getPremptionDetail3()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$limit = $_POST["limit"];
		$hnbid = $_POST["hnbid"];
		$callid = $_POST["callid"];
		$startDate = to_timestampe_string2($_POST["startDate"]);
		$endDate = to_timestampe_string2($_POST["endDate"]);

		$data = $this->log->getPremptionDetail3($hnbid, $callid, $startDate, $endDate, $limit);

		echo json_encode($data);
	}

	public function getHoDetail4()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$hnbid = $_POST["hnb"];
		$callid = $_POST["callid"];
		$startDate = to_timestampe_string2($_POST["startDate"]);
		$endDate = to_timestampe_string2($_POST["endDate"]);
		$events = $_POST["events"];
		$domain = $_POST["domain"];
		$type = $_POST["type"];

		$data = $this->log->getHoDetail4($hnbid, $callid, $startDate, $endDate, $events, $domain, $type);

		echo json_encode($data);		
	}

	public function getRttDetail3()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$limit = $_POST["limit"];
		$error = $_POST["error"];
		$hnbid = $_POST["hnbid"];
		$callid = $_POST["callid"];
		$startDate = to_timestampe_string2($_POST["startDate"]);
		$endDate = to_timestampe_string2($_POST["endDate"]);
		$type = $_POST["type"];

		$data = $this->log->getRttDetail3($hnbid, $callid, $startDate, $endDate, $limit, $error, $type);

		echo json_encode($data);
	}
}

/* End of file log.php */
/* Location: ./application/controllers/log.php */