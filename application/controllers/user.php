<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

		$this->load->helper(array('url', 'form','cookie','time_helper','file_helper','user_helper'));

		$models = array(
			'user_model'	=> 'user',
			'common_model'	=> 'commondb',
		);

		foreach ($models as $file => $object_name)
		{
			$this->load->model($file, $object_name);
		}

		session_start();
	}

	public function index()
	{
		if(!$this->input->cookie('login_user'))
			redirect('login', 'refresh');
		else
			$data['user'] = ucfirst($this->input->cookie('login_user'));

		$data['kpiWarn'] = $this->isKpiWarnProcessed();
		$data['alarmWarn'] = $this->isAlarmWarnProcessed();

		$data['title'] = 'User Account Details';
		$data['version'] = $this->config->item("version");
		$data['userDetails'] = $this->get_currentUserInfo();

		$this->load->view('user_view', $data);
	}

	public function isKpiWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isKpiWarnProcessed();
	}

	public function isAlarmWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isAlarmWarnProcessed();
	}

	public function get_allUserInfo()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$data = array();
		$data["user"] = $this->user->get_allUserInfo();
		$data["role"] = $this->user->get_allRoleInfo();

		echo json_encode($data);
	}

	public function add_user()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$create_by = $this->input->cookie('login_user');
		$username = $_POST['username'];
		$password = $_POST['password'];
		$email = $_POST['email'];
		$role = $_POST['role_option'];

		$hash = hash_password($password);
		echo $this->user->add_user($username, $hash, $email, $role, $create_by);
	}

	public function delete_user()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$users = $_POST['selectedUser'];
		$current_user = $this->input->cookie('login_user');

		echo $this->user->delete_user($users, $current_user);
	}

	public function get_currentUserInfo()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		return $this->user->get_currentUserInfo();
	}

	public function update_user()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$update_by = $this->input->cookie('login_user');
		$username = $_POST['username'];
		$password = $_POST['password'];
		$email = $_POST['email'];
		$roleId = $_POST['roleId'];

		if($password == NULL)
			$hash = NULL;
		else
			$hash = hash_password($password);

		echo $this->user->update_user($username, $hash, $email, $roleId, $update_by);
	}

	public function get_userRole()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$user = $this->input->cookie("login_user");

		echo $this->user->get_userRoleId($user);
	}

	public function get_allRoleInfo()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$data = $this->user->get_allRoleInfo();
		$num = $this->user->get_no_of_role();
		foreach($num as $k => $v)
		{
			$data[$k]["count"] = $v["count"];
		}
		echo json_encode($data);
	}
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */