<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kpi_report extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));	# error_reporting(E_ALL & ~E_NOTICE);
		ini_set('display_errors', 1);
		set_time_limit(0);

		$this->load->helper(array('time_helper','file_helper','url'));

		$models = array(
			'report_model' => 'reportdb',
			'common_model' => 'commondb',
		);

		foreach ($models as $file => $object_name)
		{
			$this->load->model($file, $object_name);
		}
	}

	public function index()
	{
		if(!$this->input->cookie('login_user'))
			redirect('login', 'refresh');
		else
			$data['user'] = ucfirst($this->input->cookie('login_user'));

		$data['kpiWarn'] = $this->isKpiWarnProcessed();
		$data['alarmWarn'] = $this->isAlarmWarnProcessed();

		$data['title'] = 'KPI Report Page';
		$data['version'] = $this->config->item("version");

		$this->load->view('kpi_report_view', $data);
	}

	public function isKpiWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isKpiWarnProcessed();
	}

	public function isAlarmWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isAlarmWarnProcessed();
	}

	public function get_kpireport_rawdata()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$version = $_POST["version"];
		$url = $_POST["url"];

		$criteria["startDate"] = $_POST["startDate"];
		$criteria["endDate"] = $_POST["endDate"];
		$criteria["userName"] = $this->input->cookie('login_user');
		$criteria["recalculate"] = ($_POST["recalculate"] == "true") ? ("Y") : ("N");

		## for output excel
		## Curl Post
		$fields_string = "";
		//url-ify the data for the POST
		foreach($criteria as $key => $value) {
			$fields_string .= $key.'='.$value.'&';
		}
		$fields_string = rtrim($fields_string,'&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	// make result retrun string, not just boolean
		curl_setopt($ch, CURLOPT_POST, count($_POST));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

		//execute post
		$result = curl_exec($ch);
		// $err = curl_errno ( $ch );
		// $errmsg = curl_error ( $ch );
		// $header = curl_getinfo ( $ch );
		// $httpCode = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );

		//close connection
		curl_close($ch);
		// $result = json_decode($result);	## return data in json data array
		## Curl Post End

		$temp = $this->config->item("kpi_pref");
		ksort($temp);

		$data["data"] = json_decode($result);
		$data["kpiName"] = $temp;
		echo json_encode($data);
	}

	public function getCSV()
	{
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"table-data.csv\"");
		$data=stripcslashes($_REQUEST['csv_text']);
		echo $data;
	}

	public function get_kpiDetail_report()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$url = $_POST["url"];

		$criteria["queryType"] = $_POST["queryType"];
		$criteria["date"] = $_POST["date"];
		$criteria["kpiName"] = $_POST["kpiName"];
		$criteria["userName"] = $this->input->cookie('login_user');

		## for output excel
		## Curl Post
		$fields_string = "";
		//url-ify the data for the POST
		foreach($criteria as $key => $value) {
			$fields_string .= $key.'='.$value.'&';
		}
		$fields_string = rtrim($fields_string,'&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	// make result retrun string, not just boolean
		curl_setopt($ch, CURLOPT_POST, count($_POST));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

		//execute post
		$result = curl_exec($ch);
		// $err = curl_errno ( $ch );
		// $errmsg = curl_error ( $ch );
		// $header = curl_getinfo ( $ch );
		// $httpCode = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );

		//close connection
		curl_close($ch);
		// $result = json_decode($result);	## return data in json data array
		echo $result;
		## Curl Post End

		// $data["data"] = json_decode($result);
		// $data["kpiName"] = $this->config->item("kpi_pref");
		// echo json_encode($data);
	}

	public function get_KPIDetail2_hourly()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$url = $_POST["url"];

		$criteria["queryType"] = $_POST["queryType"];
		$criteria["date"] = $_POST["date"];
		$criteria["kpiName"] = $_POST["kpiName"];
		$criteria["hnbMAC"] = $_POST["hnbMAC"];

		## for output excel
		## Curl Post
		$fields_string = "";
		//url-ify the data for the POST
		foreach($criteria as $key => $value) {
			$fields_string .= $key.'='.$value.'&';
		}
		$fields_string = rtrim($fields_string,'&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	// make result retrun string, not just boolean
		curl_setopt($ch, CURLOPT_POST, count($_POST));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

		//execute post
		$result = curl_exec($ch);
		// $err = curl_errno ( $ch );
		// $errmsg = curl_error ( $ch );
		// $header = curl_getinfo ( $ch );
		// $httpCode = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );

		//close connection
		curl_close($ch);
		// $result = json_decode($result);	## return data in json data array
		echo $result;
		## Curl Post End

		// $data["data"] = json_decode($result);
		// $data["kpiName"] = $this->config->item("kpi_pref");
		// echo json_encode($data);
	}

	public function get_KPIDetail3_raw()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$url = $_POST["url"];

		$criteria["queryType"] = $_POST["queryType"];
		$criteria["date"] = $_POST["date"];
		$criteria["kpiName"] = $_POST["kpiName"];
		$criteria["hnbMAC"] = $_POST["hnbMAC"];
		$criteria["hour"] = $_POST["hour"];

		## for output excel
		## Curl Post
		$fields_string = "";
		//url-ify the data for the POST
		foreach($criteria as $key => $value) {
			$fields_string .= $key.'='.$value.'&';
		}
		$fields_string = rtrim($fields_string,'&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	// make result retrun string, not just boolean
		curl_setopt($ch, CURLOPT_POST, count($_POST));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

		//execute post
		$result = curl_exec($ch);
		// $err = curl_errno ( $ch );
		// $errmsg = curl_error ( $ch );
		// $header = curl_getinfo ( $ch );
		// $httpCode = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );

		//close connection
		curl_close($ch);
		// $result = json_decode($result);	## return data in json data array
		echo $result;
		## Curl Post End
	}
}

/* End of file kpi_report.php */
/* Location: ./application/controllers/kpi_report.php */