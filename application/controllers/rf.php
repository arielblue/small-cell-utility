<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rf extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));	# error_reporting(E_ALL & ~E_NOTICE);
		ini_set('display_errors', 1);
		set_time_limit(0);

		$this->load->helper(array('url', 'time_helper'));

		$models = array(
			'common_model' => 'commondb',
			'rf_model' => 'rf'
		);

		foreach ($models as $file => $object_name)
		{
			$this->load->model($file, $object_name);
		}
	}

	public function index()
	{
		if(!$this->input->cookie('login_user'))
			redirect('login', 'refresh');
		else
			$data['user'] = ucfirst($this->input->cookie('login_user'));

		$data['kpiWarn'] = $this->isKpiWarnProcessed();
		$data['alarmWarn'] = $this->isAlarmWarnProcessed();

		$data['title'] = 'RF Page';
		$data['version'] = $this->config->item("version");
		$data['hnb'] = $this->rf->getHnbList();

		$this->load->view('rf_view', $data);
	}

	public function isKpiWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isKpiWarnProcessed();
	}

	public function isAlarmWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isAlarmWarnProcessed();
	}

	public function get_graph_data()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$hnb = $_POST["hnb"];
		$startDate = $_POST["startDate"].":00:00";
		$endDate = $_POST["endDate"].":00:00";
		$startDate = str_replace(array(" ","-"), array(":",":"), $startDate);
		$endDate = str_replace(array(" ","-"), array(":",":"), $endDate);
		
		echo $this->rf->get_graph_data($hnb, $startDate, $endDate);
	}

	public function get_graph_2hrData()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$hnb = $_POST["hnb"];
		$startDate = to_datetime_obj($_POST["datetime"]);
		$endDate = to_datetime_obj($_POST["datetime"]);
		$startDate = $startDate->sub(new DateInterval('PT3600S'));
		$endDate = $endDate->add(new DateInterval('PT3600S'));
		$startDate = $startDate->format("Y:m:d:H:i:s");
		$endDate = $endDate->format("Y:m:d:H:i:s");

		echo $this->rf->get_graph_data($hnb, $startDate, $endDate);
	}

	public function message($to = 'World')
	{
		echo "Hello {$to}!".PHP_EOL;
	}
}

/* End of file rf.php */
/* Location: ./application/controllers/rf.php */