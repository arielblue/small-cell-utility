<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alarm_warning extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		ini_set('display_errors', 1);
		set_time_limit(0);

		$this->load->helper(array('url'));

		$models = array(
			'warning_model' => 'warningdb',
			'common_model' => 'commondb',
		);

		foreach ($models as $file => $object_name)
		{
			$this->load->model($file, $object_name);
		}
	}

	public function index($offset)
	{
		if(!$this->input->cookie('login_user'))
			redirect('login', 'refresh');
		else
			$data['user'] = ucfirst($this->input->cookie('login_user'));

		$data['kpiWarn'] = $this->isKpiWarnProcessed();
		$data['alarmWarn'] = $this->isAlarmWarnProcessed();

		$data['title'] = 'Unprocessed Alarm Warning Page';
		$data['version'] = $this->config->item("version");
		$data['note_maxlengh'] = $this->config->item("note_maxlengh");

		$total_segments = $this->uri->total_segments();

		$this->paginationInit(NULL);

		// decide page offset
		if($offset == NULL || $offset == "" || !is_numeric($this->uri->segment($total_segments)) ) $offset = 1; // don't delete !
		else $offset = $this->uri->segment($total_segments);

		$data['offset'] = $offset;	// don't delete!
		$data['per_page'] = $this->config->item("page_size"); // don't delete!
		$data['links']  = $this->pagination->create_links();

		$data['result'] = $this->searchUnproAlarmWarning(NULL, $offset, $this->config->item("page_size"));

		$this->load->view('alarm_warning_view', $data);
	}

	public function isKpiWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isKpiWarnProcessed();
	}

	public function isAlarmWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isAlarmWarnProcessed();
	}

	public function updateAlarmWarning()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$data = json_decode($_POST["data"]);
		$user = $this->input->cookie('login_user');
		echo $this->warningdb->updateAlarmWarning($data, $user);
	}

	public function paginationInit($data)
	{
		$this->load->library('pagination');
		$config['uri_segment'] = 2;
		$config['base_url'] = current_url();

		$config['total_rows'] = $this->warningdb->get_alram_listCountUnprocess();

		$config['per_page'] = $this->config->item("page_size");
		$config['num_links'] = $this->config->item("link_num");
		$config['use_page_numbers']  = TRUE;

		$config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '
		';
		$config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '
		';
		$config['cur_tag_open'] = "<b>
		";
		$config['cur_tag_close'] = "
		</b>";

		$config['first_link'] = "First";
		$config['last_link'] = "Last";

		$this->pagination->initialize($config);
	}

	public function searchUnproAlarmWarning($data, $offset, $per_page)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		return $this->warningdb->searchUnproAlarmWarning($data, $offset, $per_page);
	}


	public function get_report_rawdata()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$start_time = $_POST["start_time"];
		$end_time = $_POST["end_time"];
		$report_name = $_POST["report_name"];
		$hnb = $_POST["hnb"];
		$version = $_POST["version"];
		$url = $_POST["url"];
		unset($_POST['url']);
		unset($_POST['version']);

		$realreportname = $this->config->item("report_name");

		## for output excel
		if($report_name != key($realreportname))## not getting the first report name
		{
			## Curl Post
			$fields_string = "";
			//url-ify the data for the POST
			foreach($_POST as $key => $value) {
				$fields_string .= $key.'='.$value.'&';
			}
			$fields_string = rtrim($fields_string,'&');

			//open connection
			$ch = curl_init();

			//set the url, number of POST vars, POST data
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	// make result retrun string, not just boolean
			curl_setopt($ch, CURLOPT_POST, count($_POST));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

			//execute post
			$result = curl_exec($ch);
			// $err = curl_errno ( $ch );
			// $errmsg = curl_error ( $ch );
			// $header = curl_getinfo ( $ch );
			// $httpCode = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );

			//close connection
			curl_close($ch);
			$result = json_decode($result);	## return data in json data array
			## Curl Post End

			echo $this->gen_table($report_name, $result, $realreportname[$report_name], $version);
		}
		else
		{
			$output = $this->reportdb->get_report_rawdata($report_name, $hnb, $start_time, $end_time);

			## for Daily Summary to prevent get null data
			$sum = 0;
			foreach($output as $k => $v)
			{
				$sum = $sum + count($v);
				if($sum != 0)
					break;
			}

			if($sum != 0)
			{
				echo $this->compute_dailysum_data($report_name, $output, $version);
				unset($output);
			}
			else
				echo "NO DATA";
		}
	}

	public function gen_table($reportname, $output, $realreportname, $version)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$attr = $this->config->item("report_attr");
		$attr = $attr[$reportname];

		if($version == -100) ## NOT IE
		{
			$buf = "<br><b>$realreportname</b> <button type='button' class='juibtn' id='clickExcelBtn'>Download</button><br><div id='reportRawData_div'><table><tr><th>Time</th>";
		}
		else
		{
			$buf = "<br><b>$realreportname</b> <form action='report/getCSV' method='POST' style='display:inline;'><input type='hidden' name='csv_text' id='csv_text'><input type='submit' class='juibtn' id='clickExcelBtn' value='Download' onclick='getCSVData()'></form><br><div id='reportRawData_div'><table id='reportRawData_tbl'><tr><th>Time</th>";
		}

		# add average
		foreach($attr as $k)
			$buf = $buf."<th>$k</th>";
		$buf = $buf."</tr><tr><td><b>RATE</b>: ".$output->{'rate'}."%</td>";

		# add summary
		foreach($attr as $k)
			$buf = $buf."<td>".$output->{$k}."</td>";
		$buf = $buf."</tr>";

		foreach ($output->{"rawdata"} as $k => $v)
		{
			$timeStr = explode(" ", $v[0]);
			$timeStr = $timeStr[0]."-".$timeStr[1];
			$buf = $buf."<tr><td>$timeStr</td>";
			for($i = 1; $i <= count($attr); $i++)
			{
				if($v[$i] != NULL && $v[$i] != "")
					$buf = $buf."<td>$v[$i]</td>";
				else
					$buf = $buf."<td></td>";
			}
			$buf = $buf."</tr>";
		}
		$buf = $buf."</table></div>";
		echo $buf;
	}

	public function getCSV()
	{
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"table-data.csv\"");
		$data=stripcslashes($_REQUEST['csv_text']);
		echo $data;
	}
}

/* End of file alarm_warning.php */
/* Location: ./application/controllers/alarm_warning.php */