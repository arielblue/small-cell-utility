<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Housekeeping Class
 * Retrieve housekeeping info (days), delete Database data and raw logs (xml & tgz)
 *
 * @category	CLI Tool (process)
 * @author		Wendy Yu
 * @link		php index.php housekeeping start $days (unit: day) [under project folder]
 * @link		https://localhost/configtool/index.php/housekeeping/start/25
 *
 *
 */
class Housekeeping extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));	# error_reporting(E_ALL & ~E_NOTICE);
		ini_set('display_errors', 1);
		set_time_limit(0);

		$this->load->helper(array('time_helper','file_helper','url'));
		$this->load->model('configtool_model','tooldb');
	}

	public function rawdata($days)
	{
		## 1 get input and config of pmfile and log
		$rawlogfolder	= $this->config->item('rawlogfolder');
		$pmfilefolder	= $this->config->item('pmfilefolder');

		if($_POST['days'] == NULL)
		{
			if($days === NULL)
			{
				$data = json_decode($this->tooldb->get_housekeep_info());
				$days = intval($data->{'keep_rawdata_days'});
			}
			else
				$days = intval($days);
		}
		else
			$days = intval($_POST['days']);

		$start_time = get_current_time_in_sec();
		if($days !== NULL)
		{
			// gui_log("*** Raw Data: Delete pmfile before $days days ***");
			echo timestamp_string()." *** Raw Data: Delete pmfile before $days days ***\n<br>";

			$now = date("Y-m-d"); #"Y-m-d H:i:s"
			$now = new DateTime($now);
			$now->modify("-$days day");
			$before = $now->format("Y-m-d");
			$thedate = new DateTime($before);

			// gui_log("Delete PMfiles under $pmfilefolder before $before(not include)");
			echo timestamp_string()." Delete PMfiles under $pmfilefolder before $before(not include)\n<br>";

			## 2 get pmfiles in pmfile folder
			$pmfiles = get_list($pmfilefolder); #C20140701.125900
			$delete = array();
			foreach($pmfiles as $f)
			{
				$temp = explode(".", $f);
				$hnb = explode("_", $temp[2]);
				$hnb = $hnb[1];

				$filetime = substr($temp[0], 1);
				$filetime = new DateTime(substr($filetime, 0, 4)."-".substr($filetime, 4, 2)."-".substr($filetime, 6, 2));
				$diff = $thedate->diff($filetime);

				$buf = "filetime: ".$filetime->format("Ymd").", hnb: $hnb, ".$diff->format(' %R%a days');
				// gui_log($buf);
				echo timestamp_string()." $buf\n<br>";

				## 3 delete pmfiles created $days days before
				if($diff->format('%R') == "-")
				{
					array_push($delete, $f);
					// unlink("$pmfilefolder\\$f"); #delete the file
				}
			}
			## 4 get rawlog in rawlog folder
			## 5 delete rawlog created $days days before
		}

		$end_time = get_current_time_in_sec();
		$total_time = $end_time-$start_time+1; // how much time spent this turn

		// gui_log("");
		// gui_log("It took $total_time seconds. Deleting rawdata done.");
		echo timestamp_string()." It took $total_time seconds. Deleting rawdata done.\n<br>";
		echo timestamp_string()."\n<br>";
	}

	public function dbdata($days)
	{
		## 1 get input and default days
		if($_POST['days'] == NULL)	// $_POST will be set anyway, so it never be actually NULL, at least ""
		{
			if($days === NULL)
			{
				$data = json_decode($this->tooldb->get_housekeep_info());
				$days = intval($data->{'keep_dbdata_days'});
			}
			else
				$days = intval($days);
		}
		else
			$days = intval($_POST['days']);

		$start_time = get_current_time_in_sec();
		if($days !== NULL)
		{
			// gui_log("*** DB Data: Delete database data before $days days ***");
			echo timestamp_string()." *** DB Data: Delete database data before $days days ***\n<br>";

			$now = date("Y-m-d"); #"Y-m-d H:i:s"
			$now = new DateTime($now);
			$now->modify("-$days day");
			$before = $now->format("Y-m-d");

			## 2 delete database data $days days before
			// gui_log("Delete database data before $before(not include)");
			echo timestamp_string()." Delete database data before $before(not include)\n<br>";
			$before = $now->format("Y-m-d H:i:s");
			$this->tooldb->housekeep_deleteDB($before);
		}

		$end_time = get_current_time_in_sec();
		$total_time = $end_time-$start_time+1; // how much time spent this turn

		// gui_log("");
		// gui_log("It took $total_time seconds. Deleting database data done.");
		echo timestamp_string()." It took $total_time seconds. Deleting database data done.\n<br>";
		echo timestamp_string()."\n<br>";
	}

	public function start($days)
	{
		// if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		## 1 get config
		$pmfilefolder	= $this->config->item('pmfilefolder');
		$rawlogfolder	= $this->config->item('rawlogfolder');

		if($_POST['days'] == NULL)	// $_POST will be set anyway, so it never be actually NULL, at least ""
		{
			if($days === NULL)
			{
				$data = json_decode($this->tooldb->get_housekeep_info());
				$days = intval($data->{'keep_rawlog_days'});
			}
			else
			{
				$days = intval($days);
			}
		}
		else
		{
			$days = intval($_POST['days']);
		}

		$start_time = get_current_time_in_sec();
		if($days !== NULL)
		{
			// gui_log("*** PMfile: Delete data before $days days ***");
			echo timestamp_string()." *** PMfile: Delete data before $days days ***\n<br>";

			$now = date("Y-m-d"); #"Y-m-d H:i:s"
			$now = new DateTime($now);
			$now->modify("-$days day");
			$before = $now->format("Y-m-d");
			$thedate = new DateTime($before);

			// gui_log("Delete PMfiles under $pmfilefolder before $before(not include)");
			echo timestamp_string()." Delete PMfiles under $pmfilefolder before $before(not include)\n<br>";

			## 2 get files in pmfile folder
			$pmfiles = get_list($pmfilefolder); #C20140701.125900
			$delete = array();
			foreach($pmfiles as $f)
			{
				$temp = explode(".", $f);
				$hnb = explode("_", $temp[2]);
				$hnb = $hnb[1];

				$filetime = substr($temp[0], 1);
				$filetime = new DateTime(substr($filetime, 0, 4)."-".substr($filetime, 4, 2)."-".substr($filetime, 6, 2));
				$diff = $thedate->diff($filetime);

				$buf = "filetime: ".$filetime->format("Ymd").", hnb: $hnb, ".$diff->format(' %R%a days');
				// gui_log($buf);
				echo timestamp_string()." $buf\n<br>";

				## 3 delete pmfiles created $days days before
				if($diff->format('%R') == "-")
				{
					array_push($delete, $f);
					// unlink("$pmfilefolder\\$f"); #delete the file
				}
			}

			## 4 delete database data $days days before
			$before = $now->format("Y-m-d H:i:s");
			$this->tooldb->housekeep_deleteDB($before);
		}
		// else 	gui_log("Enter an INTEGER and restart housekeeping process manually please");

		$end_time = get_current_time_in_sec();
		$total_time = $end_time-$start_time+1; // how much time spent this turn

		// gui_log("");
		// gui_log("It took $total_time seconds. Housekeeping done.");
		echo timestamp_string()." It took $total_time seconds. Housekeeping done.\n<br>";
		echo timestamp_string()."\n<br>";
	}

	public function get_housekeep_info()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		echo $this->tooldb->get_housekeep_info();
	}

	public function update_housekeep_info()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		echo $this->tooldb->update_housekeep_info($_POST['keep_dbdata_days'], $_POST['keep_rawdata_days']);
	}

	public function testshell()
	{
		$output = shell_exec('ipconfig');
		echo "<pre>$output</pre>";
		echo "******************************************<br>";
		$i = 0;
		exec('ipconfig /all', $response);
		foreach($response as $line)
		{
			$line = $line;
			if (strpos($line, "DNS")>0)
			{
				print (trim($line));
				echo ("\n");
			}
		}
	}
}

/* End of file housekeeping.php */
/* Location: ./application/controllers/housekeeping.php */