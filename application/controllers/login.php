<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

		$models = array(
			'user_model'	=> 'user',
			'login_model'	=> 'login_model',
		);

		foreach ($models as $file => $object_name)
		{
			$this->load->model($file, $object_name);
		}

		$this->load->helper(array('url','form','cookie','user_helper', 'file_helper'));

		session_start();
	}

	public function index()
	{
		$data['title'] = 'Login Page';
		$this->load->view('login_view', $data);

		if($this->input->cookie('login_user'))
			redirect('configtool', 'refresh');
	}

	public function signin()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$username = $_POST['username'];
		$password = $_POST['password'];

		$result = $this->login_model->signin($username, $password);
		if($result === true)
		{
			$path = "/".get_project_name();
			setcookie("login_user", $username, 0, $path, NULL, FALSE, TRUE);

			echo true;
		}
		else
			echo $result;
	}

	public function logout()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$path = "/".get_project_name();
		session_unset();
		session_destroy();
		session_write_close();
		setcookie(session_name(),'',0,'/');
		setcookie("login_user", "", -1, $path, NULL, FALSE, TRUE);

		redirect('login', 'refresh');
	}


	public function register()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$username = $_POST['username'];
		$password = $_POST['password'];
		$email = $_POST['email'];
		$role = $_POST['role'];
		
		if($role == NULL)
			$role = 3;

		$hash = hash_password($password);

		echo $this->user->create_user($username, $hash, $email, $role);
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */