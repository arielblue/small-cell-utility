<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configtool extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$this->load->helper(array('url', 'form','cookie','time_helper','file_helper','user_helper'));

		$models = array(
			'configtool_model' => 'tooldb',
			'common_model'	=> 'commondb',
		);

		foreach ($models as $file => $object_name)
		{
			$this->load->model($file, $object_name);
		}

		session_start();
	}

	public function index()
	{
		if(!$this->input->cookie('login_user'))
			redirect('login', 'refresh');
		else
			$data['user'] = ucfirst($this->input->cookie('login_user'));

		$data['kpiWarn'] = $this->isKpiWarnProcessed();
		$data['alarmWarn'] = $this->isAlarmWarnProcessed();

		$data['title'] = 'Config Page';
		$data['version'] = $this->config->item("version");

		$this->load->view('configtool_view', $data);
	}

	public function isKpiWarnProcessed()
	{
		return $this->commondb->isKpiWarnProcessed();
	}

	public function isAlarmWarnProcessed()
	{
		return $this->commondb->isAlarmWarnProcessed();
	}

	public function get_info($server)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		echo $this->tooldb->get_info($server);
	}

	public function update_server()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$ip = $_POST['ip'];
		$port = $_POST['port'];
		$configName = $_POST['configName'];
		$loginName = $_POST['loginName'];
		$password = $_POST['password'];
		$ftp_logintype = $_POST['ftp_logintype'];
		$path = $_POST['directoryPath'];
		$server = $_POST['server'];
		$userName = $this->input->cookie('login_user');

		if($path == "" && $path == NULL) $path = "/";

		if($password != NULL) $hash = server_password($password);
		else $hash = "";

		echo $this->tooldb->update_server($ip, $port, $configName, $loginName, $hash, $ftp_logintype, $path, $server, $userName);
	}

	public function delete_server($server)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$selectedIP = $_POST['selectedIP'];
		$this->tooldb->delete_server($selectedIP, $server);
	}

	public function add_server()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$ip = $_POST['addip'];
		$port = $_POST['addport'];
		$configName = $_POST['configName'];
		$loginName = $_POST['loginName'];
		$password = $_POST['password'];
		$ftp_logintype = $_POST['ftp_logintype'];
		$path = $_POST['directoryPath'];
		$server = $_POST['server'];
		$userName = $this->input->cookie('login_user');

		if($path == "" && $path == NULL) $path = "/";

		$hash = server_password($password);
		echo $this->tooldb->add_server($ip, $port, $configName, $loginName, $hash, $ftp_logintype, $path, $server, $userName);
	}

	public function get_kpiTarget_info($act)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		echo $this->tooldb->get_kpiTarget_info($act);
	}

	public function update_kpiTarget()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$target = $_POST['target'];
		$updateUser = $this->input->cookie('login_user');
		echo $this->tooldb->update_kpiTarget($target, $updateUser);
	}

	public function add_kpiTarget()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$kpi = $_POST["kpi_option"]; //kpi_option or addKpiName
		$target = $_POST["addTarget"];
		$updateUser = $this->input->cookie('login_user');
		echo $this->tooldb->add_kpiTarget($kpi, $target, $updateUser);
	}

	public function delete_kpiTarget()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$selectedKpi = $_POST['selectedKpi'];
		$this->tooldb->delete_kpiTarget($selectedKpi);
	}

	public function get_kpiPref()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$kpiPref = array();
		$kpiPref["user"] = ucfirst($this->input->cookie('login_user'));
		$kpiPref["kpi_pref"] = $this->config->item("kpi_pref");
		$kpiPref["userKpiPref"] = $this->tooldb->get_usrPref($this->input->cookie('login_user'), "kpi");
		echo json_encode($kpiPref);
	}

	public function add_kpiPref()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$kpiPref = $_POST["kpiPref"];
		$user = $_POST["user"];
		echo $this->tooldb->add_kpiPref($kpiPref, $user);
	}

	public function get_counterPref()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$counterName = $this->tooldb->get_AllCounter();
		$counterName = array_diff($counterName, array("hnbMAC", "endtime", "updateTime"));
		asort($counterName);

		$counterPref = array();
		$counterPref["user"] = ucfirst($this->input->cookie('login_user'));
		$counterPref["counter_pref"] = $counterName;
		$counterPref["userCounterPref"] = $this->tooldb->get_usrPref($this->input->cookie('login_user'), "counter");
		echo json_encode($counterPref);
	}

	public function add_counterPref()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$counterPref = $_POST["counterPref"];
		$user = $_POST["user"];
		echo $this->tooldb->add_counterPref($counterPref, $user);
	}

	public function get_HNBPref()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$hnbList = $this->tooldb->get_AllHNB();
		asort($hnbList);

		$hnbPref = array();
		$hnbPref["user"] = ucfirst($this->input->cookie('login_user'));
		$hnbPref["hnblist"] = $hnbList;
		$hnbPref["userHnbPref"] = $this->tooldb->get_usrPref($this->input->cookie('login_user'), "hnb");
		echo json_encode($hnbPref);
	}

	public function add_hnbPref()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$hnbPref = $_POST["hnbPref"];
		$user = $this->input->cookie('login_user');
		echo $this->tooldb->add_hnbPref($hnbPref, $user);
	}
}

/* End of file configtool.php */
/* Location: ./application/controllers/configtool.php */