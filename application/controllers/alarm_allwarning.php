<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alarm_allwarning extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		ini_set('display_errors', 1);
		set_time_limit(0);

		$this->load->helper(array('url'));

		$models = array(
			'warning_model' => 'warningdb',
			'common_model' => 'commondb',
		);

		foreach ($models as $file => $object_name)
		{
			$this->load->model($file, $object_name);
		}
	}

	public function index($offset)
	{
		/*				data to display in view 				*/
		if(!$this->input->cookie('login_user'))
			redirect('login', 'refresh');
		else
			$data['user'] = ucfirst($this->input->cookie('login_user'));

		$data['kpiWarn'] = $this->isKpiWarnProcessed();
		$data['alarmWarn'] = $this->isAlarmWarnProcessed();

		$data['title'] = 'Alarm All Warning Page';
		$data['version'] = $this->config->item("version");

		$total_segments = $this->uri->total_segments();

		$this->paginationInit($postData);

		// decide page offset
		if($offset == NULL || $offset == "" || !is_numeric($this->uri->segment($total_segments)) ) $offset = 1; // don't delete !
		else $offset = $this->uri->segment($total_segments);

		$data['offset'] = $offset;	// don't delete!
		$data['per_page'] = $this->config->item("page_size"); // don't delete!
		$data['links']  = $this->pagination->create_links();

		// search constraints setting
		if(count($postData) == 0)	// no search constraint
		{
			$data['result'] = $this->searchAlarmAllWarning(NULL, $offset, $this->config->item("page_size"));
		}

		$this->load->view('alarm_allwarning_view', $data);
	}

	public function isKpiWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isKpiWarnProcessed();
	}

	public function isAlarmWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isAlarmWarnProcessed();
	}

	public function paginationInit($data)
	{
		$this->load->library('pagination');
		$config['uri_segment'] = 2;
		$config['base_url'] = current_url();

		if(count($data) == 0)
			$config['total_rows'] = $this->warningdb->get_alram_listCount(NULL);

		$config['per_page'] = $this->config->item("page_size");
		$config['num_links'] = $this->config->item("link_num");
		$config['use_page_numbers']  = TRUE;

		$config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '
		';
		$config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '
		';
		$config['cur_tag_open'] = "<b>
		";
		$config['cur_tag_close'] = "
		</b>";

		$config['first_link'] = "First";
		$config['last_link'] = "Last";

		$this->pagination->initialize($config);
	}

	public function getPostData($data)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		unset($data["kpiSelect"]);

		if($data["kpiInsert"] != NULL || $data["kpiInsert"] != "")
			$data["kpiInsert"] = explode(",", $data["kpiInsert"]);
		else
			unset($data["kpiInsert"]);

		if($data["startTime"] != NULL || $data["startTime"] != "")
		{
			$data["startTime"] = $data["startTime"]." 00:00:00";
			$data["endTime"] = $data["endTime"]." 23:59:59";
		}
		else
		{
			unset($data["startTime"]);
			unset($data["endTime"]);
		}

		return $data;
	}

	public function genConstraintData($str)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$data = array();
		$temp = explode("~", $str);
		$data["kpiInsert"] = explode("-", $temp[0]);
		if($temp[1] != NULL) $data["startTime"] = $temp[1]." 00:00:00";
		if($temp[2] != NULL) $data["endTime"] = $temp[2]." 23:59:59";

		return $data;
	}

	public function searchAlarmAllWarning($data, $offset, $per_page)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		return $this->warningdb->searchAlarmAllWarning($data, $offset, $per_page);
	}
}

/* End of file alarm_allwarning.php */
/* Location: ./application/controllers/alarm_allwarning.php */