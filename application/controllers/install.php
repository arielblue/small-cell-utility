<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Install extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$this->load->helper(array('url'));

		$models = array(
			'configtool_model' => 'tooldb',
			'log_model' => 'log',
		);

		foreach ($models as $file => $object_name)
		{
			$this->load->model($file, $object_name);
		}
	}

	public function index()
	{
		$data['title'] = 'Install Page';
		$data['version'] = $this->config->item("version");
		$this->load->view('install_view', $data);
	}

	public function setup_database()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		echo $this->tooldb->setup_database();
		echo $this->tooldb->setup_faplog();
		echo $this->log->setup_database();
	}

	public function setup_logdb()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		echo $this->log->setup_database();
	}
}

/* End of file install.php */
/* Location: ./application/controllers/install.php */