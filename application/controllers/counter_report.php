<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Counter_report extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));	# error_reporting(E_ALL & ~E_NOTICE);
		ini_set('display_errors', 1);
		set_time_limit(0);

		$this->load->helper(array('time_helper','file_helper','url'));

		$models = array(
			'report_model' => 'reportdb',
			'common_model' => 'commondb',
		);

		foreach ($models as $file => $object_name)
		{
			$this->load->model($file, $object_name);
		}
	}

	public function index()
	{
		if(!$this->input->cookie('login_user'))
			redirect('login', 'refresh');
		else
			$data['user'] = ucfirst($this->input->cookie('login_user'));

		$data['kpiWarn'] = $this->isKpiWarnProcessed();
		$data['alarmWarn'] = $this->isAlarmWarnProcessed();

		$data['title'] = 'Counter Report Page';
		$data['rname'] = $this->config->item("report_name");
		$data['version'] = $this->config->item("version");

		$this->load->view('counter_report_view', $data);
	}

	public function isKpiWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isKpiWarnProcessed();
	}

	public function isAlarmWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		return $this->commondb->isAlarmWarnProcessed();
	}

	public function get_hnbMAC_list()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		echo $this->reportdb->get_hnbMAC_list();
	}

	public function get_report_rawdata()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$start_time = $_POST["start_time"];
		$end_time = $_POST["end_time"];
		$report_name = $_POST["report_name"];
		$hnb = $_POST["hnb"];
		$version = $_POST["version"];
		$url = $_POST["url"];
		unset($_POST['url']);
		unset($_POST['version']);

		$realreportname = $this->config->item("report_name");

		## for output excel
		if($report_name != key($realreportname))## not getting the first report name
		{
			## Curl Post
			$fields_string = "";
			//url-ify the data for the POST
			foreach($_POST as $key => $value) {
				$fields_string .= $key.'='.$value.'&';
			}
			$fields_string = rtrim($fields_string,'&');

			//open connection
			$ch = curl_init();

			//set the url, number of POST vars, POST data
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	// make result retrun string, not just boolean
			curl_setopt($ch, CURLOPT_POST, count($_POST));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

			//execute post
			$result = curl_exec($ch);
			// $err = curl_errno ( $ch );
			// $errmsg = curl_error ( $ch );
			// $header = curl_getinfo ( $ch );
			// $httpCode = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );

			//close connection
			curl_close($ch);
			$result = json_decode($result);	## return data in json data array
			## Curl Post End

			echo $this->gen_table($report_name, $result, $realreportname[$report_name], $version);
		}
		else
		{
			$output = $this->reportdb->get_report_rawdata($report_name, $hnb, $start_time, $end_time);

			## for Daily Summary to prevent get null data
			$sum = 0;
			foreach($output as $k => $v)
			{
				$sum = $sum + count($v);
				if($sum != 0)
					break;
			}

			if($sum != 0)
			{
				echo $this->compute_dailysum_data($report_name, $output, $version);
				unset($output);
			}
			else
				echo "NO DATA";
		}
	}

	public function gen_table($reportname, $output, $realreportname, $version)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$attr = $this->config->item("report_attr");
		$attr = $attr[$reportname];

		if($version == -100) ## NOT IE
		{
			$buf = "<br><b>$realreportname</b> <button type='button' class='juibtn' id='clickExcelBtn'>Download</button><br><div id='reportRawData_div'><table><tr><th>Time</th>";
		}
		else
		{
			$buf = "<br><b>$realreportname</b> <form action='counter_report/getCSV' method='POST' style='display:inline;'><input type='hidden' name='csv_text' id='csv_text'><input type='submit' class='juibtn' id='clickExcelBtn' value='Download' onclick='getCSVData()'></form><br><div id='reportRawData_div'><table id='reportRawData_tbl'><tr><th>Time</th>";
		}

		# add average
		foreach($attr as $k)
			$buf = $buf."<th>$k</th>";

		if($reportname != "SCTP_Abort")
			$percent = "%</td>";	// percentage
		else
			$percent = "</td>";		// count

		$buf = $buf."</tr><tr><td><b>RATE</b>: ".$output->{'rate'}.$percent;

		# add summary
		foreach($attr as $k)
			$buf = $buf."<td>".$output->{$k}."</td>";
		$buf = $buf."</tr>";

		foreach ($output->{"rawdata"} as $k => $v)
		{
			$timeStr = explode(" ", $v[0]);
			$timeStr = $timeStr[0]."-".$timeStr[1];
			$buf = $buf."<tr><td>$timeStr</td>";
			for($i = 1; $i <= count($attr); $i++)
			{
				if($v[$i] != NULL && $v[$i] != "")
					$buf = $buf."<td>$v[$i]</td>";
				else
					$buf = $buf."<td></td>";
			}
			$buf = $buf."</tr>";
		}
		$buf = $buf."</table></div>";
		echo $buf;
	}

	public function compute_dailysum_data($reportname, $data, $version)
	{
		##"RRCAttConnEstab","RABFailEstabCS" => ["endtime","value"]
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$temp = array();
		$attr = $this->config->item("report_attr");
		$attr = $attr[$reportname];
		$realreportname = $this->config->item("report_name");
		$realreportname = $realreportname[$reportname];

		## computation of all counter except HNBState
		foreach($attr as $a)
		{
			$temp[$a] = array();
			$v = $data[$a];	// {datetime $v[i][0], value $v[i][1]}

			## first benchmark (day) for computation
			$newDate = to_datetime_obj($v[0][0]);
			$value = $v[0][1];

			for($i = 1; $i < count($v); $i++)
			{
				$date = to_datetime_obj($v[$i][0]);
				if(detect_same_date($date, $newDate))
				{
					## add value of same date
					if($v[$i][1] != NULL && $v[$i][1] != "")	$value += $v[$i][1];
				}
				else
				{
					$time = $newDate->format("Y-m-d");
					array_push($temp[$a], array($time, $value));

					## new benchmark (day) for computation
					$newDate = to_datetime_obj($v[$i][0]);
					$value = $v[$i][1];
				}
			}

			## add the last computation data
			$time = $newDate->format("Y-m-d");
			array_push($temp[$a], array($time, $value));
			unset($time);
		}

		$data = array();
		foreach($attr as $a)
			$data[$a] = $temp[$a];
		unset($temp);

		## generate daily summary table
		if($version == -100)
			$buf = "<br><b>$realreportname</b> <button type='button' class='juibtn' id='clickExcelBtn'>Download</button><br><div id='reportRawData_div'><table><tr><th>Counter</th>";
		else
		{
			$buf = "<br><b>$realreportname</b> <form action='report/getCSV' method='POST' style='display:inline;'><input type='hidden' name='csv_text' id='csv_text'><input type='submit' class='juibtn' id='clickExcelBtn' value='Download' onclick='getCSVData()'></form><br><div id='reportRawData_div'><table id='reportRawData_tbl'><tr><th>Counter</th>";
		}

		$date = $data[$attr[0]];
		for($i = 0; $i < count($date); $i++)
			$buf = $buf."<th>".$date[$i][0]."</th>";
		$buf = $buf."</tr>";

		foreach($data as $k => $v)
		{
			$buf = $buf."<tr><td>$k</td>";
			for($i = 0; $i < count($v); $i++)
			{
				if(is_numeric($v[$i][1]))
					$buf = $buf."<td>".$v[$i][1]."</td>";
				else
					$buf = $buf."<td></td>";
			}
			$buf = $buf."</tr>";
		}

		$buf = $buf."</table></div>";

		echo $buf;
		unset($data);
	}

	public function getCSV()
	{
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"table-data.csv\"");
		$data=stripcslashes($_REQUEST['csv_text']);
		echo $data;
	}

	public function get_timeDuration()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$hnb = $_POST["hnb"];

		echo $this->reportdb->get_timeDuration($hnb);
	}

	public function message($to = 'World')
	{
		echo "Hello {$to}!".PHP_EOL;
	}
}

/* End of file Counter_report.php */
/* Location: ./application/controllers/counter_report.php */