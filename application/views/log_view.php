<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html" http-equiv="Content-Type;encoding" charset='utf-8'>
	<title><?php echo $title;?></title>

	<script language="javascript" type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.form.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui-timepicker-addon.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flot.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flot.pie.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-scrollTo.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.tostie.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/common.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/log.js"></script>

	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.min.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/common.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.tostie.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/log.css" type="text/css" media="screen"/>
</head>

<script>
</script>

<body>
<div class="wrapper">

	<div class="header">
		<div class="logo"></div>
		<div id="logout"><span class="ui-icon ui-icon-white ui-icon-person"></span>&nbsp;<a href="user"><?php echo $user;?></a>&nbsp;/&nbsp;<a href="logout">Logout</a></div>
	</div>

	<div class="menu" id="cssmenu">
	<ul>
		<li class="mainItem" onclick="OpenNewTab(3)">Overview</li><li class="mainItem" onclick="OpenNewTab(5)">KPI</li><li class="mainItem" onclick="OpenNewTab(6)">ALARM</li><li class="mainItem-selected" onclick="OpenNewTab(8)">LOG</li><li class="mainItem" onclick="OpenNewTab(7)">RF</li><div class="menu_item"><?php if($kpiWarn+$alarmWarn === 0) : ?><?php if (strpos($title, "Warning") === false) echo '<div id="warn_icon" onclick="OpenNewTab(4)"><img src="'.base_url().'assets/img/warning_n.png" class="menu_img"></div>'; else echo '<div id="warn_icon" onclick="OpenNewTab(4)"><img src="'.base_url().'assets/img/warning_f.png" class="menu_img"></div>'; ?><?php else : ?><?php echo '<div id="warn_icon" onclick="OpenNewTab(4)"><img src="'.base_url().'assets/img/warning_1.gif" class="menu_img"></div>'; ?><?php endif; ?>&nbsp;&nbsp;&nbsp;<div id="tool_icon" onclick="OpenNewTab(1)"><img src="<?php echo base_url();?>assets/img/tool_n.png" class="menu_img"></div></div>
	</ul>
	</div>

	<div id="submenu">
		<li class="subItem" onclick="">HO</li><li class="subItem" onclick="">Preemption</li><li class="subItem" onclick="">Reconfiguration</li><li class="subItem" onclick="">Cell Update</li><li class="subItem" onclick="">Error</li><li class="subItem" onclick="">RTT</li>
	</div>

	<div class="content">

		<div class="contentl">
		</div>

		<div class="contentr">

			<div id="mid_content"></div>
			<br>
			<h2 id="detail1_h2">&nbsp;</h2>
			<div id="detail1"></div>
			<br>
			<h2 id="detail2_h2">&nbsp;</h2>
			<div id="detail2"></div>
			<br>
			<h2 id="detail3_h2">&nbsp;</h2>
			<div id="detail3"></div>
			<br>
			<h2 id="detail4_h2">&nbsp;</h2>
			<div id="detail4"></div>

		</div>

	</div>

	<div class="footer">
		<div class="center">© Copyright <?php echo date("Y"); ?> Alpha Networks Inc.</div>
		<div class="right">Version: <?php echo $version;?></div>
	</div>

</div>

<script>
init();
</script>

</body>
</html>
