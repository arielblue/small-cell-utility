<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html;charset=utf-8" http-equiv="Content-Type" >
	<!-- <meta content="utf-8" http-equiv="encoding"> -->
	<meta charset="utf-8"/>
	<title>RF - Analysis</title>

	<style>
		body { font-size: 16px; font-family: Calibri; }
	</style>

</head>

<script>
</script>

<body>

<div id="analysis_result"></div>
<?php
	$ci = get_instance();
	$ci->load->helper('time_helper');

	// date
	echo "* $startDate ~ $endDate<br>";
	echo "* hnb: $hnb<br>";

	// DL
	echo str_repeat("&nbsp;", 3)."* DL:<br>";

	// nlpc & mart power
	if($pwr != null && $pwr != "")
	{
		echo str_repeat("&nbsp;", 5)."* Power config:<br>";
		foreach($pwr as $k => $v)
		{
			$temp = $pwr[$k];
			$date = to_timestamp_string($temp["logtime"]);
			if($temp["mart_power"] != null && $temp["mart_power"] != "")
				echo str_repeat("&nbsp;", 9)."* MART at ".$date.", power = ".$temp["mart_power"]." dBm.<br>";
			if($temp["nlpc_power"] != null && $temp["nlpc_power"] != "")
				echo str_repeat("&nbsp;", 9)."* NLPC at ".$date.", power = ".$temp["nlpc_power"]." dBm.<br>";
		}
	}
	else echo str_repeat("&nbsp;", 5)."No nlpc power & mart power.<br>";

	// im_reg
	if($im_reg["max_hue"] != null && $im_reg["max_hue"] != "")
		echo str_repeat("&nbsp;", 5)."* ".$im_reg["max_hue"]." HUE and ".$im_reg["max_mue"]." MUE register.<br>";
	else
		echo str_repeat("&nbsp;", 5)."No HUE and MUE data.<br>";

	// rscp & ecno range of each distinct UE
	$mrm = $ueIntraMrm;
	if($mrm != null && $mrm != "")
	{
		$qual_string = "";
		$rscp_string = "";

		foreach($mrm as $k => $v)
		{
			$temp = $mrm[$k];

			if( ( $temp["cnt_qual_good"] / $temp["cnt_total"] ) >= $criteria )
				$qual_string = "quality is good; ";
			else if( ( ($temp["cnt_total"]-$temp["cnt_qual_bad"]) / $temp["cnt_total"] ) >= $criteria )
				$qual_string = "quality is normal; ";
			else
				$qual_string = "quality is bad; ";

			if( ( $temp["cnt_cell_site"] / $temp["cnt_total"] ) >= $criteria )
				$rscp_string = "UE is near fap, ";
			else if( ( ($temp["cnt_total"]-$temp["cnt_cell_edge"]) / $temp["cnt_total"] ) >= $criteria)
				$rscp_string = "UE is at middle distance, ";
			else
				$rscp_string = "UE is far from fap, ";

			echo str_repeat("&nbsp;", 5)."* UE ".$temp["IMSI"].":<br>".str_repeat("&nbsp;", 9)."* ".$rscp_string.$qual_string."RSCP = ".$temp["max_rscp"]." ~ ".$temp["min_rscp"]." dBm, ECNO = ".$temp["max_ecno"]." ~ ".$temp["min_ecno"]." dBm.<br>";

			// HO
			$hoTime = $ho[$temp["IMSI"]];
			if($hoTime != null && $hoTime != "")
			{
				echo str_repeat("&nbsp;", 9)."* HO is triggered at:<br>";
				$str = "";
				foreach($hoTime as $kk => $vv)
				{
					$date = to_timestamp_string($hoTime[$kk]);
					$str .= str_repeat("&nbsp;", 11)."* ".$date."<br>";
				}
				echo $str;
			}
			else echo str_repeat("&nbsp;", 9)."* No HO attempt.<br>";
		}
	}
	else echo str_repeat("&nbsp;", 5)."No UE Intra MRM data.<br>";

	// UL
	echo str_repeat("&nbsp;", 3)."* UL:<br>";

	// ROTval
	if($ROTval != null && $ROTval != "")
	{
		echo str_repeat("&nbsp;", 5)."* ROT = ".$ROTval." dB. UL alarm threshold: ".$ULAlarmThres." dB<br>";
	}

	// ulrssi
	if($ulrssi != null && $ulrssi != "")
	{
		echo str_repeat("&nbsp;", 5)."* UL RSSI is high at: <br>";
		foreach($ulrssi as $k => $v)
		{
			$temp = $ulrssi[$k];
			$date = to_timestamp_string($v["logtime"]);
			echo str_repeat("&nbsp;", 9)."* ".$date."  (".$v["UL_RSSI"]." dBm)<br>";
		}
	}
	else
		echo str_repeat("&nbsp;", 5)."* UL RSSI is low or there's no data.<br>";

	echo "Finish<br><br>";

?>

</body>
</html>