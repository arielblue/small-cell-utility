<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html" http-equiv="Content-Type;encoding" charset='utf-8'>
	<title><?php echo $title;?></title>

	<script language="javascript" type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.form.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/validate.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/login.js"></script>

	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.min.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/login.css" type="text/css" media="screen"/>
</head>

<script>
</script>

<body>
<div class="wrapper">
	
	<div id="form">

		<form id="loginform" name="loginform" action="<?php echo site_url();?>/login/signin" method="POST">
			<table id="list_table">
				<tr><th>Login Form</th></tr>
				<tr><td><input type="text" id="username" name="username" placeholder="Your Username"></td></tr>
				<tr><td><input type="password" id="password" name="password" placeholder="Your Password"></td></tr>
				<tr><td><input type="submit" class="juibtn" value="Log in"></td></tr>
				<tr><td></td></tr>
				<tr><td style="text-align: right;"><a href="#" id="register">Register</a></td></tr>
			</table>
		</form>
	</div>

	<div id="bottom_msg">
	</div>

</div>

<script>
initial();
</script>

</body>
</html>
