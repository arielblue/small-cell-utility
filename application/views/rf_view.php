<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html" http-equiv="Content-Type;encoding" charset='utf-8'>
	<title><?php echo $title;?></title>

	<script language="javascript" type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.form.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui-timepicker-addon.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flot.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flot.selection.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flot.symbol.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flot.time.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flot.axislabels.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.tostie.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/common.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/rf.js"></script>

	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.min.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/common.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.tostie.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/rf.css" type="text/css" media="screen"/>
</head>

<script>
</script>

<body>
<div class="wrapper">

	<div class="header">
		<div id="logo" class="logo"></div>
		<div id="fap_info"></div>
		<div id="logout"><span class="ui-icon ui-icon-white ui-icon-person"></span>&nbsp;<a href="user"><?php echo $user;?></a>&nbsp;/&nbsp;<a href="logout">Logout</a></div>
	</div>

	<div class="menu" id='cssmenu'>
	<ul>
		<li class="mainItem" onclick="OpenNewTab(3)">Overview</li><li class="mainItem" onclick="OpenNewTab(5)">KPI</li><li class="mainItem" onclick="OpenNewTab(6)">ALARM</li><li class="mainItem" onclick="OpenNewTab(8)">LOG</li><li class="mainItem-selected" onclick="OpenNewTab(7)">RF</li><div class="menu_item"><?php if($kpiWarn+$alarmWarn === 0) : ?><?php if (strpos($title, "Warning") === false) echo '<div id="warn_icon" onclick="OpenNewTab(4)"><img src="'.base_url().'assets/img/warning_n.png" class="menu_img"></div>'; else echo '<div id="warn_icon" onclick="OpenNewTab(4)"><img src="'.base_url().'assets/img/warning_f.png" class="menu_img"></div>'; ?><?php else : ?><?php echo '<div id="warn_icon" onclick="OpenNewTab(4)"><img src="'.base_url().'assets/img/warning_1.gif" class="menu_img"></div>'; ?><?php endif; ?>&nbsp;&nbsp;&nbsp;<div id="tool_icon" onclick="OpenNewTab(1)"><img src="<?php echo base_url();?>assets/img/tool_n.png" class="menu_img"></div></div>
	</ul>
	</div>

	<div class="content">

		<div class="contentr">

			<div id="upheader_content">
				<table class="list_table">
					<tr>
						<td>HNB:&nbsp;<select id="hnbSelect">
							<option value=""></option>
							<?php foreach ($hnb as $v):?>
							<?php echo "<option value='$v'>$v</option>";?>
							<?php endforeach;?>
						</select></td>
						<td><div id="timeSelect">
							<div>Start Time:&nbsp;<input type='text' id='datepicker_start'>&nbsp;End Time:&nbsp;<input type='text' id='datepicker_end'></div>
						</div></td>
						<td><button type='button' class='juibtn' id='SubmitBtn'>Submit</button></td>
						<td><button type='button' class='juibtn' id='AnalyzeBtn'>Analysis</button></td>
					</tr>
				</table>
			</div>

			<div id="mid_content">
			</div>

			<div id="bottom_msg" class="ui-widget">
			</div>

		</div>

	</div>

	<div class="footer">
		<div class="center">© Copyright <?php echo date("Y"); ?> Alpha Networks Inc.</div>
		<div class="right">Version: <?php echo $version;?></div>
	</div>

</div>

<script>
init();
</script>

</body>
</html>
