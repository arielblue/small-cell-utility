<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html" http-equiv="Content-Type;encoding" charset='utf-8'>
	<title><?php echo $title;?></title>

	<script language="javascript" type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/common.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/warning.js"></script>

	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.min.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/common.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/warning.css" type="text/css" media="screen"/>
</head>

<script>
</script>

<body>
<div class="wrapper">

	<div class="header">
		<div class="logo"></div>
		<div id="logout"><span class="ui-icon ui-icon-white ui-icon-person"></span>&nbsp;<a href="user"><?php echo $user;?></a>&nbsp;/&nbsp;<a href="logout">Logout</a></div>
	</div>

	<div class="menu" id='cssmenu'>
		<ul>
			<li class="mainItem" onclick="OpenNewTab(3)">Overview</li><li class="mainItem" onclick="OpenNewTab(5)">KPI</li><li class="mainItem" onclick="OpenNewTab(6)">ALARM</li><li class="mainItem" onclick="OpenNewTab(8)">LOG</li><li class="mainItem" onclick="OpenNewTab(7)">RF</li><div class="menu_item"><?php if($kpiWarn+$alarmWarn === 0) : ?><?php if (strpos($title, "Warning") === false) echo '<div id="warn_icon" onclick="OpenNewTab(4)"><img src="'.base_url().'assets/img/warning_n.png" class="menu_img"></div>'; else echo '<div id="warn_icon" onclick="OpenNewTab(4)"><img src="'.base_url().'assets/img/warning_f.png" class="menu_img"></div>'; ?><?php else : ?><?php echo '<div id="warn_icon" onclick="OpenNewTab(4)"><img src="'.base_url().'assets/img/warning_1.gif" class="menu_img"></div>'; ?><?php endif; ?>&nbsp;&nbsp;&nbsp;<div id="tool_icon" onclick="OpenNewTab(1)"><img src="<?php echo base_url();?>assets/img/tool_n.png" class="menu_img"></div></div><!-- <div id="warn_icon" onclick="OpenNewTab(4)">&nbsp;</div> -->
		</ul>
	</div>


	<div class="content">

		<div class="contentl">
		</div>

		<div class="contentr">

			<div id="upheader_content">
				<div class="warn_f">Unprocessed KPI Warning</div> <div class="warn_n" id="allWarn">All KPI Warning</div>
			</div>

			<div id="mid_content">
			<?php if(count($result) == 0) : ?>
			<?php echo "NO DATA"; ?>
			<?php else : ?>
				<table id='list_table' cellpadding='1'><colgroup><col span="1" style="width:5%"><col span="1" style="width:5%"><col span="1" style="width:30%"><col span="1" style="width:12%"><col span="1" style="width:12%"><col span="1" style="width:16%"><col span="1" style="width:10%"><col span="1" style="width:10%"></colgroup><tr><th><input type='checkbox' id='selectall'></th><th>No</th><th>Note</th><th>HNB</th><th>End Time</th><th>KPI Name</th><th>KPI Target</th><th>KPI Value</th></tr>
				<?php
					foreach ($result as $k => $v)
					{
						echo "<tr><td><input type='checkbox' class='myCheckbox' id='myCheckbox".$k."'></td><td>".($per_page*($offset-1)+$k+1)."</td><td><textarea id='note".$k."' value='' maxlength='".$note_maxlengh."'></textarea></td><td id='hnbMAC".$k."'>".$v['hnbMAC']."</td><td id='endtime".$k."'>".$v['endtime']."</td><td style='width:150px;' id='kpiName".$k."' value='".$v['kpiName']."'>".$v['kpiDisplayName']."</td><td id='kpiTarget".$k."'>".$v['kpiTarget']."</td><td>".$v['kpiValue']."</td></tr>";
					}
				?>
				</table>
				<div style='float:left;'><button class='juibtn' id='doneBtn'>Update</button></div><div id="bottom_msg" class="ui-widget"></div>
			<?php endif; ?>
			</div>

			<div id="pagination_link">
				<br>
				<?php echo $links;?>
			</div>

			<div>
			</div>

		</div>

	</div>

	<div class="footer">
		<div class="center">© Copyright <?php echo date("Y"); ?> Alpha Networks Inc.</div>
		<div class="right">Version: <?php echo $version;?></div>
	</div>

</div>

<script>
kpiWarningInit();
</script>

</body>
</html>
