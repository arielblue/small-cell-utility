<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html" http-equiv="Content-Type;encoding" charset='utf-8'>
	<title><?php echo $title;?></title>

	<script language="javascript" type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.form.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/validate.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/common.js"></script>

	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.min.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/common.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/user.css" type="text/css" media="screen"/>

	<style>
	</style>
</head>

<script>
</script>

<body>
<div class="wrapper">

	<div class="header">
		<div class="logo"></div>
		<div id="logout"><span class="ui-icon ui-icon-white ui-icon-person"></span>&nbsp;<a href="user"><?php echo $user;?></a>&nbsp;/&nbsp;<a href="logout">Logout</a></div>
	</div>

	<div class="menu" id='cssmenu'>
	<ul>
		<li class="mainItem" onclick="OpenNewTab(3)">Overview</li><li class="mainItem" onclick="OpenNewTab(5)">KPI</li><li class="mainItem" onclick="OpenNewTab(6)">ALARM</li><li class="mainItem" onclick="OpenNewTab(8)">LOG</li><li class="mainItem" onclick="OpenNewTab(7)">RF</li><div class="menu_item"><?php if($kpiWarn+$alarmWarn === 0) : ?><?php if (strpos($title, "Warning") === false) echo '<div id="warn_icon" onclick="OpenNewTab(4)"><img src="'.base_url().'assets/img/warning_n.png" class="menu_img"></div>'; else echo '<div id="warn_icon" onclick="OpenNewTab(4)"><img src="'.base_url().'assets/img/warning_f.png" class="menu_img"></div>'; ?><?php else : ?><?php echo '<div id="warn_icon" onclick="OpenNewTab(4)"><img src="'.base_url().'assets/img/warning_1.gif" class="menu_img"></div>'; ?><?php endif; ?>&nbsp;&nbsp;&nbsp;<div id="tool_icon" onclick="OpenNewTab(1)"><img src="<?php echo base_url();?>assets/img/tool_n.png" class="menu_img"></div></div>
	</ul>
	</div>


	<div class="content">

		<div class="contentl">
		</div>

		<div class="contentr">

			<div id="upheader_content">Account Details</div>

			<div id="mid_content">
				<form id="editform" name="editform" class="editform" action="<?php echo current_url()."/update_user/"; ?>" method="POST">
				<table id='list_table' cellpadding='1'><colgroup><col span="1" style="width:40%"><col span="1" style="width:60%"></colgroup>
					<tr><th>Username</th><td id="username"><?php echo $userDetails->username; ?></td></tr>
					<tr><th>Password</th><td><input type="password" name="password" id="password" value=""></td></tr>
					<tr><th>Password Confirm</th><td><input type="password" name="passconf" id="passconf" value=""></td></tr>
					<tr><th>Email</th><td><input type="text" name="email" id="email" value="<?php echo $userDetails->email;?>"></td></tr>
					<tr><th>Role</th><td id="<?php echo $userDetails->roleId;?>"><?php echo $userDetails->roleName; ?></td></tr>
					<tr></tr>
				</table><input type="submit" class="juibtn" value="Save">
				</form>
			</div>
			<div><br>&nbsp;</div>
			<div id="bottom_msg"></div>
		</div>

	</div>

	<div class="footer">
		<div class="center">© Copyright <?php echo date("Y"); ?> Alpha Networks Inc.</div>
		<div class="right">Version: <?php echo $version;?></div>
	</div>

</div>

<script>
$(".juibtn").button().click( function (event) {
	$("#bottom_msg").html("");
	get_data();
});
</script>

</body>
</html>
