<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html;charset=utf-8" http-equiv="Content-Type" >
	<meta content="utf-8" http-equiv="encoding">
	<title><?php echo $title;?></title>

	<script language="javascript" type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.form.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/config.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/validate.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.min.css" type="text/css" media="screen"/>	
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/configtool.css" type="text/css" media="screen"/>
</head>

<script>
</script>

<body>
<div class="wrapper">

	<div class="header">
		<div id="logout"><span class="ui-icon ui-icon-white ui-icon-person"></span>&nbsp;<?php echo $user;?>&nbsp;/&nbsp;<a href="logout">Logout</a></div>
	</div>
