<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html" http-equiv="Content-Type;encoding" charset='utf-8'>
	<title><?php echo $title;?></title>

	<script language="javascript" type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.form.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/common.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/install.js"></script>

	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.min.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/common.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/configtool.css" type="text/css" media="screen"/>
</head>

<script>
</script>

<body>
<div class="wrapper">

	<div class="header">
		<div class="logo"></div>
	</div>

	<div class="menu" id='cssmenu'>
	<ul>
		<li class="mainItem_unused" onclick="">Overview</li><li class="mainItem_unused" onclick="">KPI</li><li class="mainItem_unused" onclick="">ALARM</li><li class="mainItem_unused" onclick="">LOG</li><li class="mainItem_unused" onclick="">RF</li><div class="menu_item"><div id="warn_icon" onclick=""><img src="<?php echo base_url();?>assets/img/warning_n.png" class="menu_img"></div>&nbsp;&nbsp;&nbsp;<div id="tool_icon" onclick=""><img src="<?php echo base_url();?>assets/img/tool_n.png" class="menu_img"></div></div>
	</ul>
	</div>

	<div class="content" ng-app="configApp" ng-controller="configController">

		<div class="contentr" style="padding: 15px;">
			<button type="button" id="setupdb" onclick="setupdb()">Setup Database</button>
			<br><br>
			<button type="button" id="setup_logdb" onclick="setup_logdb()">Setup Faplog DB</button>
			<div class="msg"></div>
		</div>

	</div>

	<div class="footer">
		<div class="center">© Copyright <?php echo date("Y"); ?> Alpha Networks Inc.</div>
		<div class="right">Version: <?php echo $version;?></div>
	</div>

</div>

<script>
$("#setupdb").attr('disabled','disabled');
</script>

</body>
</html>