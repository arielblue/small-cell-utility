<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Array_filter_lib {
	private $num;

	function __construct($num) {
		$this->num = $num;
	}

	function isLower($i) {
		return (float) $i < $this->num;
	}

	function isHigher($i) {
		return (float) $i > $this->num;
	}
}
