<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_hnbMAC_list()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$data = array();

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("hnbMACList_table");
		$cmd = "SELECT * FROM $table";
		$query = $this->db->query($cmd);
		foreach ($query->result_array() as $row)
		{
			array_push($data, $row['hnbMAC']);
		}
		$query->free_result();
		$this->db->close();

		return json_encode($data);
	}

	public function get_report_rawdata($reportname, $hnb, $start_time, $end_time)
	{
		$tb = $this->config->item("kpi_table");
		$db = $this->config->item("db");
		$begin_time = $start_time;
		$end_time = $end_time;
		$realreportname = $this->config->item("report_name");
		$attr = $this->config->item("report_attr");
		$attr = $attr[$reportname];
		$data = array();

		## for ouptut
		$temp = array();
		$output = array();

		$cmd = "SELECT endtime,";
		foreach($attr as $a)
		{
			$data[$a] = array();
			$cmd .= "$a,";
		}
		$cmd = rtrim($cmd, ",");
		$cmd .= " FROM $tb WHERE hnbMAC='$hnb' && endtime BETWEEN '$start_time' AND '$end_time' ORDER BY endtime asc";

		$this->db->query("USE $db");
		$query = $this->db->query($cmd);
		foreach ($query->result_array() as $row)
		{
			array_push($temp, $row["endtime"]);	## for ouptut
			foreach($attr as $a)
			{
				array_push($data[$a], array($row["endtime"], $row[$a]));
				array_push($temp, $row[$a]);	## for ouptut
			}
			array_push($output, $temp);			## for ouptut
			$temp = array();					## for ouptut
		}
		$query->free_result();
		$this->db->close();
		unset($temp);

		if($reportname != key($realreportname))
		{
			return $output;
			unset($output);
		}
		else
		{
			return $data;
			unset($data);
		}
	}

	public function get_timeDuration($hnb)
	{
		$tb = $this->config->item("kpi_table");
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$begin_time = "";
		$end_time = "";

		# get_begin_time
		$cmd = "SELECT endtime FROM $tb WHERE hnbMAC='$hnb' ORDER BY endtime asc LIMIT 1";
		$query = $this->db->query($cmd);
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$begin_time = $row->endtime;
		}
		$query->free_result();

		# get_end_time
		$cmd = "SELECT endtime FROM $tb WHERE hnbMAC='$hnb' ORDER BY endtime desc LIMIT 1";
		$query = $this->db->query($cmd);
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$end_time = $row->endtime;
		}
		$query->free_result();

		$this->db->close();
		return json_encode(array("begin_time" => $begin_time, "end_time" => $end_time));
	}
}

/* End of file report_model.php */
/* Location: ./application/models/report_model.php */