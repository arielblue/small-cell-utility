<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configtool_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$ci = get_instance();
		$ci->load->helper('user_helper');
	}

	public function query_response($cmd)
	{
		$res = $this->db->query($cmd);
		if( !$res )
		{
			$errNo   = $this->db->_error_number();
			$errMess = $this->db->_error_message();

			return $errNo;
		}
		else
			return "success";
	}

	public function query_response2($cmd)
	{
		$res = $this->db->query($cmd);
		if( !$res )
		{
			$errNo   = $this->db->_error_number();
			$errMess = $this->db->_error_message();

			return "$errNo: $errMess";
		}
		else
			return "success";
	}

	public function query_response3($funcName,$cmd)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$res = $this->db->query($cmd);
		if( !$res )
		{
			$errNo   = $this->db->_error_number();
			$errMess = $this->db->_error_message();
			return $funcName.": ".$errMess."<br>";
		}
	}

	public function setup_database()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$this->db->query("DROP DATABASE IF EXISTS $db");
		$this->db->query("CREATE DATABASE IF NOT EXISTS $db");
		$this->db->query("USE $db");

		$errMsg = "";

		$errMsg .= $this->create_hnbMAC_list_table();
		$errMsg .= $this->create_role_table();
		$errMsg .= $this->create_user_table();
		$errMsg .= $this->create_ftpserver_table();
		$errMsg .= $this->create_housekeeping_table();
		$errMsg .= $this->create_kpiTarget_table();
		$errMsg .= $this->create_kpi_table();
		$errMsg .= $this->create_kpiWarning_table();
		$errMsg .= $this->create_alarmWarning_table();
		$errMsg .= $this->create_preProcess_table();
		$errMsg .= $this->create_kpiSummary_table();

		$this->db->close();

		if($errMsg == "")
			print_r("Setup Tool Database: success<br>");	// print_r(json_encode()));
		else
			echo $errMsg;
	}

	public function create_role_table()
	{
		$role_table = $this->config->item('role_table');
		$cmd = "CREATE TABLE IF NOT EXISTS $role_table
				(
					roleId INT(11) AUTO_INCREMENT PRIMARY KEY,
					roleName VARCHAR(60) NOT NULL,
					description VARCHAR(255),
					isDefault TINYINT(1) NOT NULL DEFAULT 0,
					canDelete TINYINT(1) NOT NULL DEFAULT 1
				);";
		$msg = $this->query_response3("create_role_table", $cmd);

		$cmd = "INSERT INTO $role_table (roleName, description, isDefault, canDelete) VALUES ('Administrator','Has full control over every aspect of the site except developer tools.',0,0),('Developer','Developers typically are the only ones that can access the developer tools and have full control over every aspect of the site.',0,0),('User','This is the default user with access to login.',1,DEFAULT(canDelete))";
		$msg .= $this->query_response3("create_role_table", $cmd);

		return $msg;
	}

	public function create_user_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$user_table = $this->config->item('user_table');
		$role_table = $this->config->item('role_table');
		$cmd = "CREATE TABLE IF NOT EXISTS $user_table
				(
					username VARCHAR(30) PRIMARY KEY,
					hash CHAR(60) NOT NULL,
					email VARCHAR(120),
					roleId INT(11) NOT NULL,
					kpiPreference TEXT,
					counterPreference TEXT,
					hnbMacPreference TEXT,
					createdOn DATETIME NOT NULL DEFAULT 0,
					updateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					updateUser VARCHAR(30) NOT NULL DEFAULT 'admin',
					FOREIGN KEY (roleId) REFERENCES $role_table (roleId)
				);";
		$msg = $this->query_response3("create_user_table", $cmd);

		$date = date("Y-m-d H:i:s", time());
		$hash1 = hash_password("admin");
		$hash2 = hash_password("wendy");
		$hash3 = hash_password("frank");
		$cmd = "INSERT INTO $user_table (username,hash,email,roleId,kpiPreference,counterPreference,hnbMacPreference,createdOn,updateUser) VALUES ('admin','$hash1','admin@alphaworks.com',1,'','','','$date','system'),('wendy','$hash2','wendy@alphaworks.com',2,'','','','$date','system'),('frank','$hash3','frank@alphaworks.com',2,'','','','$date','system')";
		$msg .= $this->query_response3("create_user_table", $cmd);

		return $msg;
	}

	public function create_ftpserver_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$ftpServer = $this->config->item('ftpServer');
		$cmd = "CREATE TABLE IF NOT EXISTS $ftpServer
				(
					ip CHAR(15),
					configName VARCHAR(6),
					port INT(3) NOT NULL,
					loginName VARCHAR(30) NOT NULL,
					loginHash VARCHAR(128) NOT NULL,
					type VARCHAR(16) NOT NULL,
					directoryPath VARCHAR(128) NOT NULL,
					updateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					username VARCHAR(30) NOT NULL,
					PRIMARY KEY (ip, configName)
				);";
		$msg = $this->query_response3("create_ftpserver_table", $cmd);

		$hash2 = server_password("frank");
		$cmd = "INSERT INTO $ftpServer VALUES ('5.90.31.10','LOG','990','frank','$hash2','FTPS','/Log',CURRENT_TIMESTAMP,'admin'), ('5.90.31.10','BACKUP','990','frank','$hash2','FTPS','/BackUp',CURRENT_TIMESTAMP,'admin'), ('5.90.31.10','PM','990','frank','$hash2','FTPS','/PM',CURRENT_TIMESTAMP,'admin')";
		$msg .= $this->query_response3("create_ftpserver_table", $cmd);

		return $msg;
	}

	public function create_hnbMAC_list_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$hnbMACList_table = $this->config->item('hnbMACList_table');
		$cmd = "CREATE TABLE IF NOT EXISTS $hnbMACList_table
				(
					hnbMAC CHAR(12) PRIMARY KEY,
					updateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
				);";

		return $this->query_response3("create_hnbMAC_list_table", $cmd);
	}

	public function create_housekeeping_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$housedkeeping_table = $this->config->item('housedkeeping_table');
		$keep_dbdata_days = $this->config->item('keep_dbdata_days');	#get $days from config this time only
		$keep_rawdata_days = $this->config->item('keep_rawdata_days');
		$cmd = "CREATE TABLE IF NOT EXISTS $housedkeeping_table
				(
					keep_dbdata_days SMALLINT(4) NOT NULL,
					keep_rawdata_days SMALLINT(4) NOT NULL,
					updateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					username VARCHAR(30) NOT NULL
				);";
		$msg = $this->query_response3("create_housekeeping_table", $cmd);

		$cmd = "INSERT INTO $housedkeeping_table VALUES ($keep_dbdata_days, $keep_rawdata_days,CURRENT_TIMESTAMP,'admin')";
		$msg .= $this->query_response3("create_housekeeping_table", $cmd);

		return $msg;
	}

	public function create_kpiTarget_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$kpi_target = $this->config->item('kpi_target');
		$cmd = "CREATE TABLE IF NOT EXISTS $kpi_target
				(
					kpiName VARCHAR(64) PRIMARY KEY,
					target DOUBLE(3, 0) NOT NULL,
					updateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					updateUser VARCHAR(30) NOT NULL
				);";
		$msg = $this->query_response3("create_kpiTarget_table", $cmd);

		$cmd = "INSERT INTO $kpi_target VALUES ('CS_RAB_SR_Distribution',95,CURRENT_TIMESTAMP,'system'), ('PS_RAB_SR_Distribution',95,CURRENT_TIMESTAMP,'system'), ('RRC_Conneciton_SR_Distribution',97,CURRENT_TIMESTAMP,'system')";//TODO
		$msg .= $this->query_response3("create_kpiTarget_table", $cmd);

		return $msg;
	}

	public function create_kpi_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$hnbMACList_table = $this->config->item('hnbMACList_table');
		$kpi_table = $this->config->item('kpi_table');
		$cmd = "CREATE TABLE IF NOT EXISTS $kpi_table
				(
					-- id INT PRIMARY KEY AUTO_INCREMENT,
					hnbMAC CHAR(12) NOT NULL,
					endtime DATETIME NOT NULL,
					updateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					RABSuccEstabCS BIGINT UNSIGNED, RABFailEstabCS BIGINT UNSIGNED, RABSuccEstabPS BIGINT UNSIGNED, RABFailEstabPS BIGINT UNSIGNED, RABSuccModCS BIGINT UNSIGNED, RABFailModCS BIGINT UNSIGNED, RABSuccModPS BIGINT UNSIGNED, RABFailModPS BIGINT UNSIGNED, RABSuccRelCS BIGINT UNSIGNED, RABFailRelCS BIGINT UNSIGNED, RABSuccRelPS BIGINT UNSIGNED, RABFailRelPS BIGINT UNSIGNED, RABCSSetupTimeMean BIGINT UNSIGNED, RABCSSetupTimeMax BIGINT UNSIGNED, RABPSSetupTimeMean BIGINT UNSIGNED, RABPSSetupTimeMax BIGINT UNSIGNED, RABSuccEstabCSD BIGINT UNSIGNED, RABFailEstabCSD BIGINT UNSIGNED, RABAttemptEstabCSD BIGINT UNSIGNED, RABSuccEstabR99PS BIGINT UNSIGNED, RABFailEstabR99PS BIGINT UNSIGNED, RABAttemptEstabR99PS BIGINT UNSIGNED, RRCSuccConnEstab BIGINT UNSIGNED, RRCFailConnEstab BIGINT UNSIGNED, RRCAttConnEstab BIGINT UNSIGNED, RRCAttConnRelDCCH BIGINT UNSIGNED, RRCAttConnRelCCCH BIGINT UNSIGNED, RRCSuccConnMean BIGINT UNSIGNED, RRCSuccConnMax BIGINT UNSIGNED, OrigngConveCallMean BIGINT UNSIGNED, OrigngConveCallMax BIGINT UNSIGNED, OrigngStreangCallMean BIGINT UNSIGNED, OrigngStreangCallMax BIGINT UNSIGNED, OrigngInteractCallMean BIGINT UNSIGNED, OrigngInteractCallMax BIGINT UNSIGNED, OrigngBackgrndCallMean BIGINT UNSIGNED, OrigngBackgrndCallMax BIGINT UNSIGNED, OrigngSubscTraffCallMean BIGINT UNSIGNED, OrigngSubscTraffCallMax BIGINT UNSIGNED, TermngConveCallMean BIGINT UNSIGNED, TermngConveCallMax BIGINT UNSIGNED, TermngStreangCallMean BIGINT UNSIGNED, TermngStreangCallMax BIGINT UNSIGNED, TermngInteractCallMean BIGINT UNSIGNED, TermngInteractCallMax BIGINT UNSIGNED, TermngBackgrndCallMean BIGINT UNSIGNED, TermngBackgrndCallMax BIGINT UNSIGNED, EmerCallMean BIGINT UNSIGNED, EmerCallMax BIGINT UNSIGNED, RegistrationMean BIGINT UNSIGNED, RegistrationMax BIGINT UNSIGNED, DetachMean BIGINT UNSIGNED, DetachMax BIGINT UNSIGNED, OrigngHighPriorSigMean BIGINT UNSIGNED, OrigngHighPriorSigMax BIGINT UNSIGNED, OrigngLowPriorSigMean BIGINT UNSIGNED, OrigngLowPriorSigMax BIGINT UNSIGNED, TermngHighPriorSigMean BIGINT UNSIGNED, TermngHighPriorSigMax BIGINT UNSIGNED, TermngLowPriorSigMean BIGINT UNSIGNED, TermngLowPriorSigMax BIGINT UNSIGNED, InteRAT_CellReselMean BIGINT UNSIGNED, InteRAT_CellReselMax BIGINT UNSIGNED, InteRAT_CellChangOrderMean BIGINT UNSIGNED, InteRAT_CellChangOrderMax BIGINT UNSIGNED, CallRe_estMean BIGINT UNSIGNED, CallRe_estMax BIGINT UNSIGNED, TermngCusUnknoMean BIGINT UNSIGNED, TermngCusUnknoMax BIGINT UNSIGNED, Mbms_RecpMean BIGINT UNSIGNED, Mbms_RecpMax BIGINT UNSIGNED, Mbms_PTP_RB_ReqMean BIGINT UNSIGNED, Mbms_PTP_RB_ReqMax BIGINT UNSIGNED, Spare10Mean BIGINT UNSIGNED, Spare10Max BIGINT UNSIGNED, Spare9Mean BIGINT UNSIGNED, Spare9Max BIGINT UNSIGNED, Spare8Mean BIGINT UNSIGNED, Spare8Max BIGINT UNSIGNED, Spare7Mean BIGINT UNSIGNED, Spare7Max BIGINT UNSIGNED, Spare6Mean BIGINT UNSIGNED, Spare6Max BIGINT UNSIGNED, Spare5Mean BIGINT UNSIGNED, Spare5Max BIGINT UNSIGNED, Spare4Mean BIGINT UNSIGNED, Spare4Max BIGINT UNSIGNED, Spare3Mean BIGINT UNSIGNED, Spare3Max BIGINT UNSIGNED, Spare2Mean BIGINT UNSIGNED, Spare2Max BIGINT UNSIGNED, Spare1Mean BIGINT UNSIGNED, Spare1Max BIGINT UNSIGNED, RBSuccConnEstab BIGINT UNSIGNED, RBFailConnEstab BIGINT UNSIGNED, RBAttConnEstab BIGINT UNSIGNED, HSDPASuccRBSetup BIGINT UNSIGNED, HSDPAFailRBSetup BIGINT UNSIGNED, HSDPAAttRBSetup BIGINT UNSIGNED, HSUPASuccRBSetup BIGINT UNSIGNED, HSUPAFailRBSetup BIGINT UNSIGNED, HSUPAAttRBSetup BIGINT UNSIGNED, PagingAttCn BIGINT UNSIGNED, PagingResPagType1 BIGINT UNSIGNED, PagingResPagType2 BIGINT UNSIGNED, PagingAttUTRAN BIGINT UNSIGNED, PagingResPCH BIGINT UNSIGNED, SIGAttConnEstabCS BIGINT UNSIGNED, SIGAttConnEstabPS BIGINT UNSIGNED, IuRelCS BIGINT UNSIGNED, IuRelReqCS BIGINT UNSIGNED, IuSuccRelCS BIGINT UNSIGNED, IuRelReqCS_Radio_Conn_UE_Lost BIGINT UNSIGNED, IuRelPS BIGINT UNSIGNED, IuRelReqPS BIGINT UNSIGNED, IuSuccRelPS BIGINT UNSIGNED, IuRelReqPS_Radio_Conn_UE_Lost BIGINT UNSIGNED, IuRelCSD BIGINT UNSIGNED, RABAttemptEstabCS BIGINT UNSIGNED, RABAttemptEstabPS BIGINT UNSIGNED, RABAttemptModCS BIGINT UNSIGNED, RABAttemptModPS BIGINT UNSIGNED, RABAttemptRelCS BIGINT UNSIGNED, RABAttemptRelPS BIGINT UNSIGNED, CSInterHNBIntraFreqHORelocPrepAtt BIGINT UNSIGNED, CSInterHNBIntraFreqHORelocPrepSucc BIGINT UNSIGNED, CSInterHNBIntraFreqHORelocPrepCanc BIGINT UNSIGNED, CSInterHNBIntraFreqHORelocPrepFail BIGINT UNSIGNED, PSInterHNBIntraFreqHORelocPrepAtt BIGINT UNSIGNED, PSInterHNBIntraFreqHORelocPrepSucc BIGINT UNSIGNED, PSInterHNBIntraFreqHORelocPrepCanc BIGINT UNSIGNED, PSInterHNBIntraFreqHORelocPrepFail BIGINT UNSIGNED, CSInterHNBIntraFreqHORelocExecAtt BIGINT UNSIGNED, CSInterHNBIntraFreqHORelocExecSucc BIGINT UNSIGNED, CSInterHNBIntraFreqHORelocExecFail BIGINT UNSIGNED, PSInterHNBIntraFreqHORelocExecAtt BIGINT UNSIGNED, PSInterHNBIntraFreqHORelocExecSucc BIGINT UNSIGNED, PSInterHNBIntraFreqHORelocExecFail BIGINT UNSIGNED, CSInterHNBInterFreqHORelocPrepAtt BIGINT UNSIGNED, CSInterHNBInterFreqHORelocPrepSucc BIGINT UNSIGNED, CSInterHNBInterFreqHORelocPrepCanc BIGINT UNSIGNED, CSInterHNBInterFreqHORelocPrepFail BIGINT UNSIGNED, PSInterHNBInterFreqHORelocPrepAtt BIGINT UNSIGNED, PSInterHNBInterFreqHORelocPrepSucc BIGINT UNSIGNED, PSInterHNBInterFreqHORelocPrepCanc BIGINT UNSIGNED, PSInterHNBInterFreqHORelocPrepFail BIGINT UNSIGNED, CSInterHNBInterFreqHORelocExecAtt BIGINT UNSIGNED, CSInterHNBInterFreqHORelocExecSucc BIGINT UNSIGNED, CSInterHNBInterFreqHORelocExecFail BIGINT UNSIGNED, PSInterHNBInterFreqHORelocExecAtt BIGINT UNSIGNED, PSInterHNBInterFreqHORelocExecSucc BIGINT UNSIGNED, PSInterHNBInterFreqHORelocExecFail BIGINT UNSIGNED, CSUnkownHIAtt BIGINT UNSIGNED, CSUnkownHISucc BIGINT UNSIGNED, CSUnkownHICanc BIGINT UNSIGNED, CSUnkownHIFail BIGINT UNSIGNED, PSUnkownHIAtt BIGINT UNSIGNED, PSUnkownHISucc BIGINT UNSIGNED, PSUnkownHICanc BIGINT UNSIGNED, PSUnkownHIFail BIGINT UNSIGNED, SRBSuccConnEstab BIGINT UNSIGNED, SRBFailConnEstab BIGINT UNSIGNED, SRBAttConnEstab BIGINT UNSIGNED, SRBDrop BIGINT UNSIGNED, RRCFailConnEstabCong BIGINT UNSIGNED, RRCAccModeReject BIGINT UNSIGNED, RRCRejectOth BIGINT UNSIGNED, AttUeReg BIGINT UNSIGNED, SuccUeReg BIGINT UNSIGNED, FailUeReg BIGINT UNSIGNED, FailUeRegInvalidUEidentity BIGINT UNSIGNED, FailUeRegUEnotallowed BIGINT UNSIGNED, FailUeRegUEunauthorised BIGINT UNSIGNED, FailUeRegOverload BIGINT UNSIGNED, SuccUeDeRegHnb BIGINT UNSIGNED, AttUeDeRegHnb BIGINT UNSIGNED, SuccUeDeRegHnbGw BIGINT UNSIGNED, AttUeDeRegHnbGw BIGINT UNSIGNED, CSAttachReq BIGINT UNSIGNED, PSAttachReq BIGINT UNSIGNED, CSAttachSucc BIGINT UNSIGNED, PSAttachSucc BIGINT UNSIGNED, CSAttachRej BIGINT UNSIGNED, PSAttachRej BIGINT UNSIGNED, CSExplicitDetach BIGINT UNSIGNED, PSExplicitDetach BIGINT UNSIGNED, NumAttachUser BIGINT UNSIGNED, MeanNbrUsr BIGINT UNSIGNED, AttHnbReg BIGINT UNSIGNED, SuccHnbReg BIGINT UNSIGNED, FailHnbReg BIGINT UNSIGNED, AttHnbDeRegHnbGw BIGINT UNSIGNED, SuccHnbDeRegHnbGw BIGINT UNSIGNED, AttHnbDeRegHnb BIGINT UNSIGNED, SuccHnbDeRegHnb BIGINT UNSIGNED, RBAttemptEstabCS BIGINT UNSIGNED, RBSuccEstabCS BIGINT UNSIGNED, RBFailEstabCS BIGINT UNSIGNED, RBAttemptEstabPS BIGINT UNSIGNED, RBSuccEstabPS BIGINT UNSIGNED, RBFailEstabPS BIGINT UNSIGNED, RBReconfigAttCS BIGINT UNSIGNED, RBReconfigSuccCS BIGINT UNSIGNED, RBReconfigFailCS BIGINT UNSIGNED, RBReconfigAttPS BIGINT UNSIGNED, RBReconfigSuccPS BIGINT UNSIGNED, RBReconfigFailPS BIGINT UNSIGNED, R99AttRBSetup BIGINT UNSIGNED, R99SuccRBSetup BIGINT UNSIGNED, R99FailRBSetup BIGINT UNSIGNED, RRCAttCellUpdate BIGINT UNSIGNED, CellUpdCellRes BIGINT UNSIGNED, CellUpdPer BIGINT UNSIGNED, CellUpdULData BIGINT UNSIGNED, CellUpdPagRes BIGINT UNSIGNED, CellUpdRLFail BIGINT UNSIGNED, CellUpdRLCUnrecErr BIGINT UNSIGNED, CellUpdOth BIGINT UNSIGNED, SCTPAbortSent BIGINT UNSIGNED, SCTPAbortRcvd BIGINT UNSIGNED, SCTPShdwnSent BIGINT UNSIGNED, SCTPShdwnRcvd BIGINT UNSIGNED, OperErrSent BIGINT UNSIGNED, OperErrRcvd BIGINT UNSIGNED, SCTPRetras BIGINT UNSIGNED, IPSecTunnAtt BIGINT UNSIGNED, IPSecTunnSucc BIGINT UNSIGNED, IPSecTunnFail BIGINT UNSIGNED, CSIntraFreq3GHORelocPrepAtt BIGINT UNSIGNED, CSIntraFreq3GHORelocPrepSucc BIGINT UNSIGNED, CSIntraFreq3GHORelocPrepCanc BIGINT UNSIGNED, CSIntraFreq3GHORelocPrepFail BIGINT UNSIGNED, PSIntraFreq3GHORelocPrepAtt BIGINT UNSIGNED, PSIntraFreq3GHORelocPrepSucc BIGINT UNSIGNED, PSIntraFreq3GHORelocPrepCanc BIGINT UNSIGNED, PSIntraFreq3GHORelocPrepFail BIGINT UNSIGNED, CSIntraFreq3GHORelocExecAt BIGINT UNSIGNED, CSIntraFreq3GHORelocExecSucc BIGINT UNSIGNED, CSIntraFreq3GHORelocExecFail BIGINT UNSIGNED, PSIntraFreq3GHORelocExecAtt BIGINT UNSIGNED, PSIntraFreq3GHORelocExecSucc BIGINT UNSIGNED, PSIntraFreq3GHORelocExecFail BIGINT UNSIGNED, CSInterFreq3GHORelocPrepAtt BIGINT UNSIGNED, CSInterFreq3GHORelocPrepSucc BIGINT UNSIGNED, CSInterFreq3GHORelocPrepCanc BIGINT UNSIGNED, CSInterFreq3GHORelocPrepFail BIGINT UNSIGNED, PSInterFreq3GHORelocPrepAtt BIGINT UNSIGNED, PSInterFreq3GHORelocPrepSucc BIGINT UNSIGNED, PSInterFreq3GHORelocPrepCanc BIGINT UNSIGNED, PSInterFreq3GHORelocPrepFail BIGINT UNSIGNED, CSInterFreq3GHORelocExecAtt BIGINT UNSIGNED, CSInterFreq3GHORelocExecSucc BIGINT UNSIGNED, CSInterFreq3GHORelocExecFail BIGINT UNSIGNED, PSInterFreq3GHORelocExecAtt BIGINT UNSIGNED, PSInterFreq3GHORelocExecSucc BIGINT UNSIGNED, PSInterFreq3GHORelocExecFail BIGINT UNSIGNED, CS2GHORelocPrepAtt BIGINT UNSIGNED, CS2GHORelocPrepSucc BIGINT UNSIGNED, CS2GHORelocPrepCanc BIGINT UNSIGNED, CS2GHORelocPrepFail BIGINT UNSIGNED, PS2GHORelocPrepAtt BIGINT UNSIGNED, PS2GHORelocPrepSucc BIGINT UNSIGNED, PS2GHORelocPrepCanc BIGINT UNSIGNED, PS2GHORelocPrepFail BIGINT UNSIGNED, CS2GHORelocExecAtt BIGINT UNSIGNED, CS2GHORelocExecSucc BIGINT UNSIGNED, CS2GHORelocExecFail BIGINT UNSIGNED, PS2GHORelocExecAtt BIGINT UNSIGNED, PS2GHORelocExecSucc BIGINT UNSIGNED, PS2GHORelocExecFail BIGINT UNSIGNED, CS3GHIAtt BIGINT UNSIGNED, CS3GHISucc BIGINT UNSIGNED, CS3GHICanc BIGINT UNSIGNED, CS3GHIFail BIGINT UNSIGNED, PS3GHIAtt BIGINT UNSIGNED, PS3GHISucc BIGINT UNSIGNED, PS3GHICanc BIGINT UNSIGNED, PS3GHIFail BIGINT UNSIGNED, CS2GHIAtt BIGINT UNSIGNED, CS2GHISucc BIGINT UNSIGNED, CS2GHICanc BIGINT UNSIGNED, CS2GHIFail BIGINT UNSIGNED, PS2GHIAtt BIGINT UNSIGNED, PS2GHISucc BIGINT UNSIGNED, PS2GHICanc BIGINT UNSIGNED, PS2GHIFail BIGINT UNSIGNED, CSInterHNBHIAtt BIGINT UNSIGNED, CSInterHNBHISucc BIGINT UNSIGNED, CSInterHNBHICanc BIGINT UNSIGNED, CSInterHNBHIFail BIGINT UNSIGNED, PSInterHNBHIAtt BIGINT UNSIGNED, PSInterHNBHISucc BIGINT UNSIGNED, PSInterHNBHICanc BIGINT UNSIGNED, PSInterHNBHIFail BIGINT UNSIGNED, CSInterHNBHORelocPrepAtt BIGINT UNSIGNED, CSInterHNBHORelocPrepSucc BIGINT UNSIGNED, CSInterHNBHORelocPrepCanc BIGINT UNSIGNED, CSInterHNBHORelocPrepFail BIGINT UNSIGNED, PSInterHNBHORelocPrepAtt BIGINT UNSIGNED, PSInterHNBHORelocPrepSucc BIGINT UNSIGNED, PSInterHNBHORelocPrepCanc BIGINT UNSIGNED, PSInterHNBHORelocPrepFail BIGINT UNSIGNED, CSInterHNBHORelocExecAtt BIGINT UNSIGNED, CSInterHNBHORelocExecSucc BIGINT UNSIGNED, CSInterHNBHORelocExecFail BIGINT UNSIGNED, PSInterHNBHORelocExecAtt BIGINT UNSIGNED, PSInterHNBHORelocExecSucc BIGINT UNSIGNED, PSInterHNBHORelocExecFail BIGINT UNSIGNED, AttInboundMobility BIGINT UNSIGNED, SuccInboundMobility BIGINT UNSIGNED, FailedInboundMobility BIGINT UNSIGNED, TCXO_UnSync_Time BIGINT UNSIGNED, PagingCSMax BIGINT UNSIGNED, PagingPSMax BIGINT UNSIGNED, PDPCntxReq BIGINT UNSIGNED, PDPCntxSucc BIGINT UNSIGNED, PDPCntxDeacReq BIGINT UNSIGNED, PDPCntxDeacSucc BIGINT UNSIGNED, PDPCntxModReq BIGINT UNSIGNED, PDPCntxModSucc BIGINT UNSIGNED, SMSAttMO BIGINT UNSIGNED, SMSSuccMO BIGINT UNSIGNED, SMSAttMT BIGINT UNSIGNED, SMSSuccMT BIGINT UNSIGNED, DCHtoFACHReconf BIGINT UNSIGNED, DCHtoPCHReconf BIGINT UNSIGNED, DCHtoIdleReconf BIGINT UNSIGNED, FACHtoPCHReconf BIGINT UNSIGNED, FACHtoIdleReconf BIGINT UNSIGNED, FACHtoDCHReconf BIGINT UNSIGNED, PCHtoIdleReconf BIGINT UNSIGNED, PCHtoFACHReconf BIGINT UNSIGNED, PCHtoDCHReconf BIGINT UNSIGNED, SABPMsgRecvd BIGINT UNSIGNED, SABPFail BIGINT UNSIGNED, CBSMsgRecvd BIGINT UNSIGNED, CBSMsgDelvd BIGINT UNSIGNED, CBSMsgFail BIGINT UNSIGNED, FDSCRIRecvd BIGINT UNSIGNED, FDFACHReconf BIGINT UNSIGNED, FDPCHReconf BIGINT UNSIGNED, FDIdleReconf BIGINT UNSIGNED, NumThrExedRTCP BIGINT UNSIGNED, NumThrExedFTP BIGINT UNSIGNED, HNBUptime BIGINT UNSIGNED, HNBStatus VARCHAR(64), BootTime BIGINT UNSIGNED, MeanAMRCallDur BIGINT UNSIGNED, MeanVideoCallDur BIGINT UNSIGNED, IncIuUpCsOct BIGINT UNSIGNED, OutIuUpCsOct BIGINT UNSIGNED, IncIuUpPsPkt BIGINT UNSIGNED, OutIuUpPsPkt BIGINT UNSIGNED, IncIuUpPsOct BIGINT UNSIGNED, OutIuUpPsOct BIGINT UNSIGNED, NumRxErrPkts BIGINT UNSIGNED, NumRxDropPkts BIGINT UNSIGNED, NumUserBitsEDCHPS BIGINT UNSIGNED, NumUserBitsPS64UL BIGINT UNSIGNED, NumUserBitsPS128UL BIGINT UNSIGNED, NumUserBitsPS384UL BIGINT UNSIGNED, NumUserBitsHSDPADLPS BIGINT UNSIGNED, NumUserBitsPS64DL BIGINT UNSIGNED, NumUserBitsPS128DL BIGINT UNSIGNED, NumUserBitsPS384DL BIGINT UNSIGNED, RSCPdist BIGINT UNSIGNED, EcN0dist BIGINT UNSIGNED, ULRoT BIGINT UNSIGNED, ULInterference BIGINT UNSIGNED, IPSecDpdretransmit BIGINT UNSIGNED, IPSecDisconnect BIGINT UNSIGNED, BlindCSInterFreq3GHORelocPrepAtt BIGINT UNSIGNED, BlindCSInterFreq3GHORelocPrepSucc BIGINT UNSIGNED, BlindCSInterFreq3GHORelocPrepCanc BIGINT UNSIGNED, BlindCSInterFreq3GHORelocPrepFail BIGINT UNSIGNED, BlindCSInterFreq3GHORelocExecAtt BIGINT UNSIGNED, BlindCSInterFreq3GHORelocExecSucc BIGINT UNSIGNED, BlindCSInterFreq3GHORelocExecFail BIGINT UNSIGNED, BlindPSInterFreq3GHORelocPrepAtt BIGINT UNSIGNED, BlindPSInterFreq3GHORelocPrepSucc BIGINT UNSIGNED, BlindPSInterFreq3GHORelocPrepCanc BIGINT UNSIGNED, BlindPSInterFreq3GHORelocPrepFail BIGINT UNSIGNED, BlindPSInterFreq3GHORelocExecAtt BIGINT UNSIGNED, BlindPSInterFreq3GHORelocExecSucc BIGINT UNSIGNED, BlindPSInterFreq3GHORelocExecFail BIGINT UNSIGNED, BlindCS2GHORelocPrepAtt BIGINT UNSIGNED, BlindCS2GHORelocPrepSucc BIGINT UNSIGNED, BlindCS2GHORelocPrepCanc BIGINT UNSIGNED, BlindCS2GHORelocPrepFail BIGINT UNSIGNED, BlindCS2GHORelocExecAtt BIGINT UNSIGNED, BlindCS2GHORelocExecSucc BIGINT UNSIGNED, BlindCS2GHORelocExecFail BIGINT UNSIGNED, BlindPS2GHORelocPrepAtt BIGINT UNSIGNED, BlindPS2GHORelocPrepSucc BIGINT UNSIGNED, BlindPS2GHORelocPrepCanc BIGINT UNSIGNED, BlindPS2GHORelocPrepFail BIGINT UNSIGNED, BlindPS2GHORelocExecAtt BIGINT UNSIGNED, BlindPS2GHORelocExecSucc BIGINT UNSIGNED, BlindPS2GHORelocExecFail BIGINT UNSIGNED, BlindCSInterHNBInterFreqHORelocPrepAtt BIGINT UNSIGNED, BlindCSInterHNBInterFreqHORelocPrepSucc BIGINT UNSIGNED, BlindCSInterHNBInterFreqHORelocPrepCanc BIGINT UNSIGNED, BlindCSInterHNBInterFreqHORelocPrepFail BIGINT UNSIGNED, BlindCSInterHNBInterFreqHORelocExecAtt BIGINT UNSIGNED, BlindCSInterHNBInterFreqHORelocExecSucc BIGINT UNSIGNED, BlindCSInterHNBInterFreqHORelocExecFail BIGINT UNSIGNED, BlindPSInterHNBInterFreqHORelocPrepAtt BIGINT UNSIGNED, BlindPSInterHNBInterFreqHORelocPrepSucc BIGINT UNSIGNED, BlindPSInterHNBInterFreqHORelocPrepCanc BIGINT UNSIGNED, BlindPSInterHNBInterFreqHORelocPrepFail BIGINT UNSIGNED, BlindPSInterHNBInterFreqHORelocExecAtt BIGINT UNSIGNED, BlindPSInterHNBInterFreqHORelocExecSucc BIGINT UNSIGNED, BlindPSInterHNBInterFreqHORelocExecFail BIGINT UNSIGNED,
					PRIMARY KEY (hnbMAC, endtime),
					FOREIGN KEY (hnbMAC) REFERENCES $hnbMACList_table (hnbMAC)
				);";

		return $this->query_response3("create_kpi_table", $cmd);
	}

	public function create_kpiWarning_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$kpi_warning = $this->config->item('kpi_warning');
		$target_table = $this->config->item('kpi_target');
		$hnbMACList_table = $this->config->item('hnbMACList_table');
		$cmd = "CREATE TABLE IF NOT EXISTS $kpi_warning
				(
					hnbMAC CHAR(12),
					endtime DATETIME DEFAULT 0,
					kpiName VARCHAR(64),
					kpiTarget DOUBLE DEFAULT 0,
					kpiValue DOUBLE NOT NULL,
					note VARCHAR(200),
					isChecked TINYINT(1) NOT NULL DEFAULT 0,
					updateUser VARCHAR(30),
					updateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY (hnbMAC, endtime, kpiName, kpiTarget),
					FOREIGN KEY (hnbMAC) REFERENCES $hnbMACList_table (hnbMAC),
					FOREIGN KEY (kpiName) REFERENCES $target_table (kpiName)
				);";

		return $this->query_response3("create_kpiWarning_table", $cmd);
	}

	public function create_alarmWarning_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$alarm_warning = $this->config->item('alarm_warning');
		$hnbMACList_table = $this->config->item('hnbMACList_table');
		$cmd = "CREATE TABLE IF NOT EXISTS $alarm_warning
				(
					hnbMAC CHAR(12),
					logTime DATETIME,
					alarmType VARCHAR(22) NOT NULL,
					triggerCondition VARCHAR(8) NOT NULL,
					alarmEvents TEXT NOT NULL,
					note VARCHAR(200),
					isChecked TINYINT(1) NOT NULL DEFAULT 0,
					updateUser VARCHAR(30),
					updateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY (hnbMAC, logTime),
					FOREIGN KEY (hnbMAC) REFERENCES $hnbMACList_table (hnbMAC)
				);";

		return $this->query_response3("create_alarmWarning_table", $cmd);
	}

	public function create_preProcess_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$pre_process = $this->config->item('pre_process');
		$cmd = "CREATE TABLE IF NOT EXISTS $pre_process
				(
					processName VARCHAR(64) NOT NULL,
					howManyDaysAgo INT(10) UNSIGNED NOT NULL,
					updateUser VARCHAR(30),
					updateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY (processName)
				);";
		$msg = $this->query_response3("create_preProcess_table", $cmd);

		$cmd = "INSERT INTO $pre_process VALUES ('KPIreportPreProcess',4,'system',CURRENT_TIMESTAMP)";
		$msg .= $this->query_response3("create_preProcess_table", $cmd);

		return $msg;
	}

	public function create_kpiSummary_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$kpi_summary = $this->config->item('kpi_summary');
		$cmd = "CREATE TABLE IF NOT EXISTS $kpi_summary
				(
					kpiName VARCHAR(64) NOT NULL,
					result DOUBLE,
					endTimeDaily DATE NOT NULL,
					updateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
				);";

		return $this->query_response3("create_kpiSummary_table", $cmd);
	}

	public function setup_faplog()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("faplog");
		$this->db->query("DROP DATABASE IF EXISTS $db");
		$this->db->query("CREATE DATABASE IF NOT EXISTS $db");
		$this->db->query("USE $db");

		$errMsg = "";

		// 0 hnbMAC_lastlog
		$cmd = "CREATE TABLE IF NOT EXISTS hnbMAC_lastlog (
			hnbMAC CHAR(12) PRIMARY KEY,
			logtime CHAR(26) NOT NULL,
			msg TEXT NOT NULL,
			updateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
		)";
		$errMsg .= $this->query_response3("setup_faplog->hnbMAC_lastlog", $cmd);

		// 1 Cell Update
		$cmd = "CREATE TABLE IF NOT EXISTS CellUpdate (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			Cause VARCHAR(100) NOT NULL,
		 	PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->CellUpdate", $cmd);

		// 2 ClipUeTxPower
		$cmd = "CREATE TABLE IF NOT EXISTS ClipUeTxPower (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			IMSI VARCHAR(15) NOT NULL,
			Capped_TxPower VARCHAR(8) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->ClipUeTxPower", $cmd);

		// 3 FemtoTxPower
		$cmd = "CREATE TABLE IF NOT EXISTS FemtoTxPower (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			Channel VARCHAR(5) NOT NULL,
			PSC VARCHAR(3) NOT NULL,
			TxPower VARCHAR(10) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->FemtoTxPower", $cmd);

		// 4 GumpUlRSSI
		$cmd = "CREATE TABLE IF NOT EXISTS GumpUlRSSI (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			UL_RSSI FLOAT(2) NOT NULL,
			UL_RSSI_HNB FLOAT(2) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->GumpUlRSSI", $cmd);

		// 5 HNBState
		$cmd = "CREATE TABLE IF NOT EXISTS HNBState (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			HNB_number VARCHAR(1) NOT NULL,
			HNB_State VARCHAR(20) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->HNBState", $cmd);

		// 6 HO
		$cmd = "CREATE TABLE IF NOT EXISTS HO (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			IMSI VARCHAR(15) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->HO", $cmd);

		// 7 IM_REG
		$cmd = "CREATE TABLE IF NOT EXISTS IM_REG (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			HUE_REG INT(3) NOT NULL,
			MUE_REG INT(3) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->IM_REG", $cmd);

		// 8 IniDirectTransfer
		$cmd = "CREATE TABLE IF NOT EXISTS IniDirectTransfer (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			LU_REQ tinyint(1) NOT NULL,
			RAU_REQ tinyint(1) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->IniDirectTransfer", $cmd);

		// 9 Mart_Power
		$cmd = "CREATE TABLE IF NOT EXISTS Mart_Power (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			Mart_Power VARCHAR(10) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->Mart_Power", $cmd);

		// 10 MascUpdatedNCL
		$cmd = "CREATE TABLE IF NOT EXISTS MascUpdatedNCL (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			Channel VARCHAR(43) NOT NULL,
			PSC VARCHAR(8) NOT NULL,
			Status VARCHAR(110) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->MascUpdatedNCL", $cmd);

		// 11 NlpcSnifferCell
		$cmd = "CREATE TABLE IF NOT EXISTS NlpcSnifferCell (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			Channel VARCHAR(5) NOT NULL,
			PSC VARCHAR(3) NOT NULL,
			RSCP VARCHAR(6) NOT NULL,
			EcNo VARCHAR(6) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->NlpcSnifferCell", $cmd);

		// 12 NLPC_Power
		$cmd = "CREATE TABLE IF NOT EXISTS NLPC_Power (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			NLPC_Power VARCHAR(11) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->NLPC_Power", $cmd);

		// 13 PowerOff_Duration
		$cmd = "CREATE TABLE IF NOT EXISTS PowerOff_Duration (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			inact_start CHAR(26) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->PowerOff_Duration", $cmd);

		// 14 ROT
		$cmd = "CREATE TABLE IF NOT EXISTS ROT (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			ROTval FLOAT(2) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->ROT", $cmd);

		// 15 Timer33_Expired
		$cmd = "CREATE TABLE IF NOT EXISTS Timer33_Expired (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->Timer33_Expired", $cmd);

		// 16 Timer41_Expired
		$cmd = "CREATE TABLE IF NOT EXISTS Timer41_Expired (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->Timer41_Expired", $cmd);

		// 17 UeIntraMRM
		$cmd = "CREATE TABLE IF NOT EXISTS UeIntraMRM (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			IMSI VARCHAR(15) NOT NULL,
			PSC VARCHAR(3) NOT NULL,
			RSCP FLOAT(2) NOT NULL,
			EcNo FLOAT(2) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->UeIntraMRM", $cmd);

		// 18 UeState
		$cmd = "CREATE TABLE IF NOT EXISTS UeState (
			hnbMAC CHAR(12) NOT NULL,
			logtime CHAR(26) NOT NULL,
			IMSI VARCHAR(15) NOT NULL,
			UE_State VARCHAR(1) NOT NULL,
			PRIMARY KEY (hnbMAC, logtime)
		)";
		$errMsg .= $this->query_response3("setup_faplog->UeState", $cmd);

		$this->db->close();

		if($errMsg == "")
			print_r("Setup Faplog: success<br>");
		else
			echo $errMsg;
	}

	public function get_housekeep_info()
	{
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$housedkeeping_table = $this->config->item('housedkeeping_table');
		$cmd = "SELECT * FROM $housedkeeping_table";
		$query = $this->db->query($cmd);
		$row = $query->row();
		$query->free_result();
		$this->db->close();

		$data['updateTime'] = $row->updateTime;
		$data['username'] = $row->username;
		$data['keep_rawdata_days'] = $row->keep_rawdata_days;
		$data['keep_dbdata_days'] = $row->keep_dbdata_days;

		return json_encode($data);
	}

	public function update_housekeep_info($dbdata_days, $rawdata_days)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$housedkeeping_table = $this->config->item('housedkeeping_table');
		$cmd = "UPDATE $housedkeeping_table SET keep_dbdata_days=$dbdata_days,keep_rawdata_days=$rawdata_days";
		$this->query_response($cmd);
		$this->db->close();

		return 'success';
	}

	public function housekeep_deleteDB($thedate)
	{
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$kpi_table =  $this->config->item('kpi_table');
		$cmd = "DELETE FROM $kpi_table WHERE endtime < '$thedate'";
		echo "$cmd<br>";

		$this->db->close();
	}

	public function get_info($type)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$temp_data = array();
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$server = $this->config->item("ftpServer");
		$path = "";

		$cmd = "SELECT ip, port, configName, loginName, type, directoryPath, updateTime, username FROM $server";

		switch ($type)
		{
			case "hnb":
				$cmd .= " WHERE configName='LOG' OR configName='PM'";
				break;

			case "backup":
				$cmd .= " WHERE configName='BACKUP'";
				break;
		}

		$query = $this->db->query($cmd);
		if($query->num_rows() > 0)
		{
			foreach($query->result_array() as $row)
			{
				array_push($temp_data, $row);
			}
		}
		$query->free_result();
		$this->db->close();

		$ftp_logintype = $this->config->item('ftp_logintype');
		array_push($temp_data, $ftp_logintype);//(object) $ftp_logintype

		return json_encode($temp_data);
	}

	public function delete_server($selectedIP, $server)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$this->db->query("USE $db");

		$server = $this->config->item("ftpServer");

		$cmd = "DELETE FROM $server WHERE (configName, ip) in (";
		for($i = 0; $i < count($selectedIP); $i ++)
		{
			$item = $selectedIP[$i];
			$cmd .= "('$item[0]', '$item[1]'),";
		}
		$cmd = rtrim($cmd, ",").")";

		$res = $this->query_response2($cmd);
		$this->db->close();

		echo $res;
	}

	public function update_server($ip, $port, $configName, $loginName, $hash, $ftp_logintype, $path, $server, $userName)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$this->db->query("USE $db");

		$server = $this->config->item("ftpServer");

		if($hash != NULL)
			$cmd = "UPDATE $server SET port='$port', loginName='$loginName', loginHash='$hash', type='$ftp_logintype', directoryPath='$path', username='$userName' WHERE ip='$ip' AND configName='$configName'";
		else
			$cmd = "UPDATE $server SET port='$port', loginName='$loginName', type='$ftp_logintype', directoryPath='$path', username='$userName' WHERE ip='$ip' AND configName='$configName'";

		$res = $this->query_response2($cmd);
		$this->db->close();

		return $res;
	}

	public function add_server($ip, $port, $configName, $loginName, $hash, $ftp_logintype, $path, $server, $userName)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$this->db->query("USE $db");

		$table = $this->config->item("ftpServer");

		$cmd = "SELECT ip FROM $table WHERE configName='$configName' AND ip='$ip'";
		$query = $this->db->query($cmd);
		$num = $query->num_rows();

		if($num != 0)
		{
			return "Duplicate IP address and Category.";
		}
		else
		{
			$cmd = "INSERT INTO $table VALUES ('$configName', '$ip', '$port', '$loginName','$hash','$ftp_logintype', '$path', CURRENT_TIMESTAMP, '$userName')";

			$res = $this->query_response2($cmd);
			$this->db->close();

			return $res;
		}
	}

	public function get_kpiTarget_info($act)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$temp_data = array();
		$data = array();
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$kpiTarget_table = $this->config->item('kpi_target');
		$KpiDisplayName = $this->config->item("kpi");

		switch($act)
		{
			case "show":
				$cmd = "SELECT kpiName, target, updateTime, updateUser FROM $kpiTarget_table";
				$query = $this->db->query($cmd);
				if($query->num_rows() > 0)
				{
					foreach($query->result_array() as $row)
					{
						$row['KpiDisplayName'] = $KpiDisplayName[$row['kpiName']];
						array_push($temp_data, $row);
					}
				}
				ksort($temp_data);
				$data = $temp_data;
				break;

			case "add":
				$cmd = "SELECT kpiName FROM $kpiTarget_table";
				$query = $this->db->query($cmd);
				if($query->num_rows() > 0)
				{
					foreach($query->result() as $row)
					{
						array_push($temp_data, $row->kpiName);
					}
				}

				ksort($temp_data);
				$data["kpidata"] = $temp_data;
				$data["kpi"] = $KpiDisplayName;
				break;
		}

		$query->free_result();
		$this->db->close();
		return json_encode($data);
	}

	public function update_kpiTarget($data, $updateUser)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$kpiTarget_table = $this->config->item('kpi_target');

		$temp = "";
		$cmd = "UPDATE $kpiTarget_table SET updateUser='$updateUser', target=  CASE ";
		foreach ($data as $k => $v) {
			$cmd .= "WHEN kpiName = '$k' THEN '$v' ";
			$temp .= "'$k',";
		}
		$cmd .= "END WHERE kpiName IN (".rtrim($temp, ",").")";

		$res = $this->query_response($cmd);
		$this->db->close();

		echo $res;
	}

	public function add_kpiTarget($kpi, $target, $updateUser)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$kpiTarget_table = $this->config->item('kpi_target');

		$cmd = "INSERT INTO $kpiTarget_table VALUES ('$kpi', '$target', CURRENT_TIMESTAMP, '$updateUser')";
		$res = $this->query_response2($cmd);
		$this->db->close();

		echo $res;
	}

	public function delete_kpiTarget($selectedKpi)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$kpiTarget_table = $this->config->item('kpi_target');

		$cmd = "DELETE FROM $kpiTarget_table WHERE kpiName = '$selectedKpi[0]'";
		for($i = 1; $i < count($selectedKpi); $i ++)
		{
			$cmd .= " or kpiName = '$selectedKpi[$i]'";
		}

		$res = $this->query_response($cmd);
		$this->db->close();

		echo $res;
	}

	public function get_usrPref($user, $pref)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");

		$column = "";
		switch($pref)
		{
			case "kpi":
				$column = "kpiPreference";
				break;

			case "counter":
				$column = "counterPreference";
				break;

			case "hnb":
				$column = "hnbMacPreference";
				break;
		}

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item('user_table');
		$cmd = "SELECT $column FROM $table WHERE username='$user'";
		$query = $this->db->query($cmd);

		$userPref = "";
		if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
			$userPref = explode( '|', $row[$column] );
		}
		$query->free_result();
		$this->db->close();

		return $userPref;
	}

	public function add_kpiPref($kpi, $user)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item('user_table');

		$kpiPref = "";
		if(count($kpi) != 0)
		{
			foreach($kpi as $k => $v)
				$kpiPref = $kpiPref.$v."|";
			$kpiPref .= "Num_of_HNBs_PM_Reported";
		}
		else
			$kpiPref = "Num_of_HNBs_PM_Reported";

		$cmd = "UPDATE $table SET kpiPreference='$kpiPref',updateUser='$user' WHERE username='$user'";
		$res = $this->query_response($cmd);
		$this->db->close();

		echo $res;
	}

	public function get_AllCounter()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item('kpi_table');
		$cmd = "SELECT column_name FROM information_schema.columns WHERE table_name = '$table'";
		$query = $this->db->query($cmd);
		$counterName = array();

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
				array_push($counterName, $row['column_name']);
		}
		$query->free_result();
		$this->db->close();

		return $counterName;
	}

	public function add_counterPref($counter, $user)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item('user_table');

		$counterPref = "";
		if(count($counterPref) != 0)
		{
			foreach($counter as $k => $v)
				$counterPref = $counterPref.$v."|";
			$counterPref = rtrim($counterPref, "|");
		}

		$cmd = "UPDATE $table SET counterPreference='$counterPref',updateUser='$user' WHERE username='$user'";
		$res = $this->query_response($cmd);
		$this->db->close();

		echo $res;
	}

	public function get_AllHNB()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item('hnbMACList_table');
		$cmd = "SELECT hnbMAC FROM $table";
		$query = $this->db->query($cmd);
		$hnbList = array();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
				array_push($hnbList, $row['hnbMAC']);
		}
		$query->free_result();
		$this->db->close();

		return $hnbList;
	}

	public function add_hnbPref($hnb, $user)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit("No direct script access allowed");

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item('user_table');

		$hnbPref = "";
		if(count($hnb) != 0)
		{
			foreach($hnb as $k => $v)
				$hnbPref = $hnbPref.$v."|";
			$hnbPref = rtrim($hnbPref, "|");
		}


		$cmd = "UPDATE $table SET hnbMacPreference='$hnbPref',updateUser='$user' WHERE username='$user'";
		$res = $this->query_response($cmd);
		$this->db->close();

		echo $res;
	}

	public function check_db($db)
	{
		$cmd = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '$db'";
		$res = $this->db->query($cmd);
		$this->db->close();
		if($res->num_rows() <= 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}

/* End of file configtool_model.php */
/* Location: ./application/models/configtool_model.php */