<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alarm_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_alarm_record($startDate, $endDate, $type)
	{
		/* Cleared Mode */
		$tb = $this->config->item("alarm_warning");
		$db = $this->config->item("db");
		$result = array();

		$cmd = "SELECT hnbMAC, logTime, alarmEvents FROM $tb WHERE triggerCondition = 'New' AND alarmType = '$type' AND logTime BETWEEN '$startDate' AND '$endDate' ORDER BY logTime ASC";

		$this->db->query("USE $db");
		$query = $this->db->query($cmd);
		if($query->num_rows() > 0)
		{
			foreach($query->result_array() as $row)
			{
				array_push($result, $row);
			}
		}
		$query->free_result();
		$this->db->close();

		return $result;
	}
}

/* End of file alarm_model.php */
/* Location: ./application/models/alarm_model.php */