<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Log_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$ci = get_instance();
		$ci->load->helper('user_helper');
	}

	public function query_response($cmd)
	{
		$res = $this->db->query($cmd);
		if( !$res )
		{
			$errNo   = $this->db->_error_number();
			$errMess = $this->db->_error_message();

			return "$errNo: $errMess";
		}
	}

	public function setup_database()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("log");
		$this->db->query("DROP DATABASE IF EXISTS $db");
		$this->db->query("CREATE DATABASE IF NOT EXISTS $db");
		$this->db->query("USE $db");

		$errMsg = "";

		$errMsg .= $this->create_rtt_table();
		$errMsg .= $this->create_error_table();
		$errMsg .= $this->create_cellupdate_table();
		$errMsg .= $this->create_reconfiguration_table();
		$errMsg .= $this->create_preemption_table();
		$errMsg .= $this->create_ho_table();
		$this->db->close();

		if($errMsg == "")
			print_r("Setup Log Database: success<br>");
		else
			echo $errMsg;


		$errMsg = $this->insert_data();
		if($errMsg == "")
			print_r("Insert data to Log Database success<br>");
		else
			echo $errMsg;
	}

	public function create_error_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$table = $this->config->item("hnbMACList_table");
		$cmd = "CREATE TABLE IF NOT EXISTS error
				(
					logtime CHAR(26) PRIMARY KEY,
					hnbMAC CHAR(12) NOT NULL,
					callid CHAR(28) NOT NULL,
					radioLinkFailureIndication TINYINT(1) NOT NULL,
					iuReleaseRequest TINYINT(1) NOT NULL,
					existedProcess TINYINT(1) NOT NULL,
					channelizationCodeError TINYINT(1) NOT NULL,
					diramUlError TINYINT(1) NOT NULL,
					diramDlError TINYINT(1) NOT NULL,
					demulatorError TINYINT(1) NOT NULL,
					rlSetupRequestFailed TINYINT(1) NOT NULL,
					rlActivationFailed TINYINT(1) NOT NULL,
					rlReconfigPrepareRequestFailed TINYINT(1) NOT NULL,
					rlReconfigCommitFailed TINYINT(1) NOT NULL,
					rlDeleteFailed TINYINT(1) NOT NULL,
					compressModeFailed TINYINT(1) NOT NULL,
					reconfigCancelRequestFailed TINYINT(1) NOT NULL,
					FOREIGN KEY (hnbMAC) REFERENCES $db.$table (hnbMAC)
				);";
		$msg = $this->query_response($cmd);

		return $msg;
	}

	public function create_rtt_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$table = $this->config->item("hnbMACList_table");
		$cmd = "CREATE TABLE IF NOT EXISTS rtt
				(
					logtime CHAR(26) PRIMARY KEY,
					hnbMAC CHAR(12) NOT NULL,
					callid CHAR(28) NOT NULL,
					RrcConReq FLOAT(4,1) NOT NULL,
					RrcFailed TINYINT(1) NOT NULL,
					SmsMoCpData FLOAT(4,1) NOT NULL,
					SmsCnCpAck FLOAT(4,1) NOT NULL,
					SmsCnCpDataAck FLOAT(4,1) NOT NULL,
					SmsCnCpData FLOAT(4,1) NOT NULL,
					SmsMtCpAck FLOAT(4,1) NOT NULL,
					SmsMtCpDataAck FLOAT(4,1) NOT NULL,
					SmsFailed TINYINT(1) NOT NULL,
					CsCmServiceRequest FLOAT(4,1) NOT NULL,
					CsRadioBearerSetupComplete FLOAT(4,1) NOT NULL,
					CsAlert FLOAT(4,1) NOT NULL,
					CsFailed TINYINT(1) NOT NULL,
					PsServiceRequest FLOAT(4,1) NOT NULL,
					PsFailed TINYINT(1) NOT NULL,
					FOREIGN KEY (hnbMAC) REFERENCES $db.$table (hnbMAC)
				);";
		$msg = $this->query_response($cmd);

		return $msg;
	}

	public function create_reconfiguration_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$table = $this->config->item("hnbMACList_table");
		$cmd = "CREATE TABLE IF NOT EXISTS reconfiguration
				(
					logtime CHAR(26) PRIMARY KEY,
					hnbMAC CHAR(12) NOT NULL,
					callid CHAR(28) NOT NULL,
					reconfigurationOfDch SMALLINT NOT NULL,
					reconfigurationofFach SMALLINT NOT NULL,
					reconfigurationOfPch SMALLINT NOT NULL,
					rateAdaption SMALLINT NOT NULL,
					FOREIGN KEY (hnbMAC) REFERENCES $db.$table (hnbMAC)
				);";
		$msg = $this->query_response($cmd);

		return $msg;
	}

	public function create_ho_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$table = $this->config->item("hnbMACList_table");
		$cmd = "CREATE TABLE IF NOT EXISTS ho
				(
					logtime CHAR(26) PRIMARY KEY,
					hnbMAC CHAR(12) NOT NULL,
					callid CHAR(28) NOT NULL,
					hoEvents VARCHAR(23) NOT NULL,
					hoDomain CHAR(2) NOT NULL,
					hoType VARCHAR(8) NOT NULL,
					FOREIGN KEY (hnbMAC) REFERENCES $db.$table (hnbMAC)
				);";
		$msg = $this->query_response($cmd);

		return $msg;
	}

	public function create_cellupdate_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$table = $this->config->item("hnbMACList_table");
		$cmd = "CREATE TABLE IF NOT EXISTS cellupdate
				(
					logtime CHAR(26) PRIMARY KEY,
					hnbMAC CHAR(12) NOT NULL,
					callid CHAR(28) NOT NULL,
					rlcUnrecoverableError TINYINT(1) NOT NULL,
					radioLinkFailure TINYINT(1) NOT NULL,
					rlcUnrecoverableErrorDetectedByFap TINYINT(1) NOT NULL,
					FOREIGN KEY (hnbMAC) REFERENCES $db.$table (hnbMAC)
				);";
		$msg = $this->query_response($cmd);

		return $msg;
	}

	public function create_preemption_table()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$table = $this->config->item("hnbMACList_table");
		$cmd = "CREATE TABLE IF NOT EXISTS preemption
				(
					logtime CHAR(26) PRIMARY KEY,
					hnbMAC CHAR(12) NOT NULL,
					callid CHAR(28) NOT NULL,
					preemptBack TINYINT(1) NOT NULL,
					preempter TINYINT(1) NOT NULL,
					preemptToIdle TINYINT(1) NOT NULL,
					FOREIGN KEY (hnbMAC) REFERENCES $db.$table (hnbMAC)
				);";
		$msg = $this->query_response($cmd);

		return $msg;
	}

	public function insert_data()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$msg = "";

		$db = $this->config->item("log");
		$this->db->query("USE $db");

		// Error Table
		$cmd = "INSERT INTO error VALUES ('2015:06:24:18:18:38:332676','5C338E59ADAA','1-0xce2000b0-515020137776460',0,0,0,0,0,0,0,0,0,0,0,0,0,0),('2015:06:24:18:20:38:332676','5C338E59ADAB','1-0xce3000a0-515020137776475',0,0,0,0,1,0,1,0,0,0,1,0,0,1),('2015:06:24:19:18:38:332676','5C338E59ADAF','1-0xce4021c0-515020137776488',1,0,0,1,0,0,0,1,0,0,0,1,1,1),('2015:06:24:19:19:38:332676','5C338E59ADAA','1-0xce4021c0-515020137776488',1,1,1,0,1,1,1,0,0,1,1,0,0,0),('2015:06:24:19:20:38:332676','5C338E59ADAA','1-0xce3000a0-515020137776475',0,1,1,0,1,1,1,0,0,1,1,0,0,0),('2015:06:24:19:21:38:332676','5C338E59ADAB','1-2xce7000a0-515020137776484',0,0,1,1,1,0,0,1,0,1,1,0,0,1),('2015:06:24:19:22:38:332676','5C338E59ADAB','1-0xce4021c0-515020137776488',1,1,1,1,1,0,0,0,0,1,0,1,1,1),('2015:06:24:19:23:38:332676','5C338E59ADAF','1-2xce7000a0-515020137776484',0,0,0,0,0,0,1,0,0,0,1,1,1,1),('2015:06:24:19:24:38:332676','5C338E59ADAF','1-0xce2000b0-515020137776460',1,1,0,1,0,0,1,0,0,0,0,0,0,0)";
		$msg .= $this->query_response($cmd);		

		// Reconfiguration Table
		$cmd = "INSERT INTO reconfiguration VALUES ('2015:06:24:18:18:38:332676','5C338E59ADAA','1-0xce2000b0-515020137776460',30,0,20,10),('2015:06:24:18:20:38:332676','5C338E59ADAB','1-0xce3000a0-515020137776475',0,10,40,0),('2015:06:24:19:18:38:332676','5C338E59ADAF','1-0xce4021c0-515020137776488',1,40,0,1),('2015:06:24:19:19:38:332676','5C338E59ADAA','1-0xce4021c0-515020137776482',11,41,32,0),('2015:06:24:19:20:38:332676','5C338E59ADAA','1-0xce3000a0-515020137776477',0,0,0,0),('2015:06:24:19:21:38:332676','5C338E59ADAB','1-2xce7000a0-515020137776483',20,10,15,17),('2015:06:24:19:22:38:332676','5C338E59ADAB','1-0xce3000a0-515020137776477',0,0,11,31),('2015:06:24:19:23:38:332676','5C338E59ADAF','1-2xce7000a0-515020137776474',30,0,0,20),('2015:06:24:19:24:38:332676','5C338E59ADAF','1-0xce2000b0-515020137776461',14,11,0,0),('2015:06:24:19:25:28:332676','5C338E59ADAF','1-0xce3000a0-515020137776472',0,34,10,30),('2015:06:24:19:27:38:332676','5C338E59ADAB','1-2xce7000a0-515020137776483',10,35,1,7)";
		$msg .= $this->query_response($cmd);

		// HO Table
		$cmd = "INSERT INTO ho VALUES 
		('2015:06:24:18:18:38:332676','5C338E59ADAA','1-0xce2000b0-515020137776460','Blind_HO_prep_failed','CS','interHNB'),('2015:06:24:18:20:38:332676','5C338E59ADAB','1-0xce3000a0-515020137776475','Blind_HO_exec_failed','PS','2G'),('2015:06:24:19:18:38:332676','5C338E59ADAF','1-0xce4021c0-515020137776488','Blind_HO_canc','CS','interHNB'),('2015:06:24:19:19:38:332676','5C338E59ADAA','1-0xce4021c0-515020137776482','Blind_HO_exec_succ','PS','2G'),('2015:06:24:19:20:38:332676','5C338E59ADAA','1-0xce3000a0-515020137776477','NonBlind_HO_prep_failed','PS','3G'),('2015:06:24:19:21:38:332676','5C338E59ADAB','1-2xce7000a0-515020137776483','NonBlind_HO_exec_failed','CS','3G'),('2015:06:24:19:22:38:332676','5C338E59ADAB','1-0xce3000a0-515020137776477','NonBlind_HO_canc','CS','2G'),('2015:06:24:19:23:38:332676','5C338E59ADAF','1-2xce7000a0-515020137776474','NonBlind_HO_exec_succ','PS','3G'),('2015:06:24:19:24:38:332676','5C338E59ADAF','1-0xce2000b0-515020137776461','HI_failed','PS','interHNB'),	('2015:06:24:19:25:28:332676','5C338E59ADAF','1-0xce3000a0-515020137776472','HI_canc','CS','3G'),('2015:06:24:19:27:38:332676','5C338E59ADAB','1-2xce7000a0-515020137776483','HI_succ','PS','interHNB'),('2015:06:24:19:28:39:335676','5C338E59ADAF','1-2xce7000a0-515020137776483','Blind_HO_prep_failed','PS','3G'),('2015:06:24:19:30:49:335676','5C338E59ADAA','1-0xce2000b0-515020137776460','Blind_HO_prep_failed','CS','interHNB')";
		$msg .= $this->query_response($cmd);

		// Cell Update Table
		$cmd = "INSERT INTO cellupdate VALUES ('2015:06:24:18:18:38:332676','5C338E59ADAA','1-0xce2000b0-515020137776460',0,0,0),('2015:06:24:18:20:38:332676','5C338E59ADAB','1-0xce3000a0-515020137776475',1,0,0),('2015:06:24:19:18:38:332676','5C338E59ADAF','1-0xce4021c0-515020137776488',0,1,0),('2015:06:24:19:19:38:332676','5C338E59ADAA','1-0xce4021c0-515020137776482',0,0,1),('2015:06:24:19:20:38:332676','5C338E59ADAA','1-0xce3000a0-515020137776477',1,1,0),('2015:06:24:19:21:38:332676','5C338E59ADAB','1-2xce7000a0-515020137776483',1,0,1),('2015:06:24:19:22:38:332676','5C338E59ADAB','1-0xce3000a0-515020137776477',0,1,1),('2015:06:24:19:23:38:332676','5C338E59ADAF','1-2xce7000a0-515020137776474',1,1,1),('2015:06:24:19:24:38:332676','5C338E59ADAF','1-0xce2000b0-515020137776461',1,0,1),('2015:06:24:19:25:28:332676','5C338E59ADAF','1-0xce3000a0-515020137776472',0,1,0),('2015:06:24:19:27:38:332676','5C338E59ADAB','1-2xce7000a0-515020137776483',1,0,0)";
		$msg .= $this->query_response($cmd);

		// Preemption Table 
		$cmd = "INSERT INTO preemption VALUES ('2015:06:24:18:18:38:332676','5C338E59ADAA','1-0xce2000b0-515020137776460',0,0,0),('2015:06:24:18:20:38:332676','5C338E59ADAB','1-0xce3000a0-515020137776475',1,0,0),('2015:06:24:19:18:38:332676','5C338E59ADAF','1-0xce4021c0-515020137776488',0,1,0),('2015:06:24:19:19:38:332676','5C338E59ADAA','1-0xce4021c0-515020137776482',0,0,1),('2015:06:24:19:20:38:332676','5C338E59ADAA','1-0xce3000a0-515020137776477',1,1,0),('2015:06:24:19:21:38:332676','5C338E59ADAB','1-2xce7000a0-515020137776483',1,0,1),('2015:06:24:19:22:38:332676','5C338E59ADAB','1-0xce3000a0-515020137776477',0,1,1),('2015:06:24:19:23:38:332676','5C338E59ADAF','1-2xce7000a0-515020137776474',1,1,1),('2015:06:24:19:24:38:332676','5C338E59ADAF','1-0xce2000b0-515020137776461',1,0,1),('2015:06:24:19:25:28:332676','5C338E59ADAF','1-0xce3000a0-515020137776472',0,1,0),('2015:06:24:19:27:38:332676','5C338E59ADAB','1-2xce7000a0-515020137776483',1,0,0)";
		$msg .= $this->query_response($cmd);

		// RTT Table
		$cmd = "INSERT INTO rtt VALUES ('2015:06:24:18:18:38:332676','5C338E59ADAA','1-0xce2000b0-515020137776460',0,0,1,4,3.5,2,3,0,1,4,1,8,1,12,0),('2015:06:24:18:20:38:332676','5C338E59ADAB','1-0xce3000a0-515020137776475',2,1,3,5,1.2,0,1,3,0,1,6,2,0,5,0),('2015:06:24:19:18:38:332676','5C338E59ADAF','1-0xce4021c0-515020137776488',0,1,0,1,2,3,4,5,1,10,2,4,1,1,1),('2015:06:24:19:19:38:332676','5C338E59ADAA','1-0xce4021c0-515020137776488',9,0,1,10,6,1.5,2.8,0,0,1,12,3.5,0,1,0),('2015:06:24:19:20:38:332676','5C338E59ADAA','1-0xce3000a0-515020137776475',1,1,5,2,5,9,1,6.2,1,4,2,4,1,1,1),('2015:06:24:19:21:38:332676','5C338E59ADAB','1-2xce7000a0-515020137776484',2,1,3,6.7,1,2,0,1,0,3,1.1,0,0,12,1),('2015:06:24:19:22:38:332676','5C338E59ADAB','1-0xce4021c0-515020137776488',5.2,1,8.2,1.3,5.1,6,9.3,19,0,9.2,1.3,4,1,3.4,0),('2015:06:24:19:23:38:332676','5C338E59ADAF','1-2xce7000a0-515020137776484',3,0,3,4,5.3,1.9,2.1,4.5,0,3,2,1,1,2.5,0),('2015:06:24:19:24:38:332676','5C338E59ADAF','1-0xce2000b0-515020137776460',3.1,1,10.5,2.9,3.4,0,0,0,0,4,0,0,0,0,0)";
		$msg .= $this->query_response($cmd);

		$this->db->close();

		return $msg;
	}

	public function getERRORdata($hnb, $startDate, $endDate, $limit)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("log");
		$this->db->query("USE $db");
		$cmd = "";
		$result = array();

		// 1. callid distribution
		$str1 = $this->genErrorCmd1($limit);
		if($hnb != "")
			$cmd = "SELECT callid,COUNT(*) as count_callid FROM error WHERE hnbMAC='$hnb' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1) GROUP BY callid ORDER BY count_callid DESC";
		else
			$cmd = "SELECT callid,COUNT(*) as count_callid FROM error WHERE (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1) GROUP BY callid ORDER BY count_callid DESC";
		$query = $this->db->query($cmd);
		
		$callid = array();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				array_push($callid, $row["callid"]);
				$result[$row["callid"]] = array("num" => $row["count_callid"]);
			}
		}
		$query->free_result();

		// 2. attribute distribution
		$str2 = $this->genErrorCmd2($limit);
		foreach($callid as $v)
		{
			if($hnb != "")
				$cmd = $str2." FROM error WHERE hnbMAC='$hnb' AND callid='$v' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1)";
			else
				$cmd = $str2." FROM error WHERE callid='$v' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1)";
			$query = $this->db->query($cmd);
			if ($query->num_rows() > 0)
			{
				$row = $query->row_array();
				foreach($limit as $con)
				{
					$result[$v][$con] = $row[$con];
				}
			}
			$query->free_result();
		}

		$this->db->close();

		return $result;
	}

	public function genErrorCmd1($c)
	{
		$str = "";
		foreach ($c as $v)
			$str .= "$v=1 OR ";
		$str = rtrim($str, " OR ");
		return $str;
	}

	public function genErrorCmd2($c)
	{
		$str = "SELECT ";
		foreach($c as $v)
			$str .= "SUM($v=1) AS $v,";
		$str = rtrim($str, ",");
		return $str;
	}

	public function getERRORdetail3($hnb, $callid, $startDate, $endDate, $limit)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("log");
		$this->db->query("USE $db");
		$cmd = "";
		$result = array();

		if($hnb != "")
			$cmd = "SELECT logtime,callid FROM error WHERE hnbMAC='$hnb' AND callid='$callid' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($limit=1)";
		else
			$cmd = "SELECT logtime,callid FROM error WHERE callid='$callid' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($limit=1)";
		$query = $this->db->query($cmd);
		if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
			foreach ($query->result_array() as $row)
			{
				$temp = array();
				$temp["logtime"] = $row["logtime"];
				$temp["callid"] = $row["callid"];
				array_push($result, $temp);
			}
		}
		$query->free_result();
		$this->db->close();

		return $result;
	}

	public function getReconfigurationData($hnb, $startDate, $endDate, $limit)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("log");
		$this->db->query("USE $db");
		$cmd = "";
		$result = array();

		// 1. callid distribution
		$str1 = $this->genReconfCmd1($limit);
		if($hnb != "")
			$cmd = "SELECT callid,COUNT(*) as count_callid FROM reconfiguration WHERE hnbMAC='$hnb' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1) GROUP BY callid ORDER BY count_callid DESC";
		else
			$cmd = "SELECT callid,COUNT(*) as count_callid FROM reconfiguration WHERE (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1) GROUP BY callid ORDER BY count_callid DESC";
		$query = $this->db->query($cmd);

		$callid = array();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				array_push($callid, $row["callid"]);
				$result[$row["callid"]] = array("num" => $row["count_callid"]);
			}
		}
		$query->free_result();

		$this->db->close();

		return $result;
	}

	public function genReconfCmd1($limit)
	{
		$str = "";
		foreach($limit as $k => $v)
			$str .= "$k>=$v AND ";
		$str = rtrim($str, " AND ");

		return $str;
	}

	public function getReconfigurationDetail2($hnb, $callid, $startDate, $endDate, $limit)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("log");
		$this->db->query("USE $db");
		$cmd = "";
		$result = array();

		// 2. attribute distribution
		$str1 = $this->genReconfCmd1($limit);

		if($hnb != "")
		{
			$cmd = "SELECT * FROM reconfiguration WHERE hnbMAC='$hnb' AND callid='$callid' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1)";
		}
		else
		{
			$cmd = "SELECT * FROM reconfiguration WHERE callid='$callid' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1)";
		}
		$query = $this->db->query($cmd);

		if ($query->num_rows() > 0)
		{
			$row = $query->row_array();

			$result[$callid]["reconfigurationOfDch"] = $row["reconfigurationOfDch"];
			$result[$callid]["reconfigurationofFach"] = $row["reconfigurationofFach"];
			$result[$callid]["reconfigurationOfPch"] = $row["reconfigurationOfPch"];
			$result[$callid]["rateAdaption"] = $row["rateAdaption"];
			$result["logtime"] = $row["logtime"];
		}

		$query->free_result();
		$this->db->close();

		return $result;
	}

	public function getHoData($hnb, $startDate, $endDate, $limit)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("log");
		$this->db->query("USE $db");
		$cmd = "";
		$result = array();

		// 1. callid distribution
		$str1 = $this->genHoCmd1($limit);
		if($hnb != "")
			$cmd = "SELECT callid,COUNT(*) as count_callid FROM ho WHERE hnbMAC='$hnb' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1) GROUP BY callid ORDER BY count_callid DESC";
		else
			$cmd = "SELECT callid,COUNT(*) as count_callid FROM ho WHERE (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1) GROUP BY callid ORDER BY count_callid DESC";
		$query = $this->db->query($cmd);

		$callid = array();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				array_push($callid, $row["callid"]);
				$result[$row["callid"]] = array("num" => $row["count_callid"]);
			}
		}
		$query->free_result();

		// 2. HO domain distribution
		foreach($callid as $v)
		{
			if($hnb != "")
				$cmd = "SELECT SUM(hoDomain='CS') AS CS, SUM(hoDomain='PS') AS PS FROM ho WHERE hnbMAC='$hnb' AND callid='$v' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1)";
			else
				$cmd = "SELECT SUM(hoDomain='CS') AS CS, SUM(hoDomain='PS') AS PS FROM ho WHERE callid='$v' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1)";
			$query = $this->db->query($cmd);
			if ($query->num_rows() > 0)
			{
				$row = $query->row_array();
				foreach($limit as $con)
				{
					$result[$v]["CS"] = array("num" => $row["CS"]);
					$result[$v]["PS"] = array("num" => $row["PS"]);
				}
			}
			$query->free_result();
		}

		// 3. HO type distribution
		$str2 = $this->genHoCmd2($limit);
		$domain = $this->config->item("hoDomain");
		foreach($callid as $v)
		{
			foreach($domain as $d)
			{
				if($result[$v][$d]["num"] > 0)
				{
					if($hnb != "")
						$cmd = "SELECT SUM(hoType='2G') AS 2G, SUM(hoType='3G') AS 3G, SUM(hoType='interHNB') AS interHNB FROM ho WHERE hnbMAC='$hnb' AND callid='$v' AND hoDomain='$d' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str2)";
					else
						$cmd = "SELECT SUM(hoType='2G') AS 2G, SUM(hoType='3G') AS 3G, SUM(hoType='interHNB') AS interHNB FROM ho WHERE callid='$v' AND hoDomain='$d' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str2)";
					$query = $this->db->query($cmd);
					if ($query->num_rows() > 0)
					{
						$row = $query->row_array();
						foreach($limit as $con)
						{
							$result[$v][$d]["2G"] = $row["2G"];
							$result[$v][$d]["3G"] = $row["3G"];
							$result[$v][$d]["interHNB"] = $row["interHNB"];
						}
					}
					$query->free_result();
				}
			}
		}

		$this->db->close();

		return $result;
	}

	public function genHoCmd1($c)
	{
		$str = "";
		foreach ($c as $k => $v)
		{
			if(count($v) == 1)
				$str .= "$k='$v[0]' AND ";
			elseif(count($v) > 1)
			{
				$temp = "(";
				foreach($v as $t)
					$temp .= "$k='$t' OR ";
				$temp = rtrim($temp, " OR ");
				$temp .= ") AND ";
				$str .= $temp;
			}
		}
		$str = rtrim($str, " AND ");
		return $str;
	}

	public function genHoCmd2($c)
	{
		unset($c["hoDomain"]);
		$str = "";
		foreach ($c as $k => $v)
		{
			if(count($v) == 1)
				$str .= "$k='$v[0]' AND ";
			elseif(count($v) > 1)
			{
				$temp = "(";
				foreach($v as $t)
					$temp .= "$k='$t' OR ";
				$temp = rtrim($temp, " OR ");
				$temp .= ") AND ";
				$str .= $temp;
			}
		}
		$str = rtrim($str, " AND ");
		return $str;
	}

	public function getHoDetail4($hnbid, $callid, $startDate, $endDate, $events, $domain, $type)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("log");
		$this->db->query("USE $db");
		$cmd = "";
		$result = array();

		$str1 = $this->genHoCmd4($events);
		if($hnb != "")
			$cmd = "SELECT * FROM ho WHERE hnbMAC='$hnb' AND callid='$callid' AND (logtime BETWEEN '$startDate' AND '$endDate') AND hoDomain='$domain' AND hoType='$type' AND $str1";
		else
			$cmd = "SELECT * FROM ho WHERE callid='$callid' AND (logtime BETWEEN '$startDate' AND '$endDate') AND hoDomain='$domain' AND hoType='$type' AND $str1";
		$query = $this->db->query($cmd);

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$temp = array();
				$temp["logtime"] = $row["logtime"];
				$temp["hnb"] = $row["hnbMAC"];
				$temp["callid"] = $row["callid"];
				$temp["hoevents"] = $row["hoEvents"];
				$temp["hodomain"] = $row["hoDomain"];
				$temp["hotype"] = $row["hoType"];
				array_push($result, $temp);
			}
		}
		$query->free_result();

		$this->db->close();

		return $result;
	}

	public function genHoCmd4($e)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$str = "";
		if(count($e) == 1)
			$str .= "hoEvents='$e[0]'";
		elseif(count($e) > 1)
		{
			$temp = "(";
			foreach($e as $t)
				$temp .= "hoEvents='$t' OR ";
			$temp = rtrim($temp, " OR ");
			$temp .= ")";
			$str .= $temp;
		}

		return $str;
	}

	public function getCellUpdateData($hnbid, $startDate, $endDate, $limit)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("log");
		$this->db->query("USE $db");
		$cmd = "";
		$result = array();

		// 1. callid distribution
		$str1 = $this->genErrorCmd1($limit);
		if($hnb != "")
			$cmd = "SELECT callid,COUNT(*) as count_callid FROM cellupdate WHERE hnbMAC='$hnb' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1) GROUP BY callid ORDER BY count_callid DESC";
		else
			$cmd = "SELECT callid,COUNT(*) as count_callid FROM cellupdate WHERE (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1) GROUP BY callid ORDER BY count_callid DESC";
		$query = $this->db->query($cmd);
		$callid = array();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				array_push($callid, $row["callid"]);
				$result[$row["callid"]] = array("num" => $row["count_callid"]);
			}
		}
		$query->free_result();

		// 2. attribute distribution
		$str2 = $this->genErrorCmd2($limit);
		foreach($callid as $v)
		{
			if($hnb != "")
				$cmd = $str2." FROM cellupdate WHERE hnbMAC='$hnb' AND callid='$v' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1)";
			else
				$cmd = $str2." FROM cellupdate WHERE callid='$v' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1)";
			$query = $this->db->query($cmd);
			if ($query->num_rows() > 0)
			{
				$row = $query->row_array();
				foreach($limit as $con)
				{
					$result[$v][$con] = $row[$con];
				}
			}
			$query->free_result();
		}
		$this->db->close();

		return $result;		
	}

	public function getCellUpdatedetail3($hnbid, $callid, $startDate, $endDate, $limit)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("log");
		$this->db->query("USE $db");
		$cmd = "";
		$result = array();

		if($hnb != "")
			$cmd = "SELECT logtime,callid FROM cellupdate WHERE hnbMAC='$hnb' AND callid='$callid' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($limit=1)";
		else
			$cmd = "SELECT logtime,callid FROM cellupdate WHERE callid='$callid' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($limit=1)";
		$query = $this->db->query($cmd);
		if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
			foreach ($query->result_array() as $row)
			{
				$temp = array();
				$temp["logtime"] = $row["logtime"];
				$temp["callid"] = $row["callid"];
				array_push($result, $temp);
			}
		}
		$query->free_result();
		$this->db->close();

		return $result;
	}

	public function getPreemptionData($hnbid, $startDate, $endDate, $limit)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("log");
		$this->db->query("USE $db");
		$cmd = "";
		$result = array();

		// 1. callid distribution
		$str1 = $this->genErrorCmd1($limit);
		if($hnb != "")
			$cmd = "SELECT callid,COUNT(*) as count_callid FROM preemption WHERE hnbMAC='$hnb' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1) GROUP BY callid ORDER BY count_callid DESC";
		else
			$cmd = "SELECT callid,COUNT(*) as count_callid FROM preemption WHERE (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1) GROUP BY callid ORDER BY count_callid DESC";
		$query = $this->db->query($cmd);
		$callid = array();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				array_push($callid, $row["callid"]);
				$result[$row["callid"]] = array("num" => $row["count_callid"]);
			}
		}
		$query->free_result();

		// 2. attribute distribution
		$str2 = $this->genErrorCmd2($limit);
		foreach($callid as $v)
		{
			if($hnb != "")
				$cmd = $str2." FROM preemption WHERE hnbMAC='$hnb' AND callid='$v' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1)";
			else
				$cmd = $str2." FROM preemption WHERE callid='$v' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1)";
			$query = $this->db->query($cmd);
			if ($query->num_rows() > 0)
			{
				$row = $query->row_array();
				foreach($limit as $con)
				{
					$result[$v][$con] = $row[$con];
				}
			}
			$query->free_result();
		}
		$this->db->close();

		return $result;		
	}

	public function getPremptionDetail3($hnbid, $callid, $startDate, $endDate, $limit)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("log");
		$this->db->query("USE $db");
		$cmd = "";
		$result = array();

		if($hnb != "")
			$cmd = "SELECT logtime,callid FROM preemption WHERE hnbMAC='$hnb' AND callid='$callid' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($limit=1)";
		else
			$cmd = "SELECT logtime,callid FROM preemption WHERE callid='$callid' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($limit=1)";
		$query = $this->db->query($cmd);
		if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
			foreach ($query->result_array() as $row)
			{
				$temp = array();
				$temp["logtime"] = $row["logtime"];
				$temp["callid"] = $row["callid"];
				array_push($result, $temp);
			}
		}
		$query->free_result();
		$this->db->close();

		return $result;
	}

	public function getRTTdata($hnb, $startDate, $endDate, $limit, $error)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("log");
		$this->db->query("USE $db");
		$cmd = "";
		$result = array();

		// 1. callid distribution
		$str1 = $this->genRttCmd1($limit, $error);
		if($hnb != "")
			$cmd = "SELECT callid,COUNT(*) as count_callid FROM rtt WHERE hnbMAC='$hnb' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1) GROUP BY callid ORDER BY count_callid DESC";
		else
			$cmd = "SELECT callid,COUNT(*) as count_callid FROM rtt WHERE (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1) GROUP BY callid ORDER BY count_callid DESC";
		$query = $this->db->query($cmd);

		$callid = array();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				array_push($callid, $row["callid"]);
				$result[$row["callid"]] = array("num" => $row["count_callid"]);
			}
		}
		$query->free_result();

		// 2. Error & domain distribution
		$cs = "IF((CsCmServiceRequest>0 OR CsRadioBearerSetupComplete>0 OR CsAlert>0) AND PsServiceRequest<=0";
		$ps = "IF((CsCmServiceRequest<=0 AND CsRadioBearerSetupComplete<=0 AND CsAlert<=0) AND PsServiceRequest>0";
		$cs_ps = "IF((CsCmServiceRequest>0 OR CsRadioBearerSetupComplete>0 OR CsAlert>0) AND PsServiceRequest>0";
		$err = "IF(PsFailed=1 OR CsFailed=1 OR SmsFailed=1 OR RrcFailed=1";
		foreach($callid as $v)
		{
			if($hnb != "")
				$cmd = "SELECT SUM($cs,1,0)) AS cs, SUM($ps,1,0)) AS ps, SUM($cs_ps,1,0)) AS cs_ps, SUM($err,1,0)) AS err FROM rtt WHERE hnbMAC='$hnb' AND callid='$v' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1)";
			else
				$cmd = "SELECT SUM($cs,1,0)) AS cs, SUM($ps,1,0)) AS ps, SUM($cs_ps,1,0)) AS cs_ps, SUM($err,1,0)) AS err FROM rtt WHERE callid='$v' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1)";
			$query = $this->db->query($cmd);
			if ($query->num_rows() > 0)
			{
				$row = $query->row_array();
				foreach($limit as $con)
				{
					$result[$v]["CS"] = $row["cs"];
					$result[$v]["PS"] = $row["ps"];
					$result[$v]["CS+PS"] = $row["cs_ps"];
					$result[$v]["Error"] = $row["err"];
				}
			}
			$query->free_result();
		}
		$this->db->close();

		return $result;
	}

	public function genRttCmd1($limit, $error)
	{
		$str1 = "";
		foreach($error as $v)
			$str1 .= "$v=1 OR ";
		$str1 = rtrim($str1, " OR ");

		$str2 = "";
		foreach($limit as $k => $v)
			$str2 .= "$k>=$v OR ";
		$str2 = rtrim($str2, " OR ");

		$str = "";
		if($str1 != "")
		{
			if($str2 != "")
				$str = $str1." OR ".$str2;
			else
				$str = $str1;
		}
		else
		{
			if($str2 != "")
				$str = $str2;
		}

		return $str;
	}

	public function getRttDetail3($hnb, $callid, $startDate, $endDate, $limit, $error, $type)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("log");
		$this->db->query("USE $db");
		$cmd = "";
		$result = array();

		$str2 = "";
		$cs = "(CsCmServiceRequest>0 OR CsRadioBearerSetupComplete>0 OR CsAlert>0) AND PsServiceRequest<=0";
		$ps = "(CsCmServiceRequest<=0 AND CsRadioBearerSetupComplete<=0 AND CsAlert<=0) AND PsServiceRequest>0";
		$cs_ps = "(CsCmServiceRequest>0 OR CsRadioBearerSetupComplete>0 OR CsAlert>0) AND PsServiceRequest>0";
		$err = "PsFailed=1 OR CsFailed=1 OR SmsFailed=1 OR RrcFailed=1";

		switch($type)
		{
			case "CS":
				$str2 = $cs;
				break;
				
			case "PS":
				$str2 = $ps;
				break;

			case "CS+PS":
				$str2 = $cs_ps;
				break;

			case "Error":
				$str2 = $err;
				break;

		}

		$str1 = $this->genRttCmd1($limit, $error);
		if($hnb != "")
			$cmd = "SELECT S.logtime, S.callid FROM (SELECT * FROM rtt WHERE hnbMAC='$hnb' AND callid='$callid' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1)) AS S WHERE $str2";
		else
			$cmd = "SELECT S.logtime, S.callid FROM (SELECT * FROM rtt WHERE callid='$callid' AND (logtime BETWEEN '$startDate' AND '$endDate') AND ($str1)) AS S WHERE $str2";
		$query = $this->db->query($cmd);
		if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
			foreach ($query->result_array() as $row)
			{
				$temp = array();
				$temp["logtime"] = $row["logtime"];
				$temp["callid"] = $row["callid"];
				array_push($result, $temp);
			}
		}
		$query->free_result();
		$this->db->close();

		return $result;
	}
}

/* End of file log_model.php */
/* Location: ./application/models/log_model.php */