<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$ci = get_instance();
		$ci->load->helper('user_helper');
	}

	public function query_response($cmd)
	{
		$res = $this->db->query($cmd);
		if( !$res )
		{
			$errNo   = $this->db->_error_number();
			$errMess = $this->db->_error_message();
			return "$errNo: $errMess";
		}
		else
			return "success";
	}

	public function create_user($username, $hash, $email, $role)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$this->db->query("USE $db");

		$table = $this->config->item("user_table");
		$date = date("Y-m-d H:i:s", time());
		$cmd = "INSERT INTO $table VALUES ('$username','$hash','$email',$role,'Num_of_HNBs_PM_Reported',NULL,NULL,'$date',CURRENT_TIMESTAMP,'$username')";
		$res = $this->query_response($cmd);
		$this->db->close();

		$pos = strpos($res, "1062");
		if ($pos === false)
			return "success";
		else
			return "Duplicate username";
	}

	public function get_allUserInfo()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$temp_data = array();
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$user_table = $this->config->item('user_table');
		$role_table = $this->config->item('role_table');

		$cmd = "SELECT $user_table.username, $user_table.email, $role_table.roleName, $user_table.roleId FROM $user_table LEFT JOIN $role_table ON $user_table.roleId=$role_table.roleId ORDER BY $user_table.username";

		$query = $this->db->query($cmd);
		if($query->num_rows() > 0)
		{
			foreach($query->result_array() as $row)
			{
				array_push($temp_data, $row);
			}
		}
		$query->free_result();
		$this->db->close();

		return $temp_data;
	}

	public function add_user($username, $hash, $email, $role, $create_by)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("user_table");

		$date = date("Y-m-d H:i:s", time());
		$cmd = "INSERT INTO $table VALUES ('$username','$hash','$email','$role','Num_of_HNBs_PM_Reported','','','$date',CURRENT_TIMESTAMP,'$create_by')";
		$res = $this->query_response($cmd);
		$this->db->close();

		$pos = strpos($res, "1062");
		if ($pos === false)
			return "success";
		else
			return "Duplicate username";
	}

	public function delete_user($users, $current_user)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("user_table");

		if(in_array($current_user, $users))
			echo "Unauthorized. You can not delete yourself.";
		else
		{
			$cmd = "DELETE FROM $table WHERE username in (";
			for($i = 0; $i < count($users); $i ++)
			{
				$cmd .= "'$users[$i]',";
			}
			$cmd = rtrim($cmd, ",").")";

			$res = $this->query_response($cmd);
			$this->db->close();

			echo $res;
		}
	}

	public function get_currentUserInfo()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$temp = array();
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$user_table = $this->config->item('user_table');
		$role_table = $this->config->item('role_table');
		$currentUser = $this->input->cookie('login_user');

		$cmd = "SELECT $user_table.username, $user_table.email, $role_table.roleName, $user_table.roleId FROM $user_table INNER JOIN $role_table ON $user_table.username='$currentUser' AND $user_table.roleId=$role_table.roleId";

		$query = $this->db->query($cmd);
		if($query->num_rows() > 0)
		{
			$row = $query->row();
		}
		$query->free_result();
		$this->db->close();

		return $row;
	}

	public function update_user($username, $hash, $email, $roleId, $update_by)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("user_table");

		if($hash != NULL)
			$cmd = "UPDATE $table SET hash='$hash', email='$email', roleId=$roleId, updateUser='$update_by' WHERE username='$username'";
		else
			$cmd = "UPDATE $table SET email='$email', roleId=$roleId, updateUser='$update_by' WHERE username='$username'";

		$res = $this->query_response($cmd);
		$this->db->close();

		return $res;
	}

	public function get_allRoleInfo()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$temp_data = array();
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$role_table = $this->config->item('role_table');

		$cmd = "SELECT * FROM $role_table ";

		$query = $this->db->query($cmd);
		if($query->num_rows() > 0)
		{
			foreach($query->result_array() as $row)
			{
				array_push($temp_data, $row);
			}
		}
		$query->free_result();
		$this->db->close();

		return $temp_data;
	}

	public function get_userRoleId($user)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("user_table");

		$cmd = "SELECT roleId FROM $table WHERE username='$user'";
		$query = $this->db->query($cmd);
		if($query->num_rows() > 0)
		{
			$row = $query->row();
		}
		$query->free_result();
		$this->db->close();

		return $row->roleId;
	}

	public function get_no_of_role()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$temp_data = array();
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("user_table");
		$cmd = "SELECT roleId,COUNT(*) AS count FROM $table GROUP BY roleId";
		$query = $this->db->query($cmd);
		if($query->num_rows() > 0)
		{
			foreach($query->result_array() as $row)
			{
				array_push($temp_data, $row);
			}
		}
		$query->free_result();
		$this->db->close();

		return $temp_data;
	}
}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */