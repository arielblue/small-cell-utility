<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rf_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$ci = get_instance();
		$ci->load->helper('user_helper');
	}

	public function query_response($cmd)
	{
		$res = $this->db->query($cmd);
		if( !$res )
		{
			$errNo   = $this->db->_error_number();
			$errMess = $this->db->_error_message();

			return $funcName.": ".$errMess."<br>";
		}
	}

	public function getHnbList()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$tempdata = array();
		$db = $this->config->item("faplog");
		$this->db->query("USE $db");
		$cmd = "SELECT hnbMAC FROM hnbmac_lastlog";
		$query = $this->db->query($cmd);
		$data = array();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				array_push($data, $row["hnbMAC"]);
			}
		}
		$query->free_result();
		$this->db->close();

		return $data;
	}

	public function get_graph_data($hnb, $startDate, $endDate)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$tables = array("FemtoTxPower","UeState","ClipUeTxPower","UeIntraMRM","HNBState","MascUpdatedNCL","HO","NlpcSnifferCell","NLPC_Power","IniDirectTransfer","IniDirectTransfer","GumpUlRSSI","Mart_Power","IM_REG","IM_REG","ROT","PowerOff_Duration","Timer41_Expired","Timer33_Expired","CellUpdate");

		$columns = array(
			# graph1
			array("logtime","PSC","Channel","TxPower"),
			array("logtime","IMSI","UE_State"), 		#UeState
			array("logtime","IMSI","Capped_TxPower"),
			array("logtime","IMSI","RSCP","EcNo","PSC"),
			array("logtime","HNB_number","HNB_State"),	#HNBState
			array("logtime","Channel","PSC","Status"),
			array("logtime","IMSI"),					#HO

			# graph2
			array("logtime","PSC","RSCP","EcNo","Channel"),
			array("logtime","NLPC_Power"),	#NLPC_Power
			array("logtime","LU_REQ"),		#IniDirectTransfer
			array("logtime","RAU_REQ"),
			
			# graph3 + FemtoTxPower + NLPC_Power
			array("logtime","UL_RSSI_HNB"),	#GumpUlRSSI
			array("logtime","Mart_Power"),	#13 Mart_Power
			array("logtime","HUE_REG"),		#IM_REG
			array("logtime","MUE_REG"),

			array("logtime","ROTval"),		#ROT
			array("logtime","inact_start"),	#PowerOff_Duration
			array("logtime"),				#Timer41_Expired
			array("logtime"),
			array("logtime","Cause")		#CellUpdate
		);

		$db = $this->config->item("faplog");
		$this->db->query("USE $db");

		$cmd = "";
		$num_tables = count($tables);
		$result = array();
		for($i = 0; $i < $num_tables; $i++)
		{
			$attrs = $columns[$i];
			$num_attr = count($attrs);

			$cmd = "SELECT ";
			for($j = 0; $j < $num_attr; $j++)
				$cmd .=$attrs[$j].",";
			$cmd = rtrim($cmd, ",");
			$cmd .= " FROM ".$tables[$i]." WHERE hnbMAC='$hnb' AND (logtime BETWEEN '$startDate' AND '$endDate')";

			switch($tables[$i])
			{
				case "GumpUlRSSI":
				case "IM_REG":
				case "IniDirectTransfer":
					$cmd .= " AND ($attrs[1] != 0)";
					$tables[$i] = $attrs[1];
					break;
			}

			$query = $this->db->query($cmd);

			$temp_data = array();
			if($query->num_rows() > 0)
			{
				foreach ($query->result_array() as $row)
				{
					array_push($temp_data, $row);
				}
			}
			$result[$tables[$i]] = $temp_data;
			$query->free_result();
		}

		$this->db->close();
		return json_encode($result);
	}

}

/* End of file rf_model.php */
/* Location: ./application/models/rf_model.php */