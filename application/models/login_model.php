<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function signin($username, $password)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$this->db->query("USE $db");

		$table = $this->config->item("user_table");
		$username = $this->db->escape($username);
		$cmd = "SELECT hash FROM $table WHERE username=$username";

		$query = $this->db->query($cmd);
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$queryHash = $row->hash;
				$query->free_result();
				$this->db->close();

				if( crypt($password, $queryHash) === $queryHash )
					return true;
				else
					return "Invalid Password";
			}
		}
		else
		{
			$query->free_result();
			$this->db->close();
			return "Invalid Username";
		}
	}
}

/* End of file login_model.php */
/* Location: ./application/models/login_model.php */