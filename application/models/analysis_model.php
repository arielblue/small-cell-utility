<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Analysis_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function query_response($cmd)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$res = $this->db->query($cmd);
		if( !$res )
		{
			$errNo   = $this->db->_error_number();
			$errMess = $this->db->_error_message();
			print_r($errMess." ");
		}
	}

	public function query_response2($funcName,$cmd)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$res = $this->db->query($cmd);
		if( !$res )
		{
			$errNo   = $this->db->_error_number();
			$errMess = $this->db->_error_message();
			return $funcName.": ".$errMess."<br>";
		}
	}

	public function get_NlpcMartPwr($hnb, $startDate, $endDate)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("faplog");
		$this->db->query("USE $db");
		$data = array();

		$cmd = "SELECT logtime,mart_power, NULL AS nlpc_power FROM mart_power WHERE hnbMAC='$hnb' AND (logtime BETWEEN '$startDate' AND '$endDate') UNION ALL SELECT logtime, NULL AS mart_power, nlpc_power FROM nlpc_power WHERE hnbMAC='$hnb' AND (logtime BETWEEN '$startDate' AND '$endDate') ORDER BY logtime ASC";
		$query = $this->db->query($cmd);
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$temp = array();
				$temp["logtime"] = $row->logtime;
				$temp["mart_power"] = $row->mart_power;
				$temp["nlpc_power"] = $row->nlpc_power;
				array_push($data, $temp);
			}
		}
		$this->db->close();
		return $data;
	}

	public function get_MaxHueMue($hnb, $startDate, $endDate)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("faplog");
		$this->db->query("USE $db");
		$data = array();

		$cmd = "SELECT MAX(HUE_REG) AS max_hue, MAX(MUE_REG) AS max_mue FROM im_reg WHERE hnbMAC='$hnb' AND (logtime BETWEEN '$startDate' AND '$endDate')";

		$query = $this->db->query($cmd);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();

			$data["max_hue"] = $row->max_hue;
			$data["max_mue"] = $row->max_mue;
		}
		$this->db->close();
		return $data;
	}

	public function get_ueintramrm($hnb, $startDate, $endDate)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("faplog");
		$this->db->query("USE $db");
		$data = array();

		$cmd = "SELECT IMSI, MIN(RSCP) AS min_rscp, MAX(RSCP) AS max_rscp, MIN(EcNo) AS min_ecno, MAX(EcNo) AS max_ecno, SUM(IF(RSCP>-79,1,0)) AS cnt_cell_site, SUM(IF(RSCP<-100,1,0)) AS cnt_cell_edge, SUM(IF(EcNo>-10,1,0)) AS cnt_qual_good, SUM(IF(EcNo<-16,1,0)) AS cnt_qual_bad, COUNT(EcNo) AS cnt_total FROM UeIntraMRM WHERE PSC='101' AND hnbMAC='$hnb' AND (logtime BETWEEN '$startDate' AND '$endDate') GROUP BY IMSI";

		$query = $this->db->query($cmd);
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$temp = array();
				$temp["IMSI"] = $row->IMSI;
				$temp["min_rscp"] = $row->min_rscp;
				$temp["max_rscp"] = $row->max_rscp;
				$temp["min_ecno"] = $row->min_ecno;
				$temp["max_ecno"] = $row->max_ecno;
				$temp["cnt_cell_site"] = $row->cnt_cell_site;
				$temp["cnt_cell_edge"] = $row->cnt_cell_edge;
				$temp["cnt_qual_good"] = $row->cnt_qual_good;
				$temp["cnt_qual_bad"] = $row->cnt_qual_bad;
				$temp["cnt_total"] = $row->cnt_total;
				array_push($data, $temp);
			}
		}
		$this->db->close();
		return $data;
	}

	public function get_HObyIMSI($hnb, $startDate, $endDate)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("faplog");
		$this->db->query("USE $db");
		$data = array();

		$cmd = "SELECT * FROM HO WHERE hnbMAC='$hnb' AND (logtime BETWEEN '$startDate' AND '$endDate')";

		$query = $this->db->query($cmd);
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$imsi = $row->IMSI;
				$logtime = $row->logtime;

				if(!array_key_exists($imsi, $data))
					$data[$imsi] = array();
				array_push($data[$imsi], $logtime);
			}
		}
		$this->db->close();
		return $data;
	}

	public function get_ROTval($hnb, $startDate, $endDate)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("faplog");
		$this->db->query("USE $db");

		$cmd = "SELECT MAX(ROTval) AS ROTval FROM ROT WHERE hnbMAC='$hnb' AND (logtime BETWEEN '$startDate' AND '$endDate')";
		$query = $this->db->query($cmd);
		$this->db->close();

		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$data = $row->ROTval;
		}
		else $data = NULL;

		return $data;
	}

	public function get_ULRSSIHNB($ROTval, $hnb, $startDate, $endDate)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("faplog");
		$this->db->query("USE $db");
		$ULAlarmThres = $this->config->item("ULAlarmThres")+$ROTval;
		$data = array();

		$cmd = "SELECT logtime, UL_RSSI_HNB AS UL_RSSI FROM gumpulrssi WHERE UL_RSSI_HNB > $ULAlarmThres AND hnbMAC='$hnb' AND (logtime BETWEEN '$startDate' AND '$endDate')";
		$query = $this->db->query($cmd);
		$this->db->close();

		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$temp = array();
				$temp["logtime"] = $row->logtime;
				$temp["UL_RSSI"] = $row->UL_RSSI;
				array_push($data, $temp);
			}
		}

		return $data;
	}

}