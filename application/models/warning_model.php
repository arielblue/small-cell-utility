<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Warning_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function query_response($cmd)
	{
		$res = $this->db->query($cmd);
		if( !$res )
		{
			$errNo   = $this->db->_error_number();
			$errMess = $this->db->_error_message();
			return $errMess;
		}
		else
			return "success";
	}

	public function isWarnProcessed()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("kpi_warning");
		$cmd = "SELECT isChecked FROM $table WHERE isChecked='0'";
		$query = $this->db->query($cmd);
		$num = $query->num_rows();
		$this->db->close();

		return json_encode($num);
	}

	public function updateWarning($data, $user)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("kpi_warning");
		foreach($data as $v)
		{
			$cmd = "UPDATE $table SET note='".$v->note."',updateUser='$user',isChecked='1',updateTime=CURRENT_TIMESTAMP WHERE hnbMAC='".$v->hnbMAC."' AND endtime='".$v->endtime."' AND kpiName='".$v->kpiName."' AND kpiTarget='".$v->kpiTarget."'";
			$query = $this->query_response($cmd);

			if($query != "success")
				$result[$v->hnbMAC." ".$v->logTime] = $query;
		}
		$this->db->close();

		if($result == null)
			return "success";
		else
			return json_encode($result);
	}

	public function get_listCount($data)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$tempdata = array();
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("kpi_warning");
		$cmd = "SELECT hnbMAC,endtime,kpiName,kpiTarget,kpiValue,note,updateUser,updateTime FROM $table";

		if($data != NULL)
		{
			$genName = $this->gen_kpiname($data["kpiInsert"]);
			$genTime = $this->gen_time($data);
			if(count($data["kpiInsert"]) > 0 && $data["startTime"] != NULL)
				$cmd .= " WHERE (".$genName.") AND (".$genTime.")";
			elseif(count($data["kpiInsert"]) > 0 && $data["startTime"] == NULL)
				$cmd .= " WHERE (".$genName.")";
			elseif(count($data["kpiInsert"]) == 0 && $data["startTime"] != NULL)
				$cmd .= " WHERE (".$genTime.")";
		}

		$query = $this->db->query($cmd);

		return $query->num_rows();
	}

	public function get_listCountUnprocess()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$tempdata = array();
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("kpi_warning");
		$cmd = "SELECT hnbMAC,endtime,kpiName,kpiTarget,kpiValue,note,updateUser,updateTime FROM $table WHERE isChecked=0";
		$query = $this->db->query($cmd);

		return $query->num_rows();
	}

	public function searchAllWarning($data, $offset, $per_page)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$tempdata = array();
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("kpi_warning");
		$cmd = "SELECT hnbMAC,endtime,kpiName,kpiTarget,kpiValue,note,updateUser,updateTime FROM $table";

		if($data != NULL)
		{
			$genName = $this->gen_kpiname($data["kpiInsert"]);
			$genTime = $this->gen_time($data);
			if(count($data["kpiInsert"]) > 0 && $data["startTime"] != NULL)
				$cmd .= " WHERE (".$genName.") AND (".$genTime.")";
			elseif(count($data["kpiInsert"]) > 0 && $data["startTime"] == NULL)
				$cmd .= " WHERE (".$genName.")";
			elseif(count($data["kpiInsert"]) == 0 && $data["startTime"] != NULL)
				$cmd .= " WHERE (".$genTime.")";
		}

		$cmd .= " LIMIT ".($offset-1)*$per_page.", $per_page";
		$query = $this->db->query($cmd);
		$kpi = $this->config->item("kpi");

		foreach($query->result_array() as $row)
		{
			$temp = array();
			$temp["hnbMAC"] = $row["hnbMAC"];
			$temp["endtime"] = $row["endtime"];
			$temp["kpiName"] = $row["kpiName"];
			$temp["kpiTarget"] = $row["kpiTarget"];
			$temp["kpiValue"] = $row["kpiValue"];
			if($row["note"] == null) $temp["note"] = "";
			else $temp["note"] = $row["note"];
			if($row["updateUser"] == null) $temp["updateUser"] = "";
			else $temp["updateUser"] = $row["updateUser"];
			$temp["updateTime"] = $row["updateTime"];
			$temp["kpiDisplayName"] = $kpi[$row["kpiName"]];
			array_push($tempdata, $temp);
		}
		$this->db->close();

		return $tempdata;
	}

	public function searchUnproWarning($data, $offset, $per_page)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$tempdata = array();
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("kpi_warning");
		$cmd = "SELECT hnbMAC,endtime,kpiName,kpiTarget,kpiValue,note,updateUser,updateTime FROM $table WHERE isChecked=0";

		$cmd .= " LIMIT ".($offset-1)*$per_page.", $per_page";
		$query = $this->db->query($cmd);
		$kpi = $this->config->item("kpi");

		foreach($query->result_array() as $row)
		{
			$temp = array();
			$temp["hnbMAC"] = $row["hnbMAC"];
			$temp["endtime"] = $row["endtime"];
			$temp["kpiName"] = $row["kpiName"];
			$temp["kpiTarget"] = $row["kpiTarget"];
			$temp["kpiValue"] = $row["kpiValue"];
			if($row["note"] == null) $temp["note"] = "";
			else $temp["note"] = $row["note"];
			if($row["updateUser"] == null) $temp["updateUser"] = "";
			else $temp["updateUser"] = $row["updateUser"];
			$temp["updateTime"] = $row["updateTime"];
			$temp["kpiDisplayName"] = $kpi[$row["kpiName"]];
			array_push($tempdata, $temp);
		}
		$this->db->close();

		return $tempdata;
	}

	public function gen_kpiname($kpi)
	{
		$cmd = "kpiName='".$kpi[0]."'";

		if(count($kpi) > 1)
			for($i = 1; $i < count($kpi); $i++)
				$cmd .= " OR kpiName='".$kpi[$i]."'";

		return $cmd;
	}

	public function gen_time($data)
	{
		$cmd = "endtime BETWEEN '".$data["startTime"]."' AND '".$data["endTime"]."'";

		return $cmd;
	}

	public function get_alram_listCountUnprocess()
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$tempdata = array();
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("alarm_warning");
		$cmd = "SELECT hnbMAC,logTime,alarmType,triggerCondition,alarmEvents,note,updateUser,updateTime FROM $table WHERE isChecked=0";
		$query = $this->db->query($cmd);
		return $query->num_rows();
	}

	public function get_alram_listCount($data)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$tempdata = array();
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("alarm_warning");
		$cmd = "SELECT hnbMAC,logTime,alarmType,triggerCondition,alarmEvents,note,updateUser,updateTime FROM $table";

		$query = $this->db->query($cmd);
		return $query->num_rows();
	}

	public function searchUnproAlarmWarning($data, $offset, $per_page)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$tempdata = array();
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("alarm_warning");
		$cmd = "SELECT hnbMAC,logTime,alarmType,triggerCondition,alarmEvents,note,updateUser,updateTime FROM $table WHERE isChecked=0";

		$cmd .= " LIMIT ".($offset-1)*$per_page.", $per_page";
		$query = $this->db->query($cmd);

		foreach($query->result_array() as $row)
		{
			$temp = array();
			$temp["hnbMAC"] = $row["hnbMAC"];
			$temp["logTime"] = $row["logTime"];
			$temp["alarmType"] = $row["alarmType"];
			$temp["triggerCondition"] = $row["triggerCondition"];
			$temp["alarmEvents"] = $row["alarmEvents"];
			if($row["note"] == null) $temp["note"] = "";
			else $temp["note"] = $row["note"];
			if($row["updateUser"] == null) $temp["updateUser"] = "";
			else $temp["updateUser"] = $row["updateUser"];
			$temp["updateTime"] = $row["updateTime"];
			array_push($tempdata, $temp);
		}
		$this->db->close();

		return $tempdata;
	}

	public function updateAlarmWarning($data, $user)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("alarm_warning");

		foreach($data as $v)
		{
			$cmd = "UPDATE $table SET note='".$v->note."',updateUser='$user',isChecked='1',updateTime=CURRENT_TIMESTAMP WHERE hnbMAC='".$v->hnbMAC."' AND logTime='".$v->logTime."'";
			$query = $this->query_response($cmd);

			if($query != "success")
				$result[$v->hnbMAC." ".$v->logTime] = $query;
		}
		$this->db->close();

		if($result == null)
			return "success";
		else
			return json_encode($result);
	}

	public function searchAlarmAllWarning($data, $offset, $per_page)
	{
		if($_SERVER['HTTP_REFERER'] == NULL) exit('No direct script access allowed');

		$tempdata = array();
		$db = $this->config->item("db");
		$this->db->query("USE $db");
		$table = $this->config->item("alarm_warning");
		$cmd = "SELECT hnbMAC,logTime,alarmType,triggerCondition,alarmEvents,note,updateUser,updateTime FROM $table";

		$cmd .= " LIMIT ".($offset-1)*$per_page.", $per_page";
		$query = $this->db->query($cmd);

		foreach($query->result_array() as $row)
		{
			$temp = array();
			$temp["hnbMAC"] = $row["hnbMAC"];
			$temp["logTime"] = $row["logTime"];
			$temp["alarmType"] = $row["alarmType"];
			$temp["triggerCondition"] = $row["triggerCondition"];
			$temp["alarmEvents"] = $row["alarmEvents"];
			if($row["note"] == null) $temp["note"] = "";
			else $temp["note"] = $row["note"];
			if($row["updateUser"] == null) $temp["updateUser"] = "";
			else $temp["updateUser"] = $row["updateUser"];
			$temp["updateTime"] = $row["updateTime"];
			array_push($tempdata, $temp);
		}
		$this->db->close();

		return $tempdata;
	}
}

/* End of file warning_model.php */
/* Location: ./application/models/warning_model.php */