<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function query_response($cmd)
	{
		$res = $this->db->query($cmd);
		if( !$res )
		{
			$errNo   = $this->db->_error_number();
			$errMess = $this->db->_error_message();

			return $errMess;
		}
		else
			return "success";

	}

	public function isKpiWarnProcessed()
	{
		$db = $this->config->item("db");
		$res = $this->db->query("USE $db");
		if(	$res)
		{
			$table = $this->config->item("kpi_warning");
			$cmd = "SELECT isChecked FROM $table WHERE isChecked='0'";
			$query = $this->db->query($cmd);
			$num = $query->num_rows();
			$this->db->close();
			return $num;
		}
		else
		{
			$this->db->close();
			return "DB does not exist";
		} 
	}

	public function isAlarmWarnProcessed()
	{
		$db = $this->config->item("db");
		$res = $this->db->query("USE $db");
		if(	$res)
		{
			$this->db->query("USE $db");
			$table = $this->config->item("alarm_warning");
			$cmd = "SELECT isChecked FROM $table WHERE isChecked='0'";
			$query = $this->db->query($cmd);
			$num = $query->num_rows();
			$this->db->close();
			return $num;
		}
		else
		{
			$this->db->close();
			return "DB does not exist";
		}
	}
}

/* End of file common_model.php */
/* Location: ./application/models/common_model.php */