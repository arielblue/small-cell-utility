<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* write issue log under project folder */
function gui_log($write)
{
	$fo = str_replace("application\\helpers", "", dirname(__FILE__));
	$write = timestamp_string()." $write\n";
	$f = fopen($fo."configtool.log", 'a');

	fwrite($f, $write);
	fclose($f);
}

/* get file list of folder $path */
function get_list($path)
{
	$list = scandir($path);
	unset($list[0], $list[1]);
	$list = array_values($list);

	return $list;
}

function get_project_name()
{
	$project = explode("\\", dirname(__DIR__));
	$index = array_search('www',$project);
	$p = $project[$index+1];

	return $p;
}
