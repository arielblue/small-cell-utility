<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* get file list of folder $path */
function hash_password($pass)
{
	$cost = 10;
	$salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
	$salt = sprintf("$2a$%02d$", $cost).$salt;
	$hash = crypt($pass, $salt);

	return $hash;
}

function server_password($pass)
{
	$key = "toolofconfigtool2encryptpassword";
	$iv = "1qaz2wsx3edc4rfv";
	$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, md5($key), $pass, MCRYPT_MODE_CFB, $iv));

	return $encrypted;
}
