<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_current_time_in_sec()
{
	$time = explode( " ", microtime());
	$usec = (double)$time[0];
	$sec = (double)$time[1];

	return $sec;
}

function get_current_time_in_millisec()
{
	$time = explode( " ", microtime());
	$usec = (double)$time[0];
	$sec = (double)$time[1];

	return $sec + $usec;
}

function to_datetime_obj($datetime)
{
	// $datetime is also Y-m-d H:i:s format
	return DateTime::createFromFormat("Y-m-d H:i:s", $datetime);
}

function detect_same_date($a, $b)
{
	// if $a is the same date as $b?
	$same = ($a->format('Y-m-d') == $b->format('Y-m-d'));
	return $same;
}

/* create a current time string */
function timestamp_string()
{
	date_default_timezone_set('Asia/Taipei');
	list($usec, $sec) = explode(" ", microtime());
	$msec = substr($usec, 2, 6);
	$today = date("[Y:m:d:H:i:s", $sec).":$msec]:";

	return $today;
}

function to_timestamp_string($datetime)
{
	$time = explode(":", $datetime);
	return $time[0]."/".$time[1]."/".$time[2]." ".substr($datetime, 11, 5);
}

function to_timestampe_string2($d)
{
	$d = str_replace('-', ':', $d);
	return $d.":00:00:00:000000";
}

/* End of file time_helper.php */
/* Location: ./applicatin/helpers/time_helper.php */