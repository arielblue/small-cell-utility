<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "login";
$route['404_override'] = '';

// Authentication
$route['logout']	= 'login/logout';
$route['userinfo']	= 'user/get_currentUserInfo';

// Pagination
$route['kpi_allwarning/(:num)'] = "kpi_allwarning/index/$1";
$route['kpi_allwarning/(:any)/(:num)'] = "kpi_allwarning/index/$1/$2";
$route['kpi_allwarning/(:any)/logout'] = 'login/logout';
$route['kpi_allwarning/logout'] = 'login/logout';
$route['kpi_allwarning/(:any)'] = "kpi_allwarning/index/$1";

$route['kpi_warning/(:num)'] = "kpi_warning/index/$1";
$route['kpi_warning/logout'] = 'login/logout';

$route['alarm_allwarning/(:num)'] = "alarm_allwarning/index/$1";
$route['alarm_allwarning/(:any)/(:num)'] = "alarm_allwarning/index/$1/$2";
$route['alarm_allwarning/(:any)/logout'] = 'login/logout';
$route['alarm_allwarning/logout'] = 'login/logout';
$route['alarm_allwarning/(:any)'] = "alarm_allwarning/index/$1";

$route['alarm_warning/(:num)'] = "alarm_warning/index/$1";
$route['alarm_warning/logout'] = 'login/logout';


/* End of file routes.php */
/* Location: ./application/config/routes.php */